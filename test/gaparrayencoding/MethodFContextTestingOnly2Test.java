package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodFContextTestingOnly2Test extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodFContextTestingOnly2(3, nbDocumentsForBijection));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodFContextTestingOnly2(3, nbDocumentsForFables));
    }
    
}
