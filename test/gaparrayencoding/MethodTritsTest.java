package gaparrayencoding;

import bitlistencoding.BitSequence;
import integerencoding.BinaryEncoding;
import io.BitOutputStreamArray;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodTritsTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodTrits());
        GapArrayTest.testBijection(new MethodByTritList(new tritlistencoding.Method5TritsPerByte()));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodTrits());
        GapArrayTest.testFables(new MethodByTritList(new tritlistencoding.Method5TritsPerByte()));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodTrits());
        GapArrayTest.testComputeSize(new MethodByTritList(new tritlistencoding.Method5TritsPerByte()));
    }
    
    /**
     * Test of gapArray2bitSequence method, of class MethodTrits.
     */
    @Test
    public void testGapArray2bitSequence() {
        System.out.println("gapArray2bitSequence");
        int[] gapArray = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        String expResult, result;
        MethodTrits instance = new MethodTrits();
        result = instance.gapArray2bitSequence(gapArray).toString();
        // tritList is 12012 20222 002
        // numbers are 1 * 81 + 2 * 27 + 0 * 9 + 1 * 3 + 2 = 140,
        //             2 * 81 + 0 * 27 + 2 * 9 + 2 * 3 + 2 = 188, and
        //             0 * 81 + 0 * 27 + 2 * 9 + 0 * 3 + 0 =  18.
        BitSequence buffer = new BitSequence();
        BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(buffer);
        BinaryEncoding.writeCodeBinary(140, bitOutputStream, 8);
        BinaryEncoding.writeCodeBinary(188, bitOutputStream, 8);
        BinaryEncoding.writeCodeBinary( 18, bitOutputStream, 8);
        expResult = buffer.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of readGapArray method, of class MethodTrits.
     */
    @Test
    public void testReadGapArray() {
        System.out.println("readGapArray");
        int[] expResult, result;
        MethodTrits instance = new MethodTrits();
        BitSequence buffer = new BitSequence();
        BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(buffer);
        BinaryEncoding.writeCodeBinary(140, bitOutputStream, 8);
        BinaryEncoding.writeCodeBinary(188, bitOutputStream, 8);
        BinaryEncoding.writeCodeBinary( 18, bitOutputStream, 8);
        expResult = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        int expNbGapsLocal = 7;
        result = instance.bitSequence2gapArray(buffer, expNbGapsLocal);
        assertArrayEquals(expResult, result);
    }
    
}
