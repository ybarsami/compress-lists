package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodFContextBitTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodFContextBit(3, 4, 3));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodFContextBit(3, 4, 3));
    }

}
