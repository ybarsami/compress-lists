package gaparrayencoding;

import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.BitInputStreamArray;
import io.BitOutputStreamArray;

import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodBinaryTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        int nbBits = Tools.ceilingLog2(nbDocumentsForBijection);
        GapArrayTest.testBijection(new MethodBinary(nbBits));
    }
    
    @Test
    public void testFables() {
        int nbBits = Tools.ceilingLog2(nbDocumentsForFables);
        GapArrayTest.testFables(new MethodBinary(nbBits));
    }
    
    @Test
    public void testComputeSize() {
        int nbBits = Tools.ceilingLog2(nbDocumentsForFables);
        GapArrayTest.testComputeSize(new MethodBinary(nbBits));
    }

    /**
     * Test of writeCode method, of class MethodBinary.
     */
    @Test
    public void testWriteCode() {
        System.out.println("writeCode");
        int nbDocuments = 4;
        int nbBits = Tools.ceilingLog2(nbDocuments);
        MethodBinary instance = new MethodBinary(nbBits);
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "00");
        map.put(2, "01");
        map.put(3, "10");
        map.put(4, "11");
        // Writing 1..4
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Trying to write 5 (not possible on ceiling(log_2(4)) = 2 bits)
        x = 5;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCode method, of class MethodBinary.
     */
    @Test
    public void testReadCode() {
        System.out.println("readCode");
        int nbDocuments = 32;
        int nbBits = Tools.ceilingLog2(nbDocuments);
        MethodBinary instance = new MethodBinary(nbBits);
        int result, expResult;
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        BitInputStreamArray bitInputStream;
        // Writing and reading 1..nbDocuments
        for (expResult = 1; expResult <= nbDocuments; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = instance.readCode(bitInputStream);
            assertEquals(expResult, result);
        }
    }
    
}
