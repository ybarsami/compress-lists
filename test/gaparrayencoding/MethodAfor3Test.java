package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodAfor3Test extends GapArrayTest {
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodAfor3());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodAfor3());
    }
    
}
