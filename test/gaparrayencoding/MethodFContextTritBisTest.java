package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodFContextTritBisTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodFContextTritBis(3, 4, 3));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodFContextTritBis(3, 4, 3));
    }
    
}
