package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodFContextTestingOnly1Test extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodFContextTestingOnly1(3, nbDocumentsForBijection));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodFContextTestingOnly1(3, nbDocumentsForFables));
    }

}
