package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodInterpolativeTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodInterpolative(nbDocumentsForBijection));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodInterpolative(nbDocumentsForFables));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodInterpolative(nbDocumentsForFables));
    }
    
    /**
     * Test of documentArray method, of class MethodInterpolative.
     */
    @Test
    public void testDocumentArray() {
        System.out.println("documentArray");
        int[] gapArray, expResult;
        int nbDocuments;
        MethodInterpolative instance;
        // Regular gapArray.
        nbDocuments = 20;
        instance = new MethodInterpolative(nbDocuments);
        gapArray = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        expResult = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        Tools.gapArray2documentArray(new IntsRef(gapArray), instance.documentArrayRef);
        assertArrayEquals(expResult, instance.documentArrayRef.ints);
        // Trying to convert gapArray with 0.
        nbDocuments = 20;
        instance = new MethodInterpolative(nbDocuments);
        gapArray = new int[] { 3, 5, 1, 2, 1, 0, 4 };
        try {
            Tools.gapArray2documentArray(new IntsRef(gapArray), instance.documentArrayRef);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        /*
        // Trying to convert gapArray with sum greater than nbDocuments.
        nbDocuments = 4;
        instance = new MethodInterpolative(nbDocuments);
        gapArray = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        try {
            Tools.gapArray2documentArray(new IntsRef(gapArray), instance.documentArrayRef);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        */
    }
    
    /**
     * Test of gapArray2bitSequence method, of class MethodInterpolative.
     */
    @Test
    public void testGapArray2bitSequence() {
        System.out.println("gapArray2bitSequence");
        String expResult, result;
        int nbDocuments;
        MethodInterpolative instance;
        int[] documentArray;
        BitSequence bitSequence;
        
        // { 3, 8, 9 } in [1..10]
        nbDocuments = 10;
        instance = new MethodInterpolative(nbDocuments, true, true, false);
        documentArray = new int[] { 3, 8, 9 };
        IntsRef documentArrayRef = new IntsRef(documentArray);
        Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
        bitSequence = instance.gapArray2bitSequence(documentArray);
        result = bitSequence.toString();
        // 8 is the pointer n°1 in the 3-pointer list, we hence code 8 in [1 + 1; 10 - (3 - 1 - 1)],
        // i.e. 8 in [2; 9] -> we need to code 8-2 = 6 on ceiling(log_2(9-2+1)) = 3 bits -> 110.
        // We now code:
        //     - { 3 } in the range [1..7] -> not power of 2, 3rd on the left side with 1 middle value so (110)
        //     - and { 9 } in the range [9..10] -> { 1 } in the range [1..2] -> (0)
        expResult = "110" + "110" + "0";
        assertEquals(expResult, result);
        
        // { 1, 2, 6 } in [1..9]
        nbDocuments = 9;
        instance = new MethodInterpolative(nbDocuments, true, true, false);
        documentArray = new int[] { 1, 2, 6 };
        documentArrayRef = new IntsRef(documentArray);
        Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
        bitSequence = instance.gapArray2bitSequence(documentArray);
        result = bitSequence.toString();
        // 2 is the pointer n°1 in the 3-pointer list, we hence code 2 in [1 + 1; 9 - (3 - 1 - 1)],
        // i.e. 2 in [2; 8] -> not power of 2, first on left side with 1 middle value so (010).
        // We now code:
        //     - { 1 } in the range [1..1] ()
        //     - and { 6 } in the range [3..9] -> not power of 2, middle pointer so (00)
        expResult = "010" + "" + "00";
        assertEquals(expResult, result);
        
        // { 3, 8, 9, 11, 12, 13, 17 } in [1..20]
        nbDocuments = 20;
        instance = new MethodInterpolative(nbDocuments, true, true, false);
        documentArray = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        documentArrayRef = new IntsRef(documentArray);
        Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
        bitSequence = instance.gapArray2bitSequence(documentArray);
        result = bitSequence.toString();
        // 11 is the pointer n°3 in the 7-pointer list, we hence code 11 in [1 + 3; 20 - (7 - 3 - 1)],
        // i.e. 11 in [4; 17] -> not power of 2, second middle value so (001).
        // We now code:
        //     - { 3, 8, 9 } in the range [1..10] (1100100)
        //     - and { 12, 13, 17 } in the range [12..20] -> { 1, 2, 6 } in the range [1..9]
        expResult = "001" + "1101100" + "01000";
        assertEquals(expResult, result);
        
        /*
        // Trying to convert list with identifiers bigger than nbDocuments
        nbDocuments = 4;
        instance = new MethodInterpolative(nbDocuments, true, true, false);
        documentArray = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        documentArrayRef = new IntsRef(documentArray);
        Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
        try {
            bitSequence = instance.gapArray2bitSequence(documentArray);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        */
        
        // Trying to convert unsorted list
        nbDocuments = 20;
        instance = new MethodInterpolative(nbDocuments, true, true, false);
        documentArray = new int[] { 3, 8, 9, 11, 12, 17, 13 };
        documentArrayRef = new IntsRef(documentArray);
        try {
            Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
            bitSequence = instance.gapArray2bitSequence(documentArray);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of bitSequence2gapArray method, of class MethodInterpolative.
     */
    @Test
    public void testBitSequence2gapArray() {
        System.out.println("bitSequence2gapArray");
        int[] result;
        int nbDocuments = 20;
        MethodInterpolative instance = new MethodInterpolative(nbDocuments, true, true, false);
        BitSequence bitSequence = new BitSequence("001110110001000");
        int[] documentArray = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        IntsRef documentArrayRef = new IntsRef(documentArray);
        Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
        int expNbGapsLocal = 7;
        result = instance.bitSequence2gapArray(bitSequence, expNbGapsLocal);
        assertArrayEquals(documentArray, result);
    }
    
}
