package gaparrayencoding;

import bitlistencoding.BitSequence;
import io.BitInputStreamArray;
import io.BitOutputStreamArray;

import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodZetaTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        for (int k = 1; k < 10; k++) {
            GapArrayTest.testBijection(new MethodZeta(k));
        }
    }
    
    @Test
    public void testFables() {
        for (int k = 1; k < 10; k++) {
            GapArrayTest.testFables(new MethodZeta(k));
        }
    }
    
    @Test
    public void testComputeSize() {
        for (int k = 1; k < 10; k++) {
            GapArrayTest.testComputeSize(new MethodZeta(k));
        }
    }
    
    /**
     * Test of writeCode method, of class MethodZeta.
     */
    @Test
    public void testWriteCode() {
        System.out.println("writeCode");
        // Zeta_1
        int k = 1;
        MethodZeta instance = new MethodZeta(k);
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "100");
        map.put( 3, "101");
        map.put( 4, "11000");
        map.put( 5, "11001");
        map.put( 6, "11010");
        map.put( 7, "11011");
        map.put( 8, "1110000");
        map.put( 9, "1110001");
        map.put(10, "1110010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        // Trying to write 0
        try {
            int x = 0;
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Zeta_2
        k = 2;
        instance = new MethodZeta(k);
        map.clear();
        map.put( 1, "00");
        map.put( 2, "010");
        map.put( 3, "011");
        map.put( 4, "10000");
        map.put( 5, "10001");
        map.put( 6, "10010");
        map.put( 7, "10011");
        map.put( 8, "101000");
        map.put( 9, "101001");
        map.put(10, "101010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        // Trying to write 0
        try {
            int x = 0;
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }
    
    /**
     * Test of readCode method, of class MethodZeta.
     */
    @Test
    public void testReadCode() {
        System.out.println("readCode");
        for (int k = 1; k < 6; k++) {
            MethodZeta instance = new MethodZeta(k);
            int result, expResult;
            BitSequence buffer;
            BitInputStreamArray bitInputStream;
            BitOutputStreamArray bitOutputStream;
            // Writing and reading 1..42
            for (expResult = 1; expResult < 43; expResult++) {
                buffer = new BitSequence();
                bitOutputStream = new BitOutputStreamArray(buffer);
                instance.writeCode(expResult, bitOutputStream);
                bitInputStream = new BitInputStreamArray(buffer);
                result = instance.readCode(bitInputStream);
                assertEquals(expResult, result);
            }
        }
    }
    
}
