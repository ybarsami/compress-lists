/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaparrayencoding;

import io.Dataset;
import io.DatasetReader;

import com.github.difflib.DiffUtils;
import com.github.difflib.algorithm.DiffException;
import com.github.difflib.patch.Patch;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        System.out.println("Bijection ASCII");
        String originalFilename = "test" + File.separatorChar + "Fables_ASCII.txt";
        String createdFilename = "index_test.txt";
        DatasetReader datasetReader = Method.importFromFileASCII(originalFilename);
        int nbDocuments = datasetReader.getNbDocuments();
        File expResult = new File(originalFilename);
        Method.exportToFileASCII(createdFilename, datasetReader, nbDocuments);
        File result = new File(createdFilename);
        try {
            List<String> original = Files.readAllLines(result.toPath(), StandardCharsets.UTF_8);
            List<String> revised = Files.readAllLines(expResult.toPath(), StandardCharsets.UTF_8);

            // Compute diff. Get the Patch object. Patch is the container for computed deltas.
            Patch<String> patch = DiffUtils.diff(original, revised);
            assertEquals(patch.getDeltas().size(), 0);
        } catch (IOException e) {
            fail("IOException when testing.");
        } catch (DiffException e) {
            fail("DiffException when testing.");
        }
        result.delete();
    }
    
    @Test
    public void testFables() {
        System.out.println("Bijection(Fables) ASCII");
        String originalFilename = "test" + File.separatorChar + "Fables_ASCII.txt";
        String tmpFilename = "index_test.txt";
        String createdFilename = "index_test_ASCII.txt";
        // import the dataset from the .csv file
        DatasetReader datasetReader = Dataset.FABLES_NAME.getDatasetReader();
        int nbDocuments = datasetReader.getNbDocuments();
        // export the dataset to an ASCII index
        Method.exportToFileASCII(tmpFilename, datasetReader, nbDocuments);
        // re-import the just created index
        DatasetReader dataset = Method.importFromFileASCII(tmpFilename);
        // export the (imported, exported then imported) dataset to ASCII
        Method.exportToFileASCII(createdFilename, dataset, dataset.getNbDocuments());
        // clean temporary files
        new File(tmpFilename).delete();
        new File(tmpFilename.substring(0, tmpFilename.lastIndexOf(".")) + ".stats").delete();
        new File("test" + File.separatorChar + "Fables_ASCII.stats").delete();
        // compare this just created ASCII index to the correct ASCII file
        File expResult = new File(originalFilename);
        File result = new File(createdFilename);
        try {
            List<String> original = Files.readAllLines(result.toPath(), StandardCharsets.UTF_8);
            List<String> revised = Files.readAllLines(expResult.toPath(), StandardCharsets.UTF_8);

            // Compute diff. Get the Patch object. Patch is the container for computed deltas.
            Patch<String> patch = DiffUtils.diff(original, revised);
            assertEquals(0, patch.getDeltas().size());
        } catch (IOException e) {
            fail("IOException when testing.");
        } catch (DiffException e) {
            fail("DiffException when testing.");
        }
        result.delete();
    }
    
}
