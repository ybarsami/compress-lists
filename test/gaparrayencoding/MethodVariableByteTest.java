package gaparrayencoding;

import bitlistencoding.BitSequence;
import io.BitInputStreamArray;
import io.BitOutputStreamArray;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodVariableByteTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodVariableByte());
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodVariableByte());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodVariableByte());
    }

    /**
     * Test of writeCode method, of class MethodVariableByte.
     */
    @Test
    public void testWriteCode() {
        System.out.println("writeCode");
        MethodVariableByte instance = new MethodVariableByte();
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put(    1, "10000000");
        map.put(    2, "10000001");
        map.put(  128, "11111111");
        map.put(  129, "00000001" + "10000000");
        map.put(16384, "01111111" + "11111111");
        map.put(16385, "00000001" + "00000000" + "10000000");
        // Writing the test numbers
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCode method, of class MethodVariableByte.
     */
    @Test
    public void testReadCode() {
        System.out.println("readCode");
        MethodVariableByte instance = new MethodVariableByte();
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        ArrayList<Integer> testIntegers = new ArrayList<>();
        testIntegers.add(1);
        testIntegers.add(2);
        testIntegers.add(128);
        testIntegers.add(129);
        testIntegers.add(16384);
        testIntegers.add(16385);
        // Writing and reading the test numbers
        for (int testInteger : testIntegers) {
            expResult = testInteger;
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = instance.readCode(bitInputStream);
            assertEquals(expResult, result);
        }
    }

}
