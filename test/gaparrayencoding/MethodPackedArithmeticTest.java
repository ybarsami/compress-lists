package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodPackedArithmeticTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodPackedArithmetic());
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodPackedArithmetic());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodPackedArithmetic());
    }
    
}
