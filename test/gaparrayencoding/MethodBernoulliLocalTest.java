package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodBernoulliLocalTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodBernoulliLocal(nbDocumentsForBijection));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodBernoulliLocal(nbDocumentsForFables));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodBernoulliLocal(nbDocumentsForFables));
    }
    
}
