package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodContextBitTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodContextBit(3, 4, 3));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodContextBit(3, 4, 3));
    }

}
