package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodFContextTritTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodFContextTrit(3, 4, 3));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodFContextTrit(3, 4, 3));
    }
    
}
