package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;

import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodSimple8bTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        System.out.println("bijection");
        int nbDocuments = 15747;
        MethodSimple8b instance = new MethodSimple8b();
        // One gapArray
        int[] documentArray = new int[] { 84, 85, 510, 941, 946, 965, 978, 1008, 1009, 1774, 1862, 2248, 2254, 2755, 2756, 3494, 3495, 3716, 4428, 4462, 4676, 5218, 5219, 5430, 5455, 5470, 6007, 6229, 6408, 6467, 6500, 6601, 6654, 6850, 7757, 8261, 8262, 8263, 8264, 8265, 8324, 8359, 8423, 8438, 8808, 9413, 9739, 9885, 10512, 10766, 10842, 10962, 11124, 11140, 11141, 11188, 11222, 11780, 12146, 12148, 12415, 12455, 12456, 12644, 12736, 13643, 14131, 14153, 14172, 14239, 14240, 14250, 14254, 14262, 14596, 14860, 15032, 15033, 15042, 15043, 15428 };
        IntsRef gapArrayInputRef = new IntsRef(0);
        Tools.documentArray2gapArray(new IntsRef(documentArray), gapArrayInputRef);
        ArrayList<Long> longList = instance.longArrayListOfGapArray(gapArrayInputRef);
        IntsRef gapArrayOutputRef = new IntsRef(documentArray.length);
        instance.readGapArray(longList, gapArrayOutputRef);
        assertArrayEquals(gapArrayInputRef.ints, gapArrayOutputRef.ints);
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodSimple8b());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodSimple8b());
    }

    /**
     * Test of longArrayListOfGapArray method, of class MethodSimple8b.
     */
    @Test
    public void testLongArrayListOfGapArray() {
        System.out.println("longArrayListOfGapArray");
        MethodSimple8b instance = new MethodSimple8b();
        int[] gapArray;
        ArrayList<Long> expResult, result;
        long myLong;
        int nbGapsLocal, index;
        // [1, 1, ..., 1] (240 values in [1, 1])
        nbGapsLocal = 240;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < nbGapsLocal; i++) {
            gapArray[i] = 1;
        }
        expResult = new ArrayList<>();
        expResult.add(0L);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 1, ..., 1] (120 values in [1, 1])
        nbGapsLocal = 120;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < nbGapsLocal; i++) {
            gapArray[i] = 1;
        }
        expResult = new ArrayList<>();
        expResult.add(1L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, ..., 1, 2] (60 values in [1, 2])
        nbGapsLocal = 60;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < 30; i++) {
            gapArray[2 * i + 0] = 1;
            gapArray[2 * i + 1] = 2;
        }
        expResult = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 30; i++) {
            myLong = (myLong << 1) + 0L;
            myLong = (myLong << 1) + 1L;
        }
        expResult.add(myLong | 2L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2] (30 values in [1, 4])
        nbGapsLocal = 30;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < 8; i++) {
            index = 4 * i + 0; if (index < nbGapsLocal) gapArray[index] = 1;
            index = 4 * i + 1; if (index < nbGapsLocal) gapArray[index] = 2;
            index = 4 * i + 2; if (index < nbGapsLocal) gapArray[index] = 3;
            index = 4 * i + 3; if (index < nbGapsLocal) gapArray[index] = 4;
        }
        expResult = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 8; i++) {
            index = 4 * i + 0; if (index < nbGapsLocal) myLong = (myLong << 2) + 0L;
            index = 4 * i + 1; if (index < nbGapsLocal) myLong = (myLong << 2) + 1L;
            index = 4 * i + 2; if (index < nbGapsLocal) myLong = (myLong << 2) + 2L;
            index = 4 * i + 3; if (index < nbGapsLocal) myLong = (myLong << 2) + 3L;
        }
        expResult.add(myLong | 3L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4] (20 values in [1, 8])
        nbGapsLocal = 20;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < 3; i++) {
            index = 8 * i + 0; if (index < nbGapsLocal) gapArray[index] = 1;
            index = 8 * i + 1; if (index < nbGapsLocal) gapArray[index] = 2;
            index = 8 * i + 2; if (index < nbGapsLocal) gapArray[index] = 3;
            index = 8 * i + 3; if (index < nbGapsLocal) gapArray[index] = 4;
            index = 8 * i + 4; if (index < nbGapsLocal) gapArray[index] = 5;
            index = 8 * i + 5; if (index < nbGapsLocal) gapArray[index] = 6;
            index = 8 * i + 6; if (index < nbGapsLocal) gapArray[index] = 7;
            index = 8 * i + 7; if (index < nbGapsLocal) gapArray[index] = 8;
        }
        expResult = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 3; i++) {
            index = 8 * i + 0; if (index < nbGapsLocal) myLong = (myLong << 3) + 0L;
            index = 8 * i + 1; if (index < nbGapsLocal) myLong = (myLong << 3) + 1L;
            index = 8 * i + 2; if (index < nbGapsLocal) myLong = (myLong << 3) + 2L;
            index = 8 * i + 3; if (index < nbGapsLocal) myLong = (myLong << 3) + 3L;
            index = 8 * i + 4; if (index < nbGapsLocal) myLong = (myLong << 3) + 4L;
            index = 8 * i + 5; if (index < nbGapsLocal) myLong = (myLong << 3) + 5L;
            index = 8 * i + 6; if (index < nbGapsLocal) myLong = (myLong << 3) + 6L;
            index = 8 * i + 7; if (index < nbGapsLocal) myLong = (myLong << 3) + 7L;
        }
        expResult.add(myLong | 4L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, 4, 8, 16, 1, 2, 4, 8, 16, 1, 2, 4, 8, 16] (15 values in [1, 16])
        nbGapsLocal = 15;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < 3; i++) {
            gapArray[5 * i + 0] = 1;
            gapArray[5 * i + 1] = 2;
            gapArray[5 * i + 2] = 4;
            gapArray[5 * i + 3] = 8;
            gapArray[5 * i + 4] = 16;
        }
        expResult = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 3; i++) {
            myLong = (myLong << 4) + 0L;
            myLong = (myLong << 4) + 1L;
            myLong = (myLong << 4) + 3L;
            myLong = (myLong << 4) + 7L;
            myLong = (myLong << 4) + 15L;
        }
        expResult.add(myLong | 5L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, 4, 8, 16, 32, 1, 2, 4, 8, 16, 32] (12 values in [1, 32])
        nbGapsLocal = 12;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < 2; i++) {
            gapArray[6 * i + 0] = 1;
            gapArray[6 * i + 1] = 2;
            gapArray[6 * i + 2] = 4;
            gapArray[6 * i + 3] = 8;
            gapArray[6 * i + 4] = 16;
            gapArray[6 * i + 5] = 32;
        }
        expResult = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 2; i++) {
            myLong = (myLong << 5) + 0L;
            myLong = (myLong << 5) + 1L;
            myLong = (myLong << 5) + 3L;
            myLong = (myLong << 5) + 7L;
            myLong = (myLong << 5) + 15L;
            myLong = (myLong << 5) + 31L;
        }
        expResult.add(myLong | 6L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, 4, 8, 16, 32, 64, 1, 2, 4] (10 values in [1, 64])
        nbGapsLocal = 10;
        gapArray = new int[nbGapsLocal];
        for (int i = 0; i < 2; i++) {
            index = 7 * i + 0; if (index < nbGapsLocal) gapArray[index] = 1;
            index = 7 * i + 1; if (index < nbGapsLocal) gapArray[index] = 2;
            index = 7 * i + 2; if (index < nbGapsLocal) gapArray[index] = 4;
            index = 7 * i + 3; if (index < nbGapsLocal) gapArray[index] = 8;
            index = 7 * i + 4; if (index < nbGapsLocal) gapArray[index] = 16;
            index = 7 * i + 5; if (index < nbGapsLocal) gapArray[index] = 32;
            index = 7 * i + 6; if (index < nbGapsLocal) gapArray[index] = 64;
        }
        expResult = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 2; i++) {
            index = 7 * i + 0; if (index < nbGapsLocal) myLong = (myLong << 6) + 0L;
            index = 7 * i + 1; if (index < nbGapsLocal) myLong = (myLong << 6) + 1L;
            index = 7 * i + 2; if (index < nbGapsLocal) myLong = (myLong << 6) + 3L;
            index = 7 * i + 3; if (index < nbGapsLocal) myLong = (myLong << 6) + 7L;
            index = 7 * i + 4; if (index < nbGapsLocal) myLong = (myLong << 6) + 15L;
            index = 7 * i + 5; if (index < nbGapsLocal) myLong = (myLong << 6) + 31L;
            index = 7 * i + 6; if (index < nbGapsLocal) myLong = (myLong << 6) + 63L;
        }
        expResult.add(myLong | 7L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 2, 4, 8, 16, 32, 64, 128] (8 values in [1, 128])
        nbGapsLocal = 8;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 2;
        gapArray[2] = 4;
        gapArray[3] = 8;
        gapArray[4] = 16;
        gapArray[5] = 32;
        gapArray[6] = 64;
        gapArray[7] = 128;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 7) + 0L;
        myLong = (myLong << 7) + 1L;
        myLong = (myLong << 7) + 3L;
        myLong = (myLong << 7) + 7L;
        myLong = (myLong << 7) + 15L;
        myLong = (myLong << 7) + 31L;
        myLong = (myLong << 7) + 63L;
        myLong = (myLong << 7) + 127L;
        expResult.add(myLong | 8L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 4, 16, 32, 64, 128, 256] (7 values in [1, 256])
        nbGapsLocal = 7;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 4;
        gapArray[2] = 16;
        gapArray[3] = 32;
        gapArray[4] = 64;
        gapArray[5] = 128;
        gapArray[6] = 256;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 8) + 0L;
        myLong = (myLong << 8) + 3L;
        myLong = (myLong << 8) + 15L;
        myLong = (myLong << 8) + 31L;
        myLong = (myLong << 8) + 63L;
        myLong = (myLong << 8) + 127L;
        myLong = (myLong << 8) + 255L;
        expResult.add(myLong | 9L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 4, 16, 64, 256, 1024] (6 values in [1, 1024])
        nbGapsLocal = 6;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 4;
        gapArray[2] = 16;
        gapArray[3] = 64;
        gapArray[4] = 256;
        gapArray[5] = 1024;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 10) + 0L;
        myLong = (myLong << 10) + 3L;
        myLong = (myLong << 10) + 15L;
        myLong = (myLong << 10) + 63L;
        myLong = (myLong << 10) + 255L;
        myLong = (myLong << 10) + 1023L;
        expResult.add(myLong | 10L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 16, 64, 1024, 4096] (5 values in [1, 4096])
        nbGapsLocal = 5;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 16;
        gapArray[2] = 64;
        gapArray[3] = 1024;
        gapArray[4] = 4096;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 12) + 0L;
        myLong = (myLong << 12) + 15L;
        myLong = (myLong << 12) + 63L;
        myLong = (myLong << 12) + 1023L;
        myLong = (myLong << 12) + 4095L;
        expResult.add(myLong | 11L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 32, 1024, 32768] (4 values in [1, 32768])
        nbGapsLocal = 4;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 32;
        gapArray[2] = 1024;
        gapArray[3] = 32768;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 15) + 0L;
        myLong = (myLong << 15) + 31L;
        myLong = (myLong << 15) + 1023L;
        myLong = (myLong << 15) + 32767L;
        expResult.add(myLong | 12L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 32768, 1048576] (3 values in [1, 1048576])
        nbGapsLocal = 3;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 32768;
        gapArray[2] = 1048576;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 20) + 0L;
        myLong = (myLong << 20) + 32767L;
        myLong = (myLong << 20) + 1048575L;
        expResult.add(myLong | 13L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [1, 1073741824] (2 values in [1, 1048576])
        nbGapsLocal = 2;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = 1;
        gapArray[1] = 1073741824;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 30) + 0L;
        myLong = (myLong << 30) + 1073741823L;
        expResult.add(myLong | 14L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
        // [2^32 - 1] (1 value in [1, 2^60])
        nbGapsLocal = 1;
        gapArray = new int[nbGapsLocal];
        gapArray[0] = Integer.MAX_VALUE;
        expResult = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 60) + (long)Integer.MAX_VALUE - 1L;
        expResult.add(myLong | 15L << 60);
        result = instance.longArrayListOfGapArray(new IntsRef(gapArray));
        assertEquals(expResult, result);
    }

    /**
     * Test of readGapArray method, of class MethodSimple8b.
     */
    @Test
    public void testReadGapArray_ArrayList_int() {
        System.out.println("readGapArray");
        MethodSimple8b instance = new MethodSimple8b();
        ArrayList<Long> arrayList;
        int index, nbGapsLocal;
        int[] expResult, result;
        long myLong;
        // [1, 1, ..., 1] (240 values in [1, 1])
        nbGapsLocal = 240;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < nbGapsLocal; i++) {
            expResult[i] = 1;
        }
        arrayList = new ArrayList<>();
        arrayList.add(0L);
        result = new int[nbGapsLocal];
        IntsRef resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 1, ..., 1] (120 values in [1, 1])
        nbGapsLocal = 120;
        arrayList = new ArrayList<>();
        arrayList.add(1L << 60);
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < nbGapsLocal; i++) {
            expResult[i] = 1;
        }
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, ..., 1, 2] (60 values in [1, 2])
        nbGapsLocal = 60;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < 30; i++) {
            expResult[2 * i + 0] = 1;
            expResult[2 * i + 1] = 2;
        }
        arrayList = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 30; i++) {
            myLong = (myLong << 1) + 0L;
            myLong = (myLong << 1) + 1L;
        }
        arrayList.add(myLong | 2L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2] (30 values in [1, 4])
        nbGapsLocal = 30;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < 8; i++) {
            index = 4 * i + 0; if (index < nbGapsLocal) expResult[index] = 1;
            index = 4 * i + 1; if (index < nbGapsLocal) expResult[index] = 2;
            index = 4 * i + 2; if (index < nbGapsLocal) expResult[index] = 3;
            index = 4 * i + 3; if (index < nbGapsLocal) expResult[index] = 4;
        }
        arrayList = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 8; i++) {
            index = 4 * i + 0; if (index < nbGapsLocal) myLong = (myLong << 2) + 0L;
            index = 4 * i + 1; if (index < nbGapsLocal) myLong = (myLong << 2) + 1L;
            index = 4 * i + 2; if (index < nbGapsLocal) myLong = (myLong << 2) + 2L;
            index = 4 * i + 3; if (index < nbGapsLocal) myLong = (myLong << 2) + 3L;
        }
        arrayList.add(myLong | 3L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4] (20 values in [1, 8])
        nbGapsLocal = 20;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < 3; i++) {
            index = 8 * i + 0; if (index < nbGapsLocal) expResult[index] = 1;
            index = 8 * i + 1; if (index < nbGapsLocal) expResult[index] = 2;
            index = 8 * i + 2; if (index < nbGapsLocal) expResult[index] = 3;
            index = 8 * i + 3; if (index < nbGapsLocal) expResult[index] = 4;
            index = 8 * i + 4; if (index < nbGapsLocal) expResult[index] = 5;
            index = 8 * i + 5; if (index < nbGapsLocal) expResult[index] = 6;
            index = 8 * i + 6; if (index < nbGapsLocal) expResult[index] = 7;
            index = 8 * i + 7; if (index < nbGapsLocal) expResult[index] = 8;
        }
        arrayList = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 3; i++) {
            index = 8 * i + 0; if (index < nbGapsLocal) myLong = (myLong << 3) + 0L;
            index = 8 * i + 1; if (index < nbGapsLocal) myLong = (myLong << 3) + 1L;
            index = 8 * i + 2; if (index < nbGapsLocal) myLong = (myLong << 3) + 2L;
            index = 8 * i + 3; if (index < nbGapsLocal) myLong = (myLong << 3) + 3L;
            index = 8 * i + 4; if (index < nbGapsLocal) myLong = (myLong << 3) + 4L;
            index = 8 * i + 5; if (index < nbGapsLocal) myLong = (myLong << 3) + 5L;
            index = 8 * i + 6; if (index < nbGapsLocal) myLong = (myLong << 3) + 6L;
            index = 8 * i + 7; if (index < nbGapsLocal) myLong = (myLong << 3) + 7L;
        }
        arrayList.add(myLong | 4L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, 4, 8, 16, 1, 2, 4, 8, 16, 1, 2, 4, 8, 16] (15 values in [1, 16])
        nbGapsLocal = 15;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < 3; i++) {
            expResult[5 * i + 0] = 1;
            expResult[5 * i + 1] = 2;
            expResult[5 * i + 2] = 4;
            expResult[5 * i + 3] = 8;
            expResult[5 * i + 4] = 16;
        }
        arrayList = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 3; i++) {
            myLong = (myLong << 4) + 0L;
            myLong = (myLong << 4) + 1L;
            myLong = (myLong << 4) + 3L;
            myLong = (myLong << 4) + 7L;
            myLong = (myLong << 4) + 15L;
        }
        arrayList.add(myLong | 5L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, 4, 8, 16, 32, 1, 2, 4, 8, 16, 32] (12 values in [1, 32])
        nbGapsLocal = 12;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < 2; i++) {
            expResult[6 * i + 0] = 1;
            expResult[6 * i + 1] = 2;
            expResult[6 * i + 2] = 4;
            expResult[6 * i + 3] = 8;
            expResult[6 * i + 4] = 16;
            expResult[6 * i + 5] = 32;
        }
        arrayList = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 2; i++) {
            myLong = (myLong << 5) + 0L;
            myLong = (myLong << 5) + 1L;
            myLong = (myLong << 5) + 3L;
            myLong = (myLong << 5) + 7L;
            myLong = (myLong << 5) + 15L;
            myLong = (myLong << 5) + 31L;
        }
        arrayList.add(myLong | 6L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, 4, 8, 16, 32, 64, 1, 2, 4] (10 values in [1, 64])
        nbGapsLocal = 10;
        expResult = new int[nbGapsLocal];
        for (int i = 0; i < 2; i++) {
            index = 7 * i + 0; if (index < nbGapsLocal) expResult[index] = 1;
            index = 7 * i + 1; if (index < nbGapsLocal) expResult[index] = 2;
            index = 7 * i + 2; if (index < nbGapsLocal) expResult[index] = 4;
            index = 7 * i + 3; if (index < nbGapsLocal) expResult[index] = 8;
            index = 7 * i + 4; if (index < nbGapsLocal) expResult[index] = 16;
            index = 7 * i + 5; if (index < nbGapsLocal) expResult[index] = 32;
            index = 7 * i + 6; if (index < nbGapsLocal) expResult[index] = 64;
        }
        arrayList = new ArrayList<>();
        myLong = 0L;
        for (int i = 0; i < 2; i++) {
            index = 7 * i + 0; if (index < nbGapsLocal) myLong = (myLong << 6) + 0L;
            index = 7 * i + 1; if (index < nbGapsLocal) myLong = (myLong << 6) + 1L;
            index = 7 * i + 2; if (index < nbGapsLocal) myLong = (myLong << 6) + 3L;
            index = 7 * i + 3; if (index < nbGapsLocal) myLong = (myLong << 6) + 7L;
            index = 7 * i + 4; if (index < nbGapsLocal) myLong = (myLong << 6) + 15L;
            index = 7 * i + 5; if (index < nbGapsLocal) myLong = (myLong << 6) + 31L;
            index = 7 * i + 6; if (index < nbGapsLocal) myLong = (myLong << 6) + 63L;
        }
        arrayList.add(myLong | 7L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 2, 4, 8, 16, 32, 64, 128] (8 values in [1, 128])
        nbGapsLocal = 8;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 2;
        expResult[2] = 4;
        expResult[3] = 8;
        expResult[4] = 16;
        expResult[5] = 32;
        expResult[6] = 64;
        expResult[7] = 128;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 7) + 0L;
        myLong = (myLong << 7) + 1L;
        myLong = (myLong << 7) + 3L;
        myLong = (myLong << 7) + 7L;
        myLong = (myLong << 7) + 15L;
        myLong = (myLong << 7) + 31L;
        myLong = (myLong << 7) + 63L;
        myLong = (myLong << 7) + 127L;
        arrayList.add(myLong | 8L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 4, 16, 32, 64, 128, 256] (7 values in [1, 256])
        nbGapsLocal = 7;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 4;
        expResult[2] = 16;
        expResult[3] = 32;
        expResult[4] = 64;
        expResult[5] = 128;
        expResult[6] = 256;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 8) + 0L;
        myLong = (myLong << 8) + 3L;
        myLong = (myLong << 8) + 15L;
        myLong = (myLong << 8) + 31L;
        myLong = (myLong << 8) + 63L;
        myLong = (myLong << 8) + 127L;
        myLong = (myLong << 8) + 255L;
        arrayList.add(myLong | 9L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 4, 16, 64, 256, 1024] (6 values in [1, 1024])
        nbGapsLocal = 6;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 4;
        expResult[2] = 16;
        expResult[3] = 64;
        expResult[4] = 256;
        expResult[5] = 1024;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 10) + 0L;
        myLong = (myLong << 10) + 3L;
        myLong = (myLong << 10) + 15L;
        myLong = (myLong << 10) + 63L;
        myLong = (myLong << 10) + 255L;
        myLong = (myLong << 10) + 1023L;
        arrayList.add(myLong | 10L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 16, 64, 1024, 4096] (5 values in [1, 4096])
        nbGapsLocal = 5;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 16;
        expResult[2] = 64;
        expResult[3] = 1024;
        expResult[4] = 4096;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 12) + 0L;
        myLong = (myLong << 12) + 15L;
        myLong = (myLong << 12) + 63L;
        myLong = (myLong << 12) + 1023L;
        myLong = (myLong << 12) + 4095L;
        arrayList.add(myLong | 11L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 32, 1024, 32768] (4 values in [1, 32768])
        nbGapsLocal = 4;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 32;
        expResult[2] = 1024;
        expResult[3] = 32768;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 15) + 0L;
        myLong = (myLong << 15) + 31L;
        myLong = (myLong << 15) + 1023L;
        myLong = (myLong << 15) + 32767L;
        arrayList.add(myLong | 12L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 32768, 1048576] (3 values in [1, 1048576])
        nbGapsLocal = 3;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 32768;
        expResult[2] = 1048576;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 20) + 0L;
        myLong = (myLong << 20) + 32767L;
        myLong = (myLong << 20) + 1048575L;
        arrayList.add(myLong | 13L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [1, 1073741824] (2 values in [1, 1048576])
        nbGapsLocal = 2;
        expResult = new int[nbGapsLocal];
        expResult[0] = 1;
        expResult[1] = 1073741824;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 30) + 0L;
        myLong = (myLong << 30) + 1073741823L;
        arrayList.add(myLong | 14L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
        // [2^32 - 1] (1 value in [1, 2^60])
        nbGapsLocal = 1;
        expResult = new int[nbGapsLocal];
        expResult[0] = Integer.MAX_VALUE;
        arrayList = new ArrayList<>();
        myLong = 0L;
        myLong = (myLong << 60) + (long)Integer.MAX_VALUE - 1L;
        arrayList.add(myLong | 15L << 60);
        result = new int[nbGapsLocal];
        resultRef = new IntsRef(result, 0, nbGapsLocal);
        instance.readGapArray(arrayList, resultRef);
        assertArrayEquals(expResult, result);
    }
    
}
