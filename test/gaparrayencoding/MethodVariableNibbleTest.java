package gaparrayencoding;

import bitlistencoding.BitSequence;
import io.BitInputStreamArray;
import io.BitOutputStreamArray;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodVariableNibbleTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodVariableNibble());
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodVariableNibble());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodVariableNibble());
    }

    /**
     * Test of writeCode method, of class MethodVariableNibble.
     */
    @Test
    public void testWriteCode() {
        System.out.println("writeCode");
        MethodVariableNibble instance = new MethodVariableNibble();
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "1000");
        map.put( 2, "1001");
        map.put( 8, "1111");
        map.put( 9, "0001" + "1000");
        map.put(64, "0111" + "1111");
        map.put(65, "0001" + "0000" + "1000");
        // Writing the test numbers
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCode method, of class MethodVariableNibble.
     */
    @Test
    public void testReadCode() {
        System.out.println("readCode");
        MethodVariableNibble instance = new MethodVariableNibble();
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        ArrayList<Integer> testIntegers = new ArrayList<>();
        testIntegers.add(1);
        testIntegers.add(2);
        testIntegers.add(8);
        testIntegers.add(9);
        testIntegers.add(64);
        testIntegers.add(65);
        // Writing and reading the test numbers
        for (int testInteger : testIntegers) {
            expResult = testInteger;
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = instance.readCode(bitInputStream);
            assertEquals(expResult, result);
        }
    }

}
