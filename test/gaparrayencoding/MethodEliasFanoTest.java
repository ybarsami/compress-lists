/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodEliasFanoTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodEliasFano(nbDocumentsForBijection));
        GapArrayTest.testBijection(new MethodEliasFano(nbDocumentsForBijection, false));
        GapArrayTest.testBijection(new MethodQuasiSuccinct(nbDocumentsForBijection));
        // Dynamically-sized blocks
        GapArrayTest.testBijection(new MethodEliasFanoOptBlock());
        GapArrayTest.testBijection(new MethodQuasiSuccinctOptBlock());
        GapArrayTest.testBijection(new MethodQuasiSuccinctApproxBlock());
        // Fixed-size blocks
        GapArrayTest.testBijection(new MethodByBlockNoPadding(new MethodEliasFano(nbDocumentsForBijection)));
        GapArrayTest.testBijection(new MethodByBlockNoPadding(new MethodEliasFano(nbDocumentsForBijection, false)));
        GapArrayTest.testBijection(new MethodByBlockNoPadding(new MethodQuasiSuccinct(nbDocumentsForBijection)));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodEliasFano(nbDocumentsForFables));
        GapArrayTest.testFables(new MethodEliasFano(nbDocumentsForFables, false));
        GapArrayTest.testFables(new MethodQuasiSuccinct(nbDocumentsForFables));
        // Dynamically-sized blocks
        GapArrayTest.testFables(new MethodEliasFanoOptBlock());
        GapArrayTest.testFables(new MethodQuasiSuccinctOptBlock());
        GapArrayTest.testFables(new MethodQuasiSuccinctApproxBlock());
        // Fixed-size blocks
        GapArrayTest.testFables(new MethodByBlock(new MethodEliasFano(nbDocumentsForFables)));
        GapArrayTest.testFables(new MethodByBlock(new MethodEliasFano(nbDocumentsForFables, false)));
        GapArrayTest.testFables(new MethodByBlockNoPadding(new MethodEliasFano(nbDocumentsForFables)));
        GapArrayTest.testFables(new MethodByBlock(new MethodQuasiSuccinct(nbDocumentsForFables)));
        GapArrayTest.testFables(new MethodByBlockNoPadding(new MethodQuasiSuccinct(nbDocumentsForFables)));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodEliasFano(nbDocumentsForFables));
        GapArrayTest.testComputeSize(new MethodEliasFano(nbDocumentsForFables, false));
        GapArrayTest.testComputeSize(new MethodQuasiSuccinct(nbDocumentsForFables));
        // Dynamically-sized blocks
        GapArrayTest.testComputeSize(new MethodEliasFanoOptBlock());
        GapArrayTest.testComputeSize(new MethodQuasiSuccinctOptBlock());
        GapArrayTest.testComputeSize(new MethodQuasiSuccinctApproxBlock());
        // Fixed-size blocks
        GapArrayTest.testComputeSize(new MethodByBlock(new MethodEliasFano(nbDocumentsForFables)));
        GapArrayTest.testComputeSize(new MethodByBlock(new MethodEliasFano(nbDocumentsForFables, false)));
        GapArrayTest.testComputeSize(new MethodByBlockNoPadding(new MethodEliasFano(nbDocumentsForFables)));
        GapArrayTest.testComputeSize(new MethodByBlock(new MethodQuasiSuccinct(nbDocumentsForFables)));
        GapArrayTest.testComputeSize(new MethodByBlockNoPadding(new MethodQuasiSuccinct(nbDocumentsForFables)));
    }
    
}
