package gaparrayencoding;

import compresslists.CompressLists.ContextParameters;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodByQuatritListTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodByQuatritList(
                nbDocumentsForBijection,
                new quatritlistencoding.MethodContext(new ContextParameters(3, 4, 3))));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodByQuatritList(
                nbDocumentsForFables,
                new quatritlistencoding.MethodContext(new ContextParameters(3, 4, 3))));
    }
    
}
