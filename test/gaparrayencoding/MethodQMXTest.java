package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodQMXTest extends GapArrayTest {
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodQMX());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodQMX());
    }
    
}
