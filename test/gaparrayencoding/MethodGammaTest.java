package gaparrayencoding;

import bitlistencoding.BitSequence;
import io.BitInputStreamArray;
import io.BitOutputStreamArray;

import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodGammaTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodGamma());
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodGamma());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodGamma());
    }

    /**
     * Test of writeCode method, of class MethodGamma.
     */
    @Test
    public void testWriteCode() {
        System.out.println("writeCode");
        MethodGamma instance = new MethodGamma();
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "100");
        map.put( 3, "101");
        map.put( 4, "11000");
        map.put( 5, "11001");
        map.put( 6, "11010");
        map.put( 7, "11011");
        map.put( 8, "1110000");
        map.put( 9, "1110001");
        map.put(10, "1110010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCode method, of class MethodGamma.
     */
    @Test
    public void testReadCode() {
        System.out.println("readCode");
        MethodGamma instance = new MethodGamma();
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = instance.readCode(bitInputStream);
            assertEquals(expResult, result);
        }
    }
    
}
