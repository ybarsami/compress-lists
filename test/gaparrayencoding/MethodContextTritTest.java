package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodContextTritTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodContextTrit(3, 4, 3));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodContextTrit(3, 4, 3));
    }

}
