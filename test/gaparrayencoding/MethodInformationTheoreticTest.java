package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodInformationTheoreticTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodInformationTheoretic(nbDocumentsForBijection));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodInformationTheoretic(nbDocumentsForFables));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodInformationTheoretic(nbDocumentsForFables));
    }

}
