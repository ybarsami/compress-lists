package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodBitVectorTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodBitVector());
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodBitVector());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodBitVector());
    }

}
