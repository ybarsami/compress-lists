package gaparrayencoding;

import bitlistencoding.BitSequence;
import io.BitInputStreamArray;
import io.BitOutputStreamArray;

import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class MethodDeltaTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodDelta());
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodDelta());
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodDelta());
    }

    /**
     * Test of writeCode method, of class MethodDelta.
     */
    @Test
    public void testWriteCode() {
        System.out.println("writeCode");
        MethodDelta instance = new MethodDelta();
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "1000");
        map.put( 3, "1001");
        map.put( 4, "10100");
        map.put( 5, "10101");
        map.put( 6, "10110");
        map.put( 7, "10111");
        map.put( 8, "11000000");
        map.put( 9, "11000001");
        map.put(10, "11000010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            instance.writeCode(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCode method, of class MethodDelta.
     */
    @Test
    public void testReadCode() {
        System.out.println("readCode");
        MethodDelta instance = new MethodDelta();
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            instance.writeCode(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = instance.readCode(bitInputStream);
            assertEquals(expResult, result);
        }
    }
    
}
