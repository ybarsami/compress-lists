package gaparrayencoding;

import compresslists.CompressLists.ContextParameters;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodByTritConcatenationTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodByTritConcatenation(
                nbDocumentsForBijection,
                new tritlistencoding.MethodContext(new ContextParameters(3, 4, 3))));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodByTritConcatenation(
                nbDocumentsForFables,
                new tritlistencoding.MethodContext(new ContextParameters(3, 4, 3))));
    }
    
}
