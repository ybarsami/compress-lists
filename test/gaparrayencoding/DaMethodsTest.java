package gaparrayencoding;

import bitlistencoding.BitSequenceMarkovHandler;
import compresslists.CompressLists.BitContextParameters;
import compresslists.CompressLists.ContextParameters;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class DaMethodsTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        // Note: a true test would involve an array with more than BLOCK_SIZE
        // documents (here, 81 documents and BLOCK_SIZE = 128).
        GapArrayTest.testBijection(new MethodByBlockNoPadding(new MethodInterpolative(nbDocumentsForBijection)));
        GapArrayTest.testBijection(new DaMethods.MethodBitVectorContext(new BitContextParameters(3, 4, 3)));
        GapArrayTest.testBijection(new DaMethods.MethodBitVectorAdaptiveContext(new BitContextParameters(3, 4, 3), nbDocumentsForBijection, 1, true));
        GapArrayTest.testBijection(new DaMethods.MethodBitVectorIT());
        GapArrayTest.testBijection(new DaMethods.MethodBookstein(BitSequenceMarkovHandler.Model.M_4C1));
//        GapArrayTest.testBijection(new DaMethods.MethodDeltaContext(new BitContextParameters(3, 4, 3)));
        GapArrayTest.testBijection(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3)));
        GapArrayTest.testBijection(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3)));
        GapArrayTest.testBijection(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3, true, true)));
        GapArrayTest.testBijection(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3, true, true)));
        GapArrayTest.testBijection(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3, true, false)));
        GapArrayTest.testBijection(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3, true, false)));
        GapArrayTest.testBijection(new DaMethods.MethodTritConcatenateContext(new ContextParameters(3, 4, 3), nbDocumentsForBijection));
        GapArrayTest.testBijection(new DaMethods.MethodQuatritConcatenateContext(new ContextParameters(3, 4, 3), nbDocumentsForBijection));
        GapArrayTest.testBijection(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3), nbDocumentsForBijection));
        GapArrayTest.testBijection(new DaMethods.MethodQuatritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3), nbDocumentsForBijection));
        GapArrayTest.testBijection(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3, true, true), nbDocumentsForBijection));
        GapArrayTest.testBijection(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3, true, false), nbDocumentsForBijection));
        GapArrayTest.testBijection(new DaMethods.MethodTritListAdaptiveContext(new ContextParameters(3, 4, 3), nbDocumentsForBijection, 1, true));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodByBlockNoPadding(new MethodInterpolative(nbDocumentsForFables)));
        GapArrayTest.testFables(new MethodByBlock(new MethodInterpolative(nbDocumentsForFables)));
        GapArrayTest.testFables(new DaMethods.MethodBitVectorContext(new BitContextParameters(3, 4, 3)));
        GapArrayTest.testFables(new DaMethods.MethodBitVectorAdaptiveContext(new BitContextParameters(3, 4, 3), nbDocumentsForFables, 1, true));
        GapArrayTest.testFables(new DaMethods.MethodBitVectorIT());
        GapArrayTest.testFables(new DaMethods.MethodBookstein(BitSequenceMarkovHandler.Model.M_4C1));
//        GapArrayTest.testFables(new DaMethods.MethodDeltaContext(new BitContextParameters(3, 4, 3)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(nbDocumentsForFables, nbPointersForFables)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3, true, true)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3, true, true)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3, true, false)));
        GapArrayTest.testFables(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3, true, false)));
        GapArrayTest.testFables(new DaMethods.MethodTritConcatenateContext(new ContextParameters(3, 4, 3), nbDocumentsForFables));
        GapArrayTest.testFables(new DaMethods.MethodQuatritConcatenateContext(new ContextParameters(3, 4, 3), nbDocumentsForFables));
        GapArrayTest.testFables(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3), nbDocumentsForFables));
        GapArrayTest.testFables(new DaMethods.MethodQuatritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3), nbDocumentsForFables));
        GapArrayTest.testFables(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3, true, true), nbDocumentsForFables));
        GapArrayTest.testFables(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3, true, false), nbDocumentsForFables));
        GapArrayTest.testFables(new DaMethods.MethodTritListAdaptiveContext(new ContextParameters(3, 4, 3), nbDocumentsForFables, 1, true));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodByBlockNoPadding(new MethodInterpolative(nbDocumentsForFables)));
        GapArrayTest.testComputeSize(new MethodByBlock(new MethodInterpolative(nbDocumentsForFables)));
        GapArrayTest.testComputeSize(new DaMethods.MethodBitVectorContext(new BitContextParameters(3, 4, 3)));
        GapArrayTest.testComputeSize(new DaMethods.MethodBitVectorAdaptiveContext(new BitContextParameters(3, 4, 3), nbDocumentsForFables, 1, true));
        GapArrayTest.testComputeSize(new DaMethods.MethodBitVectorIT());
        GapArrayTest.testComputeSize(new DaMethods.MethodBookstein(BitSequenceMarkovHandler.Model.M_4C1));
//        GapArrayTest.testComputeSize(new DaMethods.MethodDeltaContext(new BitContextParameters(3, 4, 3)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(nbDocumentsForFables, nbPointersForFables)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3, true, true)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3, true, true)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(3, 4, 3, true, false)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3, true, false)));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritConcatenateContext(new ContextParameters(3, 4, 3), nbDocumentsForFables));
        GapArrayTest.testComputeSize(new DaMethods.MethodQuatritConcatenateContext(new ContextParameters(3, 4, 3), nbDocumentsForFables));
        GapArrayTest.testComputeSize(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3), nbDocumentsForFables));
        GapArrayTest.testComputeSize(new DaMethods.MethodQuatritListContext(new ContextParameters(3, new int[] { 4, 2 }, 3), nbDocumentsForFables));
        GapArrayTest.testComputeSize(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3, true, true), nbDocumentsForFables));
        GapArrayTest.testComputeSize(new DaMethods.MethodQuatritListContext(new ContextParameters(3, 4, 3, true, false), nbDocumentsForFables));
        GapArrayTest.testComputeSize(new DaMethods.MethodTritListAdaptiveContext(new ContextParameters(3, 4, 3), nbDocumentsForFables, 1, true));
    }
    
    boolean doLongTest = false;
    
    @Test
    public void testDasovich() {
        if (doLongTest) {
            GapArrayTest.testDasovich(new DaMethods.MethodBitVectorAdaptiveContext(new BitContextParameters(3, 4, 3), nbDocumentsForDasovich, 1, true));
            GapArrayTest.testDasovich(new DaMethods.MethodTritListAdaptiveContext(new ContextParameters(3, 4, 3), nbDocumentsForDasovich, 1, true));
        }
    }
    
}
