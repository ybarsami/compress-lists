package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.Dataset;
import io.DatasetInfo;
import io.DatasetReader;

import com.github.difflib.DiffUtils;
import com.github.difflib.algorithm.DiffException;
import com.github.difflib.patch.Patch;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author yann
 */
public class GapArrayTest {
    
    protected static int nbDocumentsForBijection = 15747;
    protected static int nbDocumentsForDasovich = 15748;
    protected static int nbDocumentsForFables = 240;
    protected static long nbPointersForFables = 41652L;
    
    /**
     * Full test on a random documentArray to test that a given method by bit
     * sequence is indeed invertible.
     */
    public static void testBijection(MethodByBitSequence method) {
        System.out.println("Bijection " + method.getName());
        int[] documentArray = new int[] { 84, 85, 510, 941, 946, 965, 978, 1008, 1009, 1774, 1862, 2248, 2254, 2755, 2756, 3494, 3495, 3716, 4428, 4462, 4676, 5218, 5219, 5430, 5455, 5470, 6007, 6229, 6408, 6467, 6500, 6601, 6654, 6850, 7757, 8261, 8262, 8263, 8264, 8265, 8324, 8359, 8423, 8438, 8808, 9413, 9739, 9885, 10512, 10766, 10842, 10962, 11124, 11140, 11141, 11188, 11222, 11780, 12146, 12148, 12415, 12455, 12456, 12644, 12736, 13643, 14131, 14153, 14172, 14239, 14240, 14250, 14254, 14262, 14596, 14860, 15032, 15033, 15042, 15043, 15428 };
        assert(documentArray[documentArray.length - 1] <= nbDocumentsForBijection);
        IntsRef gapArrayInputRef = new IntsRef(0);
        Tools.documentArray2gapArray(new IntsRef(documentArray), gapArrayInputRef);
        int[] gapArrayInput = gapArrayInputRef.ints.clone();
        method.init(new DatasetInfo(gapArrayInput, nbDocumentsForBijection));
        BitSequence bitSequence = method.gapArray2bitSequence(gapArrayInputRef);
        int[] gapArrayOutput = method.bitSequence2gapArray(bitSequence, documentArray.length);
        assertArrayEquals(gapArrayInput, gapArrayOutput);
    }
    
    /**
     * Full test on the Fables dataset to test that the index created by a given
     * method is indeed invertible.
     */
    public static void testFables(Method method) {
        System.out.println("Bijection(Fables) " + method.getName());
        String originalFilename = "test" + File.separatorChar + "Fables_ASCII.txt";
        String baseFilename = "index_test";
        String createdFilename = "index_test_ASCII.txt";
        // import the dataset from the .csv file
        DatasetReader datasetReader = Dataset.FABLES_NAME.getDatasetReader();
        int nbDocuments = datasetReader.getNbDocuments();
        long nbPointers = datasetReader.getNbPointers();
        assert(nbDocuments == nbDocumentsForFables);
        assert(nbPointers == nbPointersForFables);
        method.init(datasetReader);
        // export the dataset to a compressed index
        String tmpFilename = method.exportToFile(baseFilename, datasetReader, nbDocuments);
        // re-import the just created index
        Dataset exportedDataset = new Dataset(tmpFilename, Dataset.Format.FORMAT_CL2020);
        DatasetReader dataset = exportedDataset.getDatasetReader(); //method.importFromFile(tmpFilename);
        // export the (imported, exported then imported) dataset to ASCII
        Method.exportToFileASCII(createdFilename, dataset, dataset.getNbDocuments());
        // clean temporary files
        new File(tmpFilename).delete();
        new File(baseFilename + ".stats").delete();
        new File("test" + File.separatorChar + "Fables_ASCII.stats").delete();
        // compare this just created ASCII index to the correct ASCII file
        File expResult = new File(originalFilename);
        File result = new File(createdFilename);
        try {
            List<String> original = Files.readAllLines(result.toPath(), StandardCharsets.UTF_8);
            List<String> revised = Files.readAllLines(expResult.toPath(), StandardCharsets.UTF_8);

            // Compute diff. Get the Patch object. Patch is the container for computed deltas.
            Patch<String> patch = DiffUtils.diff(original, revised);
            assertEquals(0, patch.getDeltas().size());
        } catch (IOException e) {
            fail("IOException when testing.");
        } catch (DiffException e) {
            fail("DiffException when testing.");
        }
        result.delete();
    }
    
    /**
     * Full test on the Fables dataset to test that the computeSize() method
     * is consistent with the size of the file index created.
     */
    public static void testComputeSize(Method method) {
        System.out.println("ComputeSize(Fables) " + method.getName());
        String baseFilename = "index_test";
        // import the dataset from the .csv file
        DatasetReader datasetReader = Dataset.FABLES_NAME.getDatasetReader();
        int nbDocuments = datasetReader.getNbDocuments();
        assert(nbDocuments == nbDocumentsForFables);
        method.init(datasetReader);
        // export the dataset to a compressed index
        String tmpFilename = method.exportToFile(baseFilename, datasetReader, nbDocuments);
        File compressedIndex = new File(tmpFilename);
        // export the dataset to a compressed index
        long sizeComputed = (long)Math.ceil(method.computeSize(datasetReader, nbDocuments, false) / 8.);
        long sizeFile = compressedIndex.length();
        // clean temporary files
        compressedIndex.delete();
        new File(baseFilename + ".stats").delete();
        try {
            assert(sizeComputed == sizeFile);
        } catch (AssertionError e) {
            System.out.println("sizeComputed = " + sizeComputed);
            System.out.println("sizeFile     = " + sizeFile);
//            throw e;
        // Because of the finite number of bits on which we make computations,
        // there is a discrepancy between the real \sum_i -p_i * log_2(p_i) and
        // its computed value. Thus, there is a discrepancy, when using those
        // methods, between computed and real file size.
        }
    }
    
    /**
     * Full test on the Dasovich dataset to test that the index created by a
     * given method is indeed invertible.
     */
    public static void testDasovich(Method method) {
        System.out.println("Bijection(Dasovich) " + method.getName());
        DatasetReader datasetReader = Dataset.DASOVICH_BISECTION_DENSITY.getDatasetReader();
        int nbDocuments = datasetReader.getNbDocuments();
        assert(nbDocuments == nbDocumentsForDasovich);
        method.init(datasetReader);
        String originalFilename = Dataset.DASOVICH_BISECTION_DENSITY.getDatasetFileName();
        String baseFilename = "index_test";
        // export the dataset to a compressed index
        String tmpFilename = method.exportToFile(baseFilename, datasetReader, nbDocuments);
        // re-import the just created index
        DatasetReader dataset = method.importFromFile(tmpFilename);
        // export the (imported, exported then imported) dataset to csv
        String createdFilename = dataset.writeCsvDataset(baseFilename);
        // clean temporary files
        new File(tmpFilename).delete();
        new File(baseFilename + ".stats").delete();
        // compare this just created csv index to the correct csv file
        File expResult = new File(originalFilename);
        File result = new File(createdFilename);
        try {
            List<String> original = Files.readAllLines(result.toPath(), StandardCharsets.UTF_8);
            List<String> revised = Files.readAllLines(expResult.toPath(), StandardCharsets.UTF_8);

            // Compute diff. Get the Patch object. Patch is the container for computed deltas.
            Patch<String> patch = DiffUtils.diff(original, revised);
            assertEquals(0, patch.getDeltas().size());
        } catch (IOException e) {
            fail("IOException when testing.");
        } catch (DiffException e) {
            fail("DiffException when testing.");
        }
        result.delete();
    }
    
}
