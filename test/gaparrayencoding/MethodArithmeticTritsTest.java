package gaparrayencoding;

import org.junit.Test;

/**
 *
 * @author yann
 */
public class MethodArithmeticTritsTest extends GapArrayTest {
    
    @Test
    public void testBijection() {
        GapArrayTest.testBijection(new MethodArithmeticTrits(nbDocumentsForBijection, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_DENSITY));
        GapArrayTest.testBijection(new MethodArithmeticTrits(nbDocumentsForBijection, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_REAL));
        GapArrayTest.testBijection(new MethodArithmeticTrits(nbDocumentsForBijection, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_UNIFORM));
    }
    
    @Test
    public void testFables() {
        GapArrayTest.testFables(new MethodArithmeticTrits(nbDocumentsForFables, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_DENSITY));
        GapArrayTest.testFables(new MethodArithmeticTrits(nbDocumentsForFables, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_REAL));
        GapArrayTest.testFables(new MethodArithmeticTrits(nbDocumentsForFables, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_UNIFORM));
    }
    
    @Test
    public void testComputeSize() {
        GapArrayTest.testComputeSize(new MethodArithmeticTrits(nbDocumentsForFables, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_DENSITY));
        GapArrayTest.testComputeSize(new MethodArithmeticTrits(nbDocumentsForFables, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_REAL));
        GapArrayTest.testComputeSize(new MethodArithmeticTrits(nbDocumentsForFables, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_UNIFORM));
    }
    
}
