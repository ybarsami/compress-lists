/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerencoding;

import io.BitInputStreamArray;
import io.BitOutputStreamArray;
import bitlistencoding.BitSequence;

import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class GammaEncodingTest {
    
    public GammaEncodingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of writeCodeGamma method, of class GammaEncoding.
     */
    @Test
    public void testWriteCodeGamma() {
        System.out.println("writeCodeGamma");
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "100");
        map.put( 3, "101");
        map.put( 4, "11000");
        map.put( 5, "11001");
        map.put( 6, "11010");
        map.put( 7, "11011");
        map.put( 8, "1110000");
        map.put( 9, "1110001");
        map.put(10, "1110010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            GammaEncoding.writeCodeGamma(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            GammaEncoding.writeCodeGamma(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCodeGamma method, of class GammaEncoding.
     */
    @Test
    public void testReadCodeGamma() {
        System.out.println("readCodeGamma");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            GammaEncoding.writeCodeGamma(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = GammaEncoding.readCodeGamma(bitInputStream);
            assertEquals(expResult, result);
        }
    }
    
}
