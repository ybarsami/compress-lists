/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerencoding;

import io.BitInputStreamArray;
import io.BitOutputStreamArray;
import bitlistencoding.BitSequence;

import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class UnaryEncodingTest {
    
    public UnaryEncodingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of writeCodeUnary method, of class MethodUnary.
     */
    @Test
    public void testWriteCodeUnary() {
        System.out.println("writeCodeUnary");
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "10");
        map.put( 3, "110");
        map.put( 4, "1110");
        map.put( 5, "11110");
        map.put( 6, "111110");
        map.put( 7, "1111110");
        map.put( 8, "11111110");
        map.put( 9, "111111110");
        map.put(10, "1111111110");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            UnaryEncoding.writeCodeUnary(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            UnaryEncoding.writeCodeUnary(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCodeUnary method, of class UnaryEncoding.
     */
    @Test
    public void testReadCodeUnary() {
        System.out.println("readCodeUnary");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            UnaryEncoding.writeCodeUnary(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = UnaryEncoding.readCodeUnary(bitInputStream);
            assertEquals(expResult, result);
        }
    }
    
}
