/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerencoding;

import io.BitInputStreamArray;
import io.BitOutputStream;
import io.BitOutputStreamArray;
import bitlistencoding.BitSequence;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class BinaryEncodingTest {
    
    public BinaryEncodingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of writeCodeBinary method, of class BinaryEncoding.
     */
    @Test
    public void testWriteCodeBinary() {
        System.out.println("writeCodeBinary");
        int nbBitsToWrite = 2;
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put(0, "00");
        map.put(1, "01");
        map.put(2, "10");
        map.put(3, "11");
        // Writing 0..3
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            BinaryEncoding.writeCodeBinary(x, bitOutputStream, nbBitsToWrite);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write -1
        x = -1;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            BinaryEncoding.writeCodeBinary(x, bitOutputStream, nbBitsToWrite);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Trying to write 4 (not possible on 2 bits)
        x = 4;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            BinaryEncoding.writeCodeBinary(x, bitOutputStream, nbBitsToWrite);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCodeBinary method, of class BinaryEncoding.
     */
    @Test
    public void testReadCodeBinary() {
        System.out.println("readCodeBinary");
        int nbBits = 5;
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 0..31
        for (expResult = 0; expResult < 32; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            BinaryEncoding.writeCodeBinary(expResult, bitOutputStream, nbBits);
            bitInputStream = new BitInputStreamArray(buffer);
            result = BinaryEncoding.readCodeBinary(bitInputStream, nbBits);
            assertEquals(expResult, result);
        }
    }
    
    
    private static String codeBinaryNaive(int x, int lo, int hi) {
        BitSequence buffer = new BitSequence();
        BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(buffer);
        BinaryEncoding.writeCodeBinaryNaive(x, bitOutputStream, lo, hi);
        return buffer.toString();
    }
    
    /**
     * Test of writeCodeBinaryNaive method, of class BinaryEncoding.
     */
    @Test
    public void testWriteCodeBinaryNaive() {
        System.out.println("writeCodeBinaryNaive");
        String expResult, result;
        int x, lo, hi;
        lo = 1;
        hi = 20;
        // Regular number (example 1).
        x = 1;
        expResult = "00000";
        result = codeBinaryNaive(x, lo, hi);
        assertEquals(expResult, result);
        // Regular number (example 2).
        x = 10;
        expResult = "01001";
        result = codeBinaryNaive(x, lo, hi);
        assertEquals(expResult, result);
        // Out of range (down) number.
        x = 0;
        try {
            result = codeBinaryNaive(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Out of range (up) number.
        x = 21;
        try {
            result = codeBinaryNaive(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCodeBinaryNaive method, of class BinaryEncoding.
     */
    @Test
    public void testReadCodeBinaryNaive() {
        System.out.println("readCodeBinaryNaive");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            BinaryEncoding.writeCodeBinaryNaive(expResult, bitOutputStream, 1, 42);
            bitInputStream = new BitInputStreamArray(buffer);
            result = BinaryEncoding.readCodeBinaryNaive(bitInputStream, 1, 42);
            assertEquals(expResult, result);
        }
    }
    
    
    /*
     * The following code is devised for a *private static* method. Now that it
     * is public, it is no more relevant.
     */
    private static String codeBinaryInnerShorter(int x, int lo, int hi) throws Throwable {
        BitSequence buffer = new BitSequence();
        try {
            // Search for the method writeCodeBinaryInnerShorter inside class MethodBinary.
            java.lang.reflect.Method privateMethodWriteCodeBinaryInnerShorter = BinaryEncoding.class.getDeclaredMethod(
                    "writeCodeBinaryInnerShorter", int.class, BitOutputStream.class, int.class, int.class);
            // writeCodeBinary is private, so we grant access to it.
            privateMethodWriteCodeBinaryInnerShorter.setAccessible(true);
            // finally, invoke the method. It is static, so we don't need to create a new MethodBinary.
            BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(buffer);
            privateMethodWriteCodeBinaryInnerShorter.invoke(null, x, bitOutputStream, lo, hi);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // writeCodeBinary may throw, e.g., AssertionError, and we want to
            // throw it back.
            throw e.getTargetException();
        }
        return buffer.toString();
    }

    /**
     * Test of writeCodeBinaryInnerShorter method, of class BinaryEncoding.
     */
    @Test
    public void testWriteCodeBinaryInnerShorter() throws Throwable {
        System.out.println("writeCodeBinaryInnerShorter");
        String expResult, result;
        int x, lo, hi;
        lo = 14;
        hi = 20;
        // Non-powers of 2.
        HashMap<Integer, String> map = new HashMap<>();
        map.put(17, "00");       // Medium numbers
        map.put(14, "01" + "0"); // Small numbers
        map.put(15, "10" + "0");
        map.put(16, "11" + "0");
        map.put(18, "01" + "1"); // Big numbers
        map.put(19, "10" + "1");
        map.put(20, "11" + "1");
        for (int key : map.keySet()) {
            x = key;
            expResult = map.get(x);
            result = codeBinaryInnerShorter(x, lo, hi);
            assertEquals(expResult, result);
        }
        // Out of range (down) number.
        x = 13;
        try {
            result = codeBinaryInnerShorter(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Out of range (up) number.
        x = 21;
        try {
            result = codeBinaryInnerShorter(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        lo = 2;
        hi = 9;
        // Powers of 2.
        map = new HashMap<>();
        map.put(2, "000");
        map.put(3, "001");
        map.put(4, "010");
        map.put(5, "011");
        map.put(6, "100");
        map.put(7, "101");
        map.put(8, "110");
        map.put(9, "111");
        for (int key : map.keySet()) {
            x = key;
            expResult = map.get(x);
            result = codeBinaryInnerShorter(x, lo, hi);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of readCodeBinaryInnerShorter method, of class BinaryEncoding.
     */
    @Test
    public void testReadCodeBinaryInnerShorter() {
        System.out.println("readCodeBinaryInnerShorter");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            BinaryEncoding.writeCodeBinaryInnerShorter(expResult, bitOutputStream, 1, 42);
            bitInputStream = new BitInputStreamArray(buffer);
            result = BinaryEncoding.readCodeBinaryInnerShorter(bitInputStream, 1, 42);
            assertEquals(expResult, result);
        }
    }
    
    
    /*
     * The following code is devised for a *private static* method. Now that it
     * is public, it is no more relevant.
     */
    private static String codeBinaryLefterShorter(int x, int lo, int hi) throws Throwable {
        BitSequence buffer = new BitSequence();
        try {
            // Search for the method writeCodeBinaryPIBIRI inside class MethodBinary.
            java.lang.reflect.Method privateMethodWriteCodeBinaryLefterShorter = BinaryEncoding.class.getDeclaredMethod(
                    "writeCodeBinaryLefterShorter", int.class, BitOutputStream.class, int.class, int.class);
            // writeCodeBinary is private, so we grant access to it.
            privateMethodWriteCodeBinaryLefterShorter.setAccessible(true);
            // finally, invoke the method. It is static, so we don't need to create a new MethodBinary.
            BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(buffer);
            privateMethodWriteCodeBinaryLefterShorter.invoke(null, x, bitOutputStream, lo, hi);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // writeCodeBinary may throw, e.g., AssertionError, and we want to
            // throw it back.
            throw e.getTargetException();
        }
        return buffer.toString();
    }

    /**
     * Test of writeCodeBinaryLefterShorter method, of class BinaryEncoding.
     */
    @Test
    public void testWriteCodeBinaryLefterShorter() throws Throwable {
        System.out.println("writeCodeBinaryLefterShorter");
        String expResult, result;
        int x, lo, hi;
        lo = 14;
        hi = 20;
        // Non-powers of 2.
        HashMap<Integer, String> map = new HashMap<>();
        map.put(14, "00");       // Small numbers
        map.put(15, "01" + "0"); // Big numbers
        map.put(16, "01" + "1");
        map.put(17, "10" + "0");
        map.put(18, "10" + "1");
        map.put(19, "11" + "0");
        map.put(20, "11" + "1");
        for (int key : map.keySet()) {
            x = key;
            expResult = map.get(x);
            result = codeBinaryLefterShorter(x, lo, hi);
            assertEquals(expResult, result);
        }
        // Out of range (down) number.
        x = 13;
        try {
            result = codeBinaryLefterShorter(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Out of range (up) number.
        x = 21;
        try {
            result = codeBinaryLefterShorter(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        lo = 2;
        hi = 9;
        // Powers of 2.
        map = new HashMap<>();
        map.put(2, "000");
        map.put(3, "001");
        map.put(4, "010");
        map.put(5, "011");
        map.put(6, "100");
        map.put(7, "101");
        map.put(8, "110");
        map.put(9, "111");
        for (int key : map.keySet()) {
            x = key;
            expResult = map.get(x);
            result = codeBinaryLefterShorter(x, lo, hi);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of readCodeBinaryLefterShorter method, of class BinaryEncoding.
     */
    @Test
    public void testReadCodeBinaryLefterShorter() {
        System.out.println("readCodeBinaryLefterShorter");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            BinaryEncoding.writeCodeBinaryLefterShorter(expResult, bitOutputStream, 1, 42);
            bitInputStream = new BitInputStreamArray(buffer);
            result = BinaryEncoding.readCodeBinaryLefterShorter(bitInputStream, 1, 42);
            assertEquals(expResult, result);
        }
    }
    
    private static String codeBinaryOuterShorter(int x, int lo, int hi) throws Throwable {
        BitSequence buffer = new BitSequence();
        try {
            // Search for the method writeCodeBinaryOuterShorter inside class MethodBinary.
            java.lang.reflect.Method privateMethodWriteCodeBinaryOuterShorter = BinaryEncoding.class.getDeclaredMethod(
                    "writeCodeBinaryOuterShorter", int.class, BitOutputStream.class, int.class, int.class);
            // writeCodeBinary is private, so we grant access to it.
            privateMethodWriteCodeBinaryOuterShorter.setAccessible(true);
            // finally, invoke the method. It is static, so we don't need to create a new MethodBinary.
            BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(buffer);
            privateMethodWriteCodeBinaryOuterShorter.invoke(null, x, bitOutputStream, lo, hi);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // writeCodeBinary may throw, e.g., AssertionError, and we want to
            // throw it back.
            throw e.getTargetException();
        }
        return buffer.toString();
    }

    /**
     * Test of (private static) writeCodeBinaryOuterShorter method, of class BinaryEncoding.
     */
    @Test
    public void testWriteCodeBinaryOuterShorter() throws Throwable {
        System.out.println("writeCodeBinaryOuterShorter");
        String expResult, result;
        int x, lo, hi;
        lo = 14;
        hi = 20;
        // Non-powers of 2.
        HashMap<Integer, String> map = new HashMap<>();
                            // Small numbers
        map.put(14, "010"); // Medium numbers
        map.put(15, "011");
        map.put(16, "100");
        map.put(17, "101");
        map.put(18, "110");
        map.put(19, "111");
        map.put(20, "00");  // Big numbers
        for (int key : map.keySet()) {
            x = key;
            expResult = map.get(x);
            result = codeBinaryOuterShorter(x, lo, hi);
            assertEquals(expResult, result);
        }
        // Out of range (down) number.
        x = 13;
        try {
            result = codeBinaryOuterShorter(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        // Out of range (up) number.
        x = 21;
        try {
            result = codeBinaryOuterShorter(x, lo, hi);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
        lo = 2;
        hi = 9;
        // Powers of 2.
        map = new HashMap<>();
        map.put(2, "000");
        map.put(3, "001");
        map.put(4, "010");
        map.put(5, "011");
        map.put(6, "100");
        map.put(7, "101");
        map.put(8, "110");
        map.put(9, "111");
        for (int key : map.keySet()) {
            x = key;
            expResult = map.get(x);
            result = codeBinaryOuterShorter(x, lo, hi);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of readCodeBinaryOuterShorter method, of class BinaryEncoding.
     */
    @Test
    public void testReadCodeBinaryOuterShorter() {
        System.out.println("readCodeBinaryOuterShorter");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            BinaryEncoding.writeCodeBinaryOuterShorter(expResult, bitOutputStream, 1, 42);
            bitInputStream = new BitInputStreamArray(buffer);
            result = BinaryEncoding.readCodeBinaryOuterShorter(bitInputStream, 1, 42);
            assertEquals(expResult, result);
        }
    }
    
}
