/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerencoding;

import io.BitInputStreamArray;
import io.BitOutputStreamArray;
import bitlistencoding.BitSequence;

import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class ZetaEncodingTest {
    
    public ZetaEncodingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of writeCodeZeta method, of class ZetaEncoding.
     */
    @Test
    public void testWriteCodeZeta() {
        System.out.println("writeCodeZeta");
        int k = 1;
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "100");
        map.put( 3, "101");
        map.put( 4, "11000");
        map.put( 5, "11001");
        map.put( 6, "11010");
        map.put( 7, "11011");
        map.put( 8, "1110000");
        map.put( 9, "1110001");
        map.put(10, "1110010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            ZetaEncoding.writeCodeZeta(x, bitOutputStream, k);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            ZetaEncoding.writeCodeZeta(x, bitOutputStream, k);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCodeZeta method, of class ZetaEncoding.
     */
    @Test
    public void testReadCodeZeta() {
        System.out.println("readCodeZeta");
        for (int k = 1; k < 6; k++) {
            int result, expResult;
            BitSequence buffer;
            BitInputStreamArray bitInputStream;
            BitOutputStreamArray bitOutputStream;
            // Writing and reading 1..42
            for (expResult = 1; expResult < 43; expResult++) {
                buffer = new BitSequence();
                bitOutputStream = new BitOutputStreamArray(buffer);
                ZetaEncoding.writeCodeZeta(expResult, bitOutputStream, k);
                bitInputStream = new BitInputStreamArray(buffer);
                result = ZetaEncoding.readCodeZeta(bitInputStream, k);
                assertEquals(expResult, result);
            }
        }
    }
    
}
