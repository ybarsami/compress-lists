/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerencoding;

import io.BitInputStreamArray;
import io.BitOutputStreamArray;
import bitlistencoding.BitSequence;

import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class DeltaEncodingTest {
    
    public DeltaEncodingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of writeCodeDelta method, of class DeltaEncoding.
     */
    @Test
    public void testWriteCodeDelta() {
        System.out.println("writeCodeDelta");
        BitSequence buffer;
        BitOutputStreamArray bitOutputStream;
        String result, expResult;
        HashMap<Integer, String> map = new HashMap<>();
        map.put( 1, "0");
        map.put( 2, "1000");
        map.put( 3, "1001");
        map.put( 4, "10100");
        map.put( 5, "10101");
        map.put( 6, "10110");
        map.put( 7, "10111");
        map.put( 8, "11000000");
        map.put( 9, "11000001");
        map.put(10, "11000010");
        // Writing 1..10
        for (int x : map.keySet()) {
            expResult = map.get(x);
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            DeltaEncoding.writeCodeDelta(x, bitOutputStream);
            result = buffer.toString();
            assertEquals(expResult, result);
        }
        int x;
        // Trying to write 0
        x = 0;
        buffer = new BitSequence();
        bitOutputStream = new BitOutputStreamArray(buffer);
        try {
            DeltaEncoding.writeCodeDelta(x, bitOutputStream);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }

    /**
     * Test of readCodeDelta method, of class DeltaEncoding.
     */
    @Test
    public void testReadCodeDelta() {
        System.out.println("readCodeDelta");
        int result, expResult;
        BitSequence buffer;
        BitInputStreamArray bitInputStream;
        BitOutputStreamArray bitOutputStream;
        // Writing and reading 1..42
        for (expResult = 1; expResult < 43; expResult++) {
            buffer = new BitSequence();
            bitOutputStream = new BitOutputStreamArray(buffer);
            DeltaEncoding.writeCodeDelta(expResult, bitOutputStream);
            bitInputStream = new BitInputStreamArray(buffer);
            result = DeltaEncoding.readCodeDelta(bitInputStream);
            assertEquals(expResult, result);
        }
    }
    
}
