/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitlistencoding;

import static compresslists.Tools.nbBitsPerByte;

import java.util.Collections;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class BitSequenceTest {
    
    public BitSequenceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class BitSequence.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        BitSequence instance;
        int expResult, result;
        // Empty bit sequence
        instance = new BitSequence();
        expResult = 0;
        result = instance.size();
        assertEquals(expResult, result);
        // Any bit sequence
        instance = new BitSequence();
        expResult = 42;
        for (int i = 0; i < expResult; i++) {
            instance.add(1);
        }
        result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of get method, of class BitSequence.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        BitSequence instance;
        int expResult, result;
        // Empty sequence
        instance = new BitSequence();
        try {
            instance.get(0);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
        // Value "11111111"
        instance = new BitSequence();
        instance.add(1, nbBitsPerByte);
        for (int i = 0; i < nbBitsPerByte; i++) {
            expResult = 1;
            result = instance.get(0);
            assertEquals(expResult, result);
        }
        // Value "10101010010101"
        instance = new BitSequence();
        for (int i = 0; i < 4; i++) {
            instance.add(1);
            instance.add(0);
        }
        for (int i = 0; i < 3; i++) {
            instance.add(0);
            instance.add(1);
        }
        for (int i = 0; i < 4; i++) {
            expResult = 1;
            result = instance.get(2 * i);
            assertEquals(expResult, result);
            expResult = 0;
            result = instance.get(2 * i + 1);
            assertEquals(expResult, result);
        }
        for (int i = 0; i < 3; i++) {
            expResult = 0;
            result = instance.get(8 + 2 * i);
            assertEquals(expResult, result);
            expResult = 1;
            result = instance.get(8 + 2 * i + 1);
            assertEquals(expResult, result);
        }
        try {
            instance.get(-1);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
        try {
            instance.get(14);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
    }

    /**
     * Test of toByteArray method, of class BitSequence.
     */
    @Test
    public void testToByteArray() {
        System.out.println("toByteArray");
        BitSequence instance;
        byte[] expResult, result;
        // Empty sequence
        instance = new BitSequence();
        expResult = new byte[0];
        result = instance.toByteArray();
        assertArrayEquals(expResult, result);
        // Value more than 128
        instance = new BitSequence();
        instance.add(1, nbBitsPerByte);
        expResult = new byte[1];
        expResult[0] = (byte)255;
        result = instance.toByteArray();
        assertArrayEquals(expResult, result);
        // Big endian
        instance = new BitSequence();
        instance.add(1, 2);
        expResult = new byte[1];
        expResult[0] = (byte)(128 + 64);
        result = instance.toByteArray();
        assertArrayEquals(expResult, result);
        // General case
        instance = new BitSequence();
        for (int i = 0; i < 4; i++) {
            instance.add(1);
            instance.add(0);
        }
        for (int i = 0; i < 3; i++) {
            instance.add(0);
            instance.add(1);
        }
        expResult = new byte[2];
        expResult[0] = (byte)(128 + 32 + 8 + 2);
        expResult[1] = (byte)(64 + 16 + 4);
        result = instance.toByteArray();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of add method, of class BitSequence.
     */
    @Test
    public void testAdd_int() {
        System.out.println("add(int)");
        int value;
        BitSequence instance;
        String before, after;
        // Add 1
        value = 1;
        instance = new BitSequence();
        for (int i = 0; i < 3; i++) {
            instance.add(0);
            instance.add(1);
        }
        before = instance.toString();
        instance.add(value);
        after = instance.toString();
        assertEquals(before + "1", after);
        // Add 0
        value = 0;
        instance = new BitSequence();
        for (int i = 0; i < 3; i++) {
            instance.add(1);
            instance.add(1);
            instance.add(0);
        }
        before = instance.toString();
        instance.add(value);
        after = instance.toString();
        assertEquals(before + "0", after);
        // Maximum number of add
        value = 0;
        instance = new BitSequence();
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            instance.add(value);
        }
        // Too much add
        try {
            instance.add(value);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
    }

    /**
     * Test of add method, of class BitSequence.
     */
    @Test
    public void testAdd_int_int() {
        System.out.println("add(int, int)");
        int nbPositions;
        int value;
        BitSequence instance;
        String before, after;
        // Add 1
        value = 1;
        nbPositions = 2;
        instance = new BitSequence();
        for (int i = 0; i < 3; i++) {
            instance.add(0);
            instance.add(1);
        }
        before = instance.toString();
        instance.add(value, nbPositions);
        after = instance.toString();
        assertEquals(before + String.join("", Collections.nCopies(nbPositions, "1")), after);
        // Add 0
        value = 0;
        nbPositions = 5;
        instance = new BitSequence();
        for (int i = 0; i < 3; i++) {
            instance.add(1);
            instance.add(1);
            instance.add(0);
        }
        before = instance.toString();
        instance.add(value, nbPositions);
        after = instance.toString();
        assertEquals(before + String.join("", Collections.nCopies(nbPositions, "0")), after);
        // Maximum number of add
        value = 0;
        nbPositions = Integer.MAX_VALUE;
        instance = new BitSequence();
        instance.add(value, nbPositions);
        // Too much add
        try {
            instance.add(value);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
    }

    /**
     * Test of toString method, of class BitSequence.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        BitSequence instance;
        String expResult, result;
        // Empty sequence
        expResult = "";
        instance = new BitSequence("");
        result = instance.toString();
        assertEquals(expResult, result);
        // Value more than 128
        expResult = "11111111";
        instance = new BitSequence("11111111");
        result = instance.toString();
        assertEquals(expResult, result);
        // Big endian
        expResult = "11";
        instance = new BitSequence("11");
        result = instance.toString();
        assertEquals(expResult, result);
        // General case
        expResult = "10101010010101";
        instance = new BitSequence("10101010010101");
        result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
