/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compresslists;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import tritlistencoding.TritSequence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class ToolsTest {
    
    public ToolsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of tritSequence2intArray method, of class Tools.
     */
    @Test
    public void testTritSequence2intArray() {
        System.out.println("tritSequence2intArray");
        TritSequence tritSequence = new TritSequence();
        tritSequence.add(1);
        tritSequence.add(2);
        tritSequence.add(0);
        tritSequence.add(1);
        tritSequence.add(2);
        tritSequence.add(2);
        tritSequence.add(0);
        tritSequence.add(2);
        tritSequence.add(2);
        tritSequence.add(2);
        tritSequence.add(0);
        tritSequence.add(0);
        tritSequence.add(2);
        int[] expResult = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        IntsRef resultRef = new IntsRef(0);
        Tools.tritSequence2intArray(tritSequence, resultRef);
        assertArrayEquals(expResult, resultRef.ints);
    }
    
    /**
     * Test of intArray2tritSequence method, of class Tools.
     */
    @Test
    public void testIntArray2tritSequence() {
        System.out.println("intArray2tritSequence");
        int[] intArray = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        String expResult, result;
        expResult = "1201220222002";
        TritSequence tritSequence = new TritSequence();
        Tools.intArray2tritSequence(new IntsRef(intArray), tritSequence);
        result = tritSequence.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Full test to test that (tritSequence2intArray o intArray2tritSequence) is identity.
     */
    @Test
    public void testBijection() {
        System.out.println("bijection");
        int[] documentArray = new int[] { 84, 85, 510, 941, 946, 965, 978, 1008, 1009, 1774, 1862, 2248, 2254, 2755, 2756, 3494, 3495, 3716, 4428, 4462, 4676, 5218, 5219, 5430, 5455, 5470, 6007, 6229, 6408, 6467, 6500, 6601, 6654, 6850, 7757, 8261, 8262, 8263, 8264, 8265, 8324, 8359, 8423, 8438, 8808, 9413, 9739, 9885, 10512, 10766, 10842, 10962, 11124, 11140, 11141, 11188, 11222, 11780, 12146, 12148, 12415, 12455, 12456, 12644, 12736, 13643, 14131, 14153, 14172, 14239, 14240, 14250, 14254, 14262, 14596, 14860, 15032, 15033, 15042, 15043, 15428 };
        IntsRef gapArrayInputRef = new IntsRef(0);
        Tools.documentArray2gapArray(new IntsRef(documentArray), gapArrayInputRef);
        TritSequence tritSequence = new TritSequence();
        Tools.intArray2tritSequence(gapArrayInputRef, tritSequence);
        IntsRef gapArrayOutputRef = new IntsRef(0);
        Tools.tritSequence2intArray(tritSequence, gapArrayOutputRef);
        assertArrayEquals(gapArrayInputRef.ints, gapArrayOutputRef.ints);
    }
    
    /**
     * Test of bitVector2documentArray method, of class Tools.
     */
    @Test
    public void testBitVector2documentArray() {
        System.out.println("bitVector2documentArray");
        BitSequence bitSequence = new BitSequence("00100001101110001");
        int[] expResult = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        IntsRef resultRef = new IntsRef(0);
        Tools.bitVector2documentArray(bitSequence, resultRef);
        assertArrayEquals(expResult, resultRef.ints);
    }
    
    /**
     * Test of bitVector2gapArray method, of class Tools.
     */
    @Test
    public void testBitVector2gapArray() {
        System.out.println("bitVector2gapArray");
        BitSequence bitSequence = new BitSequence("00100001101110001");
        int[] expResult = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        IntsRef resultRef = new IntsRef(0);
        Tools.bitVector2gapArray(bitSequence, resultRef);
        assertArrayEquals(expResult, resultRef.ints);
    }
    
    /**
     * Test of documentArray2bitVector method, of class Tools.
     */
    @Test
    public void testDocumentArray2bitVector() {
        System.out.println("documentArray2bitVector");
        int[] documentArray;
        String expResult, result;
        // Regular documentArray.
        documentArray = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        expResult = "00100001101110001";
        BitSequence bitVector = new BitSequence();
        Tools.documentArray2bitVector(new IntsRef(documentArray), bitVector);
        result = bitVector.toString();
        assertEquals(expResult, result);
        // Trying to convert unsorted documentArray.
        documentArray = new int[] { 3, 8, 9, 12, 11, 13, 17 };
        try {
            Tools.documentArray2bitVector(new IntsRef(documentArray), bitVector);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }
    
    /**
     * Test of documentArray2gapArray method, of class Tools.
     */
    @Test
    public void testDocumentArray2gapArray() {
        System.out.println("documentArray2gapArray");
        int[] documentArray, expResult;
        IntsRef resultRef = new IntsRef(0);
        // Regular documentArray.
        documentArray = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        int[] checkDocumentArray = documentArray.clone();
        expResult = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        resultRef.ints = new int[0];
        Tools.documentArray2gapArray(new IntsRef(documentArray), resultRef);
        assertArrayEquals(checkDocumentArray, documentArray);
        assertArrayEquals(expResult, resultRef.ints);
        // In-place conversion.
        documentArray = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        expResult = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        IntsRef documentArrayRef = new IntsRef(documentArray);
        Tools.documentArray2gapArray(documentArrayRef, documentArrayRef);
        assertArrayEquals(expResult, documentArray);
        // Trying to convert unsorted documentArray.
        documentArray = new int[] { 3, 8, 9, 12, 11, 13, 17 };
        resultRef.ints = new int[0];
        try {
            Tools.documentArray2gapArray(new IntsRef(documentArray), resultRef);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }
    
    /**
     * Test of gapArray2documentArray method, of class Tools.
     */
    @Test
    public void testGapArray2documentArray() {
        System.out.println("gapArray2documentArray");
        int[] gapArray, expResult;
        IntsRef resultRef = new IntsRef(0);
        // Regular gapArray.
        gapArray = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        int[] checkGapArray = gapArray.clone();
        expResult = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        resultRef.ints = new int[0];
        Tools.gapArray2documentArray(new IntsRef(gapArray), resultRef);
        assertArrayEquals(checkGapArray, gapArray);
        assertArrayEquals(expResult, resultRef.ints);
        // In-place conversion.
        gapArray = new int[] { 3, 5, 1, 2, 1, 1, 4 };
        expResult = new int[] { 3, 8, 9, 11, 12, 13, 17 };
        IntsRef gapArrayRef = new IntsRef(gapArray);
        Tools.gapArray2documentArray(gapArrayRef, gapArrayRef);
        assertArrayEquals(expResult, gapArray);
        // Trying to convert gapArray with 0.
        gapArray = new int[] { 3, 5, 1, 2, 1, 0, 4 };
        resultRef.ints = new int[0];
        try {
            Tools.gapArray2documentArray(new IntsRef(gapArray), resultRef);
            fail("This should not be executed.");
        } catch(AssertionError e) {}
    }
    
    /**
     * Test of sortedIntersectIds method, of class Tools.
     */
    @Test
    public void testSortedIntersectIds() {
        System.out.println("sortedIntersectIds");
        int[] documentArray, referenceList, expResult, result;
        // Reach end of referenceList first
        documentArray = new int[] { 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 };
        referenceList = new int[] { 9, 18, 27, 36, 45 };
        expResult = new int[] { 1, 3 };
        result = Tools.sortedIntersectIds(documentArray, referenceList);
        assertArrayEquals(expResult, result);
        // Reach end of documentArray first
        documentArray = new int[] { 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 };
        referenceList = new int[] { 9, 18, 27, 36, 45, 54, 63 };
        expResult = new int[] { 1, 3, 5 };
        result = Tools.sortedIntersectIds(documentArray, referenceList);
        assertArrayEquals(expResult, result);
    }
    
    /**
     * Test of sortedIntersect method, of class Tools.
     */
    @Test
    public void testSortedIntersect() {
        System.out.println("sortedIntersect");
        int[] documentArray1, documentArray2, expResult, result;
        // Reach end of referenceList first
        documentArray1 = new int[] { 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 };
        documentArray2 = new int[] { 9, 18, 27, 36, 45 };
        expResult = new int[] { 18, 36 };
        result = Tools.sortedIntersect(documentArray1, documentArray2);
        assertArrayEquals(expResult, result);
        // Reach end of documentArray first
        documentArray1 = new int[] { 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 };
        documentArray2 = new int[] { 9, 18, 27, 36, 45, 54, 63 };
        expResult = new int[] { 18, 36, 54 };
        result = Tools.sortedIntersect(documentArray1, documentArray2);
        assertArrayEquals(expResult, result);
    }
    
    /**
     * Test of sortedIntersectNot method, of class Tools.
     */
    @Test
    public void testSortedIntersectNot() {
        System.out.println("sortedIntersectNot");
        int[] documentArray1, documentArray2, expResult, result;
        // Reach end of referenceList first
        documentArray1 = new int[] { 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 };
        documentArray2 = new int[] { 9, 18, 27, 36, 45 };
        expResult = new int[] { 6, 12, 24, 30, 42, 48, 54, 60 };
        result = Tools.sortedIntersectNot(documentArray1, documentArray2);
        assertArrayEquals(expResult, result);
        // Reach end of documentArray first
        documentArray1 = new int[] { 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 };
        documentArray2 = new int[] { 9, 18, 27, 36, 45, 54, 63 };
        expResult = new int[] { 6, 12, 24, 30, 42, 48, 60 };
        result = Tools.sortedIntersectNot(documentArray1, documentArray2);
        assertArrayEquals(expResult, result);
    }
    
}
