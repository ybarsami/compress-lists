/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tritlistencoding;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yann
 */
public class TritSequenceTest {
    
    public TritSequenceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class TritSequence.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        TritSequence instance;
        int expResult, result;
        // Empty bit sequence
        instance = new TritSequence();
        expResult = 0;
        result = instance.size();
        assertEquals(expResult, result);
        // Any bit sequence
        instance = new TritSequence();
        expResult = 42;
        for (int i = 0; i < expResult; i++) {
            instance.add(1);
        }
        result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of get method, of class TritSequence.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        TritSequence instance;
        int expResult, result;
        // Empty sequence
        instance = new TritSequence();
        try {
            instance.get(0);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
        // Value "11111111"
        instance = new TritSequence();
        for (int i = 0; i < 8; i++) {
            instance.add(1);
        }
        for (int i = 0; i < 8; i++) {
            expResult = 1;
            result = instance.get(0);
            assertEquals(expResult, result);
        }
        // Value "210210210210012012012"
        instance = new TritSequence();
        for (int i = 0; i < 4; i++) {
            instance.add(2);
            instance.add(1);
            instance.add(0);
        }
        for (int i = 0; i < 3; i++) {
            instance.add(0);
            instance.add(1);
            instance.add(2);
        }
        for (int i = 0; i < 4; i++) {
            expResult = 2;
            result = instance.get(3 * i);
            assertEquals(expResult, result);
            expResult = 1;
            result = instance.get(3 * i + 1);
            assertEquals(expResult, result);
            expResult = 0;
            result = instance.get(3 * i + 2);
            assertEquals(expResult, result);
        }
        for (int i = 0; i < 3; i++) {
            expResult = 0;
            result = instance.get(12 + 3 * i);
            assertEquals(expResult, result);
            expResult = 1;
            result = instance.get(12 + 3 * i + 1);
            assertEquals(expResult, result);
            expResult = 2;
            result = instance.get(12 + 3 * i + 2);
            assertEquals(expResult, result);
        }
        try {
            instance.get(-1);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
        try {
            instance.get(21);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
        // More than Integer.MAX_VALUE / 2 trits
        instance = new TritSequence();
        for (int i = 0; i < Integer.MAX_VALUE / 2; i++) {
            instance.add(0);
        }
        for (int i = 0; i < 42; i++) {
            instance.add(2);
        }
        expResult = 0;
        result = instance.get(0);
        assertEquals(expResult, result);
        expResult = 0;
        result = instance.get(Integer.MAX_VALUE / 2 - 1);
        assertEquals(expResult, result);
        expResult = 2;
        result = instance.get(Integer.MAX_VALUE / 2);
        assertEquals(expResult, result);
        expResult = 2;
        result = instance.get(Integer.MAX_VALUE / 2 + 41);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class TritSequence.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TritSequence instance;
        String expResult, result;
        // Empty sequence
        expResult = "";
        instance = new TritSequence("");
        result = instance.toString();
        assertEquals(expResult, result);
        // Value more than 128
        expResult = "11111111";
        instance = new TritSequence("11111111");
        result = instance.toString();
        assertEquals(expResult, result);
        // Big endian
        expResult = "11";
        instance = new TritSequence("11");
        result = instance.toString();
        assertEquals(expResult, result);
        // General case
        expResult = "10101010010101";
        instance = new TritSequence("10101010010101");
        result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of numberOfTwos method, of class TritSequence.
     */
    @Test
    public void testNumberOfTwos_0args() {
        System.out.println("numberOfTwos()");
        TritSequence instance = new TritSequence();
        int expResult = 0;
        int result = instance.numberOfTwos();
        assertEquals(expResult, result);
    }

    /**
     * Test of numberOfTwos method, of class TritSequence.
     */
    @Test
    public void testNumberOfTwos_int_int() {
        System.out.println("numberOfTwos(int,int)");
        int beginIndex = 0;
        int endIndex = 1;
        TritSequence instance = new TritSequence("1");
        int expResult = 0;
        int result = instance.numberOfTwos(beginIndex, endIndex);
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class TritSequence.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        int value;
        TritSequence instance;
        String before, after;
        // Add 1
        value = 1;
        instance = new TritSequence();
        for (int i = 0; i < 3; i++) {
            instance.add(0);
            instance.add(1);
        }
        before = instance.toString();
        instance.add(value);
        after = instance.toString();
        assertEquals(before + "1", after);
        // Add 0
        value = 0;
        instance = new TritSequence();
        for (int i = 0; i < 3; i++) {
            instance.add(1);
            instance.add(1);
            instance.add(0);
        }
        before = instance.toString();
        instance.add(value);
        after = instance.toString();
        assertEquals(before + "0", after);
        // Maximum number of add
        value = 0;
        instance = new TritSequence();
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            instance.add(value);
        }
        // Too much add
        try {
            instance.add(value);
            fail("This should not be executed.");
        } catch(IndexOutOfBoundsException e) {}
    }
    
}
