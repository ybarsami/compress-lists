/*
 * Copyright (c) 2014, Sindice Limited. All Rights Reserved.
 *
 * This file is part of the SIREn project.
 *
 * SIREn is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * SIREn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package afor;

import arrays.BytesRef;
import arrays.IntsRef;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Random;

/**
 *
 * @author yann
 */
public class AForBlockCompressorTest {
    
    Random random = new Random();
    
    public AForBlockCompressorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * The different block sizes to test
     */
    protected static final int[] BLOCK_SIZES = {32, 64, 128, 256, 512, 1024, 2048};
    
    private static final int MIN_LIST_SIZE = 32768;
    
    /**
     * Generate a random int value uniformly distributed between
     * <code>lower</code> and <code>upper</code>, inclusive.
     *
     * @param lower
     *            the lower bound.
     * @param upper
     *            the upper bound.
     * @return the random integer.
     * @throws IllegalArgumentException if {@code lower >= upper}.
     */
    protected int nextInt(final int lower, final int upper) {
        if (lower >= upper) {
            throw new IllegalArgumentException();
        }
        final double r = random.nextDouble();
        return (int) ((r * upper) + ((1.0 - r) * lower) + r);
    }
    
    private void doTest(final int[] values, final int blockSize,
                        final BlockCompressor compressor,
                        final BlockDecompressor decompressor) throws Exception {
        final BytesRef compressedData = new BytesRef(compressor.maxCompressedSize(blockSize));
        final IntsRef input = new IntsRef(blockSize);
        final IntsRef output = new IntsRef(blockSize);
        
        for (int i = 0; i < values.length; i += blockSize) {
            
            int offset = 0;
            
            // copy first block into the uncompressed data buffer
            for (int j = i; offset < blockSize && j < values.length; j++, offset++) {
                input.ints[offset] = values[j];
            }
            input.offset = 0;
            input.length = offset;
            
            // compress
            compressor.compress(input, compressedData);
            
            // decompress
            decompressor.decompress(compressedData, output);
            
            // check if they are equals
            for (int j = 0; j < input.length; j++) {
                assertEquals(input.ints[j], output.ints[j]);
            }
        }
    }
    
    public void doTestIntegerRange(final int minBits, final int maxBits, final int[] blockSizes,
                                   final BlockCompressor compressor,
                                   final BlockDecompressor decompressor) throws Exception {
        for (int i = minBits; i <= maxBits; i++) {
            // different length for each run
            final int length = nextInt(MIN_LIST_SIZE, MIN_LIST_SIZE * 2);
            final int[] input = new int[length];
            
            final int min = i == 1 ? 0 : (1 << (i - 1));
            final int max = (int)((1L << i) - 1);
            
            // generate integers per frame of max 8 ints
            for (int j = 0; j < length; j += 8) {
                final int size = j + 8 < length ? 8 : length - j;
                // generate randomly a sequence of 8 zero integers, in order to force
                // frame-based compression to use instructions for different frame sizes
                final boolean bool = random.nextBoolean();
                int[] ints;
                if (bool) {
                    ints = getRandomInteger(size, min, max);
                } else {
                    ints = new int[size]; // initialised by default with 0
                }
                System.arraycopy(ints, 0, input, j, ints.length);
            }

            for (final int blockSize : blockSizes) {
                doTest(input, blockSize, compressor, decompressor);
            }
        }
    }
    
    private int[] getRandomInteger(final int size, final int min, final int max) {
        final int[] ints = new int[size];
        for (int i = 0; i < size; i++) {
            ints[i] = nextInt(min, max);
        }
        return ints;
    }
    
    public void doTestIntegerRange(final int minBits, final int maxBits,
                                   final BlockCompressor compressor,
                                   final BlockDecompressor decompressor) throws Exception {
        doTestIntegerRange(minBits, maxBits, BLOCK_SIZES, compressor, decompressor);
    }
    
    @Test
    public void testIntegerRange() throws Exception {
        System.out.println("integerRange");
        doTestIntegerRange(1, 32, new AForBlockCompressor(), new AForBlockDecompressor());
    }
    
    @Test
    public void testIncompleteFrame() {
        System.out.println("incompleteFrame");
        final BlockCompressor compressor = new AForBlockCompressor();
        
        final IntsRef input = new IntsRef(64);
        final BytesRef output = new BytesRef(compressor.maxCompressedSize(64));
        
        // fill first part with 1
        for (int i = 0; i < 33; i++) {
            input.ints[i] = 1;
        }
        
        // fill the rest with random numbers
        for (int i = 33; i < 64; i++) {
            input.ints[i] = nextInt(64, Short.MAX_VALUE);
        }
        
        input.offset = 0;
        input.length = 33;
        
        // the random numbers after the end of the input array should not impact
        // compression
        compressor.compress(input, output);
        
        // should be frame code 1 : 32 ints encoded with 1 bits
        assertEquals(1, output.bytes[0]);
        // followed by 4 bytes at 255
        assertEquals(0xFF, output.bytes[1] & 0xFF);
        assertEquals(0xFF, output.bytes[2] & 0xFF);
        assertEquals(0xFF, output.bytes[3] & 0xFF);
        assertEquals(0xFF, output.bytes[4] & 0xFF);
        // then frame code 34 : 16 ints encoded with 1 bits
        assertEquals(34, output.bytes[5]);
        // followed by 1 byte with at least 128 and a second byte with 0
        assertEquals(128, output.bytes[6] & 0x80);
        assertEquals(0, output.bytes[7] & 0xFF);
        // followed by frame code 33: 16 ints encoded with 0 bits
        assertEquals(33, output.bytes[8]);
    }
    
}
