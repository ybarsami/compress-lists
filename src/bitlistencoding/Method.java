/*
 * Remark: bitlistencoding.Method is the BitSequence equivalent of
 * gaparraylistencoding.MethodByBitSequence.
 * tritlistencoding.Method is also doing the same stuff.
 *
 * N.B.: It is possible to handle the similarities with templates.
 */
package bitlistencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFile;
import io.DatasetInfo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public abstract class Method {
    
    /*
     * Some methods need to be initialized according to the bit sequence list it
     * handles.
     * REMARK: It could almost be final (it is only overriden in MethodBookstein
     * for testing purposes).
     */
    public void init(ArrayList<BitSequence> bitSequenceList) {
        initBefore();
        for (BitSequence bitSequence : bitSequenceList) {
            init(bitSequence);
        }
        initAfter();
    }
    public void initBefore() {}
    public void init(BitSequence bitSequence) {}
    public void initAfter() {}
    
    public abstract String getName();
    
    /*
     * Compute the size, in bits, that will be taken to code this bit sequence.
     */
    public void computeSizeBefore() {}
    public abstract double computeSize(BitSequence bitSequence);
    public double computeSizeAfter() { return 0.; }
    
    /*
     * Compute the size, in bits, that will be taken to code this bit sequence
     * list.
     */
    public double computeSize(ArrayList<BitSequence> bitSequenceList) {
        double size = computeSizeNeededInformation();
        for (BitSequence bitSequence : bitSequenceList) {
            size += computeSize(bitSequence);
        }
        return size;
    }
    
    /*
     * Log additional information.
     */
    public void standardOutput() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of additional material needed by the method.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public double computeSizeNeededInformation() {
        return 0.0;
    }
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void exportNeededInformation(DataOutputStream out) throws IOException {}
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void importNeededInformation(DataInputStream in) throws IOException {}
    
    /*
     * Some methods keep some bits to be written at the end.
     */
    public void outputRemainingBits(BitOutputStream bitStream) {}
    
    /*
     * All the MethodByBitSequence are obliged to read byte by byte. When
     * considering padding, the last bits of the last byte read can be
     * discarded. But when we don't use padding, we have to keep them.
     */
    public void flushRemainingBits() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the file from the bit sequences
    // Some method add a padding so that each bit sequence starts at the
    // beginning of a byte in the file --- thus no bit sequence can be encoded
    // by less than a byte.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Exports all the bit sequences.
     * The exported file will be named
     * filenameBegin + "_" + getName() + extension
     */
    public final void exportToFile(String filenameBegin, String extension, ArrayList<BitSequence> bitSequenceList, int nbDocuments) {
        exportToFile(filenameBegin + "_" + getName() + extension, bitSequenceList, nbDocuments);
    }
    
    public abstract void writeBitSequence(BitOutputStream bitStream, BitSequence bitSequence);
    
    /*
     * Writes the bit sequence list in a dataOutputStream, by first encoding
     * them with the given compression method.
     */
    public final void writeBitSequenceList(DataOutputStream dataOutputStream, ArrayList<BitSequence> bitSequenceList) throws IOException {
        try (BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(dataOutputStream)) {
            for (BitSequence bitSequence : bitSequenceList) {
                writeBitSequence(bitOutputStream, bitSequence);
            }
            outputRemainingBits(bitOutputStream);
        }
    }
    
    /*
     * Exports all the bit sequences.
     * The exported file will be named filename.
     * Some schemes split the index in two files, and in that case it is
     * possible to store only in one of those files the sizes. Hence, to gain
     * memory, we allow not to store the sizes for the other file.
     */
    public final void exportToFile(String filename, ArrayList<BitSequence> bitSequenceList, int nbDocuments, boolean exportSizes) {
        try (FileOutputStream fout = new FileOutputStream(filename);
                DataOutputStream out = new DataOutputStream(fout)) {
            if (exportSizes) {
                out.writeInt(bitSequenceList.size());
                out.writeInt(nbDocuments);
            }
            exportNeededInformation(out);
            if (exportSizes) {
                for (BitSequence bitSequence : bitSequenceList) {
                    out.writeInt(bitSequence.numberOfOnes());
                }
            }
            writeBitSequenceList(out, bitSequenceList);
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    public final void exportToFile(String filename, ArrayList<BitSequence> bitSequenceList, int nbDocuments) {
        exportToFile(filename, bitSequenceList, nbDocuments, true);
    }
    
    public abstract void readBitSequence(BitInputStream bitStream, int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence);
    
    public final void readBitSequence(BitInputStream bitStream, int nbOnesToRead, BitSequence bitSequence) {
        readBitSequence(bitStream, nbOnesToRead, true, bitSequence);
    }
    
    /*
     * Reads the gap array list from a dataInputStream, by first decoding them
     * with the given compression method.
     */
    public final ArrayList<BitSequence> readBitSequenceList(DataInputStream dataInputStream, ArrayList<Integer> nbOnesToReadList) {
        BitInputStreamFile bitInputStream = new BitInputStreamFile(dataInputStream);
        ArrayList<BitSequence> bitSequenceList = new ArrayList<>();
        int nbBitSequences = nbOnesToReadList.size();
        for (int i = 0; i < nbBitSequences; i++) {
            BitSequence bitSequence = new BitSequence();
            readBitSequence(bitInputStream, nbOnesToReadList.get(i), bitSequence);
            bitSequenceList.add(bitSequence);
        }
        flushRemainingBits();
        return bitSequenceList;
    }
    
    /*
     * Imports all the bit sequences from a file that contains the total number
     * of documents and the number of ones of each bit sequence.
     */
    public final DatasetInfo importFromFile(String filename) {
        ArrayList<int[]> gapArrayList = new ArrayList<>();
        int nbGapArrays = 0;
        int nbDocuments = 0;
        try (FileInputStream fin = new FileInputStream(filename);
                DataInputStream in = new DataInputStream(fin)) {
            nbGapArrays = in.readInt();
            nbDocuments = in.readInt();
            importNeededInformation(in);
            ArrayList<Integer> nbOnesToReadList = new ArrayList<>();
            for (int i = 0; i < nbGapArrays; i++) {
                int bitSequenceSize = in.readInt();
                nbOnesToReadList.add(bitSequenceSize);
            }
            ArrayList<BitSequence> bitSequenceList = readBitSequenceList(in, nbOnesToReadList);
            for (int i = 0; i < nbGapArrays; i++) {
                BitSequence bitSequence = bitSequenceList.get(i);
                IntsRef gapArrayRef = new IntsRef(nbOnesToReadList.get(i));
                Tools.bitVector2gapArray(bitSequence, gapArrayRef);
                gapArrayList.add(gapArrayRef.ints);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        return new DatasetInfo(gapArrayList, nbDocuments);
    }

}
