/*
 * Index compression with the Four-State Markov Model method.
 *
 * Bookstein, Klein, and Raita, "Modeling Word Occurrences for the Compression of Concordances" (1997)
 * (see the transition graphs on Figure 2, p.263)
 */
package bitlistencoding;

/**
 *
 * @author yann
 */
public abstract class BitSequenceMarkovHandler implements BitSequenceAbstractHandler {
    
   
    public static enum Model {
        // C-symmetric 4-state models
        M_4S1("M-4S1", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 3, 2 }, // On X1: read a 0, go on B; read a 1, go on X2.
            { 1, 0 }, // On X2: read a 0, go on X1; read a 1, go on C.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4S2("M-4S2", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 3, 0 }, // On X1: read a 0, go on B; read a 1, go on C.
            { 3, 0 }, // On X2: read a 0, go on B; read a 1, go on C.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4S3("M-4S3", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 2, 0 }, // On X1: read a 0, go on X2; read a 1, go on C.
            { 3, 1 }, // On X2: read a 0, go on B; read a 1, go on X1.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        // Asymmetric 4-state models
        M_4C1("M-4C1", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 2, 0 }, // On X1: read a 0, go on X2; read a 1, go on C.
            { 3, 0 }, // On X2: read a 0, go on B; read a 1, go on C.
            { 3, 0 }  // On B: read a 0, stay on B; read a 1, go on C.
        }),
        M_4C2("M-4C2", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 3, 0 }, // On X1: read a 0, go on B; read a 1, go on C.
            { 1, 0 }, // On X2: read a 0, go on X1; read a 1, go on C.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4C3("M-4C3", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 2, 0 }, // On X1: read a 0, go on X2; read a 1, go on C.
            { 3, 1 }, // On X2: read a 0, go on B; read a 1, go on X1.
            { 3, 1 }  // On B: read a 0, stay on B; read a 1, go on X1.
        }),
        M_4C4("M-4C4", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 2, 0 }, // On X1: read a 0, go on X2; read a 1, go on C.
            { 3, 0 }, // On X2: read a 0, go on B; read a 1, go on C.
            { 3, 1 }  // On B: read a 0, stay on B; read a 1, go on X1.
        }),
        M_4C5("M-4C5", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 2, 0 }, // On X1: read a 0, go on X2; read a 1, go on C.
            { 3, 0 }, // On X2: read a 0, go on B; read a 1, go on C.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4B1("M-4B1", 4, new int[][] {
            { 3, 0 }, // On C: read a 0, go on B; read a 1, stay on C.
            { 3, 0 }, // On X1: read a 0, go on B; read a 1, go on C.
            { 3, 1 }, // On X2: read a 0, go on B; read a 1, go on X1.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4B2("M-4B2", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 3, 2 }, // On X1: read a 0, go on B; read a 1, go on X2.
            { 3, 0 }, // On X2: read a 0, go on B; read a 1, go on C.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4B3("M-4B3", 4, new int[][] {
            { 2, 0 }, // On C: read a 0, go on X2; read a 1, stay on C.
            { 2, 0 }, // On X1: read a 0, go on X2; read a 1, go on C.
            { 3, 1 }, // On X2: read a 0, go on B; read a 1, go on X1.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4B4("M-4B4", 4, new int[][] {
            { 2, 0 }, // On C: read a 0, go on X2; read a 1, stay on C.
            { 3, 0 }, // On X1: read a 0, go on B; read a 1, go on C.
            { 3, 1 }, // On X2: read a 0, go on B; read a 1, go on X1.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        }),
        M_4B5("M-4B5", 4, new int[][] {
            { 1, 0 }, // On C: read a 0, go on X1; read a 1, stay on C.
            { 3, 0 }, // On X1: read a 0, go on B; read a 1, go on C.
            { 3, 1 }, // On X2: read a 0, go on B; read a 1, go on X1.
            { 3, 2 }  // On B: read a 0, stay on B; read a 1, go on X2.
        });
        
        private final String name;
        private final int nbStates;
        private final int[][] transitions;
        
        // Constructor
        Model(String name, int nbStates, int[][] transitions) {
            this.name = name;
            this.nbStates = nbStates;
            this.transitions = transitions;
        }
        Model(String name, int nbStates) {
            this(name, nbStates, new int[1][1]);
        }
        
        public String getName() { return name; }
        public int getNbStates() { return nbStates; }
        public int[][] getTransitions() { return transitions; }
    }
    
    public static final Model getModel(String modelString) {
        Model models[] = Model.values();
        for (Model model : models) {
            if (model.getName().equals(modelString)) {
                return model;
            }
        }
        throw new RuntimeException("Unknown Bookstein model.");
    }
    public static final String[] getModelNames() {
        Model models[] = Model.values();
        String[] names = new String[models.length];
        for (int i = 0; i < models.length; i++) {
            names[i] = models[i].getName();
        }
        return names;
    }
    
    // Parameters
    private final Model model;
    
    // Generic index.
    // stateId is a bijection between { 0, 1, 2, 3 } and { C, X1, X2, B }.
    private int stateId;
    
    // The bit list to handle.
    protected int nbBitsHandled;
    protected BitSequence bitSequence;
    
    public BitSequenceMarkovHandler(BitSequenceMarkovHandler.Model model) {
        this.model = model;
    }
    
    @Override
    public void init() {
        this.nbBitsHandled = 0;
        // Start on B.
        this.stateId = 3;
    }
    
    @Override
    public int getCurrentBit() {
        return bitSequence.get(nbBitsHandled);
    }
    
    @Override
    public boolean isInInitialState() {
        return false;
    }
    
    @Override
    public void updateInitialIndex() {
    }
    
    @Override
    public int getInitialIndex() {
        return 0;
    }
    
    @Override
    public boolean isInGenericState() {
        return isStillActive();
    }
    
    @Override
    public void updateGenericIndex() {
        int currentBit = getCurrentBit();
        stateId = model.getTransitions()[stateId][currentBit];
        nbBitsHandled++;
    }
    
    @Override
    public int getGenericIndex() {
        return stateId;
    }
    
}
