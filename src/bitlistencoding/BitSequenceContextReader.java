/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitlistencoding;

/**
 *
 * @author yann
 */
public class BitSequenceContextReader extends BitSequenceContextHandler implements BitSequenceAbstractReader {
    
    public BitSequenceContextReader(int k, int w, int kInit) {
        super(k, w, kInit);
    }
    
    @Override
    public void init(BitSequence bitSequence) {
        init();
        this.bitSequence = bitSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbBitsHandled < bitSequence.size();
    }
}
