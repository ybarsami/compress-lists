/**
 * Encodes a set of bit sequences (a bit sequence is sometimes called a
 * bitvector when it represents the document array ; however, we here use the
 * bit sequences in a more general way).
 * 
 * For each bit list, here what is done:
 *    0. We note T the size of bit list. The bits of this list are noted
 *       bit_1, bit_2, ... bit_T.
 *    1. We code the first min(k+w, T) bits with an arithmetic contextual method
 *       --- where the context of the i-th bit is the min(i-1, kInit) previous
 *       bits.
 *    2. Then, a contextual arithmetic method encodes the bits
 *       bit_{k+w}, ... bit_T --- where the context is the previous k bits +
 *       the number of "1" in the w bits before them.
 */

package bitlistencoding;

import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStreamFile;
import io.BitOutputStreamFile;
import static compresslists.Tools.nbBits;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class BitSequenceContextualEncoding {
    
    // To handle trit sequences
    private final BitSequenceAbstractReader bitSequenceReader;
    private final BitSequenceAbstractWriter bitSequenceWriter;
    
    // Size of the occurrence arrays.
    private int contextSize;
    private int contextSizeInit;
    private final int nbSymbols = nbBits;
    
    // Occurrences arrays.
    private long[][] occurrences;
    private int[][] occurrencesCumulative;
    private long[][] occurrencesInit;
    private int[][] occurrencesCumulativeInit;
    
    // Parameters.
    private int k;
    private int w;
    private int kInit;
    
    private int nbBitsPerOccurrence;
    private boolean onlyInit;
    
    /**
     * Creates a new instance of BitSequenceContextualEncoding.
     */
    public BitSequenceContextualEncoding(int k, int w, int kInit) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        onlyInit = (k == Integer.MAX_VALUE);
        contextSize = onlyInit ? 1 : (w + 1) * (1 << k);
        contextSizeInit = nbBits * (1 - (1 << (kInit + 1))) / (1 - nbBits);
        bitSequenceReader = new BitSequenceContextReader(k, w, kInit);
        bitSequenceWriter = new BitSequenceContextWriter(k, w, kInit);
    }
    
    public void initBefore() {
        allocateOccurrenceArrays();
    }
    
    public void initWithNewBitSequence(BitSequence bitSequence) {
        collectOccurrences(bitSequence,
                k, w, kInit,
                occurrences, occurrencesInit);
    }
    
    public void initAfter() {
        nbBitsPerOccurrence = OccurrenceTools.getNbBitsMax(occurrences);
        convertToOccurrenceIntervals();
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the bit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Warning: for this to work, we need to pass the same lists when
     * computing the cost than the ones passed to initialize the class.
     */
    public final double[] computeSizes(BitSequence bitSequence) {
        double sizeInitial = 0.;
        double sizeGeneric = 0.;
        
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            int index = bitSequenceReader.getInitialIndex();
            // Update the size
            for (int bit = 0; bit < nbSymbols; bit++) {
                double pz = ((double)occurrencesInit[index][bit]) / ((double)occurrencesCumulativeInit[index][nbSymbols]);
                sizeInitial += -pz * Tools.log2(pz);
            }
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        while (bitSequenceReader.isInGenericState()) {
            int index = bitSequenceReader.getGenericIndex();
            // Update the size
            for (int bit = 0; bit < nbSymbols; bit++) {
                double pz = ((double)occurrences[index][bit]) / ((double)occurrencesCumulative[index][nbSymbols]);
                sizeGeneric += -pz * Tools.log2(pz);
            }
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
        
        return new double[] { sizeInitial, sizeGeneric };
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the bit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    public final void writeBitSequence(BitSequence bitSequence,
            ArithmeticEncoder encoder) {
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getInitialIndex();
            // Encode the bit
            encoder.update(occurrencesCumulativeInit[index], currentBit);
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Encode the bit
            encoder.update(occurrencesCumulative[index], currentBit);
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the bit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    public static final void collectOccurrences(BitSequence bitSequence,
            int k, int w, int kInit,
            long[][] occurrences, long[][] occurrencesInit) {
        BitSequenceContextReader bitSequenceReader = new BitSequenceContextReader(k, w, kInit);
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getInitialIndex();
            // Update the occurrences
            occurrencesInit[index][currentBit]++;
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Update the occurrences
            occurrences[index][currentBit]++;
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    private void allocateOccurrenceArrays() {
	occurrences     = OccurrenceTools.allocateOccurrences(contextSize, nbSymbols);
	occurrencesInit = OccurrenceTools.allocateOccurrences(contextSizeInit, nbSymbols);
        occurrencesCumulative     = OccurrenceTools.allocateOccurrencesCumulative(contextSize, nbSymbols);
        occurrencesCumulativeInit = OccurrenceTools.allocateOccurrencesCumulative(contextSizeInit, nbSymbols);
    }
    
    private void convertToOccurrenceIntervals() {
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        OccurrenceTools.ensureTotalAndAccumulate(occurrencesInit, occurrencesCumulativeInit);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Functions that match those from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private int getNbBitsWritten() {
        return Math.addExact(
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence),
                Math.multiplyExact(contextSize, nbBits * nbBitsPerOccurrence));
    }
    
    public double computeSizeNeededInformation() {
        return 3 * 32 + Math.addExact( // k, w, kInit
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte), // occurrenceMatrix
                Math.multiplyExact(contextSizeInit, 32)); // occurrenceInitTensor
    }
    
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(k);
        out.writeInt(w);
        out.writeInt(kInit);
        if (!onlyInit) {
            try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
                GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
                // export occurrenceMatrix (for the gapSize)
                for (int context = 0; context < contextSize; context++) {
                    for (int bit = 0; bit < nbBits; bit++) {
                        BinaryEncoding.writeCodeBinary((int)occurrences[context][bit], bitStream, nbBitsPerOccurrence);
                    }
                }
            }
        }
        // export occurrenceInitTensor
        for (int context = 0; context < contextSizeInit; context++) {
            for (int bit = 0; bit < nbBits; bit++) {
                out.writeInt((int)occurrencesInit[context][bit]);
            }
        }
    }
    
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            k = in.readInt();
            w = in.readInt();
            kInit = in.readInt();
            if (!onlyInit) {
                nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            }
            allocateOccurrenceArrays();
            if (!onlyInit) {
                // import occurrenceMatrix (for the gapSize)
                for (int context = 0; context < contextSize; context++) {
                    for (int bit = 0; bit < nbBits; bit++) {
                        occurrences[context][bit] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                    }
                }
            }
            // import occurrenceInitTensor
            for (int context = 0; context < contextSizeInit; context++) {
                for (int bit = 0; bit < nbBits; bit++) {
                    occurrencesInit[context][bit] = in.readInt();
                }
            }
            convertToOccurrenceIntervals();
        }
    }
    
    /*
     * First, it uses arithmetic decoding of the initial bits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining bits.
     */
    public final void readBitSequence(int nbBitsToRead, ArithmeticDecoder decoder, boolean onlyCountOnes, BitSequence bitSequence) {
        bitSequenceWriter.init(nbBitsToRead, onlyCountOnes, bitSequence);
        while (bitSequenceWriter.isInInitialState()) {
            int index = bitSequenceWriter.getInitialIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrencesCumulativeInit[index]);
            // Update the index for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateInitialIndex();
        }
        while (bitSequenceWriter.isInGenericState()) {
            int index = bitSequenceWriter.getGenericIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrencesCumulative[index]);
            // Update the indexes for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateGenericIndex();
        }
    }
    
    public final void readBitSequence(int nbOnesToRead, ArithmeticDecoder decoder, BitSequence bitSequence) {
        readBitSequence(nbOnesToRead, decoder, true, bitSequence);
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some testing
    ////////////////////////////////////////////////////////////////////////////
    
    public final void exportProbas(FileWriter out) throws IOException {
        // export occurrenceInitTensor
        for (int context = 0; context < contextSizeInit; context++) {
            for (int bit = 0; bit < nbBits; bit++) {
                out.write(((double)occurrencesInit[context][bit] / (double)occurrencesCumulativeInit[context][nbBits]) + " ");
            }
        }
        // export occurrenceMatrix (for the gapSize)
        for (int context = 0; context < contextSize; context++) {
            for (int bit = 0; bit < nbBits; bit++) {
                out.write(((double)occurrences[context][bit] / (double)occurrencesCumulative[context][nbBits]) + "");
                out.write(context == (contextSize - 1) && bit == (nbBits - 1) ? "" : " ");
            }
        }
        out.write("\n");
    }
    
}
