/*
 * Index compression with the Four-State Markov Model method.
 *
 * Bookstein, Klein, and Raita, "Modeling Word Occurrences for the Compression of Concordances" (1997)
 *
 * Remark: The difference with the original Bookstein et alii method is that
 * we compress with only one global set of parameters (the transition
 * probabilities of the model). The original paper suggests to add 3 bits of
 * information on each term (each integer list):
 *     If we choose the best out of a possible set of 8 probabilities (as in
 *     Bookstein A., Klein S.T., Raita T., "Markov models for clusters in
 *     concordance compression", 1994), then only 3 bits are needed to store the
 *     parameters for each term.
 *
 * In order to choose different parameters (different transition probabilities)
 * for each integer list, we need to store additional data. If we store
 * different sets of probabilities, we could choose to:
 *     1. Select the set of probabilities based on the density of 1's in the
 *        bitsequence to encode;
 *     2. Select the set of probabilities based on the average gap in the
 *        bitsequence to encode;
 *     3. Any combination of the 2.
 * We already store the density of 1's for each bitsequence, so we can store
 * for instance 7 sets of probabilities by using the following 7 batches
 * (mimicking the default batches we used for our metabatch method):
 * [0.,0.0002[ ; [0.0002,0.001[ ; [0.001,0.01[ ; [0.01,0.03[ ; [0.03,0.1[ ; 
 * [0.1,0.2[ ; [0.2,1].
 * Then for each of our 7 batches, we could get additional information from the
 * average gap size. We could also make a batching (e.g., 4 batches, which
 * would require 2 additional bits per bitsequence for encoding).
 * In total, we would need to save 7 * 4 * 4 probabilities, and the selector
 * for each bitsequence would take us 2 bits.
 * We did not implement it and instead provide a "lower bound" on the size of
 * the compressed index that can be achieved.
 *
 * This is essentially the code extracted from BitSequenceContextualEncoding and
 * composed with gaparraylistencoding.MethodByArithmeticCoding.
 */
package bitlistencoding;

import arithmeticcode.ArithmeticCoderBase;
import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFile;
import static compresslists.Tools.nbBits;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodBookstein extends Method {
    
    // Arithmetic encoder and decoder.
    protected ArithmeticEncoder encoder;
    protected ArithmeticDecoder decoder;
    
    // To handle trit sequences
    private BitSequenceAbstractReader bitSequenceReader;
    private BitSequenceAbstractWriter bitSequenceWriter;
    
    // To separate the different sizes in the index.
    private double size;
    private double sizeToChooseModel;
    
    // Size of the occurrence arrays.
    // From each state, we can read a 0 or a 1, therefore there are only two
    // edges, and therefore there are as many probabilities to store as there
    // are states (we store, e.g. the probability of reading a "0" from this
    // state and deduce the probability of reading a "1").
    private int nbStates;
    private final int nbSymbols = nbBits;
    
    // Occurrences arrays.
    private long[][] occurrences;
    private int[][] occurrencesCumulative;
    
    // Identifier for the Four-State Markov Model
    private BitSequenceMarkovHandler.Model model;
    
    private int nbBitsPerOccurrence;
    
    // TESTING_ONLY: when testingLowerBound is set to false, you init the
    // method by first collecting occurrences from all bitsequences, and use
    // those occurrences to encode all bitsequences (a model with n states
    // has to explicitely store 4 transition probabilities in the compressed
    // index).
    // When set to true, it collects occurrences "on the fly" on each list,
    // just to see what can be achieved with "perfect probabilities" that would
    // adapt exactly to each bitsequence. As explained at the beginning of this
    // file, we arbitrarily affect 2 bits per bitsequence to "detect" which
    // probabilities to use, but the resulting index size is only a lower bound
    // and the method does not create an index which can be uncompressed.
    private boolean testingLowerBound;
    public final static int nbBitsToChooseParameters = 2;
    public final static int nbProbabilitiesSetsToStore = 28;
    
    /**
     * Creates a new instance of MethodBookstein.
     */
    public MethodBookstein() {}
    public MethodBookstein(BitSequenceMarkovHandler.Model model) {
        this(model, false);
    }
    public MethodBookstein(BitSequenceMarkovHandler.Model model, boolean testingLowerBound) {
        setParameters(model.ordinal(), testingLowerBound);
    }
    
    public final void setParameters(int modelOrdinal, boolean testingLowerBound) {
        boolean foundModel = false;
        for (BitSequenceMarkovHandler.Model mod : BitSequenceMarkovHandler.Model.values()) {
            if (mod.ordinal() == modelOrdinal) {
                this.model = mod;
                foundModel = true;
                break;
            }
        }
        if (!foundModel) {
            throw new RuntimeException("Wrong ordinal for Model : " + modelOrdinal + ".");
        }
        this.testingLowerBound = testingLowerBound;
        nbStates = model.getNbStates();
        bitSequenceReader = new BitSequenceMarkovReader(model);
        bitSequenceWriter = new BitSequenceMarkovWriter(model);
    }
    
    @Override
    public void init(ArrayList<BitSequence> bitSequenceList) {
        if (!testingLowerBound) {
            super.init(bitSequenceList);
        }
    }
    
    @Override
    public void initBefore() {
        allocateOccurrenceArrays();
    }
    
    @Override
    public void init(BitSequence bitSequence) {
        collectOccurrences(bitSequence, occurrences, model);
    }
    
    @Override
    public void initAfter() {
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        nbBitsPerOccurrence = OccurrenceTools.getNbBitsMax(occurrences);
    }
    
    @Override
    public String getName() {
        return "Bookstein_" + model.getName();
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeBitSequence.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.finish();
        encoder = null;
    }
    
    @Override
    public void flushRemainingBits() {
        decoder = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the bit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Warning: for this to work, we need to pass the same lists when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public final double computeSize(BitSequence bitSequence) {
        if (testingLowerBound) {
            initBefore();
            init(bitSequence);
            initAfter();
        }
        double sizeLocal = 0.;
        
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            assert(false);
        }
        while (bitSequenceReader.isInGenericState()) {
            int index = bitSequenceReader.getGenericIndex();
            // Update the size
            for (int symbol = 0; symbol < nbSymbols; symbol++) {
                double pz = ((double)occurrences[index][symbol]) / ((double)occurrencesCumulative[index][nbSymbols]);
                sizeLocal += -pz * Tools.log2(pz);
            }
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
        
        size += sizeLocal;
        double sizeToChooseModelLocal = testingLowerBound ? 2. : 0.;
        sizeToChooseModel += sizeToChooseModelLocal;
        return sizeLocal + sizeToChooseModelLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        size = 0.;
        sizeToChooseModel = 0.;
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; bits: " + (size / 8000000) +
                (testingLowerBound ? " ; choose the model: " + (sizeToChooseModel / 8000000) : "") +
                ")");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the bit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeBitSequence(BitOutputStream bitStream, BitSequence bitSequence) {
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = ArithmeticCoderBase.DEFAULT_STATE_BITS;
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        writeBitSequence(bitSequence);
    }
    
    public final void writeBitSequence(BitSequence bitSequence) {
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            assert(false);
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Encode the bit
            encoder.update(occurrencesCumulative[index], currentBit);
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the bit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    public static final void collectOccurrences(BitSequence bitSequence,
            long[][] occurrences, BitSequenceMarkovHandler.Model model) {
        BitSequenceMarkovReader bitSequenceReader = new BitSequenceMarkovReader(model);
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            assert(false);
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Update the occurrences
            occurrences[index][currentBit]++;
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    private void allocateOccurrenceArrays() {
	occurrences = OccurrenceTools.allocateOccurrences(nbStates, nbSymbols);
        occurrencesCumulative = OccurrenceTools.allocateOccurrencesCumulative(nbStates, nbSymbols);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private int getNbBitsWritten() {
        return GammaEncoding.computeSizeGamma(nbBitsPerOccurrence) + (testingLowerBound
                ? Math.multiplyExact(Math.multiplyExact(nbStates, nbBits * nbBitsPerOccurrence),
                        nbProbabilitiesSetsToStore)
                : Math.multiplyExact(nbStates, nbBits * nbBitsPerOccurrence));
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 8 + // (byte)model.ordinal()
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte); // occurrenceMatrix
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeByte((byte)model.ordinal());
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            // export occurrenceMatrix (for the gapSize)
            for (int state = 0; state < nbStates; state++) {
                for (int symbol = 0; symbol < nbSymbols; symbol++) {
                    BinaryEncoding.writeCodeBinary((int)occurrences[state][symbol], bitStream, nbBitsPerOccurrence);
                }
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            byte modelId = in.readByte();
            setParameters(modelId, false);
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            allocateOccurrenceArrays();
            // import occurrenceMatrix (for the gapSize)
            for (int state = 0; state < nbStates; state++) {
                for (int symbol = 0; symbol < nbSymbols; symbol++) {
                    occurrences[state][symbol] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                }
            }
            OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        }
    }
    
    @Override
    public final void readBitSequence(BitInputStream bitStream, int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = ArithmeticCoderBase.DEFAULT_STATE_BITS;
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        readBitSequence(nbBitsToRead, onlyCountOnes, bitSequence);
    }
    
    /*
     * It uses arithmetic decoding of the bits, by following the states in the
     * graph associated with the chosen model.
     */
    public final void readBitSequence(int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        bitSequenceWriter.init(nbBitsToRead, onlyCountOnes, bitSequence);
        while (bitSequenceWriter.isInInitialState()) {
            assert(false);
        }
        while (bitSequenceWriter.isInGenericState()) {
            int index = bitSequenceWriter.getGenericIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrencesCumulative[index]);
            // Update the indexes for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateGenericIndex();
        }
    }
    
}
