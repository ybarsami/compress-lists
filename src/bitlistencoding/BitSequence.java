/**
 * This class internally uses java.util.BitSet. We only need to append new bits
 * to the bitset, so the interface is different. Furthermore, we here provide
 * a way to know what is the last bit which is relevant. We thus store a private
 * int "lastPosition" indicating that all bits (even those which have value 0)
 * at index less or equal to lastPosition are relevant.
 * 
 * Remark: This class could implement java.util.Iterator<Integer>, if this could
 * somehow help new code.
 */

package bitlistencoding;

import compresslists.Tools;
import static compresslists.Tools.nbBitsPerByte;

import java.util.BitSet;

/**
 *
 * @author yann
 */
public class BitSequence {
    
    private BitSet bitSet;
    private int lastPosition; // The last position of a bit which have been added to this bit sequence.
                              // It is neither bitSet.size() --- which is implementation-dependant.
                              //       neither bitSet.length() --- which gives the last 1 position.
    
    /**
     * Creates a new instance of BitSequence.
     */
    public BitSequence() {
        bitSet = new BitSet();
        lastPosition = -1;
    }
    
    public BitSequence(String s) {
        this();
        for (int i = 0; i < s.length(); i++) {
            add(Integer.parseInt(s.substring(i, i + 1)));
        }
    }
    
    public BitSequence(byte[] bytes) {
        this();
        for (byte b : bytes) {
            for (int i : Tools.byte2bitArray(b)) {
                add(i);
            }
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Accessor methods
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * The number of bits in the BitSequence. Because indexes start at 0, it
     * is thus just lastPosition + 1.
     */
    public int size() {
        return lastPosition + 1;
    }
    
    /*
     * Returns the value of the bit with the specified index.
     */
    private boolean getBool(int i) throws IndexOutOfBoundsException {
        if (i <= lastPosition && i >= 0) {
            return bitSet.get(i);
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
    
    /*
     * Returns the value of the bit with the specified index.
     */
    public int get(int i) throws IndexOutOfBoundsException {
        return getBool(i) ? 1 : 0;
    }
    
    /*
     * Returns a new byte array containing all the bits in this bit sequence.
     * The bits are written in big endian by default, but this can be modified
     * by setting Tools.isBigEndian to false.
     * 
     * Remark: The BitSet class (internally used by BitSequence) also has a
     * toByteArray() method, that uses the little endian representation.
     * e.g. 242_{10} = 11110010_2 would be written as 01001111_2 = 79_{10}.
     */
    public byte[] toByteArray() {
        final int nbBytes = Tools.ceilingDivision(size(), nbBitsPerByte);
        byte[] bytes = new byte[nbBytes];
        final int[] bits = new int[nbBitsPerByte];
        for (int i = 0; i < nbBytes; i++) {
            for (int j = 0; j < nbBitsPerByte; j++) {
                bits[j] = bitSet.get(i * nbBitsPerByte + j) ? 1 : 0;
            }
            bytes[i] = Tools.bitArray2byte(bits);
        }
        return bytes;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= lastPosition; i++) {
            sb.append(String.valueOf(get(i)));
        }
        return sb.toString();
    }
    
    /*
     * Output the number of "1" in the bit sequence.
     */
    public int numberOfOnes() {
        return numberOfOnes(0, size());
    }
    
    /*
     * Output the number of "1" in the bit sequence, contained in positions k
     * such that beginIndex <= k < endIndex.
     */
    public int numberOfOnes(int beginIndex, int endIndex) {
	int nbOnes = 0;
        for (int i = beginIndex; i < endIndex; i++) {
            if (getBool(i)) {
                nbOnes++;
            }
        }
	return nbOnes;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Modifying methods
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Allow this BitSequence to be used once more. This is a memory
     * optimization to avoid frequent allocation / deallocation of the
     * long[] arrays that are used by the BitSet.
     */
    public void reset() {
        lastPosition = -1;
    }
    
    /*
     * Append the specified value to the bit sequence.
     *
     * @param value a boolean value to append.
     */
    private void add(boolean value) throws IndexOutOfBoundsException {
        if (size() == Integer.MAX_VALUE) {
            throw new IndexOutOfBoundsException();
        }
        lastPosition++;
        if (value) {
            bitSet.set(lastPosition);
        } else {
            bitSet.clear(lastPosition);
        }
    }
    
    /*
     * Append the specified value to the bit sequence.
     *
     * @param value an integer value to append.
     */
    public void add(int value) throws IndexOutOfBoundsException {
        switch(value) {
            case 0:
                add(false);
                return;
            case 1:
                add(true);
                return;
            case 2:
                throw new RuntimeException("Use BitSequence only on integer values in {0, 1}.");
        }
    }
    
    /*
     * Append the specified value to the bit sequence, nbPositions times.
     *
     * @param value a boolean value to append.
     * @param nbPositions number of positions of bits to be added.
     */
    private void add(boolean value, int nbPositions) {
        for (int i = 0; i < nbPositions; i++) {
            add(value);
        }
    }
    
    /*
     * Append the specified value to the bit sequence, nbPositions times.
     *
     * @param value an integer value to append.
     * @param nbPositions number of positions of bits to be added.
     */
    public void add(int value, int nbPositions) {
        switch(value) {
            case 0:
                add(false, nbPositions);
                return;
            case 1:
                add(true, nbPositions);
                return;
            case 2:
                throw new RuntimeException("Use BitSequence only on integer values in {0, 1}.");
        }
    }
}
