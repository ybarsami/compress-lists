/*
 * This is essentially the code extracted from BitSequenceContextualEncoding and
 * composed with gaparraylistencoding.MethodByArithmeticCoding.
 */
package bitlistencoding;

import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import compresslists.CompressLists.BitContextParameters;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFake;
import io.BitOutputStreamFile;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;
import static compresslists.Tools.nbBits;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodContext extends Method {
    
    // Arithmetic encoder and decoder.
    protected ArithmeticEncoder encoder;
    protected ArithmeticDecoder decoder;
    
    private BitOutputStreamFake bitCounter = new BitOutputStreamFake();
    private ArithmeticEncoder counter;
    
    // To handle trit sequences
    private BitSequenceAbstractReader bitSequenceReader;
    private BitSequenceAbstractWriter bitSequenceWriter;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    
    // Size of the occurrence arrays.
    private int contextSize;
    private int contextSizeInit;
    private final int nbSymbols = nbBits;
    
    // Occurrences arrays.
    private long[][] occurrences;
    private int[][] occurrencesCumulative;
    private long[][] occurrencesInit;
    private int[][] occurrencesCumulativeInit;
    
    // Parameters.
    private int k;
    private int w;
    private int kInit;
    
    private int nbBitsPerOccurrence;
    private boolean onlyInit;
    
    /**
     * Creates a new instance of MethodContext.
     */
    public MethodContext() {}
    public MethodContext(BitContextParameters contextParameters) {
        setParameters(contextParameters);
    }
    
    /*
     * A BitContextParameters object does not store any boolean named
     * onlyInit. Instead, if k is set to the value Integer.MAX_VALUE,
     * this means that we set k = 0 and only look at initial probabilities.
     */
    public final void setParameters(BitContextParameters contextParameters) {
        this.k = contextParameters.k;
        this.w = contextParameters.w;
        this.kInit = contextParameters.kInit;
        onlyInit = (k == Integer.MAX_VALUE);
        contextSize = onlyInit ? 1 : Math.multiplyExact(w + 1, Tools.pow(nbSymbols, k));
        // 1 << (kInit + 1) does not lead to what we expect if kInit > 29.
        if (kInit > 29) {
            throw new ArithmeticException();
        }
        contextSizeInit = nbBits * (1 - (1 << (kInit + 1))) / (1 - nbBits);
        bitSequenceReader = new BitSequenceContextReader(k, w, kInit);
        bitSequenceWriter = new BitSequenceContextWriter(k, w, kInit);
    }
    
    @Override
    public void initBefore() {
        allocateOccurrenceArrays();
    }
    
    @Override
    public void init(BitSequence bitSequence) {
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getInitialIndex();
            // Update the occurrences
            occurrencesInit[index][currentBit]++;
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Update the occurrences
            occurrences[index][currentBit]++;
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    @Override
    public void initAfter() {
        convertToOccurrenceIntervals();
        nbBitsPerOccurrence = OccurrenceTools.getNbBitsMax(occurrences);
    }
    
    @Override
    public String getName() {
        return "Context" +
                (onlyInit ? "_OnlyInit" : "_k" + k + "_w" + w) +
                "_kI" + kInit;
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeBitSequence.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.finish();
        encoder = null;
    }
    
    @Override
    public void flushRemainingBits() {
        decoder = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the bit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Warning: for this to work, we need to pass the same lists when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public final double computeSize(BitSequence bitSequence) {
        bitSequenceReader.init(bitSequence);
        
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getInitialIndex();
            // Update the size
            counter.update(occurrencesCumulativeInit[index], currentBit);
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        double sizeInitialLocal = bitCounter.getNbBitsWrittenAndReset();
        
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Update the size
            counter.update(occurrencesCumulative[index], currentBit);
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
        double sizeGenericLocal = bitCounter.getNbBitsWrittenAndReset();
        
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        return sizeInitialLocal + sizeGenericLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        int nbStateBits = DEFAULT_STATE_BITS;
        counter = new ArithmeticEncoder(nbStateBits, bitCounter);
        sizeInitial = 0.;
        sizeGeneric = 0.;
    }
    
    @Override
    public double computeSizeAfter() {
        if (counter == null) {
            throw new RuntimeException("Unsupported call to computeSizeAfter without prior call to computeSizeBefore.");
        }
        counter.finish();
        counter = null;
        double sizeAfter = bitCounter.getNbBitsWrittenAndReset();
        sizeGeneric += sizeAfter;
        return sizeAfter;
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) bits: " + (sizeInitial / 8000000) +
                " ; last bits: " + (sizeGeneric / 8000000) +
                ")");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the bit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeBitSequence(BitOutputStream bitStream, BitSequence bitSequence) {
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = DEFAULT_STATE_BITS;
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        writeBitSequence(bitSequence);
    }
    
    public final void writeBitSequence(BitSequence bitSequence) {
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getInitialIndex();
            // Encode the bit
            encoder.update(occurrencesCumulativeInit[index], currentBit);
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            int index = bitSequenceReader.getGenericIndex();
            // Encode the bit
            encoder.update(occurrencesCumulative[index], currentBit);
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the bit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    private void allocateOccurrenceArrays() {
	occurrences     = OccurrenceTools.allocateOccurrences(contextSize, nbSymbols);
	occurrencesInit = OccurrenceTools.allocateOccurrences(contextSizeInit, nbSymbols);
        occurrencesCumulative     = OccurrenceTools.allocateOccurrencesCumulative(contextSize, nbSymbols);
        occurrencesCumulativeInit = OccurrenceTools.allocateOccurrencesCumulative(contextSizeInit, nbSymbols);
    }
    
    private void convertToOccurrenceIntervals() {
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        OccurrenceTools.ensureTotalAndAccumulate(occurrencesInit, occurrencesCumulativeInit);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private int getNbBitsWritten() {
        return Math.addExact(
                GammaEncoding.computeSizeGamma(k + 1) +
                GammaEncoding.computeSizeGamma(w + 1) +
                GammaEncoding.computeSizeGamma(kInit + 1) +
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence),
                Math.multiplyExact(contextSize, nbBits * nbBitsPerOccurrence));
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return Math.addExact(
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte), // parameters + occurrenceMatrix
                Math.multiplyExact(contextSizeInit, nbBits * 32)); // occurrenceInitTensor
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            GammaEncoding.writeCodeGamma(k + 1, bitStream);
            GammaEncoding.writeCodeGamma(w + 1, bitStream);
            GammaEncoding.writeCodeGamma(kInit + 1, bitStream);
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            // export occurrenceMatrix (for the gapSize)
            for (int context = 0; context < contextSize; context++) {
                for (int symbol = 0; symbol < nbSymbols; symbol++) {
                    BinaryEncoding.writeCodeBinary((int)occurrences[context][symbol], bitStream, nbBitsPerOccurrence);
                }
            }
            // Padding
            bitStream.close();
            // export occurrenceInitTensor
            for (int context = 0; context < contextSizeInit; context++) {
                for (int symbol = 0; symbol < nbSymbols; symbol++) {
                    out.writeInt((int)occurrencesInit[context][symbol]);
                }
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            k = GammaEncoding.readCodeGamma(bitStream) - 1;
            w = GammaEncoding.readCodeGamma(bitStream) - 1;
            kInit = GammaEncoding.readCodeGamma(bitStream) - 1;
            setParameters(new BitContextParameters(k, w, kInit));
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            allocateOccurrenceArrays();
            // import occurrenceMatrix (for the gapSize)
            for (int context = 0; context < contextSize; context++) {
                for (int symbol = 0; symbol < nbSymbols; symbol++) {
                    occurrences[context][symbol] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                }
            }
            // import occurrenceInitTensor
            for (int context = 0; context < contextSizeInit; context++) {
                for (int symbol = 0; symbol < nbSymbols; symbol++) {
                    occurrencesInit[context][symbol] = in.readInt();
                }
            }
            convertToOccurrenceIntervals();
        }
    }
    
    @Override
    public final void readBitSequence(BitInputStream bitStream, int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = DEFAULT_STATE_BITS;
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        readBitSequence(nbBitsToRead, onlyCountOnes, bitSequence);
    }
    
    /*
     * First, it uses arithmetic decoding of the initial bits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining bits.
     */
    public final void readBitSequence(int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        bitSequenceWriter.init(nbBitsToRead, onlyCountOnes, bitSequence);
        while (bitSequenceWriter.isInInitialState()) {
            int index = bitSequenceWriter.getInitialIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrencesCumulativeInit[index]);
            // Update the index for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateInitialIndex();
        }
        while (bitSequenceWriter.isInGenericState()) {
            int index = bitSequenceWriter.getGenericIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrencesCumulative[index]);
            // Update the indexes for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateGenericIndex();
        }
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some testing
    ////////////////////////////////////////////////////////////////////////////
    
    public final void exportProbas(FileWriter out) throws IOException {
        // export occurrenceInitTensor
        for (int context = 0; context < contextSizeInit; context++) {
            for (int symbol = 0; symbol < nbSymbols; symbol++) {
                out.write(((double)occurrencesInit[context][symbol] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
            }
        }
        // export occurrenceMatrix (for the gapSize)
        for (int context = 0; context < contextSize; context++) {
            for (int symbol = 0; symbol < nbSymbols; symbol++) {
                out.write(((double)occurrences[context][symbol] / (double)occurrencesCumulative[context][nbSymbols]) + "");
                out.write(context == (contextSize - 1) && symbol == (nbSymbols - 1) ? "" : " ");
            }
        }
        out.write("\n");
    }
    
}
