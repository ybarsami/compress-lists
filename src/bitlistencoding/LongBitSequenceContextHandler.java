/*
 * Handles a list of bits so that there is:
 *     a) an initial pass
 *     b) a generic pass
 */
package bitlistencoding;

import static compresslists.Tools.nbBits;

/**
 *
 * @author yann
 */
public abstract class LongBitSequenceContextHandler implements LongBitSequenceAbstractHandler {
    
    // Parameters
    private final int kInit;
    private final int k;
    private final int w;
    
    // Initial index.
    private long nbIndexesInitBefore;
    // indexInit is a bijection between kInit-uplets of bits and { 0, ..., 2^kInit - 1 }.
    private long indexInit;
    
    // Generic index.
    // Number of "1" in the w bits before the k last ones. This is a value in { 0, ..., w }.
    private int numberOfOnes;
    // subIndex is a bijection between k-uplets of bits and { 0, ..., 2^k - 1 }.
    private long subIndex;
    // index is a bijection between the k-uplet of preceding bits,
    // the number of "1" in the w bits before the k preceding bits,
    // and { 0, ..., 2^k * (w+1) - 1 }.
    private long indexGeneric;
    private int indexNextBitToLeaveTheWBits;
    
    // The bit list to handle.
    protected int nbBitsHandled;
    protected BitSequence bitSequence;
    
    public LongBitSequenceContextHandler(int k, int w, int kInit) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
    }
    
    @Override
    public void init() {
        this.nbIndexesInitBefore = 0;
        this.indexInit = 0;
        this.nbBitsHandled = 0;
    }
    
    @Override
    public int getCurrentBit() {
        return bitSequence.get(nbBitsHandled);
    }
    
    protected void initializeGenericIndex() {
        indexNextBitToLeaveTheWBits = nbBitsHandled - k - w;
        // Initial computation of the indexes.
        numberOfOnes = bitSequence.numberOfOnes(
                indexNextBitToLeaveTheWBits,
                indexNextBitToLeaveTheWBits + w);
        subIndex = 0;
        for (int i = 0; i < k; i++) {
            subIndex *= nbBits;
            subIndex += bitSequence.get(nbBitsHandled - k + i);
        }
        indexGeneric = (w + 1) * subIndex + numberOfOnes;
    }
    
    @Override
    public boolean isInInitialState() {
        if (nbBitsHandled >= k + w) {
            initializeGenericIndex();
            return false;
        }
        return isStillActive();
    }
    
    @Override
    public void updateInitialIndex() {
        int currentSymbol = getCurrentBit();
        indexInit = (nbBits * indexInit + currentSymbol) % (1L << kInit);
        nbBitsHandled++;
        if (nbBitsHandled <= kInit) {
            nbIndexesInitBefore = nbBits * (1L - (1L << nbBitsHandled)) / (1L - nbBits);
        }
    }
    
    @Override
    public long getInitialIndex() {
        return nbIndexesInitBefore + indexInit;
    }
    
    @Override
    public boolean isInGenericState() {
        return isStillActive() && nbBitsHandled >= k + w;
    }
    
    @Override
    public void updateGenericIndex() {
        int currentSymbol = getCurrentBit();
        if (bitSequence.get(indexNextBitToLeaveTheWBits) == 1) {
            numberOfOnes--;
        }
        if (bitSequence.get(indexNextBitToLeaveTheWBits + w) == 1) {
            numberOfOnes++;
        }
        subIndex = (nbBits * subIndex + currentSymbol) % (1L << k);
        indexGeneric = (w + 1) * subIndex + numberOfOnes;
        indexNextBitToLeaveTheWBits++;
        nbBitsHandled++;
    }
    
    @Override
    public long getGenericIndex() {
        return indexGeneric;
    }
    
}
