/*
 * See BitSequenceMarkovHandler.java
 */
package bitlistencoding;

/**
 *
 * @author yann
 */
public class BitSequenceMarkovReader extends BitSequenceMarkovHandler implements BitSequenceAbstractReader {
    
    public BitSequenceMarkovReader(BitSequenceMarkovHandler.Model model) {
        super(model);
    }
    
    @Override
    public void init(BitSequence bitSequence) {
        init();
        this.bitSequence = bitSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbBitsHandled < bitSequence.size();
    }
    
}
