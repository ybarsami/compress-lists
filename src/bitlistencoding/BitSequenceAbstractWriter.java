/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitlistencoding;

/**
 *
 * @author yann
 */
public interface BitSequenceAbstractWriter extends BitSequenceAbstractHandler {
    
    public void init(int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence);
    
    public void init(int nbOnesToRead, BitSequence bitSequence);
    
    public void addBit(int bit);
    
    public BitSequence getBitSequence();
    
}
