/*
 * MethodByGapArray.java
 * For each bit vector, here what is done:
 *    0. We note gapArray = gapArray(bit vector).
 *    1. With a GapArray method, we encode gapArray.
 */
package bitlistencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodByGapArray extends Method {
    
    private final gaparrayencoding.MethodByBitSequence gapMethod;
    
    private IntsRef gapArrayRef = new IntsRef(0);
    
    /**
     * Creates a new instance of MethodByGapArray.
     */
    public MethodByGapArray(gaparrayencoding.MethodByBitSequence gapMethod) {
        this.gapMethod = gapMethod;
    }
    
    @Override
    public void initBefore() {
        gapMethod.initBefore();
    }
    
    @Override
    public void init(BitSequence bitSequence) {
        Tools.bitVector2gapArray(bitSequence, gapArrayRef);
        gapMethod.init(gapArrayRef);
    }
    
    @Override
    public void initAfter() {
        gapMethod.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same bit sequences when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(BitSequence bitSequence) {
        Tools.bitVector2gapArray(bitSequence, gapArrayRef);
        return gapMethod.computeSize(gapArrayRef);
    }
    
    @Override
    public void computeSizeBefore() {
        gapMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        gapArrayRef = new IntsRef(0);
        return gapMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        gapMethod.standardOutput();
    }
    
    @Override
    public final void writeBitSequence(BitOutputStream bitStream, BitSequence bitSequence) {
        Tools.bitVector2gapArray(bitSequence, gapArrayRef);
        gapMethod.writeGapArray(bitStream, gapArrayRef);
    }
    
    @Override
    public String getName() {
        return "ByGapArray(" + gapMethod.getName() + ")";
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return gapMethod.computeSizeNeededInformation();
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        gapMethod.exportNeededInformation(out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        gapMethod.importNeededInformation(in);
    }
    
    @Override
    public final void readBitSequence(BitInputStream bitStream, int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        assert(onlyCountOnes);
        gapArrayRef.length = nbBitsToRead;
        gapMethod.readGapArray(bitStream, gapArrayRef);
        Tools.gapArray2bitVector(gapArrayRef, bitSequence);
    }
    
}
