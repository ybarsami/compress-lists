/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitlistencoding;

/**
 *
 * @author yann
 */
public class BitSequenceContextWriter extends BitSequenceContextHandler implements BitSequenceAbstractWriter {
    
    boolean onlyCountOnes;
    private int nbBitsRead;
    private int nbBitsToRead;
    
    public BitSequenceContextWriter(int k, int w, int kInit) {
        super(k, w, kInit);
    }
    
    @Override
    public void init(int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        init();
        this.nbBitsToRead = nbBitsToRead;
        this.onlyCountOnes = onlyCountOnes;
        nbBitsRead = 0;
        bitSequence.reset();
        this.bitSequence = bitSequence;
        
    }
    
    @Override
    public void init(int nbOnesToRead, BitSequence bitSequence) {
        init(nbBitsToRead, true, bitSequence);
    }
    
    @Override
    public void addBit(int bit) {
        bitSequence.add(bit);
        if (bit == 1 || !onlyCountOnes) {
            nbBitsRead++;
        }
    }
    
    @Override
    public BitSequence getBitSequence() {
        return bitSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbBitsRead < nbBitsToRead;
    }
    
}
