/*
 * This is essentially the code extracted from BitSequenceContextualEncoding and
 * composed with gaparraylistencoding.MethodByArithmeticCoding.
 */
package bitlistencoding;

import arithmeticcode.ArithmeticCoderBase;
import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.ContextualOccurrenceTable;
import arithmeticcode.ContextualOccurrenceTableExtendedInt;
import arithmeticcode.ContextualOccurrenceTableFlatInt;
import arithmeticcode.ContextualOccurrenceTableSimpleInt;
import arithmeticcode.ContextualOccurrenceTableSparse;
import compresslists.CompressLists.BitContextParameters;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFake;
import io.BitOutputStreamFile;
import static arithmeticcode.ContextualOccurrenceTable.*;
import static compresslists.Tools.nbBits;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodAdaptiveContext extends Method {
    
    // Arithmetic encoder and decoder.
    protected ArithmeticEncoder encoder;
    protected ArithmeticDecoder decoder;
    
    private BitOutputStreamFake bitCounter = new BitOutputStreamFake();
    private ArithmeticEncoder counter;
    
    // To handle trit sequences
    private LongBitSequenceAbstractReader bitSequenceReader;
    private LongBitSequenceAbstractWriter bitSequenceWriter;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    
    // Size of the occurrence arrays.
    private long contextSize;
    private long contextSizeInit;
    private final int nbSymbols = nbBits;
    
    public static enum OccurrenceType {
        OCCURRENCE_DENSITY("Density"),
        OCCURRENCE_UNIFORM("Uniform");
        
        private String str;
        
        OccurrenceType(String str) {
            this.str = str;
        }
        
        public String getString() { return str; }
    };
    
    private OccurrenceType occurrenceType = OccurrenceType.OCCURRENCE_UNIFORM;
    
    // Occurrences arrays.
    private int occurrenceTableType = TABLE_SIMPLE_INT;
    private boolean adaptToCurrentDensity;
    private ContextualOccurrenceTable occurrenceTable;
    private ContextualOccurrenceTable occurrenceTableInit;
    
    // Parameters.
    private int k;
    private int w;
    private int kInit;
    private int nbDocuments;
    private int maxBitsOcc;
    private int firstGapArraySize;
    private int currentGapArraySize;
    
    private boolean onlyInit;
    
    /**
     * Creates a new instance of MethodAdaptiveContext.
     */
    public MethodAdaptiveContext() {}
    public MethodAdaptiveContext(BitContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
        setParameters(contextParameters, nbDocuments, firstGapArraySize, adaptToCurrentDensity);
    }
    
    public final void setParameters(int nbDocuments, int firstGapArraySize) {
        this.nbDocuments = nbDocuments;
        this.firstGapArraySize = firstGapArraySize;
    }
    public final void setParameters(BitContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
        this.k = contextParameters.k;
        this.w = contextParameters.w;
        this.kInit = contextParameters.kInit;
        this.maxBitsOcc = contextParameters.nbBitsPerOccurrence;
        onlyInit = (k == Integer.MAX_VALUE);
        contextSize = onlyInit ? 1 : Math.multiplyExact(w + 1, Tools.pow((long)nbSymbols, (long)k));
        contextSizeInit = nbBits * (1L - (1L << (kInit + 1))) / (1L - nbBits);
        bitSequenceReader = new LongBitSequenceContextReader(k, w, kInit);
        bitSequenceWriter = new LongBitSequenceContextWriter(k, w, kInit);
        setParameters(nbDocuments, firstGapArraySize);
        this.adaptToCurrentDensity = adaptToCurrentDensity;
    }
    
    @Override
    public String getName() {
        return "AdaptiveContext" +
                (adaptToCurrentDensity ? "_AdaptedInit" : "_ConstantInit") +
                "_maxBitsOcc" + maxBitsOcc +
                (onlyInit ? "_OnlyInit" : "_k" + k + "_w" + w) +
                "_kI" + kInit + 
                "_Initial" + occurrenceType.getString();
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        deallocateOccurrenceArrays();
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeBitSequence.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.finish();
        encoder = null;
    }
    
    @Override
    public void flushRemainingBits() {
        deallocateOccurrenceArrays();
        decoder = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the bit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final double computeSize(BitSequence bitSequence) {
        if (adaptToCurrentDensity) {
            int nbOnes = bitSequence.numberOfOnes();
            if (nbOnes != currentGapArraySize) {
                currentGapArraySize = nbOnes;
                setDefaultOccurrences();
            }
        }
        
        bitSequenceReader.init(bitSequence);
        
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            long index = bitSequenceReader.getInitialIndex();
            // Update the size
            counter.update(occurrenceTableInit.getOccurrencesCumulative(index), currentBit);
            occurrenceTableInit.incrementOccurrence(index, currentBit);
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        double sizeInitialLocal = bitCounter.getNbBitsWrittenAndReset();
        
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            long index = bitSequenceReader.getGenericIndex();
            // Update the size
            counter.update(occurrenceTable.getOccurrencesCumulative(index), currentBit);
            occurrenceTable.incrementOccurrence(index, currentBit);
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
        double sizeGenericLocal = bitCounter.getNbBitsWrittenAndReset();
        
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        return sizeInitialLocal + sizeGenericLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        int nbStateBits = ArithmeticCoderBase.STATE_BITS[maxBitsOcc];
        counter = new ArithmeticEncoder(nbStateBits, bitCounter);
        sizeInitial = 0.;
        sizeGeneric = 0.;
        if (occurrenceTable == null) {
            allocateOccurrenceArrays();
        }
    }
    
    @Override
    public double computeSizeAfter() {
        if (counter == null) {
            throw new RuntimeException("Unsupported call to computeSizeAfter without prior call to computeSizeBefore.");
        }
        counter.finish();
        counter = null;
        deallocateOccurrenceArrays();
        double sizeAfter = bitCounter.getNbBitsWrittenAndReset();
        sizeGeneric += sizeAfter;
        return sizeAfter;
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) bits: " + (sizeInitial / 8000000) +
                " ; last bits: " + (sizeGeneric / 8000000) +
                ")");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the bit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeBitSequence(BitOutputStream bitStream, BitSequence bitSequence) {
        if (occurrenceTable == null) {
            allocateOccurrenceArrays();
        }
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = ArithmeticCoderBase.STATE_BITS[maxBitsOcc];
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        writeBitSequence(bitSequence);
    }
    
    private void writeBitSequence(BitSequence bitSequence) {
        if (adaptToCurrentDensity) {
            int nbOnes = bitSequence.numberOfOnes();
            if (nbOnes != currentGapArraySize) {
                currentGapArraySize = nbOnes;
                setDefaultOccurrences();
            }
        }
        bitSequenceReader.init(bitSequence);
        while (bitSequenceReader.isInInitialState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            long index = bitSequenceReader.getInitialIndex();
            // Encode the bit
            encoder.update(occurrenceTableInit.getOccurrencesCumulative(index), currentBit);
            occurrenceTableInit.incrementOccurrence(index, currentBit);
            // Update the index for the next iteration.
            bitSequenceReader.updateInitialIndex();
        }
        while (bitSequenceReader.isInGenericState()) {
            int currentBit = bitSequenceReader.getCurrentBit();
            long index = bitSequenceReader.getGenericIndex();
            // Encode the bit
            encoder.update(occurrenceTable.getOccurrencesCumulative(index), currentBit);
            occurrenceTable.incrementOccurrence(index, currentBit);
            // Update the indexes for the next iteration.
            bitSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the bit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    private void allocateOccurrenceArrays() {
        switch (occurrenceTableType) {
            case TABLE_SPARSE:
                occurrenceTable     = new ContextualOccurrenceTableSparse(contextSize, nbSymbols, maxBitsOcc);
                occurrenceTableInit = new ContextualOccurrenceTableSparse(contextSizeInit, nbSymbols, maxBitsOcc);
                break;
            case TABLE_SIMPLE_INT:
                long maxContextSize = Math.max(contextSize, contextSizeInit);
                if (Math.multiplyExact(maxContextSize, nbSymbols + 1) <= Integer.MAX_VALUE) {
                    occurrenceTable     = new ContextualOccurrenceTableFlatInt((int)contextSize, nbSymbols, maxBitsOcc);
                    occurrenceTableInit = new ContextualOccurrenceTableFlatInt((int)contextSizeInit, nbSymbols, maxBitsOcc);
                } else /*if (maxContextSize > Integer.MAX_VALUE)*/ {
                    occurrenceTable     = new ContextualOccurrenceTableExtendedInt(contextSize, nbSymbols, maxBitsOcc);
                    occurrenceTableInit = new ContextualOccurrenceTableExtendedInt(contextSizeInit, nbSymbols, maxBitsOcc);
                /*
                } else {
                    occurrenceTable     = new ContextualOccurrenceTableSimpleInt((int)contextSize, nbSymbols, maxBitsOcc);
                    occurrenceTableInit = new ContextualOccurrenceTableSimpleInt((int)contextSizeInit, nbSymbols, maxBitsOcc);
                */
                }
                break;
            default:
                throw new RuntimeException("This occurrenceTableType is not handled.");
        }
        currentGapArraySize = firstGapArraySize;
        setDefaultOccurrences();
    }
    
    private void setDefaultOccurrences() {
        long[] defaultOccurrences;
        switch (occurrenceType) {
            case OCCURRENCE_DENSITY:
                defaultOccurrences = new long[] {
                    nbDocuments - currentGapArraySize,
                    currentGapArraySize
                };
                break;
            case OCCURRENCE_UNIFORM:
                defaultOccurrences = new long[] { 1L, 1L };
                break;
            default:
                throw new RuntimeException("This occurrenceType is not handled.");
        }
        occurrenceTable.setDefaultOccurrences(defaultOccurrences);
        occurrenceTableInit.setDefaultOccurrences(defaultOccurrences);
    }
    
    private void deallocateOccurrenceArrays() {
        occurrenceTable = null;
        occurrenceTableInit = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private int getNbBitsWritten() {
        return
                3 * 32 + // nbDocuments, firstGapArraySize, maxBitsOcc
                GammaEncoding.computeSizeGamma(k + 1) +
                GammaEncoding.computeSizeGamma(w + 1) +
                GammaEncoding.computeSizeGamma(kInit + 1) +
                1; // 1 boolean
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte); // parameters
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeInt(nbDocuments);
            out.writeInt(firstGapArraySize);
            out.writeInt(maxBitsOcc);
            GammaEncoding.writeCodeGamma(k + 1, bitStream);
            GammaEncoding.writeCodeGamma(w + 1, bitStream);
            GammaEncoding.writeCodeGamma(kInit + 1, bitStream);
            BinaryEncoding.writeCodeBinary(adaptToCurrentDensity ? 1 : 0, bitStream, 1);
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            nbDocuments = in.readInt();
            firstGapArraySize = in.readInt();
            maxBitsOcc = in.readInt();
            k = GammaEncoding.readCodeGamma(bitStream) - 1;
            w = GammaEncoding.readCodeGamma(bitStream) - 1;
            kInit = GammaEncoding.readCodeGamma(bitStream) - 1;
            adaptToCurrentDensity = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            setParameters(new BitContextParameters(k, w, kInit, maxBitsOcc), nbDocuments, firstGapArraySize, adaptToCurrentDensity);
        }
    }
    
    @Override
    public final void readBitSequence(BitInputStream bitStream, int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        if (occurrenceTable == null) {
            allocateOccurrenceArrays();
        }
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = ArithmeticCoderBase.STATE_BITS[maxBitsOcc];
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        readBitSequence(nbBitsToRead, onlyCountOnes, bitSequence);
    }
    
    /*
     * First, it uses arithmetic decoding of the initial bits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining bits.
     */
    private void readBitSequence(int nbBitsToRead, boolean onlyCountOnes, BitSequence bitSequence) {
        if (adaptToCurrentDensity) {
            if (nbBitsToRead != currentGapArraySize) {
                currentGapArraySize = nbBitsToRead;
                setDefaultOccurrences();
            }
        }
        bitSequenceWriter.init(nbBitsToRead, onlyCountOnes, bitSequence);
        while (bitSequenceWriter.isInInitialState()) {
            long index = bitSequenceWriter.getInitialIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrenceTableInit.getOccurrencesCumulative(index));
            occurrenceTableInit.incrementOccurrence(index, currentBit);
            // Update the index for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateInitialIndex();
        }
        while (bitSequenceWriter.isInGenericState()) {
            long index = bitSequenceWriter.getGenericIndex();
            // Decode a bit
            int currentBit = decoder.read(occurrenceTable.getOccurrencesCumulative(index));
            occurrenceTable.incrementOccurrence(index, currentBit);
            // Update the indexes for the next iteration.
            bitSequenceWriter.addBit(currentBit);
            bitSequenceWriter.updateGenericIndex();
        }
    }
    
}
