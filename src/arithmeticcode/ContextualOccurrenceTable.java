/*
 * Base class for contextual occurrence tables.
 */
package arithmeticcode;

/**
 *
 * @author yann
 */
public abstract class ContextualOccurrenceTable {
    
    public static final int TABLE_SPARSE     = 0;
    public static final int TABLE_SIMPLE     = 1;
    public static final int TABLE_SIMPLE_INT = 2;
    
    public abstract void setDefaultOccurrences(long[] defaultOccurrences);
    
    public abstract void incrementOccurrence(long context, int symbol);
    
    public abstract int getOccurrence(long context, int symbol);
    
    public abstract int[] getOccurrencesCumulative(long context);
    
    public abstract int getOccurrencesTotal(long context);
    
}
