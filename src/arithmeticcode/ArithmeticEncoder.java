/**
 * The arithmetic code implementation is adapted from the following
 * implementation. From this initial project:
 * (1) we modified the Bit(Input/Output)Stream classes to allow (a) usage of
 * in-memory bit streams in addition to file streams and (b) writing of the file
 * by buffer of bytes and not byte by byte, to optimize Output bandwidth.
 * (2) we added ContextualOccurrenceTableXXX classes to allow (a) usage of more
 * precise occurrences (long instead of int in the original XXXFrequencyTable
 * classes) and (b) sparse occurrence tables for adaptive contextual encoding.
 * 
 * Original source: "Reference arithmetic coding"
 * Copyright (c) 2018 Project Nayuki (MIT License)
 * 
 * https://www.nayuki.io/page/reference-arithmetic-coding
 * https://github.com/nayuki/Reference-arithmetic-coding
 */

package arithmeticcode;

import io.BitOutputStream;

import java.util.Objects;


/**
 * Encodes symbols and writes to an arithmetic-coded bit stream. Not thread-safe.
 * @see ArithmeticDecoder
 */
public final class ArithmeticEncoder extends ArithmeticCoderBase {
    
    /*---- Fields ----*/
    
    // The underlying bit output stream (not null).
    private BitOutputStream output;
    
    // Number of saved underflow bits. This value can grow without bound, so a
    // truly correct implementation would use a BigInteger.
    private int numUnderflow;
    
    
    /*---- Constructor ----*/
    
    /**
     * Constructs an arithmetic coding encoder based on the specified bit output
     * stream.
     * @param numBits the number of bits for the arithmetic coding range
     * @param out the bit output stream to write to
     * @throws NullPointerException if the output stream is {@code null}
     * @throws IllegalArgumentException if stateSize is outside the range [1, 62]
     */
    public ArithmeticEncoder(int numBits, BitOutputStream out) {
        super(numBits);
        output = Objects.requireNonNull(out);
        numUnderflow = 0;
    }
    
    
    /*---- Methods ----*/
    
    /**
     * Encodes the specified symbol based on the specified occurrence table.
     * Also updates this arithmetic coder's state and may write out some bits.
     * @param table the cumulative occurrence table to use
     * @param symbol the symbol to encode
     * @throws NullPointerException if the occurrence table is {@code null}
     * @throws IllegalArgumentException if the symbol has zero occurrence or the
     *         occurrence table's total is too large
     */
    public void write(int[] table, int symbol) {
        update(table, symbol);
    }
    
    /**
     * Terminates the arithmetic coding by flushing any buffered bits, so that
     * the output can be decoded properly. It is important that this method must
     * be called at the end of the each encoding process.
     */
    public void finish() {
        output.writeBit(true);
    }
    
    @Override
    protected void shift() {
        int bit = (int)(low >>> (numStateBits - 1));
        output.writeBit(bit != 0);
        
        // Write out the saved underflow bits
        for (; numUnderflow > 0; numUnderflow--)
            output.writeBit(bit == 0);
    }
    
    @Override
    protected void underflow() {
        // If you encounter this exception, please change the type of
        // numUnderflow to long and change Integer.MAX_VALUE to Long.MAX_VALUE.
        // If this is still not enough, you can use the type BigInteger and
        // remove the check.
        if (numUnderflow == Integer.MAX_VALUE)
            throw new ArithmeticException("Maximum underflow reached");
        numUnderflow++;
    }
    
    public boolean checkBitOutputStreamEquality(BitOutputStream toCheck) {
        return output.equals(toCheck);
    }
}
