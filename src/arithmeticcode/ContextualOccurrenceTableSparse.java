/*
 * Contextual occurrence tables that store only the used contexts in memory.
 */
package arithmeticcode;

import compresslists.Tools;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;
import static arithmeticcode.ArithmeticCoderBase.MAX_OCC_BITS;

import java.util.HashMap;

/**
 *
 * @author yann
 */
public class ContextualOccurrenceTableSparse extends ContextualOccurrenceTable {
    
    // Size of the occurrence arrays.
    private long contextSize;
    private int nbSymbols;
    private int cumulativeArraySize;
    
    // Occurrences arrays.
    private int[] defaultOccurrencesCumulative;
    private HashMap<Long, int[]> occurrencesCumulative = new HashMap<>();
    private long[] tmp;
    
    private int maxBitsOcc;
    private int maxOcc;
    
    /**
     * Creates a new instance of ContextualOccurrenceTableSparse.
     */
    public ContextualOccurrenceTableSparse(long contextSize, int nbSymbols) {
        this(contextSize, nbSymbols, MAX_OCC_BITS[DEFAULT_STATE_BITS]);
    }
    public ContextualOccurrenceTableSparse(long contextSize, int nbSymbols, int maxBitsOcc) {
        this.contextSize = contextSize;
        this.nbSymbols = nbSymbols;
        this.cumulativeArraySize = nbSymbols + 1;
        if (maxBitsOcc > MAX_OCC_BITS[DEFAULT_STATE_BITS]) {
            throw new RuntimeException("Maximum number of bits for occurrences is " +
                    MAX_OCC_BITS[DEFAULT_STATE_BITS] +" for a 64-bit arithmetic coder.");
        }
        this.maxBitsOcc = maxBitsOcc;
        this.maxOcc = 1 << maxBitsOcc;
        defaultOccurrencesCumulative = new int[cumulativeArraySize];
        tmp = new long[nbSymbols];
        long[] defaultOccurrences = new long[nbSymbols];
        OccurrenceTools.fillUniformOccurrences(defaultOccurrences);
        setDefaultOccurrences(defaultOccurrences);
    }
    
    @Override
    public void setDefaultOccurrences(long[] defaultOccurrences) {
        assert(defaultOccurrences.length == nbSymbols);
        OccurrenceTools.accumulate(defaultOccurrences, defaultOccurrencesCumulative);
        if (defaultOccurrencesCumulative[nbSymbols] > maxOcc) {
            for (int allSymbol = 0; allSymbol < nbSymbols; allSymbol++) {
                tmp[allSymbol] = defaultOccurrencesCumulative[allSymbol + 1] - defaultOccurrencesCumulative[allSymbol];
            }
            OccurrenceTools.normalizeAndAccumulate(tmp, defaultOccurrencesCumulative, maxBitsOcc);
        }
    }
    
    private void addContextIfNotPresent(long context) {
        assert(context < contextSize);
        if (!occurrencesCumulative.containsKey(context)) {
            occurrencesCumulative.put(context, defaultOccurrencesCumulative.clone());
        }
    }
    
    @Override
    public void incrementOccurrence(long context, int symbol) {
        assert(symbol < nbSymbols);
        addContextIfNotPresent(context);
        for (int otherSymbol = symbol + 1; otherSymbol < cumulativeArraySize; otherSymbol++) {
            occurrencesCumulative.get(context)[otherSymbol]++;
        }
        if (occurrencesCumulative.get(context)[nbSymbols] > maxOcc) {
            for (int allSymbol = 0; allSymbol < nbSymbols; allSymbol++) {
                tmp[allSymbol] = Tools.ceilingDivision(getOccurrence(context, allSymbol), 2);
            }
            OccurrenceTools.accumulate(tmp, occurrencesCumulative.get(context));
        }
    }
    
    @Override
    public int getOccurrence(long context, int symbol) {
        assert(symbol < nbSymbols);
        addContextIfNotPresent(context);
        return occurrencesCumulative.get(context)[symbol + 1] - occurrencesCumulative.get(context)[symbol];
    }
    
    @Override
    public int[] getOccurrencesCumulative(long context) {
        addContextIfNotPresent(context);
        return occurrencesCumulative.get(context);
    }
    
    @Override
    public int getOccurrencesTotal(long context) {
        addContextIfNotPresent(context);
        return occurrencesCumulative.get(context)[nbSymbols];
    }
    
}
