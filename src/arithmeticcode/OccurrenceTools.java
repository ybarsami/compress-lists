/**
 * Tools to handle occurrence tables.
 */

package arithmeticcode;

import compresslists.Tools;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;
import static arithmeticcode.ArithmeticCoderBase.MAX_OCC_BITS;

import java.util.Arrays;

/**
 *
 * @author yann
 */
public class OccurrenceTools {
    
    public static long[][] allocateOccurrences(int contextSize, int nbSymbols) {
	return new long[contextSize][nbSymbols];
    }
    
    public static int[][] allocateOccurrencesCumulative(int contextSize, int nbSymbols) {
        return new int[contextSize][nbSymbols + 1];
    }
    
    
    // Convert the occurrences to occurrences intervals.
    // Pre-condition: the sum of occurrences should be representable by an int.
    public static void accumulate(long[] occurrences, int[] occurrencesCumulative) {
        int nbSymbols = occurrences.length;
        for (int symbol = 0; symbol < nbSymbols; symbol++) {
            occurrencesCumulative[symbol + 1] = occurrencesCumulative[symbol] + (int)occurrences[symbol];
        }
    }
    
    public static void ensureTotal(long[] occurrences) {
        int nbSymbols = occurrences.length;
        long occurrencesTotal = Arrays.stream(occurrences).reduce(0, Math::addExact);
        int denomOccurrences = (1 << MAX_OCC_BITS[DEFAULT_STATE_BITS]) - 1;
        // Normalize to denomOccurrences while still preserving non-0 values.
        if (occurrencesTotal > denomOccurrences) {
            for (int symbol = 0; symbol < nbSymbols; symbol++) {
                long oldOccurrence = occurrences[symbol];
                occurrences[symbol] = (long)(((double)oldOccurrence) * ((double)denomOccurrences) / ((double)occurrencesTotal));
                if (oldOccurrence > 0 && occurrences[symbol] == 0) {
                    occurrences[symbol] = 1;
                }
            }
        }
    }
    
    
    // Convert the occurrences to occurrences intervals.
    // For each context, ensure that the sum of the occurrences of the symbols
    // do not exceed 2^MAX_OCC_BITS[DEFAULT_STATE_BITS] - 1.
    public static void ensureTotalAndAccumulate(long[][] occurrences, int[][] occurrencesCumulative) {
        int contextSize = occurrences.length;
        for (int context = 0; context < contextSize; context++) {
            ensureTotalAndAccumulate(occurrences[context], occurrencesCumulative[context]);
        }
    }
    
    // Convert the occurrences to occurrences intervals.
    // Ensure that the sum of the occurrences of the symbols do not exceed
    // 2^MAX_OCC_BITS[DEFAULT_STATE_BITS] - 1.
    public static void ensureTotalAndAccumulate(long[] occurrences, int[] occurrencesCumulative) {
        ensureTotal(occurrences);
        accumulate(occurrences, occurrencesCumulative);
    }
    
    
    // Computes the number of bits necessary to store all the occurrences.
    public static int getNbBitsMax(long[][] occurrences) {
        int contextSize = occurrences.length;
        int nbBitsMax = 0;
        for (int context = 0; context < contextSize; context++) {
            int nbSymbols = occurrences[context].length;
            for (int symbol = 0; symbol < nbSymbols; symbol++) {
                if (occurrences[context][symbol] > 0 && Tools.ceilingLog2((int)occurrences[context][symbol]) > nbBitsMax) {
                    nbBitsMax = Tools.ceilingLog2((int)occurrences[context][symbol]);
                }
            }
        }
        return nbBitsMax;
    }
    
    
    public static final void fillUniformOccurrences(long[] occurrences) {
        int nbSymbols = occurrences.length;
        for (int i = 0; i < nbSymbols; i++) {
            occurrences[i] = 1L;
        }
    }
    
    public static final void convertProbasToOccurrences(double[] probas, long[] occurrences) {
        convertProbasToOccurrences(probas, occurrences, 1 << MAX_OCC_BITS[DEFAULT_STATE_BITS]);
    }
    public static final void convertProbasToOccurrences(double[] probas, long[] occurrences, int nbOccurrencesTotal) {
        int nbSymbols = occurrences.length;
        for (int i = 0; i < nbSymbols; i++) {
            occurrences[i] = (long)(probas[i] * ((double)nbOccurrencesTotal));
            if (probas[i] > 0 && occurrences[i] == 0) {
                occurrences[i] = 1L;
            }
        }
    }
    
    
    // For each context, normalize the sum of the occurrences of the trits to 2^{nbBitsMax} - 1.
    public static void normalizeAndAccumulate(long[][] occurrences, int[][] occurrencesCumulative,
            int nbBitsMax) {
        int contextSize = occurrences.length;
        for (int context = 0; context < contextSize; context++) {
            normalizeAndAccumulate(occurrences[context], occurrencesCumulative[context], nbBitsMax);
        }
    }
    
    public static final void normalizeAndAccumulate(long[] occurrences, int[] occurrencesCumulative,
            int nbBitsMax) {
        int nbSymbols = occurrences.length;
        long occurrencesTotal = Arrays.stream(occurrences).reduce(0, Math::addExact);
        if (occurrencesTotal > 0) {
            // We set the sum of probas of all symbols, for each context, to
            // 2^{nbBitsForOccurrences} - 1 (- 1 so that all the probas can
            // always be written on nbBitsMax bits).
            int denomOccurrences = (1 << nbBitsMax) - 1;
            // Normalize to denomOccurrences while still preserving non-0 values.
            for (int symbol = 0; symbol < nbSymbols; symbol++) {
                long oldOccurrence = occurrences[symbol];
                occurrences[symbol] = (long)(((double)oldOccurrence) * ((double)denomOccurrences) / ((double)occurrencesTotal));
                if (oldOccurrence > 0 && occurrences[symbol] == 0) {
                    occurrences[symbol] = 1;
                }
            }
            // Ensure that there was no rounding error and that the sum is exactly denomOccurrences.
            long occurrencesDelta = denomOccurrences - Arrays.stream(occurrences).reduce(0, Math::addExact);
            if (occurrencesDelta != 0L) {
                int symbolMaxOcc = 0;
                long maxOcc = occurrences[0];
                for (int symbol = 1; symbol < nbSymbols; symbol++) {
                    if (maxOcc < occurrences[symbol]) {
                        symbolMaxOcc = symbol;
                        maxOcc = occurrences[symbol];
                    }
                }
                occurrences[symbolMaxOcc] += occurrencesDelta;
            }
        }
        accumulate(occurrences, occurrencesCumulative);
    }
}
