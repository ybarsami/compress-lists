/**
 * The arithmetic code implementation is adapted from the following
 * implementation. From this initial project:
 * (1) we modified the Bit(Input/Output)Stream classes to allow (a) usage of
 * in-memory bit streams in addition to file streams and (b) writing of the file
 * by buffer of bytes and not byte by byte, to optimize Output bandwidth.
 * (2) we added ContextualOccurrenceTableXXX classes to allow (a) usage of more
 * precise occurrences (long instead of int in the original XXXFrequencyTable
 * classes) and (b) sparse occurrence tables for adaptive contextual encoding.
 * 
 * Original source: "Reference arithmetic coding"
 * Copyright (c) 2018 Project Nayuki (MIT License)
 * 
 * https://www.nayuki.io/page/reference-arithmetic-coding
 * https://github.com/nayuki/Reference-arithmetic-coding
 */

package arithmeticcode;


/**
 * Provides the state and behaviors that arithmetic coding encoders and decoders
 * share.
 * @see ArithmeticEncoder
 * @see ArithmeticDecoder
 */
public abstract class ArithmeticCoderBase {
    
    /*---- Configuration fields ----*/
    
    /**
     * Number of bits for the 'low' and 'high' state variables. Must be in the
     * range [1, 62].
     * <ul>
     *   <li>For state sizes less than the midpoint of around 32, larger values
     *   are generally better - they allow a larger maximum occurrence total
     *   (maximumTotal), and they reduce the approximation error inherent in
     *   adapting fractions to integers; both effects reduce the data encoding
     *   loss and asymptotically approach the efficiency of arithmetic coding
     *   using exact fractions.</li>
     *   <li>But for state sizes greater than the midpoint, because intermediate
     *   computations are limited to the long integer type's 63-bit unsigned
     *   precision, larger state sizes will decrease the maximum occurrence
     *   total, which might constrain the user-supplied probability model.</li>
     *   <li>Therefore numStateBits=32 is recommended as the most versatile
     *   setting because it maximizes maximumTotal (which ends up being slightly
     *   over 2^30).</li>
     *   <li>Note that numStateBits=62 is legal but useless because it implies
     *   maximumTotal=1, which means an occurrence table can only support one
     *   symbol with non-zero occurrence.</li>
     * </ul>
     */
    protected final int numStateBits;
    
    /** Default number of bits used to store the state. */
    /** A value of 32 means that the occurrences can be up to 2^30 ~= 10^9 */
    public final static int DEFAULT_STATE_BITS = 32;
    
    /** Number of bits allowed for the occurrences w.r.t. numStateBits. */
    public static int[] MAX_OCC_BITS = {
        0, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
    };
    /** Number of state bits that allow occurrences up to 1 << maxOccBits. */
    public static int[] STATE_BITS = {
        62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32
    };
    /** Remark: forall 0 \leq i \leq 30, MAX_OCC_BITS[STATE_BITS[i]] == i */
    
    /** Maximum range (high+1-low) during coding (trivial), which is 2^numStateBits = 1000...000. */
    protected final long fullRange;
    
    /** The top bit at width numStateBits, which is 0100...000. */
    protected final long halfRange;
    
    /** The second highest bit at width numStateBits, which is 0010...000. This is zero when numStateBits=1. */
    protected final long quarterRange;
    
    /** Minimum range (high+1-low) during coding (non-trivial), which is 0010...010. */
    protected final long minimumRange;
    
    /** Maximum allowed total from an occurrence table at all times during coding. */
    protected final long maximumTotal;
    
    /** Bit mask of numStateBits ones, which is 0111...111. */
    protected final long stateMask;
    
    
    /*---- State fields ----*/
    
    /**
     * Low end of this arithmetic coder's current range. Conceptually has an infinite number of trailing 0s.
     */
    protected long low;
    
    /**
     * High end of this arithmetic coder's current range. Conceptually has an infinite number of trailing 1s.
     */
    protected long high;
    
    
    /*---- Constructor ----*/
    
    /**
     * Constructs an arithmetic coder, which initializes the code range.
     * @param numBits the number of bits for the arithmetic coding range
     * @throws IllegalArgumentException if stateSize is outside the range [1, 62]
     */
    public ArithmeticCoderBase(int numBits) {
        if (numBits < 1 || numBits > 62)
            throw new IllegalArgumentException("State size out of range");
        numStateBits = numBits;
        fullRange = 1L << numStateBits;
        halfRange = fullRange >>> 1;  // Non-zero
        quarterRange = halfRange >>> 1;  // Can be zero
        minimumRange = quarterRange + 2;  // At least 2
        maximumTotal = Math.min(Long.MAX_VALUE / fullRange, minimumRange);
        stateMask = fullRange - 1;
        
        low = 0;
        high = stateMask;
    }
    
    
    /*---- Methods ----*/
    
    /**
     * Updates the code range (low and high) of this arithmetic coder as a
     * result of processing the specified symbol with the specified occurrence
     * table.
     * <p>Invariants that are true before and after encoding/decoding each
     * symbol (letting fullRange = 2<sup>numStateBits</sup>):</p>
     * <ul>
     *   <li>0 &le; low &le; code &le; high &lt; fullRange. ('code' exists only
     *   in the decoder.) Therefore these variables are unsigned integers of
     *   numStateBits bits.</li>
     *   <li>low &lt; 1/2 &times; fullRange &le; high. In other words, they are
     *   in different halves of the full range.</li>
     *   <li>(low &lt; 1/4 &times; fullRange) || (high &ge; 3/4 &times; fullRange).
     *   In other words, they are not both in the middle two quarters.</li>
     *   <li>Let range = high &minus; low + 1, then fullRange/4 &lt;
     *   minimumRange &le; range &le; fullRange. These invariants for 'range'
     *   essentially dictate the maximum total that the incoming occurrence
     *   table can have, such that intermediate calculations don't overflow.</li>
     * </ul>
     * @param table the cumulative occurrence table to use
     * @param symbol the symbol that was processed
     * @throws IllegalArgumentException if the symbol has zero occurrence or the
     *         occurrence table's total is too large
     */
    public void update(int[] table, int symbol) {
        // State check
        if (low >= high || (low & stateMask) != low || (high & stateMask) != high)
            throw new AssertionError("Low or high out of range");
        long range = high - low + 1;
        if (range < minimumRange || range > fullRange)
            throw new AssertionError("Range out of range");
        
        // Cumulative occurrence table values check
        long total = table[table.length - 1];
        long symLow = table[symbol];
        long symHigh = table[symbol + 1];
        if (symLow >= symHigh)
            throw new IllegalArgumentException("Symbol has zero occurrence");
        if (total > maximumTotal)
            throw new IllegalArgumentException("Cannot code symbol because total is too large");
        
        // Update range
        long newLow  = low + symLow  * range / total;
        long newHigh = low + symHigh * range / total - 1;
        low = newLow;
        high = newHigh;
        
        // While low and high have the same top bit value, shift them out
        while (((low ^ high) & halfRange) == 0) {
            shift();
            low  = ((low  << 1) & stateMask);
            high = ((high << 1) & stateMask) | 1;
        }
        // Now low's top bit must be 0 and high's top bit must be 1
        
        // While low's top two bits are 01 and high's are 10, delete the second highest bit of both
        while ((low & ~high & quarterRange) != 0) {
            underflow();
            low = (low << 1) ^ halfRange;
            high = ((high ^ halfRange) << 1) | halfRange | 1;
        }
    }
    
    /**
     * Called to handle the situation when the top bit of {@code low} and {@code high} are equal.
     */
    protected abstract void shift();
    
    /**
     * Called to handle the situation when low=01(...) and high=10(...).
     */
    protected abstract void underflow();
}