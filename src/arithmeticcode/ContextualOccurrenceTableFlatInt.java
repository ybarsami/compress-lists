/*
 * Contextual occurrence tables that store everything in memory.
 */
package arithmeticcode;

import compresslists.Tools;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;
import static arithmeticcode.ArithmeticCoderBase.MAX_OCC_BITS;

import java.util.BitSet;

/**
 *
 * @author yann
 */
public class ContextualOccurrenceTableFlatInt extends ContextualOccurrenceTable {
    
    // Size of the occurrence arrays.
    private int contextSize;
    private int nbSymbols;
    private int cumulativeArraySize;
    
    // Occurrences arrays.
    private BitSet isPresent;
    private int[] defaultOccurrencesCumulative;
    private int[] occurrencesCumulative;
    private long[] tmp;
    private int[] tmpCumulative;
    
    private int maxBitsOcc;
    private int maxOcc;
    
    /**
     * Creates a new instance of ContextualOccurrenceTableFlatInt.
     */
    public ContextualOccurrenceTableFlatInt(int contextSize, int nbSymbols) {
        this(contextSize, nbSymbols, MAX_OCC_BITS[DEFAULT_STATE_BITS]);
    }
    public ContextualOccurrenceTableFlatInt(int contextSize, int nbSymbols, int maxBitsOcc) {
        this.contextSize = contextSize;
        this.nbSymbols = nbSymbols;
        this.cumulativeArraySize = nbSymbols + 1;
        if (maxBitsOcc > MAX_OCC_BITS[DEFAULT_STATE_BITS]) {
            throw new RuntimeException("Maximum number of bits for occurrences is " +
                    MAX_OCC_BITS[DEFAULT_STATE_BITS] +" for a 64-bit arithmetic coder.");
        }
        this.maxBitsOcc = maxBitsOcc;
        this.maxOcc = 1 << maxBitsOcc;
        isPresent = new BitSet();
        occurrencesCumulative = new int[Math.multiplyExact(contextSize, cumulativeArraySize)];
        defaultOccurrencesCumulative = new int[cumulativeArraySize];
        tmp = new long[nbSymbols];
        tmpCumulative = new int[cumulativeArraySize];
        long[] defaultOccurrences = new long[nbSymbols];
        OccurrenceTools.fillUniformOccurrences(defaultOccurrences);
        setDefaultOccurrences(defaultOccurrences);
    }
    
    @Override
    public final void setDefaultOccurrences(long[] defaultOccurrences) {
        assert(defaultOccurrences.length == nbSymbols);
        OccurrenceTools.accumulate(defaultOccurrences, defaultOccurrencesCumulative);
        if (defaultOccurrencesCumulative[nbSymbols] > maxOcc) {
            for (int allSymbol = 0; allSymbol < nbSymbols; allSymbol++) {
                tmp[allSymbol] = defaultOccurrencesCumulative[allSymbol + 1] - defaultOccurrencesCumulative[allSymbol];
            }
            OccurrenceTools.normalizeAndAccumulate(tmp, defaultOccurrencesCumulative, maxBitsOcc);
        }
    }
    
    private void addContextIfNotPresent(long context) {
        assert(context < contextSize);
        if (!isPresent.get((int)context)) {
            isPresent.set((int)context);
            System.arraycopy(
                    defaultOccurrencesCumulative, 0,
                    occurrencesCumulative, (int)context * cumulativeArraySize,
                    cumulativeArraySize);
        }
    }
    
    @Override
    public void incrementOccurrence(long context, int symbol) {
        assert(symbol < nbSymbols);
        addContextIfNotPresent(context);
        for (int otherSymbol = symbol + 1; otherSymbol < cumulativeArraySize; otherSymbol++) {
            occurrencesCumulative[(int)context * cumulativeArraySize + otherSymbol]++;
        }
        if (occurrencesCumulative[(int)context * cumulativeArraySize + nbSymbols] > maxOcc) {
            for (int allSymbol = 0; allSymbol < nbSymbols; allSymbol++) {
                tmp[allSymbol] = Tools.ceilingDivision(getOccurrence(context, allSymbol), 2);
            }
            OccurrenceTools.accumulate(tmp, tmpCumulative);
            System.arraycopy(
                    tmpCumulative, 0,
                    occurrencesCumulative, (int)context * cumulativeArraySize,
                    cumulativeArraySize);
        }
    }
    
    @Override
    public int getOccurrence(long context, int symbol) {
        assert(symbol < nbSymbols);
        addContextIfNotPresent(context);
        return occurrencesCumulative[(int)context * cumulativeArraySize + symbol + 1] - occurrencesCumulative[(int)context * cumulativeArraySize + symbol];
    }
    
    @Override
    public int[] getOccurrencesCumulative(long context) {
        addContextIfNotPresent(context);
        System.arraycopy(
                occurrencesCumulative, (int)context * cumulativeArraySize,
                tmpCumulative, 0,
                cumulativeArraySize);
        return tmpCumulative;
    }
    
    @Override
    public int getOccurrencesTotal(long context) {
        addContextIfNotPresent(context);
        return occurrencesCumulative[(int)context * cumulativeArraySize + nbSymbols];
    }
    
}
