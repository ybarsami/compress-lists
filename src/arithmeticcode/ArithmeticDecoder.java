/**
 * The arithmetic code implementation is adapted from the following
 * implementation. From this initial project:
 * (1) we modified the Bit(Input/Output)Stream classes to allow (a) usage of
 * in-memory bit streams in addition to file streams and (b) writing of the file
 * by buffer of bytes and not byte by byte, to optimize Output bandwidth.
 * (2) we added ContextualOccurrenceTableXXX classes to allow (a) usage of more
 * precise occurrences (long instead of int in the original XXXFrequencyTable
 * classes) and (b) sparse occurrence tables for adaptive contextual encoding.
 * 
 * Original source: "Reference arithmetic coding"
 * Copyright (c) 2018 Project Nayuki (MIT License)
 * 
 * https://www.nayuki.io/page/reference-arithmetic-coding
 * https://github.com/nayuki/Reference-arithmetic-coding
 */

package arithmeticcode;

import io.BitInputStream;

import java.util.Objects;


/**
 * Reads from an arithmetic-coded bit stream and decodes symbols. Not thread-safe.
 * @see ArithmeticEncoder
 */
public final class ArithmeticDecoder extends ArithmeticCoderBase {
    
    /*---- Fields ----*/
    
    // The underlying bit input stream (not null).
    private BitInputStream input;
    
    // The current raw code bits being buffered, which is always in the range [low, high].
    private long code;
    
    
    /*---- Constructor ----*/
    
    /**
     * Constructs an arithmetic coding decoder based on the specified bit input
     * stream, and fills the code bits.
     * @param numBits the number of bits for the arithmetic coding range
     * @param in the bit input stream to read from
     * @throws NullPointerException if the input steam is {@code null}
     * @throws IllegalArgumentException if stateSize is outside the range [1, 62]
     */
    public ArithmeticDecoder(int numBits, BitInputStream in) {
        super(numBits);
        input = Objects.requireNonNull(in);
        code = 0;
        for (int i = 0; i < numStateBits; i++)
            code = code << 1 | readCodeBit();
    }
    
    
    /*---- Methods ----*/
    
    /**
     * Decodes the next symbol based on the specified occurrence table and
     * returns it. Also updates this arithmetic coder's state and may read in
     * some bits.
     * @param table the cumulative occurrence table to use
     * @return the next symbol
     * @throws NullPointerException if the occurrence table is {@code null}
     * @throws IllegalArgumentException if the occurrence table's total is too
     *         large
     */
    public int read(int[] table) {
        // Translate from coding range scale to occurrence table scale
        long total = table[table.length - 1];
        if (total > maximumTotal)
            throw new IllegalArgumentException("Cannot decode symbol because total is too large");
        long range = high - low + 1;
        long offset = code - low;
        long value = ((offset + 1) * total - 1) / range;
        if (value * range / total > offset)
            throw new AssertionError();
        if (value < 0 || value >= total)
            throw new AssertionError();
        
        // A kind of binary search. Find highest symbol such that table.getLow(symbol) <= value.
        int start = 0;
        int end = table.length - 1;
        while (end - start > 1) {
            int middle = (start + end) >>> 1;
            if (table[middle] > value)
                end = middle;
            else
                start = middle;
        }
        if (start + 1 != end)
            throw new AssertionError();
        
        int symbol = start;
        if (offset < table[symbol] * range / total || table[symbol + 1] * range / total <= offset)
            throw new AssertionError();
        update(table, symbol);
        if (code < low || code > high)
            throw new AssertionError("Code out of range");
        return symbol;
    }
    
    @Override
    protected void shift() {
        code = ((code << 1) & stateMask) | readCodeBit();
    }
    
    @Override
    protected void underflow() {
        code = (code & halfRange) | ((code << 1) & (stateMask >>> 1)) | readCodeBit();
    }
    
    // Returns the next bit (0 or 1) from the input stream. The end of stream is
    // treated as an infinite number of trailing zeros.
    private int readCodeBit() {
        try {
            return input.getNextBit();
        } catch (IndexOutOfBoundsException e) {
            return 0;
        }
    }
    
    public boolean checkBitInputStreamEquality(BitInputStream toCheck) {
        return input.equals(toCheck);
    }
}
