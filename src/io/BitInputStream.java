/**
 * Maintain a stream of bits that can be read one by one.
 */

package io;

/**
 *
 * @author yann
 */
public abstract class BitInputStream {
    
    /*
     * Get the next bit from this bit stream.
     * Throws IndexOutOfBoundsException when the end of stream is reached.
     */
    public abstract int getNextBit() throws IndexOutOfBoundsException;

}
