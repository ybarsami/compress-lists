/**
 * Allows a DataOutputStream to be written bit by bit. The most little piece of
 * data that can be written to a DataOutputStream is a byte, so we must here
 * write 8 bits by 8 bits.
 * When using this class, it is really important to always wrap the creation of
 * the BitOutputStreamFile inside a try-with-resources, or to call close(), to
 * ensure that all bits are correctly written.
 */

package io;

import compresslists.Tools;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class BitOutputStreamFile extends BitOutputStream implements AutoCloseable {
    
    // The BitOutputStream here comes from an external file, converted to a data
    // output stream.
    private final DataOutputStream dataOutputStream;
    // It is not possible to directly write the file bit by bit, so we write the
    // file byte by byte (8 bits by 8 bits). When we write a bit, we copy inside
    // this array...
    private final int[] currentBits = new int[nbBitsPerByte];
    private int nbCurrentBitsWritten;
    // ... when it's full (nbCurrentBitsWritten == 8), we convert it to a byte
    // and write it in a buffer byte array...
    // (size of the buffer is the same as in Files.java)
    private static final int BUFFER_SIZE = 8192;
    private final byte[] currentBytes = new byte[BUFFER_SIZE];
    private int nbCurrentBytesWritten;
    // ... when it's full (nbCurrentBytesWritten == BYTE_BUFFER_SIZE) or when we
    // have to close this BitOutputStream, we write the byte array in the
    // DataOutputStream.
    
    /**
     * Creates a new instance of BitOutputStreamFile.
     */
    public BitOutputStreamFile(DataOutputStream dataOutputStream) {
        this.dataOutputStream = dataOutputStream;
        this.nbCurrentBitsWritten = 0;
        this.nbCurrentBytesWritten = 0;
    }
    
    /*
     * Write a bit to this bit stream.
     * In the general case, we write the bit at position nbCurrentBitsWritten
     * inside the array currentBits (of size nbBitsPerByte = 8).
     * If we reach the end of the array currentBits, we convert the array to a
     * byte and write it in the dataOutputStream.
     * The bits are written in big endian by default, but this can be modified
     * by setting Tools.isBigEndian to false.
     * In both cases, we update nbCurrentBitsWritten.
     */
    @Override
    public void writeBit(int value) throws IndexOutOfBoundsException {
        // Write a bit.
        currentBits[nbCurrentBitsWritten++] = value;
        // If the array is full, write the byte in the byte buffer.
        if (nbCurrentBitsWritten == nbBitsPerByte) {
            currentBytes[nbCurrentBytesWritten++] = Tools.bitArray2byte(currentBits);
            nbCurrentBitsWritten = 0;
            // If the buffer is full, write the bytes in the file.
            if (nbCurrentBytesWritten == BUFFER_SIZE) {
                try {
                    dataOutputStream.write(currentBytes);
                    nbCurrentBytesWritten = 0;
                } catch (IOException e) {
                    throw new IndexOutOfBoundsException();
                }
            }
        }
    }
    
    /**
     * Closes this stream but not the underlying DataOutputStream.
     * If called when some bits have not been output on the DataOutputStream,
     * then a padding made of "0" bits is performed before closing the stream.
     * @throws IOException if an I/O exception occurred.
     */
    @Override
    public void close() throws IOException {
        // Warning: Do not close the underlying DataOutputStream, because this
        // is called in the try-with-resources construct in writeGapArray, and
        // writeGapArray must be callable multiple times, so the stream must
        // stay open.
        while (nbCurrentBitsWritten != 0) {
            writeBit(0);
        }
        dataOutputStream.write(currentBytes, 0, nbCurrentBytesWritten);
        nbCurrentBytesWritten = 0;
    }
    
    @Override
    public boolean equals(Object object) {
        return object instanceof BitOutputStreamFile &&
                ((BitOutputStreamFile)object).checkDataOutputStreamEquality(dataOutputStream);
    }
    
    @Override
    public int hashCode() {
        return dataOutputStream.hashCode();
    }
    
    public boolean checkDataOutputStreamEquality(DataOutputStream toCheck) {
        return toCheck == dataOutputStream;
    }

}
