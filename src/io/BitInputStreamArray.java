/**
 * Allows a BitSequence to be read bit by bit.
 */

package io;

import bitlistencoding.BitSequence;

/**
 *
 * @author yann
 */
public class BitInputStreamArray extends BitInputStream {
    
    private final BitSequence bitSequence;
    
    private int nbBitsRead;
    
    /**
     * Creates a new instance of BitInputStreamArray.
     */
    public BitInputStreamArray(BitSequence bitSequence) {
        this.bitSequence = bitSequence;
        this.nbBitsRead = 0;
    }
    
    /*
     * Get the next bit from this bit stream.
     */
    @Override
    public int getNextBit() throws IndexOutOfBoundsException {
        return bitSequence.get(nbBitsRead++);
    }
    
    @Override
    public boolean equals(Object object) {
        return object instanceof BitInputStreamArray &&
                ((BitInputStreamArray)object).checkBitSequenceEquality(bitSequence);
    }
    
    @Override
    public int hashCode() {
        return bitSequence.hashCode();
    }
    
    public boolean checkBitSequenceEquality(BitSequence toCheck) {
        return toCheck == bitSequence;
    }

}
