/**
 * Allows a DataInputStream to be read bit by bit. The most little piece of data
 * that can be read on a DataInputStream is a byte, so we must here read 8 bits
 * by 8 bits.
 */

package io;

import static compresslists.Tools.*;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class BitInputStreamFile extends BitInputStream implements AutoCloseable {
    
    // The BitInputStream here comes from an external file, converted to a data
    // input stream.
    private final DataInputStream dataInputStream;
    // It is not possible to directly read the file bit by bit, so we read the
    // file byte by byte (8 bits by 8 bits). When we load a byte, we copy the 8
    // bits inside this array...
    private int[] currentBits;
    // ... and we keep track of the number of bits we have already used among
    // those 8 bits (when we reach 8, we reload a new byte, and so on).
    private int nbCurrentBitsRead;
    // We keep track of whether the end of file was reached or not, in case we
    // keep asking bits after the exception.
    private boolean ioexceptionRaised = false;
    
    /**
     * Creates a new instance of BitInputStreamFile.
     */
    public BitInputStreamFile(DataInputStream dataInputStream) {
        this.dataInputStream = dataInputStream;
        this.nbCurrentBitsRead = nbBitsPerByte;
    }
    
    /*
     * Get the next bit from this bit stream.
     * In the general case, we read the bit at position nbCurrentBitsRead inside
     * the array currentBits (of size nbBitsPerByte = 8).
     * If we reach the end of the array currentBits, we read a new set of
     * nbBitsPerByte = 8 bits from the dataInputStream.
     * The bits are read in big endian by default, but this can be modified
     * by setting Tools.isBigEndian to false.
     * In both cases, we update nbCurrentBitsRead.
     */
    @Override
    public int getNextBit() throws IndexOutOfBoundsException {
        // If we already reached the end of file.
        if (ioexceptionRaised) {
            throw new IndexOutOfBoundsException();
        }
        // If there are no more bits to read, read a new byte in the file.
        if (nbCurrentBitsRead == nbBitsPerByte) {
            try {
                nbCurrentBitsRead = 0;
                currentBits = byte2bitArray(dataInputStream.readByte());
            } catch (IOException e) {
                ioexceptionRaised = true;
                throw new IndexOutOfBoundsException();
            }
        }
        // Extract a bit.
        return currentBits[nbCurrentBitsRead++];
    }
    
    /**
     * Closes this stream but not the underlying DataInputStream.
     * @throws IOException if an I/O exception occurred.
     */
    @Override
    public void close() throws IOException {
        // Warning: Do not close the underlying DataInputStream, because this
        // might be called in try-with-resources constructs that could be
        // called multiple times, see close() in BitOutputStreamFile.java.
    }
    
    @Override
    public boolean equals(Object object) {
        return object instanceof BitInputStreamFile &&
                ((BitInputStreamFile)object).checkDataInputStreamEquality(dataInputStream);
    }
    
    @Override
    public int hashCode() {
        return dataInputStream.hashCode();
    }
    
    public boolean checkDataInputStreamEquality(DataInputStream toCheck) {
        return toCheck == dataInputStream;
    }

}
