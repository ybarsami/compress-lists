/**
 * Allows a BitSequence to be written bit by bit.
 */

package io;

import bitlistencoding.BitSequence;

/**
 *
 * @author yann
 */
public class BitOutputStreamArray extends BitOutputStream {
    
    private final BitSequence bitSequence;
    
    /**
     * Creates a new instance of BitOutputStreamArray.
     */
    public BitOutputStreamArray(BitSequence bitSequence) {
        this.bitSequence = bitSequence;
    }
    
    @Override
    public void writeBit(int value) throws IndexOutOfBoundsException {
        bitSequence.add(value);
    }
    
    @Override
    public boolean equals(Object object) {
        return object instanceof BitOutputStreamArray &&
                ((BitOutputStreamArray)object).checkBitSequenceEquality(bitSequence);
    }
    
    @Override
    public int hashCode() {
        return bitSequence.hashCode();
    }
    
    public boolean checkBitSequenceEquality(BitSequence toCheck) {
        return toCheck == bitSequence;
    }

}
