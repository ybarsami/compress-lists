/*
 * Enables the reading of a dataset stored in main memory.
 * (for small datasets only)
 */
package io;

import arrays.IntsRef;
import compresslists.Tools;
import java.io.BufferedReader;
import java.io.FileReader;
import tritlistencoding.TritSequence;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author yann
 */
public class DatasetInfo extends DatasetReader {
    
    public ArrayList<int[]> gapArrayList;
    
    /**
     * Creates a new instance of DatasetInfo.
     */
    public DatasetInfo(ArrayList<int[]> gapArrayList, int nbDocuments) {
        this.gapArrayList = gapArrayList;
        this.nbDocuments = nbDocuments;
        collectStatistics();
    }
    
    public DatasetInfo(int[] gapArray, int nbDocuments) {
        this.gapArrayList = new ArrayList<>();
        gapArrayList.add(gapArray);
        this.nbDocuments = nbDocuments;
        collectStatistics();
    }
    
    public DatasetInfo(DatasetReader datasetReader) {
        datasetReader.restartReader();
        nbDocuments = datasetReader.getNbDocuments();
        gapArrayList = new ArrayList<>();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            intArrayRef = datasetReader.getNextGapArray();
            gapArrayList.add(Arrays.copyOf(intArrayRef.ints, intArrayRef.length));
        }
        collectStatistics();
    }
    
    @Override
    protected final void collectStatistics() {
        nbWords = gapArrayList.size();
        nbPointers = 0;
        minGapArrayLength = Integer.MAX_VALUE;
        maxGapArrayLength = 0;
        gapArrayLengths = new ArrayList<>();
        for (int[] gapArrayLocal : gapArrayList) {
            int nbGapsLocal = gapArrayLocal.length;
            nbPointers = Math.addExact(nbPointers, nbGapsLocal);
            if (nbGapsLocal < minGapArrayLength) {
                minGapArrayLength = nbGapsLocal;
            }
            if (nbGapsLocal > maxGapArrayLength) {
                maxGapArrayLength = nbGapsLocal;
            }
            gapArrayLengths.add(nbGapsLocal);
        }
    }
    
    /*
     * Returns a new gap array list containing only the gap arrays whose length
     * is >= minLength and <= maxLength.
     */
    public DatasetInfo cutByLengths(int minLength, int maxLength) {
        ArrayList<int[]> gapArrayListNew = new ArrayList<>();
        for (int[] gapArrayLocal : gapArrayList) {
            if (gapArrayLocal.length >= minLength && gapArrayLocal.length <= maxLength) {
                gapArrayListNew.add(gapArrayLocal);
            }
        }
        return new DatasetInfo(gapArrayListNew, nbDocuments);
    }
    
    /*
     * Returns a new gap array list containing only the gap arrays whose length
     * is >= minLength.
     */
    public DatasetInfo cutByMinimumLength(int minLength) {
        ArrayList<int[]> gapArrayListNew = new ArrayList<>();
        for (int[] gapArrayLocal : gapArrayList) {
            if (gapArrayLocal.length >= minLength) {
                gapArrayListNew.add(gapArrayLocal);
            }
        }
        return new DatasetInfo(gapArrayListNew, nbDocuments);
    }
    
    /*
     * Transforms this list of gap arrays to a list of gap array lengthes, i.e.,
     * instead of storing each gap, it stores the number of bits needed for each
     * gap, in base 2.
     */
    public DatasetInfo toGapArrayLength() {
        ArrayList<int[]> gapArrayLengthList = new ArrayList<>();
        TritSequence tritSequence = new TritSequence();
        for (int[] gapArray : gapArrayList) {
            Tools.intArray2tritSequence(new IntsRef(gapArray), tritSequence);
            IntsRef gapSizeArrayRef = new IntsRef(gapArray.length);
            Tools.tritSequence2intSizeArray(tritSequence, gapSizeArrayRef);
            gapArrayLengthList.add(gapSizeArrayRef.ints);
        }
        return new DatasetInfo(gapArrayLengthList, nbDocuments);
    }
    
    /*
     * Apply a given permutation to the lists in the gap array list.
     */
    public DatasetInfo permute(String fileNameCSV) {
        try (FileReader fileReader = new FileReader(fileNameCSV);
                BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line = bufferedReader.readLine();
            String[] splitLine = line.split(", ");
            assert(splitLine.length == gapArrayList.size());
            int[] sigma = new int[splitLine.length];
            int idWord = 0;
            for (String str : splitLine) {
                sigma[idWord++] = Integer.parseInt(str);
            }
            return permute(sigma);
        } catch (IOException e) {
            System.out.println("IOException in file '" + fileNameCSV + "'.");
            e.printStackTrace();
            return this;
        }
    }
    /*
     * Apply a given permutation to the lists in the gap array list.
     * N.B.: sigma is in [1..n].
     */
    public DatasetInfo permute(int[] sigma) {
        assert(sigma.length == gapArrayList.size());
        ArrayList<int[]> randomizedGapArrayList = new ArrayList<>();
        for (int i = 0; i < sigma.length; i++) {
            randomizedGapArrayList.add(gapArrayList.get(sigma[i] - 1));
        }
        return new DatasetInfo(randomizedGapArrayList, nbDocuments);
    }
    
    /*
     * Randomize lists in the gap array list.
     */
    public DatasetInfo randomize() {
        SecureRandom secureRandomizer = new SecureRandom();
        int n = gapArrayList.size();
        // Generating a permutation of [1..n].
        int[] sigma = new int[n];
        for (int i = 0; i < n; i++) {
            sigma[i] = i + 1;
        }
        for (int i = 0; i < n; i++) {
            int k = secureRandomizer.nextInt(n - i);
            int tmp = sigma[k];
            sigma[k] = sigma[n - i - 1];
            sigma[n - i - 1] = tmp;
        }
        // Apply the permutation.
        return permute(sigma);
    }
    
    /*
     * Sort the gap array list by ascending order of length of gap array.
     */
    public void sortByDensity() {
        final Comparator<int[]> comparatorOnLengths =
                (int[] e1, int[] e2) -> Integer.compare(e1.length, e2.length);
        Collections.sort(gapArrayList, comparatorOnLengths);
        collectStatistics();
    }
    
    
    @Override
    protected void reopenFile() {}
    
    @Override
    protected void goToGapArrays() throws IOException {}
    
    /*
     * Get the next gap array from this dataset.
     */
    @Override
    protected void readNextArray(boolean asGapArray) {
        if (nbIntArraysRead >= nbWords) {
            throw new RuntimeException("Always call restartReader() before iterating with getNext(Gap/Document)Array() !");
        }
        int[] gapArray = gapArrayList.get(nbIntArraysRead++);
        if (asGapArray) {
            intArrayRef = new IntsRef(gapArray.clone());
        } else {
            Tools.gapArray2documentArray(new IntsRef(gapArray), intArrayRef);
        }
    }
    @Override
    protected void skipNextArrayAux() {}
}
