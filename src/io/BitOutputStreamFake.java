/**
 * Just counts the number of bits written.
 */

package io;

/**
 *
 * @author yann
 */
public class BitOutputStreamFake extends BitOutputStream {
    
    long nbBitsWritten = 0L;
    
    /**
     * Creates a new instance of BitOutputStreamFake.
     */
    public BitOutputStreamFake() {}
    
    @Override
    public void writeBit(int value) throws IndexOutOfBoundsException {
        try {
            nbBitsWritten = Math.addExact(nbBitsWritten, 1);
        } catch (ArithmeticException e) {
            throw new IndexOutOfBoundsException();
        }
    }
    
    public long getNbBitsWrittenAndReset() {
        long nbBits = nbBitsWritten;
        nbBitsWritten = 0L;
        return nbBits;
    }

}
