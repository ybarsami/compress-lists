/*
 * Enables the reading of a dataset stored in a file in the ASCII format.
 */
package io;

import compresslists.Tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author yann
 */
public abstract class DatasetReaderASCII extends DatasetReader {
    
    // The GapArrayInputStream comes from an external file, converted to a
    // buffered reader.
    private FileReader fileReader = null;
    protected BufferedReader bufferedReader = null;
    
    /**
     * Creates a new instance of GapArrayInputStreamCsv.
     */
    public DatasetReaderASCII(String filename) {
        super(filename);
    }
    
    @Override
    protected void reopenFile() {
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
                fileReader.close();
            }
            fileReader = new FileReader(filename);
            bufferedReader = new BufferedReader(fileReader);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    protected abstract void readGapArray(String line);
    
    protected void readDocumentArray(String line) {
        readGapArray(line);
        Tools.gapArray2documentArray(intArrayRef, intArrayRef);
    }
    
    /*
     * Get the next gap array from this dataset.
     */
    @Override
    protected void readNextArray(boolean asGapArray) {
        if (bufferedReader == null || nbIntArraysRead >= nbWords) {
            throw new RuntimeException("Always call restartReader() before iterating with getNext(Gap/Document)Array() !");
        }
        try {
            String line = bufferedReader.readLine();
            if (line == null) {
                throw new IOException();
            }
            if (asGapArray) {
                readGapArray(line);
            } else {
                readDocumentArray(line);
            }
            nbIntArraysRead++;
        } catch (IOException e) {}
    }
    @Override
    protected void skipNextArrayAux() throws IOException {
        if (bufferedReader == null) {
            throw new RuntimeException("Always call restartReader() before iterating with getNext(Gap/Document)Array() !");
        }
        bufferedReader.readLine();
    }
    
}
