/*
 * Enables the reading of a dataset stored in a file.
 */
package io;

import compresslists.Tools;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author yann
 */
public abstract class DatasetReaderBinary extends DatasetReader {
    
    // The GapArrayInputStream comes from an external file, converted to a
    // data input stream.
    private FileInputStream fileInputStream = null;
    protected DataInputStream dataInputStream = null;
    
    /**
     * Creates a new instance of DatasetReaderBinary.
     */
    public DatasetReaderBinary(String filename) {
        super(filename);
    }
    
    @Override
    protected void reopenFile() {
        try {
            if (dataInputStream != null) {
                dataInputStream.close();
                fileInputStream.close();
            }
            fileInputStream = new FileInputStream(filename);
            dataInputStream = new DataInputStream(fileInputStream);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    protected abstract void readNextGapArray() throws IOException;
    
    protected void readNextDocumentArray() throws IOException {
        readNextGapArray();
        Tools.gapArray2documentArray(intArrayRef, intArrayRef);
    }
    
    /*
     * Get the next gap array from this dataset.
     */
    @Override
    protected void readNextArray(boolean asGapArray) {
        if (dataInputStream == null || nbIntArraysRead >= nbWords) {
            throw new RuntimeException("Always call restartReader() before iterating with getNext(Gap/Document)Array() !");
        }
        try {
            if (asGapArray) {
                readNextGapArray();
            } else {
                readNextDocumentArray();
            }
            nbIntArraysRead++;
        } catch (IOException e) {}
    }
    @Override
    protected void skipNextArrayAux() throws IOException {
        if (dataInputStream == null) {
            throw new RuntimeException("Always call restartReader() before iterating with getNext(Gap/Document)Array() !");
        }
        readNextGapArray();
    }
    
}
