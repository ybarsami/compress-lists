/*
 * Description of the dataset file formats.
 * Constants to enumerate the different datasets provided in the project.
 * Helper functions to read datasets.
 */
package io;

import compresslists.Tools;
import gaparrayencoding.Method;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author yann
 */
public class Dataset {
    
    public static enum Format {
/*
 * Format chosen for our datasets.
 * It is a text file in the comma-separated values format.
 *    [nbWords=N]\n
 *    [nbDocuments]\n
 *    [gap(1,1)],[gap(1,2)],...,[gap(1,length1)]\n
 *    [gap(2,1)],[gap(2,2)],...,[gap(2,length2)]\n
 *    ...
 *    [gap(N,1)],[gap(N,2)],...,[gap(N,lengthN)]
 *
 * Gaps gap(i,j) are in [1; nbDocuments].
 */
        FORMAT_CSV("csv", new String[] { "csv" }),
/*
 * Format chosen for datasets from the uiHRDC implementation.
 * It is not a text file, it is a file containing ints (no spaces)
 *    [nbWords=N]
 *    [maxPostValue]
 *    [length1][post(1,1)][post(1,2)]...[post(1,length1)]
 *    [length2][post(2,1)][post(2,2)]...[post(2,length2)]
 *    ...
 *    [lengthN][post(N,1)][post(N,2)]...[post(N,lengthN)]
 *
 * Posting values post(i,j) are in [0; maxPostValue].
 */
        FORMAT_UIHRDC("uiHRDC", new String[] {}),
/*
 * Format chosen for datasets from the PISA implementation.
 * It is not a text file, it is a file containing ints (no spaces)
 *    [1][nbDocuments]
 *    [length1][post(1,1)][post(1,2)]...[post(1,length1)]
 *    [length2][post(2,1)][post(2,2)]...[post(2,length2)]
 *    ...
 *    [lengthN][post(N,1)][post(N,2)]...[post(N,lengthN)]
 *
 * Posting values post(i,j) are in [0; nbDocuments - 1].
 */
        FORMAT_PISA_DOCS("PISA_docs", new String[] { "docs" }),
/*
 * Format chosen for datasets from the FastPFOR implementation.
 * (Integer Compression 2014).
 * It is not a text file, it is a file containing ints (no spaces)
 *    [length1][post(1,1)][post(1,2)]...[post(1,length1)]
 *    [length2][post(2,1)][post(2,2)]...[post(2,length2)]
 *    ...
 *    [lengthN][post(N,1)][post(N,2)]...[post(N,lengthN)]
 *
 * Posting values post(i,j) start from 0.
 */
        FORMAT_IC2014("IC2014", new String[] { "sorted", "unsorted" }),
/*
 * A file generated with this library.
 * (Compress Lists 2020).
 */
        FORMAT_CL2020("CL2020", new String[] {
            "MetaBatch",
            "MetaCluster",
            "Afor3",
            "ArithmeticTrits",
            "ArithmeticXTrits",
            "BernoulliGlobal",
            "BernoulliLocal",
            "Binary",
            "BitVector",
            "ByBitComposition",
            "ByBitVector",
            "ByBlock",
            "ByBlockNoPadding",
            // Methods inherited from JavaFastPFOR
            "BinaryPacking",
            "FastPFOR",
            "NewPFD",
            "OptPFD",
            "Simple9",
            "Simple16",
            // End methods inherited from JavaFastPFOR
            "ByQuatritConcatenation",
            "ByQuatritList",
            "BySplitB2B01",
            "BySplitTLB01",
            "ByTritConcatenation",
            "ByTritList",
            "ContextBit",
            "ContextTrit",
            "Delta",
            "DynamicEliasFano",
            "EliasFano",
            "EliasFanoOptBlock",
            "ContextLengthBit",
            "ContextLengthTestingOnly1",
            "ContextLengthTestingOnly2",
            "ContextLengthTestingOnly3",
            "ContextLengthTrit",
            "ContextLengthTritBis",
            "Gamma",
            "GolombGlobal",
            "GolombLocal",
            "InformationTheoretic",
            "InformationTheoreticByElement",
            "DynamicInterpolative",
            "DynamicInterpolativeOptimistic",
            "Interpolative",
            "PackedArithmetic",
            "QMX",
            "QuasiSuccinct",
            "QuasiSuccinctApproxBlock",
            "QuasiSuccinctOptBlock",
//            "RePair",
            "Rice",
            "Simple8b",
            "Trits",
            "Unary",
            "VariableByte",
            "VariableNibble",
            "VariableXBits",
            "Zeta"
        });
        
        private final String name;
        private final String[] extensions;
        
        Format(String name, String[] extensions) {
            this.name = name;
            this.extensions = extensions;
        }
        
        public String getName() { return name; }
        public String[] getExtensions() { return extensions; }
    };
    
    public static final Format getFormat(String formatString) {
        Format formats[] = Format.values();
        for (Format format : formats) {
            if (format.getName().equals(formatString)) {
                return format;
            }
        }
        throw new RuntimeException("Unknown dataset format.");
    }
    public static final String getFormatType(String extension) {
        Format formats[] = Format.values();
        for (Format format : formats) {
            for (String possibleExtension : format.getExtensions()) {
                if (extension.startsWith(possibleExtension)) {
                    return format.getName();
                }
            }
        }
        return Format.FORMAT_CL2020.getName();
    }
    public static final String[] getFormatNames() {
        Format formats[] = Format.values();
        String[] names = new String[formats.length];
        for (int i = 0; i < formats.length; i++) {
            names[i] = formats[i].getName();
        }
        return names;
    }
    
    
    // All datasets are stored in this git repository, under the "datasets"
    // folder.
    public static final String datasetsFolderName = "datasets";
    
    private final String datasetFileName;
    public final Format datasetFormat;
    
    /**
     * Creates a new instance of Dataset.
     */
    public Dataset(String name) {
        this(name, Format.FORMAT_CSV);
    }
    public Dataset(String name, Format datasetFormat) {
        this.datasetFileName = name;
        this.datasetFormat = datasetFormat;
    }
    
    /*
     * Reads a dataset file and converts it into a list of arrays. There are as
     * many arrays as there are different words in the original dataset, and
     * each array is the gaps corresponding to a word in the original dataset.
     */
    public DatasetReader getDatasetReader() {
        switch(datasetFormat) {
            case FORMAT_CSV:
                return datasetReaderCsv(datasetFileName);
            case FORMAT_UIHRDC:
                return datasetReaderUiHRDC(datasetFileName);
            case FORMAT_PISA_DOCS:
                return datasetReaderPISA(datasetFileName);
            case FORMAT_IC2014:
                return datasetReaderIC2014(datasetFileName);
            case FORMAT_CL2020:
                int indexExtension = datasetFileName.lastIndexOf(".");
                int indexParenthesis = datasetFileName.substring(indexExtension).indexOf("(");
                int indexParameters = indexParenthesis == -1
                        ? datasetFileName.substring(indexExtension).indexOf("_")
                        : indexParenthesis;
                String indexName = indexParameters == -1
                        ? datasetFileName.substring(indexExtension + 1)
                        : datasetFileName.substring(indexExtension + 1, indexExtension + indexParameters);
                try {
                    Class methodClass = Class.forName("gaparrayencoding.Method" + indexName);
                    Method method = (Method)methodClass.getDeclaredConstructor().newInstance();
                    try (FileInputStream fileInputStream = new FileInputStream(datasetFileName);
                            DataInputStream in = new DataInputStream(fileInputStream)) {
                        method.nbDocuments = in.readInt();
                        method.importNeededInformation(in);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException("Unable to open file '" + datasetFileName + "'.");
                    } catch (IOException e) {
                        throw new RuntimeException("IOException in file '" + datasetFileName + "'.");
                    }
                    return method.importFromFile(datasetFileName);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
                    throw new RuntimeException(indexName + " is not a valid method for CompressLists." +
                            " Valid methods are: " +
                            String.join(", ", Format.FORMAT_CL2020.getExtensions()) + ".");
                }
            default:
                throw new RuntimeException("Dataset format not handled.");
        }
    }
    
    public String getDatasetFileName() {
        return datasetFileName;
    }
    
    public String getDatasetName() {
        int indexSlash = datasetFileName.lastIndexOf(File.separatorChar);
        String fileName = indexSlash == -1 ? datasetFileName : datasetFileName.substring(indexSlash + 1);
        if (datasetFormat == Format.FORMAT_IC2014) {
            return fileName;
        }
        int indexDot = fileName.lastIndexOf(".");
        return indexDot == -1 ? fileName : fileName.substring(0, indexDot);
    }
    
    public String getClusterFileName() {
        int indexSlash = datasetFileName.lastIndexOf(File.separatorChar);
        String fileName = indexSlash == -1 ? datasetFileName : datasetFileName.substring(indexSlash + 1);
        int indexUnion = fileName.lastIndexOf("-");
        int indexReordering = indexUnion == -1
                ? fileName.lastIndexOf(".")
                : indexUnion;
        return (indexReordering == -1
                ? datasetFileName
                : datasetFileName.substring(0, indexSlash + 1 + indexReordering)) +
                ".clusters";
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some datasets than can be used for testing.
    ////////////////////////////////////////////////////////////////////////////
    
    // Small dataset
    public static final Dataset FABLES_NAME = new Dataset(
            datasetsFolderName + File.separatorChar + "Fables-name.csv");
    // Medium-size e-mail dataset
    public static final Dataset DASOVICH_BISECTION_DENSITY = new Dataset(
            datasetsFolderName + File.separatorChar + "dasovich-bisection-sortedByDensity.csv");
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Helper functions to read little-endian sequences of integers.
    ////////////////////////////////////////////////////////////////////////////
    
    private static ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[4]).order(ByteOrder.LITTLE_ENDIAN);
    
    /*
     * Reads a 32-bit little-endian integer from the file.
     */
    private static int read32BitLittleEndianInt(DataInputStream in) throws IOException {
        if (in.read(byteBuffer.array()) == 4) {
            byteBuffer.rewind();
            return byteBuffer.getInt();
        } else {
            throw new IOException("Not able to read a 32-bit little-endian int from the file.");
        }
    }
    
    /*
     * Reads a sequence of ints from the file, arranged in:
     * length, int_1, int_2, ..., int_length.
     * Each int is a a 32-bit little-endian integer.
     * This function also adds one to each of the values of the sequence (not
     * the length), because we view integers in [1; nbDocuments] and other
     * datasets view integers in [0; nbDocuments - 1].
     */
    private static int[] readIntSequence(DataInputStream in) throws IOException {
        int sequenceSize = read32BitLittleEndianInt(in);
        int[] intSequence = new int[sequenceSize];
        readIntSequence(in, sequenceSize, intSequence);
        return intSequence;
    }
    private static void readIntSequence(DataInputStream in, int sequenceSize, int[] intSequence) throws IOException {
        for (int i = 0; i < sequenceSize; i++) {
            intSequence[i] = read32BitLittleEndianInt(in) + 1;
        }
    }
    
    private static void skipFully(DataInputStream in, long nbBytesToSkip) throws IOException {
        long nbBytesSkipped = 0;
        while (nbBytesSkipped < nbBytesToSkip) {
            nbBytesSkipped += in.skip(nbBytesToSkip - nbBytesSkipped);
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Helper functions to read some datasets. If you plan to use a different
    // format than those ones, just provide a new function.
    ////////////////////////////////////////////////////////////////////////////
    
    public static DatasetReader datasetReaderCsv(String baseFileName) {
        return new DatasetReaderASCII(baseFileName) {
            @Override
            protected void goToGapArrays() throws IOException {
                bufferedReader.readLine();
                bufferedReader.readLine();
            }
            
            @Override
            protected void readGapArray(String line) {
                // It may happen that a gapArray is empty when computing the reference lists.
                if (line.equals("")) {
                    return;
                }
                String[] splitLine = line.split(",");
                assert(intArrayRef.length >= splitLine.length);
                int idGap = 0;
                for (String str : splitLine) {
                    intArrayRef.ints[idGap++] = Integer.parseInt(str);
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileReader fileReader = new FileReader(filename);
                        BufferedReader br = new BufferedReader(fileReader)) {
                    // First line of the file is the number of words.
                    nbWords = Integer.parseInt(br.readLine());
                    // Second line of the file is the number of documents.
                    nbDocuments = Integer.parseInt(br.readLine());
                    // Then come the gap arrays, we must read them to count nbPointers.
                    for (int idWord = 0; idWord < nbWords; idWord++) {
                        updateStatisticsWithNewGapArray(br.readLine().split(",").length);
                    }
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + filename + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in file '" + filename + "'.");
                }
            }
        };
    }
    
    public static DatasetReader datasetReaderUiHRDC(String baseFileName) {
        return new DatasetReaderBinary(baseFileName) {
            @Override
            protected void goToGapArrays() throws IOException {
                read32BitLittleEndianInt(dataInputStream);
                read32BitLittleEndianInt(dataInputStream);
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                readNextDocumentArray();
                Tools.documentArray2gapArray(intArrayRef, intArrayRef);
            }
            
            @Override
            protected void readNextDocumentArray() throws IOException {
                int sequenceSize = read32BitLittleEndianInt(dataInputStream);
                assert(intArrayRef.length >= sequenceSize);
                readIntSequence(dataInputStream, sequenceSize, intArrayRef.ints);
            }
            
            @Override
            protected void skipNextArrayAux() throws IOException {
                int sequenceSize = read32BitLittleEndianInt(dataInputStream);
                skipFully(dataInputStream, sequenceSize * 4L);
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    // First int of the file is the number of words.
                    nbWords = read32BitLittleEndianInt(in);
                    // Second int of the file is the number of documents.
                    nbDocuments = read32BitLittleEndianInt(in) + 1;
                    // Then come the gap arrays, we must read them to count nbPointers.
                    for (int idWord = 0; idWord < nbWords; idWord++) {
                        int sequenceSize = read32BitLittleEndianInt(in);
                        skipFully(in, sequenceSize * 4L);
                        updateStatisticsWithNewGapArray(sequenceSize);
                    }
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + filename + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in file '" + filename + "'.");
                }
            }
        };
    }
    
    public static DatasetReader datasetReaderPISA(String baseFileName) {
        return new DatasetReaderBinary(baseFileName) {
            @Override
            protected void goToGapArrays() throws IOException {
                readIntSequence(dataInputStream);
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                readNextDocumentArray();
                Tools.documentArray2gapArray(intArrayRef, intArrayRef);
            }
            
            @Override
            protected void readNextDocumentArray() throws IOException {
                int sequenceSize = read32BitLittleEndianInt(dataInputStream);
                assert(intArrayRef.length >= sequenceSize);
                readIntSequence(dataInputStream, sequenceSize, intArrayRef.ints);
            }
            
            @Override
            protected void skipNextArrayAux() throws IOException {
                int sequenceSize = read32BitLittleEndianInt(dataInputStream);
                skipFully(dataInputStream, sequenceSize * 4L);
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    // First int sequence of the file is the number of documents.
                    // We remove 1 because we add 1 in readIntSequence (only useful for the document array).
                    nbDocuments = readIntSequence(in)[0] - 1;
                    // Then come the gap arrays, we must read them to count nbWords and nbPointers.
                    while (true) {
                        try {
                            int sequenceSize = read32BitLittleEndianInt(in);
                            skipFully(in, sequenceSize * 4L);
                            nbWords++;
                            updateStatisticsWithNewGapArray(sequenceSize);
                        } catch (IOException e) {
                            break;
                        }
                    }
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + filename + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in file '" + filename + "'.");
                }
            }
        };
    }
    
    public static DatasetReader datasetReaderIC2014(String baseFileName) {
        return new DatasetReaderBinary(baseFileName) {
            @Override
            protected void goToGapArrays() throws IOException {}
            
            @Override
            protected void readNextGapArray() throws IOException {
                readNextDocumentArray();
                Tools.documentArray2gapArray(intArrayRef, intArrayRef);
            }
            
            @Override
            protected void readNextDocumentArray() throws IOException {
                int sequenceSize = read32BitLittleEndianInt(dataInputStream);
                assert(intArrayRef.length >= sequenceSize);
                readIntSequence(dataInputStream, sequenceSize, intArrayRef.ints);
            }
            
            @Override
            protected void skipNextArrayAux() throws IOException {
                int sequenceSize = read32BitLittleEndianInt(dataInputStream);
                skipFully(dataInputStream, sequenceSize * 4L);
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    // The file immediately contains gap arrays.
                    // We must read them to count nbDocuments, nbWords, and nbPointers.
                    while (true) {
                        try {
                            int sequenceSize = read32BitLittleEndianInt(in);
                            // Skip all the document ids in the list except the last one.
                            skipFully(in, (sequenceSize - 1) * 4L);
                            // Store the last document id in the list.
                            int documentId = read32BitLittleEndianInt(in) + 1;
                            nbWords++;
                            // Use this maximum document id to update nbDocuments if necessary
                            if (documentId > nbDocuments) {
                                nbDocuments = documentId;
                            }
                            updateStatisticsWithNewGapArray(sequenceSize);
                        } catch (IOException e) {
                            break;
                        }
                    }
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + filename + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in file '" + filename + "'.");
                }
            }
        };
    }
    
}
