/**
 * Maintain a stream of bits that can be written one by one.
 */

package io;

/**
 *
 * @author yann
 */
public abstract class BitOutputStream {
    
    /*
     * Write a bit to this bit stream.
     */
    public abstract void writeBit(int value) throws IndexOutOfBoundsException;
    
    /*
     * Write a bit to this bit stream.
     */
    public void writeBit(boolean value) throws IndexOutOfBoundsException {
        writeBit(value ? 1 : 0);
    }

}
