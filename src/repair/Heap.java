package repair;

import java.util.Map;

/**
 *
 * @author yann
 */
public class Heap {
    
    public Map.Entry<Integer, Integer>[] elements;
    public int inserted, k;
    
    public Heap(int k) {
	elements = new Map.Entry[k+1];
	for (int i = 0; i < k + 1; i++) {
            elements[i] = null;
        }
	inserted = 1;
        this.k = k;
    }
    
    void insert(Map.Entry<Integer, Integer> elem) {
	if (inserted <= k) { // Insercion normal
            elements[inserted] = elem;
            for (int j = inserted; j > 1 && elements[j].getValue() < elements[j/2].getValue(); j /= 2) {
                Map.Entry<Integer, Integer> t = elements[j/2];
                elements[j/2] = elements[j];
                elements[j] = t;
            }
            inserted++;
	} else if (elements[1].getValue() < elem.getValue()) { // Debemos eliminar alguno
            elements[1] = elem;
            int j = 1;
            while (2 * j <= k) {
                int p = 2*j;
                if (p + 1 <= k && elements[p + 1].getValue() < elements[p].getValue()) {
                    p++;
                }
                if (elements[j].getValue() < elements[p].getValue()) {
                    break;
                }
                Map.Entry<Integer, Integer> t = elements[j];
                elements[j] = elements[p];
                elements[p] = t;
                j = p;
            }
	} else {
            //** no es más frecuente que los K existentes.
        }
    }
}
