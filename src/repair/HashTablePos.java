package repair;

/**
 *
 * @author yann
 */
public class HashTablePos {
    
    public final double maxAlpha = 0.6;
    public int dataLen, buffLen;
    public int[] data;
    public int[] buff;
    public long queries;
    public long inserted;
    public long collisions;
    
    public HashTablePos(int[] data, int dataLen, int buffLen) {
	this.data = data;
	this.dataLen = dataLen;
	this.buff = new int[2 * buffLen];
	this.buffLen = buffLen;
	for (int i = 0; i < 2 * buffLen; i++) {
            buff[i] = -1;                         //**valor por defecto = -1 < 0
        }
	inserted = collisions = 0;
	queries = 0;
    }
    
    //** Inserta un par en la tabla hash, y si ya estaba incrementa su contador de ocurrencias.
    public boolean increment(int pos) {
	queries++;
	int p = h(pos);
	int lcollisions = 0;
	while (buff[2 * p] != -1) {    //** el slot [2p, 2p+1] de la tabla hash NO está libre
            if (lcollisions >= buffLen * maxAlpha) {
                return false;
            }
            if (cmp(pos, buff[2 * p])) {  //** el slot [2p,2p+1] de la tabla hash almacena info del par en la posición <<data[pos],data[pos+1]>>
                buff[2 * p + 1]++;
                return true;
            }
            collisions++;
            lcollisions++;
            p = (p + s(pos)) % buffLen;
	}
	
	//** en este momento p es la posición donde estaría el par "data[pos],data[pos+1]" en la tabla hash.
	//** y ese par todavía no está en la TH.
	
	if ((double)inserted / buffLen >= maxAlpha) {
            return false;
	}
	//printf("Inserted: %d\tAlpha: %f\n",inserted,(double)inserted/buffLen);
	buff[2 * p] = pos;
	buff[2 * p + 1] = 1;
	inserted++;
	return true;
    }
    
    public int get(int pos) {
	int p = h(pos);
	int lcollisions = 0;
	while (buff[2 * p] != -1) {
            if (lcollisions >= buffLen * maxAlpha) {
                return 0;
            }
            if (cmp(pos, buff[2 * p])) {
                return buff[2 * p + 1] + 1;
            }
            lcollisions++;
            p = (p + s(pos)) % buffLen;
	}
	return 0;
    }
    
    public boolean insert(int pos, int value) {
	int p = h(pos);
	int lcollisions = 0;
	while (buff[2 * p] != -1) {
            if (lcollisions >= buffLen * maxAlpha) {
                return false;
            }
            if (cmp(pos, buff[2 * p])) {
                buff[2 * p + 1] = value;
                return true;
            }
            collisions++;
            lcollisions++;
            p = (p + s(pos)) % buffLen;
	}
	if ((double)inserted / buffLen >= maxAlpha) {
            return false;
        }
	//printf("Inserted: %d\tAlpha: %f\n",inserted,(double)inserted/buffLen);
	buff[2 * p] = pos;
	buff[2 * p + 1] = value;
	inserted++;
	return true;
    }
    
    //** funcion hash --> devuelve pos dentro de la hash table ( xx % buffLen )
    public int h(int pos) {
        // YANN: Unsigned operations are needed.
        return Integer.remainderUnsigned(data[pos] + (data[pos] << 16)^data[pos + 1], buffLen);
    }
    
    public int s(int pos) {
	return data[pos];
    }
    
    public boolean cmp(int pos1, int pos2) {
	return data[pos1] == data[pos2] && data[pos1 + 1] == data[pos2 + 1];
    }
}
