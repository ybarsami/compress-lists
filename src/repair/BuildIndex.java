package repair;

import static repair.Basics.bits;
import static repair.Basics.SetField;
import static repair.Basics.WW32;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class BuildIndex {
    
    public static void buildIndex(String filename, int mem, int K) {
        buildIndex(filename, mem, K, 0.0);
    }
    
    // **   INPUT FILE FORMAT...
    // **   #nodes #edges nodeId ed ed ed ed nodeId ed ed ...
    // **     5000  23400     -1  2  3  4 76     -2 23 34 ...
    public static void buildIndex(String filename, int mem, int K, double CORTE_REPAIR) {
        try (FileInputStream fin = new FileInputStream(filename);
                DataInputStream in = new DataInputStream(fin)) {
            int nodes = in.readInt(); //** reads the number of nodes
            int edges = in.readInt(); //** reads the number of edges
            
            int[] text = new int[nodes + edges + 1];
            int[] offsets = new int[nodes + 1]; //**  ej real. offsets= [0][13][17][19][19][20][20][23][28][33]
            
            int min = 0;
            int nodes_read = 0;
            boolean isFirst = false;
            int last = 0;
            for (int i = 0; i < nodes + edges; i++) {
                int k = in.readInt(); //** reads either [nodeId < 0] or [edgeId > 0].
                if (k < 0) { //** a node
                    offsets[-k - 1] = i + k + 1;
                    if (k < min) {
                        min = k;
                    }
                    nodes_read++;
                    text[i] = k;
                    isFirst = true;
                } else { //** an edge
                    if (isFirst) {
                        text[i] = k;
                        last = k; //** value of the first edge of the current node.
                        isFirst = false;
                    } else {
                        text[i] = k - last; //** computes dgaps from the fst value.
                        last = k;
                    }
                }
            }
            
            min--;  //** min = "min k_i" - 1;    //** los nodos se quedan en el intervalo [0-->nodes]
            text[nodes + edges] = 0;   //** virtual  //** los ejes toman los valores  [nodes+valor anterior en text[i]]  
            offsets[nodes] = edges;  //** virtual  //**                      ==> ahora text[i]>nodes.
            for (int i = 0; i < nodes + edges; i++) {
                text[i] -= min;
            }
            
            int plen = bits(edges);    //** ptr a los lugares donde est�n los ejes para cada nodo. en el texto T, no en el comprimido (por ahora) **/
            int ptrs_len = (nodes + 1) * plen / WW32 + 1;
            int[] ptrs = new int[ptrs_len];   //** offsets codificados como "k-bit integers"
            for (int i = 0; i <= nodes; i++) {
                SetField(ptrs, plen, i, offsets[i]);
            }
            
            System.out.println("Nodes: " + nodes);
            System.out.println("Edges: " + edges);
            System.out.println("Nodes read: " + nodes_read);
            
            RePair rc = new RePair(text, edges + nodes, mem, filename, K);
            rc.compress(nodes, CORTE_REPAIR); //** nodes es el n�mero de nodos, y tambi�n indica que los ejes toman "valores > nodes"
            rc.save(nodes);
            
            try (FileOutputStream foutPtr = new FileOutputStream(filename + ".ptr");
                    DataOutputStream outPtr = new DataOutputStream(foutPtr)) {
                outPtr.writeInt(min);
                outPtr.writeInt(nodes);
                outPtr.writeInt(edges);
                for (int i = 0; i < ptrs_len; i++) {
                    outPtr.writeInt(ptrs[i]);
                }
            } catch (IOException e) {
                throw new RuntimeException("IOException in file '" + filename + ".ptr'.");
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        
        System.out.println("\n ================== build_index program ends ==================\n");
    }
}
