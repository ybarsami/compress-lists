/* bitarray.h
   Copyright (C) 2005, Rodrigo Gonzalez, all rights reserved.

   New RANK, SELECT, SELECT-NEXT and SPARSE RANK implementations.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
package repair;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import static repair.Basics.WW32;

/**
 *
 * @author yann
 */
public class BitMapW32Int {

    private int[] data;
    private boolean owner;	
    
    public int n, integers;
    
    public BitMapW32Int(int[] bitarray, int _n, boolean owner){
        data = bitarray;
        this.owner = owner;
        this.n=_n;
        integers = n / WW32 + 1;
    }
    
    public boolean IsBitSet(int i) {
        return ((1 << (i % WW32)) & data[i / WW32]) != 0;
    }
    
    public void save(DataOutputStream out) throws IOException {
        out.writeInt(n);
        for (int i = 0; i < n / WW32 + 1; i++) {
            out.writeInt(data[i]);
        }
    }
    
    public void load(DataInputStream in) throws IOException {
        n = in.readInt();
        data = new int[n / WW32 + 1];
        for (int i = 0; i < n / WW32 + 1; i++) {
            data[i] = in.readInt();
        }
        this.owner = true;
    }
    
    public void BitMapW32Int(DataInputStream in) throws IOException {
        load(in);
    }
    
    public int SpaceRequirementInBits() {
        return owner ? n : 0; 
    }
}
