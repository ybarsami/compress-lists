package repair;

import static repair.Basics.bits;
import static repair.Basics.GetField;
import static repair.Basics.SetField;
import static repair.Basics.sizeOfInt;
import static repair.Basics.WW32;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class RepairPost {
    
    public int m; // Tamano final
    public int n; // Tamano original
    public BitRankW32Int BR; // 1->original, 0->reemplazado
    public BitRankW32Int BRR; // Se construye pal arbolito
    public int[] symbols; // simbolos con reemplazos
    public int max_value;  // maximo valor del orginal,
    public int max_assigned;  // maximo valor asignado  (  = max_value + num reglas creado:: véase addRule)
    public int shift,min_value; 
    public int[] symbols_new;
    public int symbols_new_len; // diccionario y su largo
    public int nbits;
    public int nodes, edges;
    public int e;
    
    public int[] ptrs;
    public int ptrs_len;
    public int plen;
    
    public int[] info;   //**VECTOR TEMPORAL USADO PARA EXTRAER SÍMBOLOS (extract())
    public int pos;
    
    public int[] csymb_n;
    public int bits_sn;
    
    public int[] lenPost;		//len of each posting list
    
    public int sizeUncompressed;       //Len of uncompressed set of lists (including lenlist)  
    public int maxLenPosting;  //max len of any posting list.
    
    //**contructor that initializes object for querying
    public RepairPost(String filename) {
        readConstants(filename);
        load(filename);
        readLenPostings(filename);
    }	
    
    /** Number of phrases that where generated during "repair" for the node v |  v \in [1..nodes] **/
    public int lenList(int v){
	int x, y;
	v--;
	x = GetField(ptrs, plen, v); 	//symbols_new[rank_aux]);
	y = GetField(ptrs, plen, v+1);  //symbols_new[rank_aux]);
	return y - x;
    }
    
    /** return the number of bits used by each posting list (compressed) **/
    public int lenListBits(int v){
	int x, y;
	v--;
	x = GetField(ptrs, plen, v); 	//symbols_new[rank_aux]);
	y = GetField(ptrs, plen, v+1);  //symbols_new[rank_aux]);
	int len = y - x;
	int numbitsList = len * bits_sn;
	return numbitsList;
    }
    
    public final void load(String basename) {
        String filenameCrp = basename + ".crp";
        try (FileInputStream fin = new FileInputStream(filenameCrp);
                DataInputStream in = new DataInputStream(fin)) {
            min_value = in.readInt();
            max_value = in.readInt();
            max_assigned = in.readInt();
            nbits = in.readInt();
            m = in.readInt();
            n = in.readInt();
            int symbolsLength = m / 32 * nbits + m % 32 * nbits / 32 + 1;
            symbols = new int[symbolsLength];
            for (int i = 0; i < symbolsLength; i++) {
                symbols[i] = in.readInt();
            }
            BR = new BitRankW32Int(in);   //** se usar� para calcular ptr, y despu�s se desecha **
            
            System.err.println("\n m vale = " + m + " n vale = " + n);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filenameCrp + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filenameCrp + "'.");
        }
        
        //** CARGAMOS EL DICCIONARIO COMPRIMIDO...
        String filenameCdict = basename + ".cdict";
        try (FileInputStream fin = new FileInputStream(filenameCdict);
                DataInputStream in = new DataInputStream(fin)) {
            symbols_new_len = in.readInt();
            symbols_new = new int[symbols_new_len];
            for (int i = 0; i < symbols_new_len; i++) {
                symbols_new[i] = in.readInt();
            }
            BRR = new BitRankW32Int(in);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filenameCdict + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filenameCdict + "'.");
        }
        
        int max_sn = 0;
        for (int i = 0; i < symbols_new_len; i++) {     //** calcula el m�ximo s�mbolo en symbols_new;
            max_sn = Math.max(max_sn, symbols_new[i]);
        }
        bits_sn = bits(max_sn);                   //** numero de bits necesarios para los valores en "symbols_new"
        
        csymb_n = new int[symbols_new_len * bits_sn / WW32 + 1];   //** valores de symbols_new codificados con "bits_sn" bits.
        
        for (int i = 0; i < symbols_new_len; i++) {
            SetField(csymb_n, bits_sn, i, symbols_new[i]);  //** copio en csymb_n los valores de symbols_new.
        }
        
        System.err.println("\n RSS[] array has been compacted with skip_info into c_symb !!!!");
        
        //** hasta aqu�, respecto al Cdict... 
        //**   -csymb_n[]:  son los s�mbolos que hab�a en symbols_new[], pero compactados.
        //**   -bits_sn: es el n�mero de bits con que se representa cada entero en csymb_n
        //**   -symbols_new_len: es el n�mero de symbolos en el vocabulario
        //**   -BBR es el bitmap que indica si hay un terminal (1) o un s�mbolo-interno (0);
        
        String filenamePtr = basename + ".ptr";
        try (FileInputStream fin = new FileInputStream(filenamePtr);
                DataInputStream in = new DataInputStream(fin)) {
            int r = in.readInt();
            nodes = in.readInt();
            edges = in.readInt();
            plen = bits(edges); // YANN: modif'
            ptrs_len = (nodes + 1) * plen / WW32 + 1;  
            ptrs = new int[ptrs_len];
            for (int i = 0; i < ptrs_len; i++) {
                ptrs[i] = in.readInt();
            }
            
            int[] ptr2 = new int[nodes + 2];   //** longitud = nodes + 2 !!
            for (int i = 0; i <= nodes; i++) {
                ptr2[i] = BR.rank(GetField(ptrs, plen, i)) - 1;    //** ptr2[i] = rank(BR,ptr[i]);  == n� s�mbolos "finales" hasta la posici�n del ptr[i] en la seq comprimida
            }
            ptr2[nodes + 1] = m;                                  //** ptr2[nodes+1]= m == n� s�mbolos "finales" en la seq comprimida "completa" ;)
            
            plen = bits(m + 1);
            ptrs_len = (nodes + 2) * plen / WW32 + 1;
            ptrs = new int[ptrs_len];
            for (int i = 0; i <= nodes + 1; i++) {
                SetField(ptrs, plen, i, ptr2[i]);
            }
            
            e = edges;
            shift = min_value + r;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filenamePtr + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filenamePtr + "'.");
        }
    }
    
    public final void readConstants(String basename) {
        //** reads some constants: maxLenPosting and sizeUncompressed from "convert program".
        String filename = basename + ".const";
        try (FileInputStream fin = new FileInputStream(filename);
                DataInputStream in = new DataInputStream(fin)) {
            sizeUncompressed = in.readInt(); //Len of uncompressed set of lists (including lenlist)
            maxLenPosting = in.readInt();    //max len of any posting list. ==> needed for malloc on info[]
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        System.err.println(" sizeUncompressed is " + sizeUncompressed + " and maxLenPosting is " + maxLenPosting);
    }
    
    public final void readLenPostings(String basename) {
        String filename = basename + ".lens";
        try (FileInputStream fin = new FileInputStream(filename);
                DataInputStream in = new DataInputStream(fin)) {
            lenPost = new int[nodes+1];
            for (int i = 1; i <= nodes; i++) {
                lenPost[i] = in.readInt(); //Len of Posting-lists for id \in [1..nodes]
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        System.err.println(" loaded lenPosts[] array");
    }
    
    public int sizetobits() {
        int maxs, bits_s;
	int total;
	
	// size of lenList
	maxs = 0;
	for (int i = 1; i <= nodes; i++) {     //** calcula el m�ximo valor
            maxs = Math.max(maxs, lenPost[i]);
        }
	bits_s = bits(maxs);                   
	total = ((nodes+1) * bits_s / WW32 +1) * sizeOfInt;   //** valores codificados con "bits_s" bits.
        
	return total;
    }
    
    public int size() {
        int sum = 0;
        
	sum += sizeOfInt * ptrs_len;                     // ptrs  (in bits)	                    
  	sum += sizeOfInt * (m / 32 * nbits + m % 32 * nbits / 32 + 1);   // compressed sequence
  	sum += BRR.SpaceRequirementInBits() / 8;             // Bitmap-rank BRR
	sum += (symbols_new_len * bits_sn / WW32 + 1) * sizeOfInt;  // c_symb[]
	//sum += sizeOfInt * (nodes + 1);                   //**  lenPost array.
	sum += sizetobits();
        
        System.err.println(" ptrs_size =" + sizeOfInt * ptrs_len);
	System.err.println(" compressed seq=" + (sizeOfInt * (m / 32 * nbits + m % 32 * nbits / 32 + 1)));
	System.err.println(" Bitmap Rb (ya quitadas estructs rank)=" + (BRR.SpaceRequirementInBits() / 8) + " bytes");
	System.err.println(" Rs (dic)=" + ((symbols_new_len * bits_sn / WW32 + 1) * sizeOfInt));
//	System.err.println(" LenPosting[] array (lens of each posting OLD!!) = " + (sizeOfInt * (nodes + 1)));
	System.err.println(" LenPosting[] array (lens of each posting BITS) = " + sizetobits());
	System.err.println(" VOCAB SIZE = ptrs[] + lenList[] = " + (sizetobits() + sizeOfInt * ptrs_len) + " bytes");
        
        return sum;
    }
    
/********************************************************************************/
    
    public int[] adj(int v) {
	info = new int[maxLenPosting + 5];	
	v--;   //** v tendr� valor entre 1+ m, pero "symbols se indexa desde el "0" (symbols es la secuencia comprimida)
        
	int i = GetField(ptrs, plen, v); //symbols_new[rank_aux]);
	int j = GetField(ptrs, plen, v + 1);   //symbols_new[rank_aux]);
	
	pos = 0;
	info[0] = 0;
 	for (int k = i; k < j; k++) {
            expand(GetField(symbols, nbits, k));
        }
	info[0] = pos;  //** info[0] guarda el n�mero de "elementos" que han sido expandidos = valor actual de pos.
        return info;  
    }
    
    public void expand(int v) {
	if (v <= max_value) {
            pos++;                       //** pos dentro de "info".
            info[pos] = v + info[pos - 1];   //** suma lo anterior (d-gaps)
            return;
	}
	//**  else // a non-terminal node
	int aux1 = v - max_value - 1;      //** no es un s�mbolo terminal. ==> 
	assert(BRR.IsBitSet(aux1));
	int aux2 = 1;                    //** descomprimir hasta que aux2 ==0;   (mientras haya m�s 1's que 0's)
	int rank_aux = aux1 - BRR.rank(aux1) + 1;
	while (aux2 != 0) {
            if (BRR.IsBitSet(aux1)) {
                aux2++;
            } else {
                aux2--;
                expand(GetField(csymb_n, bits_sn, rank_aux)); //symbols_new[rank_aux]);
                rank_aux++;
            }
            aux1++;
	}
    }
    
    public int outdegree(int v) {
	return adj(v)[0];
    }
}
