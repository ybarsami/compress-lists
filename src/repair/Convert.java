package repair;

import static repair.Basics.sizeOfInt;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class Convert {
    
    /* Reads the post file. */
    private static ArrayList<Integer> integersFromFile(String filename) {
        int nbWords, maxPostValue;
        ArrayList<Integer> integers = new ArrayList<>();
        try (FileInputStream fin = new FileInputStream(filename);
                DataInputStream in = new DataInputStream(fin)) {
            // First int of the file is the number of words.
            nbWords = in.readInt();
            integers.add(nbWords);
            // Second int of the file is the maximum post value.
            maxPostValue = in.readInt();
            integers.add(maxPostValue);
            // Then come the post arrays.
            for (int iWord = 0; iWord < nbWords; iWord++) {
                int postArraySize = in.readInt();
                integers.add(postArraySize);
                for (int iPost = 0; iPost < postArraySize; iPost++) {
                    integers.add(in.readInt());
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        return integers;
    }
    
    /* Reads the .csv file. */
    private static ArrayList<Integer> integersFromFileCsv(String filename) {
        int nbWords, nbDocuments;
        ArrayList<Integer> integers = new ArrayList<>();
        try (FileReader fileReader = new FileReader(filename);
                BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            // First line of the file is the number of words.
            nbWords = Integer.parseInt(bufferedReader.readLine());
            integers.add(nbWords);
            // Second line of the file is the number of documents.
            nbDocuments = Integer.parseInt(bufferedReader.readLine());
            integers.add(nbDocuments);
            // Then come the gap arrays.
            for (int iWord = 0; iWord < nbWords; iWord++) {
                line = bufferedReader.readLine();
                String[] splitLine = line.split(",");
                int gapArraySize = splitLine.length;
                integers.add(gapArraySize);
                int post = -1; // the ids of the posts have to start at 0
                for (String str : splitLine) {
                    post += Integer.parseInt(str); // convert the gaps to posts
                    integers.add(post);
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        return integers;
    }
    
    /* Reads the .csv file. */
    private static ArrayList<Integer> integersFromGapArrayList(ArrayList<int[]> gapArrayList, int nbDocuments) {
        int nbWords;
        ArrayList<Integer> integers = new ArrayList<>();
        nbWords = gapArrayList.size();
        integers.add(nbWords);
        integers.add(nbDocuments);
        // Then come the gap arrays.
        for (int[] gapArray : gapArrayList) {
            int gapArraySize = gapArray.length;
            integers.add(gapArraySize);
            int post = -1; // the ids of the posts have to start at 0
            for (int gap : gapArray) {
                post += gap; // convert the gaps to posts
                integers.add(post);
            }
        }
        return integers;
    }
    
    public static int convert(String filenameIn, String filenameOut) {
        ArrayList<Integer> integers = integersFromFile(filenameIn);
        return convertAux(integers, filenameOut);
    }
    
    public static int convertFromCsv(String filenameIn, String filenameOut) {
        ArrayList<Integer> integers = integersFromFileCsv(filenameIn);
        return convertAux(integers, filenameOut);
    }
    
    public static int convertFromGapArrayList(ArrayList<int[]> gapArrayList, int nbDocuments, String filenameOut) {
        ArrayList<Integer> integers = integersFromGapArrayList(gapArrayList, nbDocuments);
        return convertAux(integers, filenameOut);
    }
    
    private static int convertAux(ArrayList<Integer> integers, String filenameOut) {
        int size = integers.size();
        int nbPosts = 0;
        
        try (FileOutputStream fout = new FileOutputStream(filenameOut);
                DataOutputStream out = new DataOutputStream(fout)) {
            // the number of words
            int nbWords = integers.get(0);
            out.writeInt(nbWords);
            
            // the number of posts
            nbPosts = size - nbWords - 2;
            System.out.print("inlen: " + size);
            out.writeInt(nbPosts);
            
            int[] lens = new int[nbWords];
            int sourcepos = 2;
            int maxLenPosting = 0;
            
            for (int iWord = 0; iWord < nbWords; iWord++) {
                // length of the postArray
                lens[iWord] = integers.get(sourcepos++);
                if (lens[iWord] > maxLenPosting) {
                    maxLenPosting = lens[iWord];
                }
                
                // output a negative number so that repair knows it is a new postArray
                out.writeInt(-iWord - 1);
                
                // output the postArray
                for (int j = 0; j < lens[iWord]; j++) {
                    out.writeInt(integers.get(sourcepos++) + 1);
                }
            }
            System.out.println(", sourcepos: " + sourcepos);
            
            // to keep the len of the posting lists of all terms.
            String filenameLens = filenameOut + ".lens";
            try (FileOutputStream foutLens = new FileOutputStream(filenameLens);
                    DataOutputStream outLens = new DataOutputStream(foutLens)) {
                for (int iWord = 0; iWord < nbWords; iWord++) {
                    outLens.writeInt(lens[iWord]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            // saves some constants: maxPostingLen and lenOfSourceFile.
            String filenameConst = filenameOut + ".const";
            try (FileOutputStream foutConst = new FileOutputStream(filenameConst);
                    DataOutputStream outConst = new DataOutputStream(foutConst)) {
                long valueIn = (long)size * (long)sizeOfInt; // <--------------- Len of source set of lists...
                outConst.writeLong(valueIn);
                outConst.writeInt(maxLenPosting);
                System.out.println(" writing constants file: valueIn=" + valueIn + ", maxLenPosting=" + maxLenPosting);
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            System.out.println(" words: " + nbWords + ", posts: " + nbPosts + ", repairPosts:" + nbPosts
                    +", curTerm: " + (-nbWords) + ", sourcepos: " + sourcepos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        System.out.println("\n ================== convert program ends ==================\n");
        return nbPosts;
    }
}
