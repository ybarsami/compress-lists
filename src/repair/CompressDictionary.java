package repair;

import static repair.Basics.bits;
import static repair.Basics.bitset;
import static repair.Basics.SetField;
import static repair.Basics.WW32;
import me.lemire.integercompression.IntWrapper;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class CompressDictionary {
    
    private int[] symbols_pair;
    private int[] symbols_new_bit; 
    private int symbols_new_bit_len;
    private int[] symbols;
    private int n, m, nbits;
    
    private BitRankW32Int BRR; //bitmap to indicate the structure of symbols_new
    private BitRankW32Int BR;
    private int[] symbols_new; // table to decompress de new codes
    private int symbols_new_len; // len of symbols_new
    private int min_value;
    private int max_value;
    private int max_assigned;

    private int pos;
    
    public CompressDictionary() {
        
    }
    
    private void unroll(int s, int i) {
        if (s > max_value) {
            unroll(symbols_pair[2*(s-max_value-1)],i);
            unroll(symbols_pair[1+2*(s-max_value-1)],i);
            return;
        }
        pos++;
    }
    
    private void fillBR(int[] bits) {
        pos = 0;
        for (int i = 0; i < m; i++) {
            bitset(bits, pos);
            unroll(symbols[i], i);   /* increases pos */
        }
    }
    
    private void fillBR2() {
        int bits_BR_len = n + 2;
        int[] bits_BR = new int[(bits_BR_len + WW32 - 1) / WW32];
        
        fillBR(bits_BR);
        bitset(bits_BR, pos); /** �la �ltima posici�n, para marcar el final ??? :: sin comprobar */
        BR = new BitRankW32Int(bits_BR, bits_BR_len, true, 20); 
    }
    
    private void compress_pair_table() {
        int aux;
        /* Compress table of pairs */
        fillBR2();
        
        //** max_assigned-max_value == n�mero de reglas que se crearon = num nuevos s�mbolos . 
        int[] symbols_pair_tmp = new int[max_assigned-max_value];
        
        for (int i = 0 ; i < max_assigned - max_value; i++) {
            aux = symbols_pair[2 * i];
            if (aux > max_value) {
                symbols_pair_tmp[aux - max_value - 1]++;  //**establece un "1"
            }
            aux = symbols_pair[2 * i + 1];
            if (aux > max_value) {
                symbols_pair_tmp[aux - max_value - 1]++;  //**establece un "1"
            }
        }
        
        int nbNonTerminalSymbols = 0;
        for (int i = 0; i < max_assigned - max_value; i++) {
            if (symbols_pair_tmp[i] != 0) {
                nbNonTerminalSymbols++;  /** cuenta los que no son terminales ??? */
            }
        }
        
        symbols_new_len = 2 * (max_assigned - max_value) - nbNonTerminalSymbols;
        symbols_new = new int[symbols_new_len];
        symbols_new_bit = new int[(symbols_new_len + (max_assigned - max_value)) / 32 + 1];
        int[] symbols_new_value = new int[max_assigned - max_value];
        
        IntWrapper j = new IntWrapper();
        j.increment();
        IntWrapper k = new IntWrapper();
        for (int i = 0; i < max_assigned - max_value; i++) {
            if (symbols_pair_tmp[i] == 0) {
                symbols_new_value[i] = j.get();
                bitset(symbols_new_bit, j.get() - 1);
                j.increment();
                new_value(symbols_pair, symbols_new_value, k, j, i);
            }
        }
        symbols_new_bit_len = j.get();
        // fromIndex is inclusive, toIndex is exclusive.
        symbols_new_bit = Arrays.copyOfRange(symbols_new_bit, 0, symbols_new_bit_len / 32 + 1);
        
        /* Solo para verificar */
        //for (i =0 ; i<(max_assigned-max_value);i++)
        //    if (symbols_new_value[i]==0) printf("s[%lu]=%lu, frec=%lu\n",i,symbols_new_value[i], symbols_pair_tmp[i]);
        
        int cuentame = 0, cuentame2 = 0, distancia = 0;
        double maximo = 0.0;
        nbits = bits(symbols_new_bit_len + max_value + 1);
        int[] symbols_aux = new int[(m / 32 * nbits + m % 32 * nbits / 32) + 1];
        
        for (int i = 0; i < m; i++) {
            aux = symbols[i];
            if (aux > max_value) {
                SetField(symbols_aux, nbits, i, symbols_new_value[aux - max_value - 1] + max_value);
                //symbols[i] = symbols_new_value[aux-max_value-1]+max_value;
                /*if(i>=18412900)
                  printf("i=%u aux=%u\n",i,aux); fflush(stdout);*/
                distancia += BR.select(i + 2) - BR.select(i + 1);
                cuentame2++;
            } else {
                SetField(symbols_aux, nbits, i, aux);
                maximo += distancia * distancia;
                distancia = 0;
                maximo++;
                cuentame++;
            }
        }
        symbols = symbols_aux;
        
        /* Compact symbols_new */
        /*
        symbols_aux = (uint *) malloc(sizeof(uint)*((symbols_new_len*nbits)/32+1));
        for (i =0 ; i< symbols_new_len ; i++) {
          SetField(symbols_aux,nbits,i,symbols_new[i]);
        }
        free(symbols_new);
        symbols_new=symbols_aux;
        */
        
        BRR = new BitRankW32Int(symbols_new_bit, symbols_new_bit_len, true, 20); 
        
        System.out.printf("Sin Comprimir=%d, comprimido=%d\n", cuentame, cuentame2); 
        System.out.printf("Costo prom descompr=%f\n", maximo / (double)n);
        System.out.printf("Original=%d\n", n); 
        System.out.printf("Simpolos finales=%d (%.6f%%)\n", m, (double)m / (double)n * 100.0);
        System.out.printf("Conjunto de pares=%d (%.6f%%) hubo %d reemplazos\n",
                2 * (max_assigned - max_value),
                (double)(2* (max_assigned - max_value)) / (double)n * 100.0,
                max_assigned - max_value);
        aux = symbols_new_bit_len / 32 + 1;
        System.out.printf("Conjunto de pares2=%d+%d (%.6f%%) ahorro  %d\n",
                symbols_new_len,
                aux,
                (double)(symbols_new_len + aux) / (double)n * 100.0,
                2 * (max_assigned - max_value) - symbols_new_len - aux);
        System.out.printf("Estructura para rank para descomprimir=%d (%.6f%%) tira de bits+5%% extra \n",
                n / 32 + 1+ (n / 32 + 1) * 5 / 100,
                (double)( n / 32 + 1 + (n / 32 + 1) * 5 / 100) / (double)n * 100.0);
        System.out.printf("Sumas simbolos+pares=%d (%.6f%%)\n",
                m + 2 * (max_assigned - max_value),
                (double)(m + 2 * (max_assigned - max_value)) / (double)n * 100.0);
        System.out.printf("Sumas simbolos+pares2=%d (%.6f%%)\n",
                m + symbols_new_len + aux,
                (double)(m + symbols_new_len + aux) / (double)n * 100.0);
    }
    
    // IntWrapper is a Java equivalent for a pointer to a single int (int*), used
    // to modify this int and not to pass an array.
    private void new_value(int[] symbols_pair, int[] symbols_new_value, IntWrapper k, IntWrapper j, int pos) {
        int izq, der;
        izq = symbols_pair[2 * pos];
        der = symbols_pair[2 * pos + 1];
        
        if (izq > max_value) {
            izq = izq - max_value - 1;
            if (symbols_new_value[izq] == 0) {
                symbols_new_value[izq] = j.get();
                bitset(symbols_new_bit, j.get() - 1);
                j.increment();
                new_value(symbols_pair,symbols_new_value, k, j, izq);
            } else {
                symbols_new[k.get()] = symbols_new_value[izq] + max_value;
                j.increment();
                k.increment();
            }
        } else {
            symbols_new[k.get()] = izq;
            j.increment();
            k.increment();
        }
        
        if (der > max_value) {
            der = der - max_value - 1;
            if (symbols_new_value[der] == 0) {
                symbols_new_value[der] = j.get();
                bitset(symbols_new_bit, j.get() - 1);
                j.increment();
                new_value(symbols_pair, symbols_new_value, k, j, der);
            } else {
                symbols_new[k.get()] = symbols_new_value[der] + max_value;
                j.increment();
                k.increment();
            }
        } else {
            symbols_new[k.get()] = der;
            j.increment();
            k.increment();
        }
    }
    
    public void compressDictionary(String filename) {
	//** cargar de fichero .rp....
        int nodes = 0;
        String filenameRP = filename + ".rp";
        try (FileInputStream fin = new FileInputStream(filenameRP);
                DataInputStream in = new DataInputStream(fin)) {
            min_value = in.readInt();
            max_value = in.readInt();
            max_assigned = in.readInt();
            m = in.readInt();
            n = in.readInt();
            nodes = in.readInt();
            System.out.printf("m=%d\n", ++m);
            symbols = new int[m];
            for (int i = 0; i < m; i++) {
                symbols[i] = in.readInt();
            }
            System.out.printf("read=%d\n", m);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filenameRP + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filenameRP + "'.");
        }
        
        System.out.printf("nodes=%d\n", nodes);
        System.out.printf("max_value=%d\nmax_assigned=%d\n", max_value, max_assigned);
        max_value -= nodes;
        max_assigned -= nodes;
        System.out.printf("max_value=%d\nmax_assigned=%d\n", max_value, max_assigned);
        
        //** cargar de fichero .dict...
	symbols_pair = new int[2 * (max_assigned - max_value)];
        String filenameDict = filename + ".dict";
        try (FileInputStream fin = new FileInputStream(filenameDict);
                DataInputStream in = new DataInputStream(fin)) {
            for (int i = 0; i < max_assigned - max_value; i++) {
                int nv, p1, p2;
                nv = in.readInt();
                p1 = in.readInt();
                p2 = in.readInt();
                //**  resta "nodes" a todas la reglas (al igual que se hizo a los datos en el fichero .rp antes de guardarlo)
                nv -= nodes;
                p1 -= nodes;
                p2 -= nodes;
                assert(p1 < max_assigned);
                assert(p2 < max_assigned);
                assert(nv <= max_assigned);
                assert(nv == i + max_value + 1);
                symbols_pair[2 * i] = p1;
                symbols_pair[2 * i + 1] = p2;
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filenameDict + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filenameDict + "'.");
        }
        
	compress_pair_table();  //** !!!  Crea symbols_new, symbols_new_len 
	
        try (FileOutputStream fout = new FileOutputStream(filename + ".cdict");
                DataOutputStream out = new DataOutputStream(fout)) {
            out.writeInt(symbols_new_len);
            for (int i = 0; i < symbols_new_len; i++) {
                out.writeInt(symbols_new[i]);
            }
            BRR.save(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        try (FileOutputStream fout = new FileOutputStream(filename + ".crp");
                DataOutputStream out = new DataOutputStream(fout)) {
            nbits = bits(symbols_new_bit_len + max_value + 1);
            out.writeInt(min_value);
            out.writeInt(max_value);
            out.writeInt(max_assigned);
            out.writeInt(nbits);
            out.writeInt(m);
            out.writeInt(n);
            for (int i = 0; i < m / 32 * nbits + m % 32 * nbits / 32 + 1; i++) {
                out.writeInt(symbols[i]);
            }
            BR.save(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        System.out.println("\n ================== compress_dictionary program ends ==================\n");
    }
}
