/* bitarray.h
   Copyright (C) 2005, Rodrigo Gonzalez, all rights reserved.

   New RANK, SELECT, SELECT-NEXT and SPARSE RANK implementations.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
package repair;

import static repair.Basics.bits;
import static repair.Basics.mask31;
import static repair.Basics.popcount;
import static repair.Basics.popcount8;
import static repair.Basics.select_tab;
import static repair.Basics.sizeOfInt;
import static repair.Basics.WW32;
import static repair.Basics.Wminusone;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class BitRankW32Int {
    
/////////////
//Rank(B,i)// 
/////////////
//_factor = 0  => s=WW32*lgn
//_factor = P  => s=WW32*P
//Is interesting to notice
//factor=2 => overhead 50%
//factor=3 => overhead 33%
//factor=4 => overhead 25%
//factor=20=> overhead 5%
    
    private int factor, b, s;
    private int[] Rs; //superblock array
    
    public int[] data;
    public boolean owner;
    public int n, integers;
    
    public BitRankW32Int(int[] bitarray, int n, boolean owner, int factor){
        data = bitarray;
        this.owner = owner;
        this.n = n;
        int lgn = bits(n - 1);
        this.factor = factor == 0 ? lgn : factor;
        b = 32;
        s = b * this.factor;
        integers = n / WW32 + 1;
        BuildRank();
    }
    
    //crea indice para rank
    //Metodo que realiza la busqueda d
    private void BuildRank(){
        int num_sblock = n / s;
        Rs = new int[num_sblock + 5];// +1 pues sumo la pos cero
        for (int i = 0; i < num_sblock + 5; i++) {
            Rs[i] = 0;
        }
        Rs[0] = 0;
        for (int j = 1; j <= num_sblock; j++) {
            Rs[j] = Rs[j - 1];
            Rs[j] += BuildRankSub((j - 1) * factor, factor);
        }
    }
    
    //uso interno para contruir el indice rank
    private int BuildRankSub(int ini, int bloques){
        int rank = 0, aux;
        for (int i = ini; i < ini + bloques; i++) {
            if (i < integers) {
                aux = data[i];
                rank += popcount(aux);
            }
        }
        return rank; //retorna el numero de 1's del intervalo
    }
    
    public int rank(int i) {
        ++i;
        int resp = Rs[i / s];
        int aux = (i / s) * factor;
        for (int a = aux; a < i / WW32; a++) {
            resp += popcount(data[a]);
        }
        resp += popcount(data[i / WW32]  & ((1 << (i & mask31)) - 1));
        return resp;
    }
    
    public boolean IsBitSet(int i) {
        return ((1 << (i % WW32)) & data[i / WW32]) != 0;
    }
    
    public void save(DataOutputStream out) throws IOException {
        out.writeInt(n);
        out.writeInt(factor);
        for (int i = 0; i < n / WW32 + 1; i++) {
            out.writeInt(data[i]);
        }
        for (int i = 0; i < n / s + 1; i++) {
            out.writeInt(Rs[i]);
        }
    }
    
    public void load(DataInputStream in) throws IOException {
        n = in.readInt();
        b = 32; // b is a word
        factor = in.readInt();
        s = b * factor;
        int aux = (n + 1) % WW32;
        integers = (aux != 0) ? (n + 1) / WW32 + 1 : (n + 1) / WW32;
        data = new int[n / WW32 + 1];
        for (int i = 0; i < n / WW32 + 1; i++) {
            data[i] = in.readInt();
        }
        this.owner = true;
        Rs = new int[n / s + 1];
        for (int i = 0; i < n / s + 1; i++) {
            Rs[i] = in.readInt();
        }
    }
    
    public BitRankW32Int(DataInputStream in) throws IOException {
        load(in);
    }
    
    public int SpaceRequirementInBits() {
        int sizeOfBitRankW32Int = 4 * 3 + // private int factor, b, s;
                                      4 + // private int[] Rs; //superblock array
                                      4 + // public int[] data;
                                      4 + // public boolean owner;
                                  4 * 2; // public int n, integers;
        return (owner ? n : 0) + (n / s) * sizeOfInt * 8 + sizeOfBitRankW32Int * 8; 
    }
    
    // returns the position of the previous 1 bit before and including start.
    // tuned to 32 bit machine
    public int prev(int start) {
        int i = start >>> 5;
        int offset = (start % WW32);
        int answer = start;
        int val = data[i] << (Wminusone - offset);
        
        if (val == 0) {
            val = data[--i];
            answer -= 1 + offset;
        }
        while (val == 0) {
            val = data[--i];
            answer -= WW32;
        }
        if ((val & 0xFFFF0000) == 0) {
            val <<= 16;
            answer -= 16;
        }
        if ((val & 0xFF000000) == 0) {
            val <<= 8;
            answer -= 8;
        }
        while ((val & 0x80000000) == 0) {
            val <<= 1;
            answer--;
        }
        return Math.max(answer, 0);
    }
    
    public int next(int k) {
        int count = k;
        int des, aux2;
        des = count % WW32; 
        aux2 = data[count / WW32] >>> des;
        if (aux2 > 0) {
            if ((aux2 & 0xff) > 0) {
                return count +      select_tab[ aux2         & 0xff] - 1;
            } else if ((aux2 & 0xff00) > 0) {
                return count +  8 + select_tab[(aux2 >>>  8) & 0xff] - 1;
            } else if ((aux2 & 0xff0000) > 0) {
                return count + 16 + select_tab[(aux2 >>> 16) & 0xff] - 1;
            } else {
                return count + 24 + select_tab[(aux2 >>> 24) & 0xff] - 1;
            }
        }
        
        for (int i = count / WW32 + 1; i < integers; i++) {
            aux2 = data[i];
            if (aux2 > 0) {
                if ((aux2 & 0xff) > 0) {
                    return i * WW32 +      select_tab[ aux2         & 0xff] - 1;
                } else if ((aux2 & 0xff00) > 0) {
                    return i * WW32 +  8 + select_tab[(aux2 >>>  8 ) &0xff] - 1;
                } else if ((aux2 & 0xff0000) > 0) {
                    return i * WW32 + 16 + select_tab[(aux2 >>> 16) & 0xff] - 1;
                } else {
                    return i * WW32 + 24 + select_tab[(aux2 >>> 24) & 0xff] - 1;
                }
            }
        }
        return n;
    } 
    
    public int select(int x) {
        // returns i such that x=rank(i) && rank(i-1)<x or n if that i not exist
        // first binary search over first level rank structure
        // then sequential search using popcount over a int
        // then sequential search using popcount over a char
        // then sequential search bit a bit
        
        //binary search over first level rank structure
        int l = 0;
        int r = n / s;
        int mid = (l + r) / 2;
        int rankmid = Rs[mid];
        while (l <= r) {
            if (rankmid < x) {
                l = mid + 1;
            } else {
                r = mid - 1;
            }
            mid = (l + r) / 2;
            rankmid = Rs[mid];
        }
        //sequential search using popcount over a int
        int left = mid * factor;
        x -= rankmid;
        int j = data[left];
        int ones = popcount(j);
        while (ones < x) {
            x -= ones;
            left++;
            if (left > integers) {
                return n;
            }
            j = data[left];
            ones = popcount(j);
        }
        //sequential search using popcount over a char
        left = left * b;
        rankmid = popcount8(j);
        if (rankmid < x) {
            j = j >>> 8;
            x -= rankmid;
            left += 8;
            rankmid = popcount8(j);
            if (rankmid < x) {
                j = j >>> 8;
                x -= rankmid;
                left += 8;
                rankmid = popcount8(j);
                if (rankmid < x) {
                    j = j >>> 8;
                    x -= rankmid;
                    left += 8;
                }
            }
        }
        
        // then sequential search bit a bit
        while (x > 0) {
            if ((j & 1) != 0) {
                x--;
            }
            j = j >>> 1;
            left++;
        }
        return left - 1;
    }
}
