package repair;

/**
 *
 * @author yann
 */
public class HashTablePairs {
    
    public final double maxAlpha = 0.6;
    public int buffLen;
    public int[] buff;
    public long queries;
    public long inserted;
    public long collisions;
    
    public HashTablePairs(int buffLen) {
	this.buff = new int[3 * buffLen];
	this.buffLen = buffLen;
	for (int i = 0; i < 3 * buffLen; i++) {
            buff[i] = -1;
        }
	inserted = collisions = 0;
	queries = 0;
    }
    
    public int get(int p1, int p2) {
	int p = h(p1, p2);
	int lcollisions = 0;
	while (buff[3 * p] != -1) {
            if (lcollisions >= buffLen * maxAlpha) {
                return 0;
            }
            if (p1 == buff[3 * p] && p2 == buff[3 * p + 1]) {
                return buff[3 * p + 2] + 1;
            }
            lcollisions++;
            p = (p + s(p1, p2)) % buffLen;
	}
	return 0;
    }
    
    public boolean insert(int p1, int p2, int value) {
	queries++;
	int p = h(p1, p2);
	int lcollisions = 0;
	while (buff[3 * p] != -1) {
            if (lcollisions >= buffLen * maxAlpha) {
                return false;
            }
            if (p1 == buff[3 * p] && p2 == buff[3 * p + 1]) {
                buff[3 * p + 2] = value;
                return true;
            }
            collisions++;
            lcollisions++;
            p = (p + s(p1, p2)) % buffLen;
	}
	if ((double)inserted / buffLen >= maxAlpha) {
            return false;
        }
	//printf("Inserted: %d\tAlpha: %f\n",inserted,(double)inserted/buffLen);
	buff[3 * p] = p1;
	buff[3 * p + 1] = p2;
	buff[3 * p + 2] = value;
	inserted++;
	return true;
    }
    
    public int h(int p1, int p2) {
        // YANN: Unsigned operations are needed.
        return Integer.remainderUnsigned(p1 + (p1 << 16)^p2, buffLen);
    }
    
    public int s(int p1, int p2) {
	return 1;
    }
}
