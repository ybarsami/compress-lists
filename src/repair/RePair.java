package repair;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class RePair {

    private static final int FIRST    = 0;
    private static final int REPLACED = 1;
    private static final int NOT_SEEN = 2;
    
    private double CORTE_REPAIR = 0.0;
    private final int[] data;
    private final int totalLen;
    private int max_value, K;
    private int mini;
    private int lastpos = 0;
    private final String basename;
    private int max_assigned;
    private Heap heap = null;
    private HashTablePos hpos;
    private HashTablePairs hpairs;
    private DataOutputStream fpDict;
    
    // Aun no usado
    private int m; // Tamano final
    private int n; // Tamano original
    
    /** Creates a RePair Object ready for compressing 
     * The buffer is now owned by the RePair object.
     */
    public RePair(int[] data, int dataLength, int totalLen, String basename, int K) {
        // HashTablePos uses the value data[dataLength], see "text[nodes + edges] = 0" in BuildIndex.java
        this.data = data;
	m = dataLength;
	n = dataLength;
	this.totalLen = totalLen;
	mini = data[0];                  //** en ejemplo cnr data_tmp[0] = nodes
	long maxi = data[0];
	for (int i = 1; i < m; i++) {
            if (mini > data[i]) mini = data[i];  //** minimo valor en el buff.
            if (maxi < data[i]) maxi = data[i];  //** máximo valor en el buff
	}
	mini--;
	for (int i = 0; i < dataLength; i++) {
            data[i] -= mini;   //** valores a assignar son trasladados de n, n' --> n-mini, n' -mini (alguno será zero)
        }
	max_assigned = (int)(maxi - mini);
	max_value = max_assigned;   //**
	
	//** printf("\n\n mini %d    maxi %d    max_assigned %d   max_value %d",mini,maxi,max_assigned,max_value);
	//**              mini 0    maxi 650371    max_assigned 650371   max_value 650371
	
	this.K = K;
	this.basename = basename;
        try {
            FileOutputStream fout = new FileOutputStream(basename + ".dict");
            fpDict = new DataOutputStream(fout);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //** mete en "heap" los k pares más frecuentes   { <pos_1, freq_1>  <pos_2, freq_2>...<pos_k, freq_k> }
    private void fillHeap() {
        heap = new Heap(K); //** heap para almacenar hasta K elementos
        
        int hashsize = (totalLen - m - 1) / 2;
        hpos = new HashTablePos(data, m, hashsize);   //** tabla hash es inicializada <posIni,contador>
        
        lastpos %= m;
        int blastpos = lastpos; 
        boolean save = true; 
        int pos = lastpos;
        
        do {  // ** recorremos todo el texto de forma circular... desde el valor de lastpos...
            if (!hpos.increment(pos)) {   //** Inserta un par en la tabla hash, y si ya estaba incrementa su contador de ocurrencias.
                if (save) {
                    save = false;
                    lastpos = pos;
                }
            }
            pos = (pos + 1) % m;
        } while(pos != blastpos);
        
        for (int i = 0; i < hashsize; i++) { //** INSERCIÓN EN EL HEAP ==> ya calcula los K más frecuentes.
            if (hpos.buff[2 * i] >= 0) { //** slot ocupado (pues es != -1)  --> insertar en la heap (se queda con los k más frecuentes)
                heap.insert(new AbstractMap.SimpleEntry<>(hpos.buff[2 * i], hpos.buff[2 * i + 1]));
            }
        }
    }
    
    /** Selects the candidates to be replaced 
     */
    private int replacePairs() {
        fillHeap();   //** Heap contendrá los K pares <posInic, contador> más frecuentes. Ojo, alguno podría quedar con "contador =1" !!
        int c = 0;
        int nbRules = 0;
        int hashsize = (totalLen - m - 1) / 2;
        
        for (int i = 1; i < heap.inserted; i++) { //** cuenta el número de pares con freq > 1
            if (heap.elements[i] != null && heap.elements[i].getValue() > 1) {
                c++;
            }
        }
        
        if (c == 0) {  //** no hay pares con freq > 1   ==> final ==> return 0
            heap = null;
            return 0;
        }
        
        //** ***** PASO 3: Reemplazo simultáneo en una única pasada sobre data. *****
        AbstractMap.SimpleEntry<Integer, Integer>[] ret = new AbstractMap.SimpleEntry[c];  //** defino array de c "pares" <pos, count>
        hpairs = new HashTablePairs(2 * hashsize / 3);
        for (int i = 1; i < heap.inserted; i++) {
            if (heap.elements[i] != null && heap.elements[i].getValue() > 1) {
                int pos = heap.elements[i].getKey();             //** mete los pares en la hash table de Pares (orden decreciente de freq)
                ret[--c] = new AbstractMap.SimpleEntry<>(pos, NOT_SEEN);  //** lo pone como "not_seen"
                hpairs.insert(data[pos], data[pos + 1], c);  // c es el número de par!!
            }
        }
        
        for (int i = 0; i < m; i++) {      //**reemplazo de los k pares que se han formado.                
            c = hpairs.get(data[i], data[i + 1]);
            if (c != 0) {
                c--;
                switch(ret[c].getValue()) {
                    case NOT_SEEN:
                        ret[c] = new AbstractMap.SimpleEntry<>(i, FIRST);
                        break;
                    case FIRST:
                        if (data[ret[c].getKey()] == data[i] && data[ret[c].getKey() + 1] == data[i + 1]) {  //**si aún es posible añadirlo
                            if (i == ret[c].getKey() + 1) {
                                break;
                            }
                            addRule(++max_assigned, data[i], data[i + 1]);
                            ret[c].setValue(max_assigned);
                            data[ret[c].getKey()    ] = ret[c].getValue();
                            data[ret[c].getKey() + 1] = 0;
                            nbRules++;
                        } else {
                            ret[c] = new AbstractMap.SimpleEntry<>(i, ret[c].getValue());
                            break;
                        }
                    default:
                        data[i    ] = ret[c].getValue();
                        data[i + 1] = 0;
                }
            }
        }
        
        //** ******* PASO 4. compactar T, y alargar HashTable sobre data  ******
        rearrange();
        heap = null;
        hpairs = null;
        
        return nbRules;
    }
    
    /** Eliminate 0's from the data 
     */
    //compacta vector (quitando los segundos elementos "inválidos == 0"
    private void rearrange() {
	int rptr = 0;
	for (int i = 0; i < m; i++) {
            if (data[i] != 0) {
                data[rptr] = data[i];
                rptr++;
            }
	}
	data[rptr] = 0;
	m = rptr;
    }
    
    private void addRule(int nvalue, int p1, int p2) {
        try {
            fpDict.writeInt(nvalue);
            fpDict.writeInt(p1);
            fpDict.writeInt(p2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /** Compresses the data 
     */
    public void compress(int nodes, double corte) {
        CORTE_REPAIR = corte;
        compress(nodes);
    }
    
    public void compress(int nodes) {
        int i = 1;
        int lasti = 1;
        double prevRatio = 100.0;
        
        while (true) {
            System.out.println("m vale = " + m);
            while (replacePairs() > 0) {  //** mientras se hayan hecho reemplazos...
                double currentRatio = 100.0 * (double)m / (double)n;
                System.out.println("Repair.compress: It: " + (i++) + " compressed: " + currentRatio
                        + "% m=" + m + ", diff prev iter =" + (prevRatio - currentRatio));
                if (prevRatio - currentRatio < CORTE_REPAIR) {
                    break;
                }
                prevRatio = currentRatio;
            }
            
            if (lasti <= i) {
                break;
            }
            
            int sorted = 0;                           //** Normalmente no entra aquí !!! **/
            System.out.print("Reordering symbols: ");
            for (int j = 0; j < m; j++) {
                if (data[j] <= nodes) {   
                    for (int k = j + 1; k <= m; k++) {
                        if (data[k] <= nodes) {
                            // fromIndex is inclusive, toIndex is exclusive.
                            Arrays.sort(data, j + 1, k);
                            sorted++;
                            break;
                        }
                    }
                }
            }
            System.out.println(sorted);
            lasti = i;    
        }
    }
    
    /** Saves the compressed data 
     */
    public void save(int nodes) {
        try {
            fpDict.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveRPFile(nodes); 
    }
    
    /** Saves the RPFile 
     */
    private void saveRPFile(int nodes) {
        nodes++;
        try (FileOutputStream fout = new FileOutputStream(basename + ".rp");
                DataOutputStream out = new DataOutputStream(fout)) {
            out.writeInt(mini);
            out.writeInt(max_value);
            out.writeInt(max_assigned);    //** max_assigned = max_value + numNuevasReglasCreadas.
            int writ = 0;
            for (int i = 0; i < m; i++) {
                if (data[i] > nodes) {     //** sólo escribe en "rp" los "edges".
                    writ++;                //** cuenta el número de elementos escritos.
                }
            }
            out.writeInt(writ);
            out.writeInt(n - m + writ);    //** tamaño real del texto comprimido quitando "nodos"
            out.writeInt(nodes);
            for (int i = 0; i < m; i++) {
                if (data[i] > nodes) {     //** sólo escribe en "rp" los "edges".
                    data[i] -= nodes;      //** resta "nodes" a todos los ejes  **
                    out.writeInt(data[i]);
                }
            }
            out.writeInt(data[m]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
