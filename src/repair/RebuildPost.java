package repair;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class RebuildPost {
    
    public static void rebuildPost(String basename) {
        int max_outdegree = 0;
        try (FileOutputStream fout = new FileOutputStream(basename + ".rebuilt");
                DataOutputStream out = new DataOutputStream(fout)) {
            RepairPost g = new RepairPost(basename);
            System.out.println("Nodes: " + g.nodes);
            System.out.println("Edges: " + g.edges);
            System.out.println("Graph size: " + g.size());
            System.out.println("Bits per link: " + (double)g.size() / (double)g.edges * 8.0);
            out.writeInt(g.nodes);
            out.writeInt(g.edges);
            for (int i = 1; i <= g.nodes; i++) {
                int[] adj = g.adj(i);
                int outd = g.outdegree(i);
                Arrays.sort(adj, 1, adj[0] + 1); //** !!!!!!!!!!!!  SORT VALUES  NO HACE FALTA !!!!
                int w = -i;
                out.writeInt(w);
                for (int j = 1; j <= adj[0]; j++) {
                    out.writeInt(adj[j]);
                }
                if (adj[0] > max_outdegree) {
                    max_outdegree=adj[0];
                }
                if (adj[0] != outd) {
                    System.out.println("Error, el outdegree no coincide!!!");
                    break;
                }
            }
            System.out.println("Max outdegree: " + max_outdegree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
}
