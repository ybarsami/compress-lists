/* basics.h
   Copyright (C) 2005, Rodrigo Gonzalez, all rights reserved.

   Some preliminary stuff

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
package repair;

public class Basics {
    
    /* int is 4 bytes in java */
    public static final int sizeOfInt = 4;
    
    public static final int mask31 = 0x0000001F;
    
    /* numero de bits del entero de la maquina */
    public static final int WW32 = 32;
    /* WW32-1 */
    public static final int Wminusone = 31;
    /* numero de bits del entero de la maquina */
    public static final int WW64 = 64;
    
    public static final char[] __popcount_tab = {
        0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
        1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
        1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
        2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
        1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
        2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
        2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
        3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
    };
    
    public static final char[] select_tab = {
        0,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        6,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        7,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        6,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        8,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        6,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        7,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
        6,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,
    };
    
    /* bits needed to represent a number between 0 and n */
    public static int bits(int n) {
        int b = 0;
        while (n != 0) {
            b++;
            n >>>= 1;
        }
        return b;
    }
    
    /* reads bit p from e */
    public static int bitget(int[] e, int p) {
        return ((e[p / WW32] >>> (p % WW32))) & 1;
    }
    
    /* sets bit p in e */
    public static void bitset(int[] e, int p) {
        e[p / WW32] |= (1 << (p % WW32));
    }
    
    /* cleans bit p in e */
    public static void bitclean(int[] e, int p) {
        e[p / WW32] &= ~(1 << (p % WW32));
    }
    
    /* numero de enteros necesarios para representar e elementos de largo n */
    public static int enteros(int e, int n) {
        return (e * n) / WW32 + (((e * n) % WW32 > 0) ? 1 : 0);
    }
    
    /*
     * YANN
     * A is an array that represents the concatenation of len-bits numbers.
     * len is the number of bits of each number stored in this array.
     * here, we want to store the number "x" (which has to be encodable with at
     * most len bits) at the index-th position in A (viewed as an array of
     * len-bits numbers).
     * In fact, A is a 32-bits array, so we have to do the bit computations.
     */
    public static int GetField(int[] A, int len, int index) {
        // int i = index*len/WW32, j = index*len-WW32*i, result;
        int i = (index / WW32) * len + ((index % WW32) * len) / WW32;
        int j = ((index % WW32) * len) % WW32;
        int result;
        if (j + len <= WW32) {
            result = (A[i] << (WW32 - j - len)) >>> (WW32 - len);
        } else {
            result = A[i] >>> j;
            result = result | (A[i+1] << (WW64 - j - len)) >>> (WW32 - len);
        }
        return result;
    }
    public static void SetField(int[] A, int len, int index, int x) {
        assert(bits(x) <= len);
        // int i = index*len/WW32, j = index*len-i*WW32;
        int i = (index / WW32) * len + ((index % WW32) * len) / WW32;
        int j = ((index % WW32) * len) % WW32;
        int mask = ((j + len) < WW32 ? ~0 << (j + len) : 0) | ((WW32-j) < WW32 ? ~0 >>> (WW32-j) : 0);
        A[i] = (A[i] & mask) | x << j;
        if (j + len > WW32) {
            mask = (~0 << (len + j - WW32));
            A[i + 1] = (A[i + 1] & mask) | x >>> (WW32 - j);
        }
    }
    
    /*
     * YANN
     * when len = 32, no bit manipulation is needed, we just use the array A.
     */
    public static int GetFieldW32(int[] A, int index) {
        return A[index];
    }
    public static void SetField32(int[] A, int index, int x) {
        A[index] = x;
    }
    
    /*
     * YANN
     * when len = 16, less bit manipulation is required on the array A.
     */
    public static int GetFieldW16(int[] A, int index) {
        int i = index / 2;
        int j = (index & 1) << 4;
        return (A[i] << (16 - j)) >>> 16;
    }
    
    /*
     * YANN
     * when len = 4, less bit manipulation is required on the array A.
     */
    public static int GetFieldW4(int[] A, int index) {
        int i = index / 8;
        int j = (index & 0x7) << 2;
        /* int i = index/8, j = index*4-32*i; */
        return (A[i] << (28 - j)) >>> 28;
    }
    
    public static int popcount(int x) {
      return  __popcount_tab[ x         & 0xff] +
              __popcount_tab[(x >>>  8) & 0xff] +
              __popcount_tab[(x >>> 16) & 0xff] +
              __popcount_tab[(x >>> 24) & 0xff];
    }
    
    public static int popcount16(int x){
      return  __popcount_tab[ x         & 0xff] +
              __popcount_tab[(x >>>  8) & 0xff];
    }
    
    public static int popcount8(int x){
      return  __popcount_tab[ x         & 0xff];
    }
}