/*
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 117
 * A further development is the \delta code, in which the prefix indicating
 * the number of binary suffix bits is represented by the \gamma code rather
 * than the unary code. Taking the same example of x = 9, the unary prefix
 * of 1110 coding 4 is replaced by 11000, the \gamma code for 4. That is,
 * the \delta code for x = 9 is 11000 001.
 */
package integerencoding;

import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;
import static compresslists.Tools.ilog2;

/**
 *
 * @author yann
 */
public class DeltaEncoding {
    
    public static int computeSizeDelta(int x) {
        return 1 + 2 * Tools.ilog2(1 + Tools.ilog2(x)) + Tools.ilog2(x);
    }
    
    public static void writeCodeDelta(int x, BitOutputStream bitStream) {
        int ilog2x = ilog2(x);
        GammaEncoding.writeCodeGamma(1 + ilog2x, bitStream);
        int residual = x - (1 << ilog2x);
        BinaryEncoding.writeCodeBinary(residual, bitStream, ilog2x);
    }
    
    public static int readCodeDelta(BitInputStream bitStream) {
        int ilog2x = GammaEncoding.readCodeGamma(bitStream) - 1;
        int residual = BinaryEncoding.readCodeBinary(bitStream, ilog2x);
        return residual + (1 << ilog2x);
    }
    
}
