/*
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 117
 * One is the \gamma code, which represents the number x as a unary code for
 * 1 + floor(log_2(x)) followed by a code of floor(log_2(x)) bits that
 * represents the value of x - 2^{floor(x)} in binary. The unary part specifies
 * how many bits are required to code x, and then the binary part actually codes
 * x in that many bits. For example, consider x = 9. Then floor(log_2(x)) = 3,
 * and so 4 = 1 + 3 is coded in unary (code 1110) followed by 1 = 9 - 8 as a
 * three-bit binary number (code 001), which combine to give a codeword of
 * 1110 001.
 */
package integerencoding;

import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;
import static compresslists.Tools.ilog2;

/**
 *
 * @author yann
 */
public class GammaEncoding {
    
    public static int computeSizeGamma(int x) {
        return 1 + 2 * Tools.ilog2(x);
    }
    
    public static void writeCodeGamma(int x, BitOutputStream bitStream) {
        int ilog2x = ilog2(x);
        UnaryEncoding.writeCodeUnary(1 + ilog2(x), bitStream);
        int residual = x - (1 << ilog2x);
        BinaryEncoding.writeCodeBinary(residual, bitStream, ilog2x);
    }
    
    public static int readCodeGamma(BitInputStream bitStream) {
        int ilog2x = UnaryEncoding.readCodeUnary(bitStream) - 1;
        int residual = BinaryEncoding.readCodeBinary(bitStream, ilog2x);
        return residual + (1 << ilog2x);
    }
    
}
