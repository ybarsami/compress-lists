/*
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 117
 * One such code is the unary code. In this code an integer x >= 1 is coded
 * as x - 1 one bits followed by a zero bit, so that the code for integer 3
 * is 110.
 */
package integerencoding;

import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class UnaryEncoding {
    
    public static int computeSizeUnary(int x) {
        return x;
    }
    
    public static void writeCodeUnary(int x, BitOutputStream bitStream) {
        assert(x >= 1);
        // x - 1 "1"
        for (int i = 0; i < x - 1; i++) {
            bitStream.writeBit(1);
        }
        // One "0"
        bitStream.writeBit(0);
    }
    
    public static int readCodeUnary(BitInputStream bitStream) {
        int value = 0;
        boolean hasReadAZero = false;
        while (!hasReadAZero) {
            // Extract a bit.
            int bitRead = bitStream.getNextBit();
            if (bitRead == 1) {
                value++;
            } else {
                hasReadAZero = true;
            }
        }
        return value + 1;
    }
    
    public static void writeCodeUnaryInverted(int x, BitOutputStream bitStream) {
        assert(x >= 1);
        // x - 1 "0"
        for (int i = 0; i < x - 1; i++) {
            bitStream.writeBit(0);
        }
        // One "1"
        bitStream.writeBit(1);
    }
    
}
