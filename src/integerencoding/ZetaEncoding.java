/*
 * Boldi and Vigna, "The WebGraph Framework II: Codes For The World-Wide Web" (2003)
 * Given a fixed positive integer k, the shrinking factor, a positive integer x
 * in the interval [2^{hk}, 2^{(h+1)k} - 1] is Zeta_k-coded by writing h + 1 in
 * unary, followed by a minimal binary coding of x - 2^{hk} in the interval
 * [0, 2^{(h+1)k} - 2^{hk} - 1].
 * Note that for our purposes the minimal binary coding of x in the interval
 * [0, z - 1] is defined as follows: let s = ceiling(log_2(z)); if x < 2^s - z,
 * then x is coded using the x-th binary word of length s - 1 (in
 * lexicographical order); otherwise, x is coded using the (x - z + 2^s)-th
 * binary word of length s.
 * 
 * Remark: Zeta(1) = Gamma.
 */
package integerencoding;

import io.BitInputStream;
import io.BitOutputStream;
import static compresslists.Tools.ilog2;

/**
 *
 * @author yann
 */
public class ZetaEncoding {
    
    public static int computeSizeZeta(int x, int k) {
        int h = ilog2(x) / k;
        return (x - (1 << (h * k)) < (1 << (h * k)))
                    ? (h + 1) * (k + 1) - 1
                    : (h + 1) * (k + 1);
    }
    
    public static void writeCodeZeta(int x, BitOutputStream bitStream, int k) {
        int h = ilog2(x) / k;
        UnaryEncoding.writeCodeUnary(1 + h, bitStream);
        // z = 2^{(h+1)k} - 2^{hk}
        // s = ceiling(log_2(z)) = (h + 1) * k
        // 2^s - z = 2^{(h+1)k} - (2^{(h+1)k} - 2^{hk}) = 2^{hk} = 2^ilog2x
        int residual = x - (1 << (h * k));
        if (residual < 1 << (h * k)) {
            BinaryEncoding.writeCodeBinary(residual, bitStream, (h + 1) * k - 1);
        } else {
            BinaryEncoding.writeCodeBinary(x, bitStream, (h + 1) * k);
        }
    }
    
    public static int readCodeZeta(BitInputStream bitStream, int k) {
        int h = UnaryEncoding.readCodeUnary(bitStream) - 1;
        int readTest = BinaryEncoding.readCodeBinary(bitStream, (h + 1) * k - 1);
        if (readTest < 1 << (h * k)) {
            return readTest + (1 << (h * k));
        } else {
            return readTest * 2 + bitStream.getNextBit();
        }
    }
    
}
