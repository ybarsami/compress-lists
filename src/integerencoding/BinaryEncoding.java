/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerencoding;

import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class BinaryEncoding {
    
    /*
     * Writes x on just nbBitsToWrite bits.
     * Assumes that 0 <= x < 2^nbBitsToWrite.
     */
    public static void writeCodeBinary(int x, BitOutputStream bitStream, int nbBitsToWrite) {
        assert(nbBitsToWrite >= 0);
        assert(x >= 0);
        assert(x < (1 << nbBitsToWrite));
        int bitMask = 1 << (nbBitsToWrite - 1);
        for (int j = 0; j < nbBitsToWrite; j++) {
            bitStream.writeBit((x & bitMask) != 0);
            bitMask >>= 1;
        }
    }
    
    public static int readCodeBinary(BitInputStream bitStream, int nbBitsToRead) {
        int value = 0;
        for (int j = 0; j < nbBitsToRead; j++) {
            // Extract a bit.
            int bitRead = bitStream.getNextBit();
            value *= 2;
            value += bitRead;
        }
        return value;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Binary codes when coding numbers that fall in [lo..hi].
    // In all the coding functions hereafter, we assume that lo <= x <= hi.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Let us define r as hi - lo + 1.
     * Writes x on just ceiling(log_2(r)) bits.
     * Assumes that lo <= x <= hi.
     */
    public static void writeCodeBinaryNaive(int x, BitOutputStream bitStream, int lo, int hi) {
        writeCodeBinary(x - lo, bitStream, Tools.ceilingLog2(hi - lo + 1));
    }
    public static int computeSizeCodeBinaryNaive(int x, int lo, int hi) {
        return Tools.ceilingLog2(hi - lo + 1);
    }
    public static int readCodeBinaryNaive(BitInputStream bitInputStream, int lo, int hi) {
        // The number of bits we have to read for the current int to extract.
        int nbBitsToRead = Tools.ceilingLog2(hi - lo + 1);
        return readCodeBinary(bitInputStream, nbBitsToRead) + lo;
    }
    
    // Minimal binary code, the shorter code values at the left of [lo..hi].
    public static void writeCodeBinaryLefterShorter(int x, BitOutputStream bitStream, int lo, int hi) {
        // we remap x in [lo..hi] by val in [0..r).
        int val = x - lo;
        int r = hi - lo + 1;
        assert(r > 0);
        assert(val < r);
        int floor = Tools.ilog2(r);
        long nbShortCodes = (1L << (floor + 1)) - r;
        if (val < nbShortCodes) {
            writeCodeBinary(val, bitStream, floor);
        } else {
            val += nbShortCodes;
            writeCodeBinary(val, bitStream, floor + 1);
        }
    }
    public static int computeSizeCodeBinaryLefterShorter(int x, int lo, int hi) {
        // we remap x in [lo..hi] by val in [0..r).
        int val = x - lo;
        int r = hi - lo + 1;
        assert(r > 0);
        assert(val < r);
        int floor = Tools.ilog2(r);
        int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
        if (val < nbShortCodes) {
            return floor;
        } else {
            return floor + 1;
        }
    }
    public static int readCodeBinaryLefterShorter(BitInputStream bitInputStream, int lo, int hi) {
        // we remap [lo..hi] by [0..r).
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
        int val = readCodeBinary(bitInputStream, floor);
        if (val < nbShortCodes) {
            return val + lo;
        } else {
            return (val << 1) + readCodeBinary(bitInputStream, 1) - nbShortCodes + lo;
        }
    }
    
    // Minimal binary code, the shorter code values at the center of [lo..hi].
    /*
     * Let us define r as hi - lo + 1.
     * Writes x on just floor = floor(log_2(r)) or ceiling = ceiling(log_2(r))
     * bits. Let us define nbShortCodes as 2^(ceiling) - r.
     * This encoding uses floor bits for the nbShortCodes middle values, and
     * ceiling bits for the remaining 2r - 2^(ceiling) outer values.
     * Assumes that lo <= x <= hi.
     *
     * --- FIRST VALUES --- i.e. val in {1, ..., (r - 2^(ceiling - 1))}
     * [binaryCodeOf(nbShortCodes + val - 1) on floor bits + '0']
     *  --- MIDDLE VALUES ---
     * [binaryCodeOf(val - r + 2^(ceiling - 1) - 1) on floor bits]
     * --- LAST VALUES --- i.e. i in {(2^(ceiling - 1) + 1), ..., r}
     * [binaryCodeOf(nbShortCodes + val - 2^(ceiling - 1) - 1) on floor bits + '1']
     */
    public static void writeCodeBinaryInnerShorter(int x, BitOutputStream bitStream, int lo, int hi) {
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int ceiling = Tools.ceilingLog2(r);
        if (floor == ceiling) {
            writeCodeBinaryNaive(x, bitStream, lo, hi);
        } else {
            int val = x - lo + 1; // we remap x in [lo..hi] by val in [1..r].
            int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
            int powerTwo = 1 << (ceiling - 1);
            if (val <= r - powerTwo) {
                writeCodeBinary(nbShortCodes + val - 1, bitStream, floor);
                bitStream.writeBit(0);
            } else if (val >= powerTwo + 1) {
                writeCodeBinary(nbShortCodes + val - powerTwo - 1, bitStream, floor);
                bitStream.writeBit(1);
            } else {
                writeCodeBinary(val - r + powerTwo - 1, bitStream, floor);
            }
        }
    }
    public static int computeSizeCodeBinaryInnerShorter(int x, int lo, int hi) {
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int ceiling = Tools.ceilingLog2(r);
        if (floor == ceiling) {
            return computeSizeCodeBinaryNaive(x, lo, hi);
        } else {
            int val = x - lo + 1; // we remap x in [lo..hi] by val in [1..r].
            int powerTwo = 1 << (ceiling - 1);
            if ((val <= r - powerTwo) || (val >= powerTwo + 1)) {
                return ceiling;
            } else {
                return floor;
            }
        }
    }
    public static int readCodeBinaryInnerShorter(BitInputStream bitInputStream, int lo, int hi) {
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int ceiling = Tools.ceilingLog2(r);
        if (floor == ceiling) {
            return readCodeBinaryNaive(bitInputStream, lo, hi);
        } else {
            int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
            int powerTwo = 1 << (ceiling - 1);
            int val = readCodeBinary(bitInputStream, floor);
            if (val < nbShortCodes) {
                return val + r - powerTwo + lo;
            } else {
                int bit = readCodeBinary(bitInputStream, 1);
                if (bit == 0) {
                    return val - nbShortCodes + lo;
                } else {
                    return val - nbShortCodes + powerTwo + lo;
                }
            }
        }
    }
    
    // Minimal binary code, the shorter code values at the edges of [lo..hi].
    /*
     * Let us define r as hi - lo + 1.
     * Writes x on just floor = floor(log_2(r)) or ceiling = ceiling(log_2(r))
     * bits. Let us define nbShortCodes as 2^(ceiling) - r.
     * This encoding uses floor bits for the nbShortCodes outer values, and
     * ceiling bits for the remaining 2r - 2^(ceiling) middle values.
     * Assumes that lo <= x <= hi.
     *
     * --- FIRST VALUES --- i.e. val in {1, ..., floor(nbShortCodes / 2)}
     * [binaryCodeOf(val - 1) on floor bits]
     *  --- MIDDLE VALUES ---
     * [binaryCodeOf(val - floor(nbShortCodes / 2) - 1 + 2 * nbShortCodes) on ceiling bits]
     * --- LAST VALUES --- i.e. i in {r - ceiling(nbShortCodes / 2) + 1, ..., r}
     * [binaryCodeOf(val - r - 1 + nbShortCodes) on floor bits]
     */
    public static void writeCodeBinaryOuterShorter(int x, BitOutputStream bitStream, int lo, int hi) {
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int ceiling = Tools.ceilingLog2(r);
        if (floor == ceiling) {
            writeCodeBinaryNaive(x, bitStream, lo, hi);
        } else {
            int val = x - lo + 1; // we remap x in [lo..hi] by i in [1..r].
            int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
            if (val <= nbShortCodes / 2) {
                writeCodeBinary(val - 1, bitStream, floor);
            } else if (val >= r - Tools.ceilingDivision(nbShortCodes, 2) + 1) {
                writeCodeBinary(val - r - 1 + nbShortCodes, bitStream, floor);
            } else {
                writeCodeBinary(val - nbShortCodes / 2 - 1 + 2 * nbShortCodes, bitStream, ceiling);
            }
        }
    }
    public static int computeSizeCodeBinaryOuterShorter(int x, int lo, int hi) {
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int ceiling = Tools.ceilingLog2(r);
        if (floor == ceiling) {
            return computeSizeCodeBinaryNaive(x, lo, hi);
        } else {
            int i = x - lo + 1; // we remap x in [lo..hi] by i in [1..r].
            int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
            if ((i <= nbShortCodes / 2) || (i >= r - Tools.ceilingDivision(nbShortCodes, 2) + 1)) {
                return floor;
            } else {
                return ceiling;
            }
        }
    }
    public static int readCodeBinaryOuterShorter(BitInputStream bitInputStream, int lo, int hi) {
        int r = hi - lo + 1;
        int floor = Tools.ilog2(r);
        int ceiling = Tools.ceilingLog2(r);
        if (floor == ceiling) {
            return readCodeBinaryNaive(bitInputStream, lo, hi);
        } else {
            int nbShortCodes = (int)((1L << (floor + 1)) - r); // Temporary use of long if b is too big.
            int firstReadCode = readCodeBinary(bitInputStream, floor);
            if (firstReadCode < nbShortCodes / 2) {
                return firstReadCode + lo;
            } else if (firstReadCode < nbShortCodes) {
                return firstReadCode + r - nbShortCodes + lo;
            } else {
                return (firstReadCode << 1) + readCodeBinary(bitInputStream, 1) + nbShortCodes / 2 - 2 * nbShortCodes + lo;
            }
        }
    }
    
}
