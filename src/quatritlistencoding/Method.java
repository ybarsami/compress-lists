/*
 * Remark: quatritlistencoding.Method is (almostà the QuatritSequence equivalent
 * of gaparraylistencoding.MethodByBitSequence.
 * bitlistencoding.Method and tritlistencoding.Method are also doing the same
 * stuff.
 *
 * Here, we don't need to handle directly an index, as these methods are only
 * used by MethodByQuatritConcatenation.
 */
package quatritlistencoding;

import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public abstract class Method {
    
    /*
     * Some methods need to be initialized according to the quatrit sequence
     * list it handles.
     */
    public void initBefore() {}
    public void init(QuatritSequence quatritSequence) {}
    public void initAfter() {}
    
    /*
     * Some methods need to be initialized according to the occurrences that
     * have been pre-computed and stored in a file.
     */
    public void init(String filename) {}
    
    public abstract String getName();
    
    /*
     * Compute the size, in bits, that will be taken to code this quatrit
     * sequence.
     */
    public void computeSizeBefore() {}
    public abstract double computeSize(QuatritSequence quatritSequence);
    public double computeSizeAfter() { return 0.; }
    
    /*
     * Compute the size, in bits, that will be taken to code this quatrit
     * sequence list.
     */
    public double computeSize(ArrayList<QuatritSequence> quatritSequenceList) {
        double size = computeSizeNeededInformation();
        for (QuatritSequence quatritSequence : quatritSequenceList) {
            size += computeSize(quatritSequence);
        }
        return size;
    }
    
    /*
     * Log additional information.
     */
    public void standardOutput() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of additional material needed by the method.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public double computeSizeNeededInformation() {
        return 0.0;
    }
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void exportNeededInformation(DataOutputStream out) throws IOException {}
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void importNeededInformation(DataInputStream in) throws IOException {}
    
    /*
     * Some methods keep some bits to be written at the end.
     */
    public void outputRemainingBits(BitOutputStream bitStream) {}
    
    /*
     * All the MethodByBitSequence are obliged to read byte by byte. When
     * considering padding, the last bits of the last byte read can be
     * discarded. But when we don't use padding, we have to keep them.
     */
    public void flushRemainingBits() {}
    
    public abstract void writeQuatritSequence(BitOutputStream bitStream, QuatritSequence quatritSequence);
    
    public abstract void readQuatritSequence(BitInputStream bitStream, int nbThreesToRead, QuatritSequence quatritSequence);
    
}
