/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatritlistencoding;

/**
 *
 * @author yann
 */
public class QuatritSequenceSingleReader extends QuatritSequenceSingleHandler implements QuatritSequenceAbstractReader {
    
    public QuatritSequenceSingleReader(int k, int w, int kInit, boolean merge01ForContext) {
        super(k, w, kInit, merge01ForContext);
    }
    
    public QuatritSequenceSingleReader(int k, int w, int kInit) {
        this(k, w, kInit, false);
    }
    
    @Override
    public void init(QuatritSequence quatritSequence) {
        init();
        this.quatritSequence = quatritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbTritsHandled + nbThreesHandled < quatritSequence.size();
    }
    
}
