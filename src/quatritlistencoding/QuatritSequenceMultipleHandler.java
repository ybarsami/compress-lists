/*
 * Handles a list of quatrits so that there is:
 *     a) an initial pass
 *     b) a generic pass
 */
package quatritlistencoding;

import compresslists.Tools;
import static compresslists.Tools.nbQuatrits;
import static compresslists.Tools.nbTrits;

import java.util.Arrays;

/**
 *
 * @author yann
 */
public abstract class QuatritSequenceMultipleHandler implements QuatritSequenceAbstractHandler {
    
    // Parameters
    private final int kInit;
    private final int k;
    private final int[] wList;
    private final int wSum;
    // 2 symbols if we merge 0 and 1, 3 symbols otherwise
    private final boolean merge01ForContext;
    private final int nbSymbolsForContext;
    
    // Initial index.
    private int nbIndexesInitBefore;
    // indexInit is a bijection between kInit-uplets of trits and { 0, ..., nbSymbolsForContext^kInit - 1 }.
    private int indexInit;
    
    // Generic index.
    // Number of "2" in the w trits before the k last ones. This is a value in { 0, ..., w }.
    private final int[] numberOfTwos;
    // subIndex is a bijection between k-uplets of trits and { 0, ..., nbSymbolsForContext^k - 1 }.
    private int subIndex;
    // index is a bijection between the k-uplet of preceding trits,
    // the number of "2" in the w trits before the k preceding trits,
    // and { 0, ..., nbSymbolsForContext^k * (w+1) - 1 }.
    private int indexGeneric;
    private final int[] indexNextTritToLeaveTheWTrits;
    private final int[] indexNextTritToEnterTheWTrits;
    
    // The quatrit list to handle.
    protected int nbTritsHandled;
    protected int nbThreesHandled;
    protected QuatritSequence quatritSequence;
    
    public QuatritSequenceMultipleHandler(int k, int[] wList, int kInit, boolean merge01ForContext) {
        this.k = k;
        this.wList = wList;
        this.kInit = kInit;
        this.merge01ForContext = merge01ForContext;
        nbSymbolsForContext = merge01ForContext ? nbTrits - 1 : nbTrits;
        wSum = Arrays.stream(wList).reduce(0, Integer::sum);
        numberOfTwos = new int[wList.length];
        indexNextTritToLeaveTheWTrits = new int[wList.length];
        indexNextTritToEnterTheWTrits = new int[wList.length];
    }
    
    @Override
    public void init() {
        this.nbIndexesInitBefore = 0;
        this.indexInit = 0;
        this.nbTritsHandled = 0;
        this.nbThreesHandled = 0;
    }
    
    @Override
    public int getCurrentQuatrit() {
        return quatritSequence.get(nbTritsHandled + nbThreesHandled);
    }
    
    public static int quatrit2symbol(int quatrit) {
        switch(quatrit) {
            case 0:
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            default:
                throw new RuntimeException("Use quatrit2symbol only on integer values in {0, 1, 2, 3}.");
        }
    }
    
    private int getSymbol(int position) {
        return merge01ForContext
                ? quatrit2symbol(quatritSequence.get(position))
                : quatritSequence.get(position);
    }
    
    protected void initializeGenericIndex() {
        int currentIndex = nbTritsHandled + nbThreesHandled - 1;
        // Computation of the 'k' zone.
        int nbTrits = 0;
        while (nbTrits < k) {
            if (quatritSequence.get(currentIndex) != 3) {
                nbTrits++;
            }
            currentIndex--;
        }
        for (int i = 0; i < wList.length; i++) {
            indexNextTritToEnterTheWTrits[i] = currentIndex + 1;
            // Computation of the 'w' zone.
            nbTrits = 0;
            while (nbTrits < wList[i]) {
                if (quatritSequence.get(currentIndex) != 3) {
                    nbTrits++;
                }
                currentIndex--;
            }
            indexNextTritToLeaveTheWTrits[i] = currentIndex + 1;
        }
        // Initial computation of the indexes.
        for (int i = 0; i < wList.length; i++) {
            numberOfTwos[i] = quatritSequence.numberOfTwos(
                    indexNextTritToLeaveTheWTrits[i],
                    indexNextTritToEnterTheWTrits[i]);
        }
        subIndex = 0;
        int indexStartK = wList.length == 0 ? currentIndex + 1 : indexNextTritToEnterTheWTrits[0];
        for (int i = indexStartK; i < nbTritsHandled + nbThreesHandled; i++) {
            int symbol = getSymbol(i);
            // symbol == nbSymbolsForContext means that the quatrit is a 3
            if (symbol != nbSymbolsForContext) {
                subIndex *= nbSymbolsForContext;
                subIndex += symbol;
            }
        }
        indexGeneric = subIndex;
        for (int i = 0; i < wList.length; i++) {
            indexGeneric = (wList[i] + 1) * indexGeneric + numberOfTwos[i];
        }
    }
    
    @Override
    public boolean isInInitialState() {
        if (nbTritsHandled >= k + wSum) {
            initializeGenericIndex();
            return false;
        }
        return isStillActive();
    }
    
    @Override
    public void updateInitialIndex() {
        int currentSymbol = getSymbol(nbTritsHandled + nbThreesHandled);
        // currentSymbol == nbSymbolsForContext means that the quatrit is a 3
        if (currentSymbol == nbSymbolsForContext) {
            nbThreesHandled++;
        } else {
            indexInit = (nbSymbolsForContext * indexInit + currentSymbol) % Tools.pow(nbSymbolsForContext, kInit);
            nbTritsHandled++;
            if (nbTritsHandled <= kInit) {
                nbIndexesInitBefore = nbQuatrits * (1 - Tools.pow(nbSymbolsForContext, nbTritsHandled)) / (1 - nbSymbolsForContext);
            }
        }
    }
    
    @Override
    public int getInitialIndex() {
        return nbIndexesInitBefore + indexInit;
    }
    
    @Override
    public boolean isInGenericState() {
        return isStillActive() && nbTritsHandled >= k + wSum;
    }
    
    @Override
    public void updateGenericIndex() {
        int currentSymbol = getSymbol(nbTritsHandled + nbThreesHandled);
        // currentSymbol == nbSymbolsForContext means that the quatrit is a 3
        if (currentSymbol == nbSymbolsForContext) {
            nbThreesHandled++;
        } else {
            for (int i = 0; i < wList.length; i++) {
                // We here make the assumption that no two '3' can be in a row,
                // because there are no empty trit sequences. If we need a more
                // generic approach, we can transform it with a while.
                if (quatritSequence.get(indexNextTritToLeaveTheWTrits[i]) == 3) {
                    indexNextTritToLeaveTheWTrits[i]++;
                }
                if (quatritSequence.get(indexNextTritToEnterTheWTrits[i]) == 3) {
                    indexNextTritToEnterTheWTrits[i]++;
                }
                if (quatritSequence.get(indexNextTritToLeaveTheWTrits[i]) == 2) {
                    numberOfTwos[i]--;
                }
                if (quatritSequence.get(indexNextTritToEnterTheWTrits[i]) == 2) {
                    numberOfTwos[i]++;
                }
                indexNextTritToLeaveTheWTrits[i]++;
                indexNextTritToEnterTheWTrits[i]++;
            }
            subIndex = (nbSymbolsForContext * subIndex + currentSymbol) % Tools.pow(nbSymbolsForContext, k);
            indexGeneric = subIndex;
            for (int i = 0; i < wList.length; i++) {
                indexGeneric = (wList[i] + 1) * indexGeneric + numberOfTwos[i];
            }
            nbTritsHandled++;
        }
    }
    
    @Override
    public int getGenericIndex() {
        return indexGeneric;
    }
    
}
