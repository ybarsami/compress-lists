/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatritlistencoding;

/**
 *
 * @author yann
 */
public class QuatritSequenceSingleWriter extends QuatritSequenceSingleHandler implements QuatritSequenceAbstractWriter {
    
    private int nbThreesRead;
    private int nbThreesToRead;
    
    public QuatritSequenceSingleWriter(int k, int w, int kInit, boolean merge01ForContext) {
        super(k, w, kInit, merge01ForContext);
    }
    
    public QuatritSequenceSingleWriter(int k, int w, int kInit) {
        this(k, w, kInit, false);
    }
    
    @Override
    public void init(int nbThreesToRead, QuatritSequence quatritSequence) {
        init();
        this.nbThreesToRead = nbThreesToRead;
        nbThreesRead = 0;
        quatritSequence.reset();
        this.quatritSequence = quatritSequence;
    }
    
    @Override
    public void addQuatrit(int quatrit) {
        quatritSequence.add(quatrit);
        if (quatrit == 3) {
            nbThreesRead++;
        }
    }
    
    @Override
    public QuatritSequence getQuatritSequence() {
        return quatritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbThreesRead < nbThreesToRead;
    }
    
}
