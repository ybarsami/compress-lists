/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatritlistencoding;

/**
 *
 * @author yann
 */
public class QuatritSequenceMultipleWriter extends QuatritSequenceMultipleHandler implements QuatritSequenceAbstractWriter {
    
    private int nbThreesRead;
    private int nbThreesToRead;
    
    public QuatritSequenceMultipleWriter(int k, int[] wList, int kInit, boolean merge01ForContext) {
        super(k, wList, kInit, merge01ForContext);
    }
    
    public QuatritSequenceMultipleWriter(int k, int[] wList, int kInit) {
        this(k, wList, kInit, false);
    }
    
    @Override
    public void init(int nbThreesToRead, QuatritSequence quatritSequence) {
        init();
        this.nbThreesToRead = nbThreesToRead;
        nbThreesRead = 0;
        quatritSequence.reset();
        this.quatritSequence = quatritSequence;
    }
    
    @Override
    public void addQuatrit(int quatrit) {
        quatritSequence.add(quatrit);
        if (quatrit == 3) {
            nbThreesRead++;
        }
    }
    
    @Override
    public QuatritSequence getQuatritSequence() {
        return quatritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbThreesRead < nbThreesToRead;
    }
    
}
