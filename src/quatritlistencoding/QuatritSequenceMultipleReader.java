/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatritlistencoding;

/**
 *
 * @author yann
 */
public class QuatritSequenceMultipleReader extends QuatritSequenceMultipleHandler implements QuatritSequenceAbstractReader {
    
    public QuatritSequenceMultipleReader(int k, int[] wList, int kInit, boolean merge01ForContext) {
        super(k, wList, kInit, merge01ForContext);
    }
    
    public QuatritSequenceMultipleReader(int k, int[] wList, int kInit) {
        this(k, wList, kInit, false);
    }
    
    @Override
    public void init(QuatritSequence quatritSequence) {
        init();
        this.quatritSequence = quatritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbTritsHandled + nbThreesHandled < quatritSequence.size();
    }
    
}
