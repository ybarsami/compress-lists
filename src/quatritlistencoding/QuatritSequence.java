/*
 * Sequence of quatrits (integers in {0, 1, 2, 3}).
 *
 * In TritSequence.java, we encode a trit on 2 bits, so we can use the same
 * underlying code to manage quatrit sequences.
 */
package quatritlistencoding;

import tritlistencoding.TritSequence;

/**
 *
 * @author yann
 */
public class QuatritSequence extends TritSequence {
    
    /**
     * Creates a new instance of QuatritSequence.
     */
    public QuatritSequence() {
        super();
    }
    
    public QuatritSequence(String s) {
        super(s);
    }
    
    /*
     * WARNING: it does not copy anything but the pointers, so any modification
     * of bitSet1 / bitSet2 in the trit sequence given as argument or the newly
     * created quatrit sequence will affect the other one.
     */
    public QuatritSequence(TritSequence tritSequence) {
        super(tritSequence);
    }
    
    /*
     * Copies a quatrit sequence from the specified source sequence, beginning
     * at the specified position, to the destination sequence.
     * <p>
     * A subsequence of quatrits are copied from the source sequence referenced
     * by <code>src</code> to the destination sequence referenced by
     * <code>dest</code>. The number of quatrits copied is equal to
     * <code>endIndex - beginIndex + 1</code> argument. The components at
     * positions <code>beginIndex</code> through <code>endIndex - 1</code> in
     * the source sequence are copied into positions <code>0</code> through
     * <code>endIndex - beginIndex + 1</code>, respectively, of the destination
     * sequence.
     * <p>
     * If the <code>src</code> and <code>dest</code> arguments refer to the
     * same QuatritSequence object, then it empties it without copying anything.
     * <p>
     * If <code>dest</code> is <code>null</code>, then a
     * <code>NullPointerException</code> is thrown.
     * <p>
     * If <code>src</code> is <code>null</code>, then a
     * <code>NullPointerException</code> is thrown and the destination
     * sequence is not modified.
     */
    public static void sequenceCopy(QuatritSequence src, int beginIndex, int endIndex, QuatritSequence dest) {
        if (src == null) {
            throw new NullPointerException();
        }
        dest.reset();
        for (int i = beginIndex; i < endIndex; i++) {
            dest.addChecked(src.get(i));
        }
    }
    
    /*
     * Append the specified value to the quatrit sequence.
     *
     * @param value an integer value to append.
     */
    @Override
    public void add(int value) throws IndexOutOfBoundsException {
        switch(value) {
            case 0:
            case 1:
            case 2:
            case 3:
                addChecked(value);
                return;
            default:
                throw new RuntimeException("Use QuatritSequence only on integer values in {0, 1, 2, 3}.");
        }
    }
    
    /*
     * Concatenate the specified quatrit sequence to the current quatrit
     * sequence.
     *
     * @param value a quatrit sequence to be concatenated.
     */
    public void concatenate(QuatritSequence quatritSequence) throws IndexOutOfBoundsException {
        try {
            Math.addExact(size(), quatritSequence.size());
            for (int i = 0; i < quatritSequence.size(); i++) {
                addChecked(quatritSequence.get(i));
            }
        } catch (ArithmeticException e) {
            throw new IndexOutOfBoundsException();
        }
    }
    
}
