/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatritlistencoding;

/**
 *
 * @author yann
 */
public interface QuatritSequenceAbstractHandler {
    
    public void init();
    
    public int getCurrentQuatrit();
    
    public boolean isStillActive();
    
    public boolean isInInitialState();
    
    public void updateInitialIndex();
    
    public int getInitialIndex();
    
    public boolean isInGenericState();
    
    public void updateGenericIndex();
    
    public int getGenericIndex();
    
}
