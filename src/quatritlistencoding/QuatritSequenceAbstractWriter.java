/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatritlistencoding;

/**
 *
 * @author yann
 */
public interface QuatritSequenceAbstractWriter extends QuatritSequenceAbstractHandler {
    
    public void init(int nbThreesToRead, QuatritSequence quatritSequence);
    
    public void addQuatrit(int quatrit);
    
    public QuatritSequence getQuatritSequence();
    
}
