/*
 * Approximation algorithm to find the best partition for the Quasi-Succinct
 * method.
 * Ottaviano and Venturini, "Partitioned Elias-Fano Indexes" (2014)
 *
 * Original source code:
 * https://github.com/ot/partitioned_elias_fano
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodQuasiSuccinctApproxBlock extends MethodQuasiSuccinct {
    
    private static MethodByElement nbDocumentsMethod = new MethodDelta();
    
    // As a small optimization, we handle differently the singleton gap arrays.
    // We always know in advance the sizes of each gap array. Thus, when the
    // array is a singleton, it is enough to just store the single gap, without
    // re-encoding the size "1".
    private boolean specialCaseSingletonGapArrays = true;
    
    // Ottaviano and Venturini, "Partitioned Elias-Fano Indexes" (2014)
    // p.7: Hence, we set \epsilon_2 = 0.3 to control the construction time."
    // p.8: "In the following, we set \epsilon_1 = 0.03."
    private double epsilon1 = 0.03;
    private double epsilon2 = 0.3;
    
    private class OptimalPartition {
        
        ArrayList<Integer> partition = new ArrayList<>();
        double costOpt = 0; // the costs are in bits
        
        private class CostWindow {
            // a window represents the cost of the interval [start, end) from
            // the array documentArray
            
            int[] documentArray;
            int offset;
            // starting and ending position of the window
            int start;
            int end; // end-th position is not in the current window
            int docIdBase; // element that preceed the first element of the window
            int docIdMax;
            
            double costUpperBound; // The maximum cost for this window
            
            public CostWindow(IntsRef documentArrayRef, double costUpperBound) {
                this.documentArray = documentArrayRef.ints;
                this.offset = documentArrayRef.offset;
                this.start = 0;
                this.end = 0;
                this.docIdBase = 0;
                this.docIdMax = documentArray[offset];
                this.costUpperBound = costUpperBound;
            }
            
            int universe() {
                return docIdMax - docIdBase + 1;
            }
            
            int size() {
                return end - start;
            }
            
            void advance_start() {
                docIdBase = documentArray[offset + start] + 1;
                ++start;
            }
            
            void advance_end() {
                docIdMax = documentArray[offset + end];
                ++end;
            }

        }
        
        public OptimalPartition(IntsRef documentArrayRef, double eps1, double eps2) {
            int size = documentArrayRef.length;
            double singleBlockCost =
                    nbDocumentsMethod.computeSize(size) +
                    MethodQuasiSuccinct.computeBlockUpperBound(documentArrayRef.ints[documentArrayRef.offset + documentArrayRef.length - 1], documentArrayRef.length);
            int arraySize = size + 1;
            // Current minimum cost found for a window ending at position i.
            double[] minCost = new double[arraySize];
            for (int i = 0; i < arraySize; i++) {
                minCost[i] = singleBlockCost;
            }
            minCost[0] = 0.;
            
            // create the required window: one for each power of approx_factor
            ArrayList<CostWindow> windows = new ArrayList<>();
            double costLowerBound =
                    nbDocumentsMethod.computeSize(1) +
                    MethodQuasiSuccinct.computeBlockUpperBound(1, 1); // minimum cost
            double costBound = costLowerBound;
            while (eps1 == 0 || costBound < costLowerBound / eps1) {
                windows.add(new CostWindow(documentArrayRef, costBound));
                if (costBound >= singleBlockCost) {
                    break;
                }
                costBound = costBound * (1 + eps2);
            }
            
            int[] path = new int[arraySize];
            for (int i = 0; i < arraySize; i++) {
                path[i] = 0;
            }
            for (int i = 0; i < size; i++) {
                int lastEnd = i + 1;
                for (CostWindow window : windows) {
                    assert(window.start == i);
                    while (window.end < lastEnd) {
                        window.advance_end();
                    }
                    
                    double windowCost;
                    while (true) {
                        windowCost =
                                nbDocumentsMethod.computeSize(window.size()) +
                                MethodQuasiSuccinct.computeBlockUpperBound(window.universe(), window.size());
                        if ((minCost[i] + windowCost < minCost[window.end])) {
                            minCost[window.end] = minCost[i] + windowCost;
                            path[window.end] = i;
                        }
                        lastEnd = window.end;
                        if (window.end == size) {
                            break;
                        }
                        if (windowCost >= window.costUpperBound) {
                            break;
                        }
                        window.advance_end();
                    }
                    
                    window.advance_start();
                }
            }
            
            int curr_pos = size;
            while (curr_pos != 0) {
                partition.add(curr_pos);
                curr_pos = path[curr_pos];
            }
            // reverse the ArrayList position
            for (int i = 0; i < partition.size() / 2; i++) {
                int tmp = partition.get(i);
                int id_to_swap = partition.size() - i - 1;
                partition.set(i, partition.get(id_to_swap));
                partition.set(id_to_swap, tmp);
            }
            costOpt = minCost[size];
        }
    }
    
    /**
     * Creates a new instance of MethodQuasiSuccinctApproxBlock.
     */
    public MethodQuasiSuccinctApproxBlock() {
        super(1);
    }
    
    @Override
    public String getName() {
        return "QuasiSuccinctApproxBlock";
    }
    
    @Override
    protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
        int offset = documentArrayRef.offset;
        if (specialCaseSingletonGapArrays && documentArrayRef.length == 1) {
            return universeMethod.computeSize(documentArrayRef.ints[offset]);
        }
        OptimalPartition optimalPartition = new OptimalPartition(documentArrayRef, epsilon1, epsilon2);
        int baseDocId = 0;
        double size = 0.;
        int firstPos = 0;
        for (int lastPos : optimalPartition.partition) {
            int nbDocumentsLocal = lastPos - firstPos;
            size += nbDocumentsMethod.computeSize(nbDocumentsLocal);
            size += super.computeSizeBlockDocumentArray(new IntsRef(
                    documentArrayRef.ints,
                    offset + firstPos,
                    nbDocumentsLocal), baseDocId);
            baseDocId = documentArrayRef.ints[offset + lastPos - 1];
            firstPos = lastPos;
        }
        return size;
    }
    
    // TODO: writeDocumentArray ? (then writeGapArray is final in EliasFano)
    @Override
    public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        int offset = gapArrayRef.offset;
        if (specialCaseSingletonGapArrays && gapArrayRef.length == 1) {
            universeMethod.writeCode(gapArrayRef.ints[offset], bitStream);
            return;
        }
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        OptimalPartition optimalPartition = new OptimalPartition(documentArrayRef, epsilon1, epsilon2);
        int firstPos = 0;
        for (int lastPos : optimalPartition.partition) {
            int nbDocumentsLocal = lastPos - firstPos;
            nbDocumentsMethod.writeCode(nbDocumentsLocal, bitStream);
            super.writeGapArrayBlock(bitStream, new IntsRef(
                    gapArrayRef.ints,
                    offset + firstPos,
                    nbDocumentsLocal));
            firstPos = lastPos;
        }
    }
    
    // TODO: readDocumentArray ? (then readGapArray is final in EliasFano)
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        if (specialCaseSingletonGapArrays && nbGapsLocal == 1) {
            gapArrayRef.ints[0] = universeMethod.readCode(bitStream);
            return;
        }
        int indexBegin = 0;
        while (indexBegin < nbGapsLocal) {
            int nbDocumentsLocal = nbDocumentsMethod.readCode(bitStream);
            IntsRef gapArrayRefLocal = new IntsRef(nbDocumentsLocal);
            super.readGapArrayBlock(bitStream, gapArrayRefLocal);
            for (int i = 0; i < nbDocumentsLocal; i++) {
                gapArrayRef.ints[indexBegin + i] = gapArrayRefLocal.ints[i];
            }
            indexBegin += nbDocumentsLocal;
        }
    }
    
}
