/*
 * MethodByBlock.java
 * For each gapArray, here what is done:
 *    0. We split the gapArray by sub-gapArrays of size BLOCK_SIZE.
 *    1. With a GapArray method, we encode those sub-gapArrays.
 *
 * Remark: Between two consecutive sub-gapArrays, there will be padding.
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodByBlock extends Method {
    
    private Method gapMethod;
    
    // To separate the different sizes in the index.
    private double sizeGaps;
    private double sizePadding;
    
    private final int BLOCK_SIZE = 128;
    private final int[] subGapArray = new int[BLOCK_SIZE];
    
    /**
     * Creates a new instance of MethodByBlock.
     */
    public MethodByBlock() {}
    public MethodByBlock(Method gapMethod) {
        this.gapMethod = gapMethod;
    }
    
    @Override
    public void initBefore() {
        gapMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            gapMethod.init(new IntsRef(gapArrayRef.ints, offset + idBlock * BLOCK_SIZE, curBlockSize));
        }
    }
    
    @Override
    public void initAfter() {
        gapMethod.initAfter();
    }
    
    @Override
    public void computeSizeBefore() {
        sizeGaps = 0.;
        sizePadding = 0.;
        gapMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        double sizeGapsLocal = 0.;
        double sizePaddingLocal = 0.;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            // We call computeSizeBlock and not computeSize to enable block
            // optimizations.
            double sizeLocal = gapMethod.computeSizeBlock(
                    new IntsRef(gapArrayRef.ints, offset + idBlock * BLOCK_SIZE, curBlockSize));
            sizeGapsLocal += sizeLocal;
            sizePaddingLocal += Tools.nextMultiple((int)Math.ceil(sizeLocal), nbBitsPerByte) - sizeLocal;
        }
        sizeGaps += sizeGapsLocal;
        sizePadding += sizePaddingLocal;
        return sizePaddingLocal + sizeGapsLocal;
    }
    
    @Override
    public double computeSizeAfter() {
        return gapMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        gapMethod.standardOutput();
        System.out.print(" (padding: " + (sizePadding / 8000000) +
                " ; gaps: " + (sizeGaps / 8000000) +
                ")");
    }
    
    @Override
    public String getName() {
        return "ByBlock(" + gapMethod.getName() + ")";
    }
    
    public static final double computeSizeGapMethod(Method gapMethod) {
        return
                8. +
                gapMethod.getClass().getName().length() * 16. +
                gapMethod.computeSizeNeededInformation();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return computeSizeGapMethod(gapMethod);
    }
   
    public static final void exportGapMethod(Method gapMethod, DataOutputStream out) throws IOException {
        String gapMethodName = gapMethod.getClass().getName();
        out.writeByte(gapMethodName.length());
        out.writeChars(gapMethodName);
        gapMethod.exportNeededInformation(out);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        exportGapMethod(gapMethod, out);
    }
    
    public static final Method importGapMethod(DataInputStream in) throws IOException {
        byte gapMethodNameLength = in.readByte();
        String gapMethodName = "";
        for (int i = 0; i < gapMethodNameLength; i++) {
            gapMethodName += in.readChar();
        }
        try {
            Class methodClass = Class.forName(gapMethodName);
            Method gapMethod = (Method)methodClass.newInstance();
            gapMethod.importNeededInformation(in);
            return gapMethod;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
            System.out.println(gapMethodName + " is not a valid method for CompressLists.");
            return null;
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        gapMethod = importGapMethod(in);
    }
    
    @Override
    public final void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            // We call writeGapArrayBlock and not writeGapArray to enable block
            // optimizations.
            gapMethod.writeGapArrayBlock(
                    dataOutputStream,
                    new IntsRef(gapArrayRef.ints, offset + idBlock * BLOCK_SIZE, curBlockSize));
        }
    }
    
    @Override
    public final void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        int nbGapsLocal = gapArrayRef.length;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        int idGap = 0;
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            // We call readGapArrayBlock and not readGapArray to enable block
            // optimizations.
            IntsRef subGapArrayRef = new IntsRef(subGapArray, 0, curBlockSize);
            gapMethod.readGapArrayBlock(dataInputStream, subGapArrayRef);
            
            for (int i = 0; i < curBlockSize; i++) {
                int gap = subGapArray[i];
                gapArrayRef.ints[idGap++] = gap;
            }
        }
    }
    
}
