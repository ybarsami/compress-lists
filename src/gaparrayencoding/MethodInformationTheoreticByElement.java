/**
 * MethodITByElement.java
 *
 * Created on 25 juin 2019
 */

package gaparrayencoding;

import arithmeticcode.ArithmeticCoderBase;
import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFile;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodInformationTheoreticByElement extends MethodByElement {
    
    // Size of the occurrence arrays.
    private int arraySize;
    
    // Occurrence arrays.
    private long[] occurrences;
    private int[] occurrencesCumulative;
    
    // Arithmetic encoder and decoder.
    private ArithmeticEncoder encoder;
    private ArithmeticDecoder decoder;
    
    private int nbBitsPerOccurrence;
    private double nbBitsPerGap;
    
    /**
     * Creates a new instance of MethodInformationTheoreticByElement.
     */
    public MethodInformationTheoreticByElement() {}
    public MethodInformationTheoreticByElement(int maxIntToEncode) {
        setParameters(maxIntToEncode + 1);
    }
    
    public final void setParameters(int arraySize) {
        this.arraySize = arraySize;
        allocateOccurrences();
    }
    
    private void allocateOccurrences() {
        occurrences = new long[arraySize];
	occurrencesCumulative = new int[arraySize + 1];
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int maxIntToEncode = arraySize - 1;
        long[] occurrencesLocal = Tools.gapOccurrences(gapArrayRef, maxIntToEncode);
        for (int i = 0; i < arraySize; i++) {
            occurrences[i] += occurrencesLocal[i];
        }
    }
    
    @Override
    public void initAfter() {
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Computes the number of bits necessary to store all the occurrences.
        nbBitsPerOccurrence = 0;
        for (int i = 0; i < arraySize; i++) {
            if (occurrences[i] > 0 && Tools.ceilingLog2((int)occurrences[i]) > nbBitsPerOccurrence) {
                nbBitsPerOccurrence = Tools.ceilingLog2((int)occurrences[i]);
            }
        }
        
        // Compute the number of bits needed per gap.
        nbBitsPerGap = 0.;
        for (int i = 0; i < arraySize; i++) {
            double pi = (double)occurrences[i] / ((double)occurrencesCumulative[arraySize]);
            nbBitsPerGap += -pi * Tools.log2(pi);
        }
    }
    
    @Override
    public String getName() {
        return "InformationTheoreticByElement";
    }
    
    @Override
    public double computeSize(int x) {
        return nbBitsPerGap;
    }
    
    // As a small optimization, here computeSize(int[]) is overriden to do a
    // simple multiplication instead of length additions.
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        return gapArrayRef.length * nbBitsPerGap;
    }
    
    private int getNbBitsWritten() {
        return
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence) +
                Math.multiplyExact(arraySize, nbBitsPerOccurrence);
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                32 + // arraySize
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte); // occurrenceMatrix
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeInt(arraySize);
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            // export occurrenceMatrix
            for (int i = 0; i < arraySize; i++) {
                BinaryEncoding.writeCodeBinary((int)occurrences[i], bitStream, nbBitsPerOccurrence);
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            setParameters(in.readInt());
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            // import occurrenceMatrix
            for (int i = 0; i < arraySize; i++) {
                occurrences[i] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
            }
            OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        }
    }
    
    /*
     * Some methods keep some bits to be written at the end.
     */
    @Override
    public void outputRemainingBits(BitOutputStream bitStream) {
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeCode.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.finish();
        encoder = null;
    }
    
    @Override
    public void flushRemainingBits() {
        decoder = null;
    }
    
    /*
     * Write the code of x inside the BitSequence.
     */
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = ArithmeticCoderBase.DEFAULT_STATE_BITS;
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.update(occurrencesCumulative, x);
    }
    
    /*
     * Read the code of an integer which starts at the beginning of the BitStream.
     */
    @Override
    public int readCode(BitInputStream bitStream) {
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = ArithmeticCoderBase.DEFAULT_STATE_BITS;
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        return decoder.read(occurrencesCumulative);
    }

}
