/*
 * Index compression with the Quasi-Succinct method (sometimes called just the
 * Elias-Fano method).
 * Remark: The difference with the Elias-Fano method is that in the latter,
 * no optimization is made for dense sequences.
 *
 * Vigna, "Quasi-Succinct Indices" (2012), p.4
 * In some cases, the quasi-succinct representation we described is not very
 * efficient in term of space: this happens, for instance, for very dense
 * sequences. There is however an alternate representation for strictly monotone
 * sequences with skipping: we simply store a list of u bits in which bit k is
 * set if k is part of the list x_0, x_1, ... , x_{n-1}. This is equivalent to
 * storing the list in gap-compressed form by writing in unary the gaps
 * x_i - x_{i-1} - 1, and guarantees by definition that no more than u bits will
 * be used.
 */
package gaparrayencoding;

import arrays.IntsRef;
import integerencoding.BinaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class MethodQuasiSuccinct extends MethodEliasFano {
    
    protected static final MethodBitVector methodBitVector = new MethodBitVector();
    
    /**
     * Creates a new instance of MethodQuasiSuccinct.
     */
    public MethodQuasiSuccinct() {
        super();
    }
    public MethodQuasiSuccinct(int nbDocuments) {
        super(nbDocuments);
    }
    
    @Override
    public String getName() {
        return "QuasiSuccinct";
    }
    
    public static double computeBlockUpperBound(int universe, int length) {
        return 1. + Math.min(MethodEliasFano.computeBlockUpperBound(universe, length, true), universe);
    }
    
    @Override
    protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
        double sizeEliasFano = super.computeSizeDocumentArray(documentArrayRef);
        double sizeBitVector = methodBitVector.computeSizeDocumentArray(documentArrayRef, 0);
        return 1. + Math.min(sizeEliasFano, sizeBitVector);
    }
    @Override
    protected double computeSizeBlockDocumentArray(IntsRef documentArrayRef, int baseDocId) {
        double sizeEliasFano = super.computeSizeBlockDocumentArray(documentArrayRef, baseDocId);
        double sizeBitVector = methodBitVector.computeSizeDocumentArray(documentArrayRef, baseDocId);
        return 1. + Math.min(sizeEliasFano, sizeBitVector);
    }
    
    @Override
    protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef) {
        double sizeEliasFano = super.computeSizeDocumentArray(documentArrayRef);
        double sizeBitVector = methodBitVector.computeSizeDocumentArray(documentArrayRef, 0);
        boolean eliasFanoWasChosen = sizeEliasFano < sizeBitVector;
        writeDocumentArray(bitStream, documentArrayRef, eliasFanoWasChosen);
    }
    protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef, boolean eliasFanoWasChosen) {
        BinaryEncoding.writeCodeBinary(eliasFanoWasChosen ? 1 : 0, bitStream, 1);
        if (eliasFanoWasChosen) {
            super.writeDocumentArray(bitStream, documentArrayRef);
        } else {
            methodBitVector.writeDocumentArray(bitStream, documentArrayRef, 0);
        }
    }
    
    @Override
    protected void writeDocumentArrayBlock(BitOutputStream bitStream, IntsRef documentArrayRef, int baseDocId) {
        double sizeEliasFano = super.computeSizeBlockDocumentArray(documentArrayRef, baseDocId);
        double sizeBitVector = methodBitVector.computeSizeDocumentArray(documentArrayRef, baseDocId);
        boolean eliasFanoWasChosen = sizeEliasFano < sizeBitVector;
        writeDocumentArrayBlock(bitStream, documentArrayRef, baseDocId, eliasFanoWasChosen);
    }
    protected void writeDocumentArrayBlock(BitOutputStream bitStream, IntsRef documentArrayRef, int baseDocId, boolean eliasFanoWasChosen) {
        BinaryEncoding.writeCodeBinary(eliasFanoWasChosen ? 1 : 0, bitStream, 1);
        if (eliasFanoWasChosen) {
            super.writeDocumentArrayBlock(bitStream, documentArrayRef, baseDocId);
        } else {
            methodBitVector.writeDocumentArray(bitStream, documentArrayRef, baseDocId);
        }
    }
    
    @Override
    protected void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef) {
        boolean eliasFanoWasChosen = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
        if (eliasFanoWasChosen) {
            super.readDocumentArray(bitStream, documentArrayRef);
        } else {
            methodBitVector.readDocumentArray(bitStream, documentArrayRef, 0);
        }
    }
    @Override
    protected void readDocumentArrayBlock(BitInputStream bitStream, IntsRef documentArrayRef, int baseDocId) {
        boolean eliasFanoWasChosen = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
        if (eliasFanoWasChosen) {
            super.readDocumentArrayBlock(bitStream, documentArrayRef, baseDocId);
        } else {
            methodBitVector.readDocumentArray(bitStream, documentArrayRef, baseDocId);
        }
    }
    
}
