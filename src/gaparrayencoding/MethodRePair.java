/*
 * Index compression with the RePair method.
 *
 * Claude, Farina, Navarro, and Martinez-Prieto, "Indexes for Highly Repetitive Document Collections" (2011)
 * 
 * Implementation adapted from the C++ library uiHRDC.
 * https://github.com/migumar2/uiHRDC
 */
package gaparrayencoding;

import arrays.IntsRef;
import io.Dataset;
import io.DatasetInfo;
import io.DatasetReader;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodRePair extends Method {
    
    /**
     * Creates a new instance of MethodRePair.
     */
    public MethodRePair() {}
    
    @Override
    public String getName() {
        return "RePair";
    }
    
    /*
     * This method does not really belong to the gaparrayencoding methods...
     * For now, it can only work from a file. Conversion to the more general
     * context of the gaparrayencoding is not a direct concern.
     *
     * XXX: DO NOT CALL the 3 next methods !
     */
    @Override
    public final double computeSize(IntsRef gapArrayRef) {
        return 0.0;
    }
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) {}
    @Override
    public void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the file from the gapArrays
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Exports all the gapArrays.
     * The exported file shall have the name filename.
     */
    @Override
    public String exportToFile(String filename, DatasetReader datasetReader, int nbDocuments) {
        String datasetName = "TmpDataset";
        // convert INPUT  gapArrayList nbDocuments
        // convert OUTPUT datasetName datasetName.const datasetName.lens
        DatasetInfo datasetInfo = new DatasetInfo(datasetReader);
        int nbPosts = repair.Convert.convertFromGapArrayList(datasetInfo.gapArrayList, nbDocuments, datasetName);
        // mem has to be > nbPosts, and is recommended to be >= 1.2 * nbPosts
        int mem = 2 * nbPosts;
        int K = 100000;
        // buildIndex INPUT  datasetName
        // buildIndex OUTPUT datasetName.dict datasetName.rp datasetName.ptr
        repair.BuildIndex.buildIndex(datasetName, mem, K);
        // compressDictionary INPUT  datasetName.dict datasetName.ptr datasetName.rp
        // compressDictionary OUTPUT datasetName.cdict datasetName.crp
        new repair.CompressDictionary().compressDictionary(datasetName);
        
        /*
         * To get the original sequence of calls, uncomment the following:
        System.out.println("===== ./convert " + Dataset.BIBLE_NAME.gapArrayListFileName + " " + datasetName + " =====");
        System.out.println("===== ./buildIndex " + datasetName + " " + mem + " " + K + " =====");
        System.out.println("===== ./compressDictionary " + datasetName + " =====");
        System.out.println("===== ./rebuildPost " + datasetName + " =====");
         */
        
        // TODO: .ptr file can be reconstructed from .lens, so do not store it.
        try {
            File[] files = {
                new File(datasetName + ".cdict"),
                new File(datasetName + ".const"),
                new File(datasetName + ".crp"),
                new File(datasetName + ".lens"),
                new File(datasetName + ".ptr")
            };
            int nbFiles = files.length;
            FileInputStream[] fiss = new FileInputStream[nbFiles];
            FileOutputStream fos = new FileOutputStream(filename);
            int len = 0;
            byte[] buf = new byte[1024 * 1024]; // 1MB buffer
            for (int i = 0; i < nbFiles; i++) {
                try {
                    fiss[i] = new FileInputStream(files[i]);
                    while ((len = fiss[i].read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    fiss[i].close();
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + files[i].getName() + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in file '" + files[i].getName() + " or " + filename + "'.");
                }
            }
            fos.close();
        } catch (IOException e) {
            System.out.println("IOException in file '" + filename + "'.");
            e.printStackTrace();
        }
        
        File[] files = {
            new File(datasetName + ".cdict"),
            new File(datasetName + ".const"),
            new File(datasetName + ".crp"),
            new File(datasetName + ".lens"),
            new File(datasetName + ".ptr"),
            new File(datasetName),
            new File(datasetName + ".dict"),
            new File(datasetName + ".rp")
        };
        for (File file : files) {
            file.delete();
        }
        return filename;
    }
    
    /*
     * Imports all the gapArrays from a file that contains the total number of
     * documents.
     */
    @Override
    public DatasetInfo importFromFile(String filename) {
        String datasetName = "TmpDataset";
        
        // TODO: read filename and extract the following files:
        // rebuildPost INPUT  datasetName.cdict datasetName.const datasetName.crp datasetName.lens datasetName.ptr
        // rebuildPost OUTPUT datasetName.rebuilt
        repair.RebuildPost.rebuildPost(filename);
        
        File[] files = {
            new File(datasetName + ".cdict"),
            new File(datasetName + ".const"),
            new File(datasetName + ".crp"),
            new File(datasetName + ".lens"),
            new File(datasetName + ".ptr")
        };
        for (File file : files) {
            file.delete();
        }
        
        return new DatasetInfo(Dataset.datasetReaderUiHRDC(filename));
    }
    
}
