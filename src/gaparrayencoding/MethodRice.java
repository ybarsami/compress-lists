/*
 * Index compression with the Rice method.
 *
 * The Rice method is just Golomb with parameter b = 2^k.
 */
package gaparrayencoding;

import compresslists.Tools;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodRice extends MethodGolombGlobal {
    
    // The parameter.
    public int k;
    
    /**
     * Creates a new instance of MethodRice.
     */
    public MethodRice() {}
    public MethodRice(int k) {
        super(1 << k);
        this.k = k;
    }
    private MethodRice(double p) {
        this(Tools.ceilingLog2(golombParameterFromFrequency(p)));
    }
    public MethodRice(int nbWords, int nbDocuments, long nbPointers) {
        this((double)nbPointers / ((double)nbWords * (double)nbDocuments));
    }
    
    @Override
    public final void setParameter(int nbWords, int nbDocuments, long nbPointers) {
        super.setParameter(nbWords, nbDocuments, nbPointers);
        double frequency = (double)nbPointers / ((double)nbWords * (double)nbDocuments);
        this.k = Tools.ceilingLog2(golombParameterFromFrequency(frequency));
    }
    
    @Override
    public String getName() {
        return "Rice_" + k;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 32; // k
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(k);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        k = in.readInt();
    }
    
}
