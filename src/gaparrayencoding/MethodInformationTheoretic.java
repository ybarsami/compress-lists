/**
 * Arithmetic coding on the frequencies of each gap, i.e., we look at the
 * global occurrences of each gap, and code it with -p_gap * log(p_gap) bits.
 * 
 * This method is called "Entropy" in the paper:
 * Moffat and Zobel, "Parameterised Compression for Sparse Bitmaps" (1992), Table 9, p. 283
 *
 * It is also called "Independent" in the paper:
 * Bookstein, Klein, and Raita, "Modeling Word Occurrences for the Compression of Concordances" (1997), Table III, p. 285
 * --- which is the model used in their previous paper ---
 * Bookstein, Klein, and Raita, "Model Based Concordance Compression" (1992).
 * 
 * It is also called "Global observed frequency model" in the book:
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), Table 3.8, p. 129
 */

package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStreamFile;
import io.BitOutputStreamFile;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodInformationTheoretic extends MethodByArithmeticCoding {
    
    // Size of the occurrence arrays.
    private int arraySize;
    
    // Occurrence arrays.
    private long[] occurrences;
    private int[] occurrencesCumulative;
    
    private int nbBitsPerOccurrence;
    private double nbBitsPerGap;
    
    /**
     * Creates a new instance of MethodInformationTheoretic.
     */
    public MethodInformationTheoretic() {}
    public MethodInformationTheoretic(int maxIntToEncode) {
        setParameters(maxIntToEncode + 1);
    }
    
    public final void setParameters(int arraySize) {
        this.arraySize = arraySize;
        allocateOccurrences();
    }
    
    private void allocateOccurrences() {
        occurrences = new long[arraySize];
	occurrencesCumulative = new int[arraySize + 1];
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int maxIntToEncode = arraySize - 1;
        long[] occurrencesLocal = Tools.gapOccurrences(gapArrayRef, maxIntToEncode);
        for (int i = 0; i < arraySize; i++) {
            occurrences[i] += occurrencesLocal[i];
        }
    }
    
    @Override
    public void initAfter() {
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Computes the number of bits necessary to store all the occurrences.
        nbBitsPerOccurrence = 0;
        for (int i = 0; i < arraySize; i++) {
            if (occurrences[i] > 0 && Tools.ceilingLog2((int)occurrences[i]) > nbBitsPerOccurrence) {
                nbBitsPerOccurrence = Tools.ceilingLog2((int)occurrences[i]);
            }
        }
        
        // Compute the number of bits needed per gap.
        nbBitsPerGap = 0.;
        for (int i = 0; i < arraySize; i++) {
            double pi = (double)occurrences[i] / ((double)occurrencesCumulative[arraySize]);
            nbBitsPerGap += -pi * Tools.log2(pi);
        }
    }
    
    @Override
    public String getName() {
        return "InformationTheoretic";
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        return gapArrayRef.length * nbBitsPerGap;
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                ")");
    }
    
    private int getNbBitsWritten() {
        return
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence) +
                Math.multiplyExact(arraySize, nbBitsPerOccurrence);
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                32 + // arraySize
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte); // occurrenceMatrix
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeInt(arraySize);
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            // export occurrenceMatrix
            for (int i = 0; i < arraySize; i++) {
                BinaryEncoding.writeCodeBinary((int)occurrences[i], bitStream, nbBitsPerOccurrence);
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            setParameters(in.readInt());
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            // import occurrenceMatrix
            for (int i = 0; i < arraySize; i++) {
                occurrences[i] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
            }
            OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        }
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            encoder.update(occurrencesCumulative, gapArray[offset + idGap]);
        }
    }
    
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            int gap = decoder.read(occurrencesCumulative);
            gapArrayRef.ints[idGap] = gap;
        }
    }

}
