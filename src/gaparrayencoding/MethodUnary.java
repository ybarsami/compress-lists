/**
 * Index compression with the unary method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 117
 * One such code is the unary code. In this code an integer x >= 1 is coded
 * as x - 1 one bits followed by a zero bit, so that the code for integer 3
 * is 110.
 */

package gaparrayencoding;

import integerencoding.UnaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class MethodUnary extends MethodByElement {
    
    /**
     * Creates a new instance of MethodUnary.
     */
    public MethodUnary() {}
    
    @Override
    public String getName() {
        return "Unary";
    }
    
    @Override
    public double computeSize(int x) {
        return UnaryEncoding.computeSizeUnary(x);
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        UnaryEncoding.writeCodeUnary(x, bitStream);
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return UnaryEncoding.readCodeUnary(bitStream);
    }

}
