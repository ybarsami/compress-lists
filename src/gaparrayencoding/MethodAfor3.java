/*
 * Index compression with the Afor3 method.
 *
 * Delbru, Campinas, and Tummarello, "Searching Web Data: an Entity Retrieval and High-Performance Indexing Model" (2010)
 *
 * https://github.com/sirensolutions/siren/blob/master/siren-core/src/main/java/com/sindicetech/siren/index/codecs/block/AForBlockCompressor.java
 *
 * NB: It is possible not to store the size of the compressedInts array.
 * [same thing as in MethodByJavaFastPFOR]
 * At least two possible ways to do it:
 *     1. Manage a buffer of bytes (when we read too much bytes, they are kept
 *        and used when decompressing the next gapArray)
 *     2. Use a java.io.PushbackInputStream or a java.io.RandomAccessFile.
 * 
 * Alternatively, it could be possible not to store the size of the gapArray.
 * It means that we should have a way of knowing, given an array of compressed
 * ints, the maximum size of the uncompressed array.
 */
package gaparrayencoding;

import afor.AForBlockCompressor;
import afor.AForBlockDecompressor;
import arrays.BytesRef;
import arrays.IntsRef;
import compresslists.Tools;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodAfor3 extends Method {
    
    // The Afor3 method works on integer sequences that have a length which is a
    // multiple of 32, see AForBlockCompressor.java, line 71.
    // The Afor3 then encodes the integers frame by frame, where a frame is a
    // continguous set of 32 integers in the sequence.
    private static final int NB_INTS_BY_AFOR3_FRAME = 32;
    
    private final AForBlockCompressor compressor = new AForBlockCompressor();
    private final AForBlockDecompressor decompressor = new AForBlockDecompressor();
    
    /**
     * Creates a new instance of MethodAfor3.
     */
    public MethodAfor3() {}
    
    @Override
    public String getName() {
        return "Afor3";
    }
    
    private BytesRef bytesRef = new BytesRef(0);
    
    private void compress(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        final int intsRefLength = Tools.nextMultiple(nbGapsLocal, NB_INTS_BY_AFOR3_FRAME);
        final int bytesRefLength = compressor.maxCompressedSize(intsRefLength);
        // Reset the IntsRef
        if (gapArrayRef.ints.length < intsRefLength) {
            gapArrayRef.ints = Arrays.copyOf(gapArrayRef.ints, intsRefLength);
        } else if (gapArrayRef.ints.length % NB_INTS_BY_AFOR3_FRAME != 0) {
            // Remark: If this was your preferred method, then it would be more
            // efficient to make all gapArrayRef.ints to have length multiple of
            // NB_INTS_BY_AFOR3_FRAME in DatasetReader.java to avoid any use of
            // copyOf (here used to extend the size).
            gapArrayRef.ints = Arrays.copyOf(gapArrayRef.ints, Tools.nextMultiple(gapArrayRef.ints.length, NB_INTS_BY_AFOR3_FRAME));
        }
        gapArrayRef.length = intsRefLength;
        // Reset the BytesRef
        if (bytesRef.bytes.length < bytesRefLength) {
            bytesRef.bytes = new byte[bytesRefLength];
        }
        bytesRef.offset = 0;
        bytesRef.length = bytesRefLength;
        // Compress the data
        compressor.compress(gapArrayRef, bytesRef);
        // Put back initial values of length and offset, which may have been
        // altered by the compressor.
        gapArrayRef.length = nbGapsLocal;
        gapArrayRef.offset = offset;
    }
    
    @Override
    public final double computeSize(IntsRef gapArrayRef) {
        compress(gapArrayRef);
        return
                32. + // we also store the length of the compressed array
                8. * bytesRef.length; // the (byte[]) compressed array
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the file from the gapArrays
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        compress(gapArrayRef);
        int numberOfBytes = bytesRef.length;
        dataOutputStream.writeInt(numberOfBytes);
        for (int i = 0; i < numberOfBytes; i++) {
            dataOutputStream.writeByte(bytesRef.bytes[i]);
        }
    }
    
    @Override
    public void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        int nbGapsLocal = gapArrayRef.length;
        final int intsRefLength = Tools.nextMultiple(nbGapsLocal, NB_INTS_BY_AFOR3_FRAME);
        final int bytesRefLength = compressor.maxCompressedSize(intsRefLength);
        // Reset the IntsRef
        if (gapArrayRef.ints.length < intsRefLength) {
            gapArrayRef.ints = new int[intsRefLength];
        }
        gapArrayRef.offset = 0;
        gapArrayRef.length = intsRefLength;
        // Reset the BytesRef
        if (bytesRef.bytes.length < bytesRefLength) {
            bytesRef.bytes = new byte[bytesRefLength];
        }
        bytesRef.offset = 0;
        // Fill the BytesRef
        int numberOfBytes = dataInputStream.readInt();
        bytesRef.length = numberOfBytes;
        for (int i = 0; i < numberOfBytes; i++) {
            bytesRef.bytes[i] = dataInputStream.readByte();
        }
        // Uncompress the data
        decompressor.decompress(bytesRef, gapArrayRef);
        // Put correct values of length and offset, which may be wrongly set by
        // the decompressor.
        gapArrayRef.length = nbGapsLocal;
        gapArrayRef.offset = 0;
    }
    
}
