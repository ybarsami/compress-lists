/**
 * MethodContextTrit.java
 * For each gapArray, here what is done:
 *    0. We note tritList = tritList(gapArray) and T the size of tritList.
 *    1. We code the first min(k+w, T) trits from tritList with an arithmetic
 *       contextual method --- where the context of the i-th trit is the
 *       min(i-1, kInit) previous trits.
 *    2. Then, a contextual arithmetic method encodes the trits
 *       trit_{k+w}, ... trit_T --- where the context is the previous k trits +
 *       the number of "2" in the w trits before them.
 * 
 * NB: This is the implementation the closest to Andre's initial code.
 * 
 * It is possible to treat the 0 and 1 in the tritlist as equivalent, which
 * saves a lot of probabilities to be stored --- simply set merge01ForContext to
 * true.
 * 
 * PS: It is not possible to use "the most used context as initial context",
 * because it would lead to inconsistent values.
 */

package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.DatasetInfo;
import tritlistencoding.TritSequence;
import tritlistencoding.TritSequenceContextualEncoding;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodContextTrit extends MethodByArithmeticCoding {
    
    private final TritSequenceContextualEncoding tritSequenceEncoding;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    
    // Parameters. Here, we have three parameters k, w, and kInit.
    protected int k;
    protected int w;
    protected int kInit;
    
    private TritSequence tritSequence = new TritSequence();
    private boolean merge01ForContext;
    private boolean merge01ForProbas;
    
    /**
     * Creates a new instance of MethodContextTrit.
     */
    protected MethodContextTrit(int k, int w, int kInit, boolean merge01ForContext, boolean merge01ForProbas) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        this.merge01ForContext = merge01ForContext;
        this.merge01ForProbas = merge01ForProbas;
        tritSequenceEncoding = new TritSequenceContextualEncoding(k, w, kInit, merge01ForContext, merge01ForProbas);
    }
    
    public MethodContextTrit(int k, int w, int kInit) {
        this(k, w, kInit, false, false);
    }
    
    @Override
    public void initBefore() {
        tritSequenceEncoding.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        tritSequenceEncoding.initWithNewTritSequence(tritSequence);
    }
    
    @Override
    public void initAfter() {
        tritSequenceEncoding.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        double[] phase1Sizes = tritSequenceEncoding.computeSizes(tritSequence);
        
        double sizeInitialLocal = phase1Sizes[0];
        double sizeGenericLocal = phase1Sizes[1];
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        return sizeInitialLocal + sizeGenericLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeInitial = 0.;
        sizeGeneric = 0.;
        super.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) trits: " + (sizeInitial / 8000000) +
                " ; last trits: " + (sizeGeneric / 8000000) +
                ")");
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        tritSequenceEncoding.writeTritSequence(tritSequence, encoder);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String getName() {
        return "ContextTrit" +
                (merge01ForContext ? "_Merge01Context" : "") +
                (merge01ForProbas ? "_Merge01Probas" : "") +
                "_k" + k + "_w" + w + "_kI" + kInit;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return tritSequenceEncoding.computeSizeNeededInformation();
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        tritSequenceEncoding.exportNeededInformation(out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        tritSequenceEncoding.importNeededInformation(in);
    }
    
    /*
     * This is the reverse of the updateBitSequenceWithGapArray function. It reads nbGapsLocal
     * gaps in the buffer BitStream, according to the inverse of the encoding function.
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        tritSequenceEncoding.readTritSequence(nbGapsLocal, decoder, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some testing
    ////////////////////////////////////////////////////////////////////////////
    
    public final void exportProbas(FileWriter out) {
        tritSequenceEncoding.exportProbas(out);
    }
    
    public static void testExportProbas(ArrayList<int[]> gapArrayList, int nbDocuments, int[] contextArray) {
        int kContext     = contextArray[0];
        int wContext     = contextArray[1];
        int kInitContext = contextArray[2];
        
        // Only test batches [0, nbDocs] for which there are new lists w.r.t.
        // the previous batch [0, nbDocs - 1].
        boolean[] createABatchForNbDocs = new boolean[nbDocuments];
        for (int[] gapArray : gapArrayList) {
            createABatchForNbDocs[gapArray.length] = true;
        }
        ArrayList<ArrayList<int[]>> gapArrayListByBatch = new ArrayList<>();
        ArrayList<Integer> batchMaxLengths = new ArrayList<>();
        for (int nbDocsLocal = 0; nbDocsLocal < nbDocuments; nbDocsLocal++) {
            if (createABatchForNbDocs[nbDocsLocal]) {
                batchMaxLengths.add(nbDocsLocal);
                gapArrayListByBatch.add(new ArrayList<>());
            }
        }
        int nbBatches = batchMaxLengths.size();
        
        // Separation of the full gapArrayList by batch.
        for (int[] gapArray : gapArrayList) {
            for (int i = 0; i < nbBatches; i++) {
                if (gapArray.length == batchMaxLengths.get(i)) {
                    gapArrayListByBatch.get(i).add(gapArray);
                    break;
                }
            }
        }
        
        String filename1 = "ProbasContextTritBis.data";
        String filename2 = "ProbasContextBit.data";
        
        // Test the methods.
        try (FileWriter out1 = new FileWriter(filename1);
                FileWriter out2 = new FileWriter(filename2)) {
            out1.write("Method Context Trit Bis with k = " + kContext + ", w = " + wContext + " and kInit = " + kInitContext +"\n");
            out2.write("Method Context Bit with k = " + kContext + ", w = " + wContext + " and kInit = " + kInitContext +"\n");
            for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
                // The methods to test.
                final DatasetInfo datasetInfo_Real = new DatasetInfo(gapArrayListByBatch.get(idBatch), nbDocuments);
                MethodContextTrit method1 = new MethodContextTrit(kContext, wContext, kInitContext, true, true);
                MethodContextBit  method2 = new MethodContextBit (kContext, wContext, kInitContext);
                method1.init(datasetInfo_Real);
                method2.init(datasetInfo_Real);
                
                // Testing the best method.
                method1.exportProbas(out1);
                method2.exportProbas(out2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
