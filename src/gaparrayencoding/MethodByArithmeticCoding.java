/**
 * MethodByArithmeticCoding.java
 *
 * Created on 26 nov. 2019
 */

package gaparrayencoding;

import arithmeticcode.ArithmeticCoderBase;
import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arrays.IntsRef;
import io.BitInputStream;
import io.BitOutputStream;
import io.BitOutputStreamFake;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author yann
 */
public abstract class MethodByArithmeticCoding extends MethodByBitSequence {
    
    // Arithmetic encoder and decoder.
    protected ArithmeticEncoder encoder;
    protected ArithmeticDecoder decoder;
    
    protected BitOutputStreamFake bitCounter = new BitOutputStreamFake();
    protected ArithmeticEncoder counter;
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeGapArray.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        outputRemainingBits();
        encoder.finish();
        encoder = null;
    }
    
    public void outputRemainingBits() {}
    
    @Override
    public void flushRemainingBits() {
        decoder = null;
    }
    
    @Override
    public void computeSizeBefore() {
        int nbStateBits = DEFAULT_STATE_BITS;
        counter = new ArithmeticEncoder(nbStateBits, bitCounter);
    }
    
    @Override
    public double computeSizeAfter() {
        if (counter == null) {
            throw new RuntimeException("Unsupported call to computeSizeAfter without prior call to computeSizeBefore.");
        }
        counter.finish();
        counter = null;
        double sizeAfter = bitCounter.getNbBitsWrittenAndReset();
        return sizeAfter + super.computeSizeAfter();
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = ArithmeticCoderBase.DEFAULT_STATE_BITS;
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        writeGapArray(gapArrayRef);
    }
    
    public abstract void writeGapArray(IntsRef gapArrayRef);
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = ArithmeticCoderBase.DEFAULT_STATE_BITS;
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        readGapArray(gapArrayRef);
    }
    
    public abstract void readGapArray(IntsRef gapArrayRef);
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods that use Arithmetic Coding MUST use writeGapArrayList /
    // readGapArrayList instead of writeGapArray / readGapArray.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) {
        throw new IllegalArgumentException("Methods that use Arithmetic Coding must use " +
                "writeGapArrayList / readGapArrayList instead of writeGapArray / readGapArray.");
    }
    
    @Override
    public final void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) {
        throw new IllegalArgumentException("Methods that use Arithmetic Coding must use " +
                "writeGapArrayList / readGapArrayList instead of writeGapArray / readGapArray.");
    }

}
