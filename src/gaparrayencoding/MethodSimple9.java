/**
 * Index compression with the Simple-9 method.
 *
 * Anh and Moffat, "Inverted Index Compression Using Word-Aligned Binary Codes" (2005)
 * 
 * Implementation from the java library JavaFastPFOR.
 * https://github.com/lemire/JavaFastPFOR
 */

package gaparrayencoding;

import me.lemire.integercompression.Simple9;

/**
 *
 * @author yann
 */
public class MethodSimple9 extends MethodByJavaFastPFOR {
    
    /**
     * Creates a new instance of MethodSimple9.
     */
    public MethodSimple9() {
        super(new Simple9());
    }

}
