/**
 * Index compression with the NewPFD method.
 * 
 * Yan, Ding, and Suel, "Inverted Index Compression and Query Processing with Optimized Document Ordering" (2009)
 * 
 * Implementation from the java library JavaFastPFOR.
 * https://github.com/lemire/JavaFastPFOR
 */

package gaparrayencoding;

import me.lemire.integercompression.NewPFD;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.VariableByte;

/**
 *
 * @author yann
 */
public class MethodNewPFD extends MethodByJavaFastPFOR {
    
    /**
     * Creates a new instance of MethodNewPFD.
     */
    public MethodNewPFD() {
        // It uses a block size of 128 integers.
        super(new Composition(new NewPFD(), new VariableByte()));
    }

}
