/**
 * Index compression with the BP32 method.
 * 
 * Lemire and Boytsov, "Decoding billions of integers per second through vectorization" (2013)
 * Reading and writing unaligned data can be as fast as reading and writing
 * aligned data on recent Intel processors—as long as we do not cross a 64-byte
 * cache line. Nevertheless, we still wish to align data on 32-bit boundaries
 * when using regular binary packing. Each block of 32 bit-packed integers
 * should be preceded by a descriptor that stores the bit width (b) of integers
 * in the block. The number of bits used by the block is always divisible by 32.
 * Hence, to keep blocks aligned on 32-bit boundaries, we group the blocks and
 * respective descriptors into meta-blocks each of which contains four
 * successive blocks. A meta-block is preceded by a 32-bit descriptor that
 * combines 4 bit widths b (8 bits/width). We call this scheme BP32.
 * 
 * NOTE: The fastest method from the paper, SIMD-BP128 uses blocks of 128
 * integers instead of blocks of 32 integers, as is the case here. The BP128
 * method is not included in the java repository, but it is reported to always
 * take more memory than BP32, see Tables IV and V.
 * https://github.com/lemire/JavaFastPFOR
 */

package gaparrayencoding;

import me.lemire.integercompression.BinaryPacking;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.VariableByte;

/**
 *
 * @author yann
 */
public class MethodBinaryPacking32 extends MethodByJavaFastPFOR {
    
    /**
     * Creates a new instance of MethodBinaryPacking32.
     */
    public MethodBinaryPacking32() {
        // It uses a block size of 32 integers.
        super(new Composition(new BinaryPacking(), new VariableByte()));
    }

}
