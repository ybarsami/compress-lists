/*
 * Index compression with the Golomb method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 119
 * The following method was first described in 1966 by Solomon Golomb of the
 * University of Southern California and is referred to as the Golomb code. For
 * some parameter b, any number x > 0 is coded in two parts: first, q + 1 in
 * unary, where the quotient q = floor((x — 1) / b); then the remainder
 * r = x - qb - 1 coded in binary, requiring either floor(log b) or
 * ceiling(log b) bits. For example, with b = 3 there are three possible
 * remainders r, and these are coded as 0, 10, and 11, respectively, for r = 0,
 * r - 1, and r = 2. Similarly, for b = 6, there are six possible remainders r,
 * with values 0 through 5, and these are assigned codes of 00, 01, 100, 101,
 * 110, and 111. Then, if the value x = 9 is to be coded relative to b = 3,
 * calculation yields q = 2 and r = 2 because 9 - 1 = 2 * 3 + 2; so the encoding
 * is 110 followed by 11. Relative to b = 6, the values calculated are q = 1 and
 * r = 2, resulting in a code of 10 followed by 100.
 */
package gaparrayencoding;

import integerencoding.BinaryEncoding;
import integerencoding.UnaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodGolombGlobal extends MethodByElement {
    
    // The parameter.
    public int b;
    
    /**
     * Creates a new instance of MethodGolombGlobal.
     */
    public MethodGolombGlobal() {}
    public MethodGolombGlobal(int b) {
        this.b = b;
    }
    private MethodGolombGlobal(double p) {
        this(golombParameterFromFrequency(p));
    }
    public MethodGolombGlobal(int nbWords, int nbDocuments, long nbPointers) {
        this((double)nbPointers / ((double)nbWords * (double)nbDocuments));
    }
    
    public final static int golombParameterFromFrequency(double p) {
        return (int)Math.ceil(-Math.log(2. - p) / Math.log(1. - p));
    }
    
    public void setParameter(int nbWords, int nbDocuments, long nbPointers) {
        double frequency = (double)nbPointers / ((double)nbWords * (double)nbDocuments);
        this.b = golombParameterFromFrequency(frequency);
    }
    
    @Override
    public String getName() {
        return "GolombGlobal_" + b;
    }
    
    @Override
    public double computeSize(int x) {
        return computeSizeGolomb(x, b);
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        writeCodeGolomb(x, bitStream, b);
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return readCodeGolomb(bitStream, b);
    }
    
    public static double computeSizeGolomb(int x, int b) {
        int q = (x - 1) / b;
        int r = (x - 1) - q * b;
        return q + 1 + BinaryEncoding.computeSizeCodeBinaryLefterShorter(r, 0, b - 1);
    }
    
    public static void writeCodeGolomb(int x, BitOutputStream bitStream, int b) {
        int q = (x - 1) / b;
        int r = (x - 1) - q * b;
        UnaryEncoding.writeCodeUnary(q + 1, bitStream);
        BinaryEncoding.writeCodeBinaryLefterShorter(r, bitStream, 0, b - 1);
    }
    
    public static int readCodeGolomb(BitInputStream bitStream, int b) {
        int q = UnaryEncoding.readCodeUnary(bitStream) - 1;
        int r = BinaryEncoding.readCodeBinaryLefterShorter(bitStream, 0, b - 1);
        return q * b + r + 1;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 32; // b
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(b);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        b = in.readInt();
    }

}
