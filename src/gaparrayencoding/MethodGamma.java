/**
 * Index compression with the gamma method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 117
 * One is the \gamma code, which represents the number x as a unary code for
 * 1 + floor(log_2(x)) followed by a code of floor(log_2(x)) bits that
 * represents the value of x - 2^{floor(x)} in binary. The unary part specifies
 * how many bits are required to code x, and then the binary part actually codes
 * x in that many bits. For example, consider x = 9. Then floor(log_2(x)) = 3,
 * and so 4 = 1 + 3 is coded in unary (code 1110) followed by 1 = 9 - 8 as a
 * three-bit binary number (code 001), which combine to give a codeword of
 * 1110 001.
 */

package gaparrayencoding;

import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class MethodGamma extends MethodByElement {
    
    /**
     * Creates a new instance of MethodGamma.
     */
    public MethodGamma() {}
    
    @Override
    public String getName() {
        return "Gamma";
    }
    
    @Override
    public double computeSize(int x) {
        return GammaEncoding.computeSizeGamma(x);
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        GammaEncoding.writeCodeGamma(x, bitStream);
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return GammaEncoding.readCodeGamma(bitStream);
    }

}
