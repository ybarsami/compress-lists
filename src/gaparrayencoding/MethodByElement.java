/**
 * This abstract method is useful for methods that encode the gaps in their
 * gapArrays uniformly, i.e., when the code of a gap does not depend of the
 * whole gapArray, but only on the value of the gap itself.
 */

package gaparrayencoding;

import arrays.IntsRef;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public abstract class MethodByElement extends MethodByBitSequence {
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        double nbBits = 0;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            nbBits += computeSize(gapArray[offset + idGap]);
        }
        return nbBits;
    }
    
    /*
     * Compute the size, in bits, that will be taken to code x.
     */
    public abstract double computeSize(int x);
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            writeCode(gapArray[offset + idGap], bitStream);
        }
    }
    
    /*
     * Write the code of x inside the BitSequence.
     */
    public abstract void writeCode(int x, BitOutputStream bitStream);
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            // Extract a gap.
            int gap = readCode(bitStream);
            // Add the gap to the gap array.
            gapArrayRef.ints[idGap] = gap;
        }
    }
    
    /*
     * Read the code of an integer from the given bit input stream.
     */
    public abstract int readCode(BitInputStream bitStream);

}
