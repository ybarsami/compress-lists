/*
 * MethodBitVector.java
 * For each gapArray, here what is done:
 *    0. We note bitVector = bitVector(gapArray).
 *    1. We output the bits from bitVector.
 */
package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class MethodBitVector extends MethodByBitSequence {
    
    private BitSequence bitVector = new BitSequence();
    
    /**
     * Creates a new instance of MethodBitVector.
     */
    public MethodBitVector() {}
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.gapArray2bitVector(gapArrayRef, bitVector);
        return bitVector.size();
    }
    public double computeSizeDocumentArray(IntsRef documentArrayRef, int baseDocId) {
        Tools.documentArray2bitVector(documentArrayRef, bitVector, baseDocId);
        return bitVector.size();
    }
    
    @Override
    public double computeSizeAfter() {
        bitVector = new BitSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.gapArray2bitVector(gapArrayRef, bitVector);
        for (int i = 0; i < bitVector.size(); i++) {
            bitStream.writeBit(bitVector.get(i));
        }
    }
    public final void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef, int baseDocId) {
        Tools.documentArray2bitVector(documentArrayRef, bitVector, baseDocId);
        for (int i = 0; i < bitVector.size(); i++) {
            bitStream.writeBit(bitVector.get(i));
        }
    }
    
    @Override
    public String getName() {
        return "BitVector";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        bitVector = new BitSequence();
    }
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        readBitVector(bitStream, gapArrayRef.length);
        Tools.bitVector2gapArray(bitVector, gapArrayRef);
    }
    public final void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef, int baseDocId) {
        readBitVector(bitStream, documentArrayRef.length);
        Tools.bitVector2documentArray(bitVector, documentArrayRef, baseDocId);
    }
    private void readBitVector(BitInputStream bitStream, int nbOnesToRead) {
        bitVector.reset();
        int nbOnesRead = 0;
        while (nbOnesRead < nbOnesToRead) {
            int bit = bitStream.getNextBit();
            bitVector.add(bit);
            if (bit == 1) {
                nbOnesRead++;
            }
        }
    }
    
}
