/*
 * Index compression with the Elias-Fano method.
 * Remark: The difference with the Quasi-Succinct indices is that in the latter,
 * an optimization is made for dense sequences.
 *
 * Fano, "On the number of bits required to implement an associative memory" (1971)
 * Elias, "On binary representations of monotone sequences" (1972)
 * Elias, "Efficient storage and retrieval by content and address of static files" (1974)
 * Vigna, "Quasi-Succinct Indices" (2012)
 *
 * Vigna, "Quasi-Succinct Indices" (2012), pp. 2--3
 * In this section we give a detailed description of the high bits/low bits
 * representation of a monotone sequence proposed by Elias [1974]. We assume to
 * have a monotonically increasing sequence of n > 0 natural numbers
 * 0 ≤ x_0 ≤ x_1 ... ≤ x_{n-1} ≤ x_{n-1} ≤ u
 * where u > 0 is any upper bound on the last value. The choice u = x_{n-1} is
 * of course possible (and optimal), but storing explicitly x_{n-1} might be
 * costly, and a suitable value for u might be known from external information,
 * as we will see shortly. We will represent such a sequence in two bit arrays
 * as follows:
 *    * the lower l = max(0, floor(log_2(u / n))) bits of each x_i are stored
 * explicitly and contiguously in the lower-bits array;
 *    * the upper bits are stored in the upper-bits array as a sequence of
 * unary-coded gaps.
 * In Figure 1 we show an example.
 *
 * Figure 1: A simple example of the quasi-succinct encoding from [1974]. We
 * consider the list 5, 8, 8, 15, 32 with upper bound 36, so
 * l = floor(log_2(36 / 5)) = 2. On the right, the lower l bits of all elements
 * are concatenated to form the lower-bits array. On the left, the gap of the
 * values of the upper bits are stored sequentially in unary code in the
 * upper-bits array.
 * 
 *    5     8      8      15      32
 *  1|01  10|00  10|00  11|11  1000|00
 *
 *                             low bits : 01|00|00|11|00
 * high numbers : 1 2 2 3 8
 * high gaps    : 1 1 0 1 5
 * unary code   : 01|01|1|01|000001
 *
 * Vigna, "Quasi-Succinct Indices" (2012), p. 4
 * Strictly monotone sequences. In case the sequence x_0, x_1, ..., x_{n-1} to
 * be represented is strictly monotone [...], it is possible to reduce the space
 * usage by storing the sequence x_i - i using the upper bound u - n. [...]
 * This mechanism was already noted by Elias [1972].
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;
import io.DatasetReader;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodEliasFano extends MethodByBitSequence {
    
    protected int[] lowInts = new int[0];
    protected IntsRef documentArrayRef = new IntsRef(0);
    
    /*
     * The method that is used to encode the universe, in case we use the
     * block or dynamic Elias-Fano method.
     */
    protected static final MethodByElement universeMethod = new MethodVariableByte();
    
    /*
     * Elias-Fano can work on any increasing sequence of numbers >= 0.
     * If we have a *strictly* increasing sequence of numbers >= 1, we can
     *substract (idDoc + 1) to each element to save space.
     */
    private boolean useStrictlyIncreasing;
    
    /**
     * Creates a new instance of MethodEliasFano.
     */
    public MethodEliasFano() {}
    public MethodEliasFano(int nbDocuments) {
        this(nbDocuments, true);
    }
    public MethodEliasFano(int nbDocuments, boolean useStrictlyIncreasing) {
        this.nbDocuments = nbDocuments;
        this.useStrictlyIncreasing = useStrictlyIncreasing;
    }
    
    /*
     * To be used when there is no way of capturing a global "nbDocuments" that
     * can be used as a bound on all the integers to encode.
     */
    public static class MethodDynamicEliasFano extends MethodEliasFano {
        
        public MethodDynamicEliasFano(int nbDocuments) {
            super(nbDocuments);
        }
        public MethodDynamicEliasFano(int nbDocuments, boolean useStrictlyIncreasing) {
            super(nbDocuments, useStrictlyIncreasing);
        }
        
        @Override
        public String getName() {
            return "DynamicEliasFano";
        }
        
        @Override
        protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
            return computeSizeBlockDocumentArray(documentArrayRef, 0);
        }
        
        @Override
        public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
            writeGapArrayBlock(bitStream, gapArrayRef);
        }
        
        @Override
        public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
            readGapArrayBlock(bitStream, gapArrayRef);
        }
    }
    
    @Override
    public String getName() {
        return "EliasFano" + (useStrictlyIncreasing ? "_Strict" : "");
    }
    
    protected static int computeL(int universe, int length) {
        return Math.max(0, Tools.ilog2(universe / length));
    }
    
    public static double computeBlockUpperBound(int universe, int length, boolean isStrictSequence) {
        double size = universeMethod.computeSize(universe + 1);
        if (length > 1 && universe != length) {
            universe--;
            length--;
            if (isStrictSequence) {
                universe -= length;
            }
            int l = computeL(universe, length);
            size += length * (1. + l) + (universe >>> l);
        }
        return size;
    }
    
    /*
     * As an optimization when we use a DatasetReader that stores document
     * arrays instead of gap arrays, we do not read a document array, transform
     * it into a gap array, then transform it back into a document array...
     * (this is essentially a copy / paste from Method.java)
     * TODO: create specific classes for methods that work on document arrays.
     */
    @Override
    public final double computeSize(DatasetReader datasetReader) {
        computeSizeBefore();
        double size = computeSizeNeededInformation();
        datasetReader.restartReader();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            size += computeSizeDocumentArray(datasetReader.getNextDocumentArray());
        }
        size += computeSizeAfter();
        return size;
    }
    /*
     * (this is essentially a copy / paste from Method.java)
     * TODO: factorize what can be factorized w.r.t. the code in Method.java
     */
    @Override
    public double computeSize(DatasetReader datasetReader, int percentageBegin, double sizeAlreadyComputed) {
        computeSizeBefore();
        double size = percentageBegin == 0 ? computeSizeNeededInformation() : sizeAlreadyComputed;
        int nbWords = datasetReader.getNbWords();
        int idWordBegin = Tools.ceilingDivision(nbWords * percentageBegin, 100);
        int nbWordsToHandle = nbWords - idWordBegin;
        int[] idWords = new int[nbWordsToHandle];
        for (int i = 0; i < nbWordsToHandle; i++) {
            idWords[i] = i + idWordBegin;
        }
        datasetReader.restartReader(idWords);
        int currentPercentage = percentageBegin - 1;
        for (int idWord : idWords) {
            if (idWord * 100 / nbWords > currentPercentage) {
                currentPercentage = idWord * 100 / nbWords;
                System.out.println(getName() + " (" + currentPercentage + "%) = " +
                        (size / 8000000.) + " MB (" + size + " bits)");
            }
            size += computeSizeDocumentArray(datasetReader.getNextDocumentArray());
        }
        size += computeSizeAfter();
        double sizeLengths = computeSizeLengths(datasetReader, nbDocuments);
        double sizeFull = size + sizeLengths;
        System.out.println(getName() + " = " + (sizeFull / 8000000.) +
                " --- " + (size / 8000000.));
        return size;
    }
    
    @Override
    public final double computeSize(IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        return computeSizeDocumentArray(documentArrayRef);
    }
    protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        return computeSizeEliasFano(documentArrayRef, nbDocuments - (useStrictlyIncreasing ? nbDocumentsLocal : 0), useStrictlyIncreasing);
    }
    
    @Override
    public double computeSizeAfter() {
        documentArrayRef = new IntsRef(0);
        return super.computeSizeAfter();
    }
    
    /*
     * There is an optimization when working block by block. We need to store
     * the maximum value for decoding.
     */
    @Override
    public final double computeSizeBlock(IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        return computeSizeBlockDocumentArray(documentArrayRef, 0);
    }
    protected double computeSizeBlockDocumentArray(IntsRef documentArrayRef, int baseDocId) {
        int nbDocumentsLocal = documentArrayRef.length;
        int offset = documentArrayRef.offset;
        int universe = documentArrayRef.ints[offset + nbDocumentsLocal - 1] - baseDocId;
        double size = universeMethod.computeSize(universe - (nbDocumentsLocal - 1));
        // When universe is nbDocumentsLocal, it means all the gaps are 1.
        if (nbDocumentsLocal > 1 && universe != nbDocumentsLocal) {
            documentArrayRef.length--;
            size += computeSizeEliasFano(documentArrayRef, universe - 1 - (useStrictlyIncreasing ? nbDocumentsLocal : 0), useStrictlyIncreasing, baseDocId);
            documentArrayRef.length++;
        }
        return size;
    }
    
    public static double computeSizeEliasFano(IntsRef intArrayRef, int universe, boolean isStrictSequence) {
        return computeSizeEliasFano(intArrayRef, universe, isStrictSequence, 0);
    }
    public static double computeSizeEliasFano(IntsRef intArrayRef, int universe, boolean isStrictSequence, int baseDocId) {
        int[] intArray = intArrayRef.ints;
        int offset = intArrayRef.offset;
        int length = intArrayRef.length;
        double nbBits = 0;
        int l = computeL(universe, length);
        int previousHighNumber = 0;
        for (int idDoc = 0; idDoc < length; idDoc++) {
            // Elias-Fano can work on any increasing sequence of numbers >= 0.
            // If we have a *strictly* increasing sequence of numbers >= 1,
            // we can substract (idDoc + 1) to each element to save space.
            int highNumber = (intArray[offset + idDoc] - baseDocId - (isStrictSequence ? idDoc + 1 : 0)) >> l;
            int highGap = highNumber - previousHighNumber;
            nbBits += l + integerencoding.UnaryEncoding.computeSizeUnary(highGap + 1);
            previousHighNumber = highNumber;
        }
        return nbBits;
    }
    
    @Override
    public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        writeDocumentArray(bitStream, documentArrayRef);
    }
    protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        writeIntArrayEliasFano(bitStream, documentArrayRef, nbDocuments - (useStrictlyIncreasing ? nbDocumentsLocal : 0), useStrictlyIncreasing);
    }
    
    /*
     * There is an optimization when working block by block. We need to store
     * the maximum value for decoding.
     */
    @Override
    public void writeGapArrayBlock(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        writeDocumentArrayBlock(bitStream, documentArrayRef, 0);
    }
    protected void writeDocumentArrayBlock(BitOutputStream bitStream, IntsRef documentArrayRef, int baseDocId) {
        int nbDocumentsLocal = documentArrayRef.length;
        int universe = documentArrayRef.ints[nbDocumentsLocal - 1];
        universeMethod.writeCode(universe - (nbDocumentsLocal - 1), bitStream);
        // When universe is nbDocumentsLocal, it means all the gaps are 1.
        if (nbDocumentsLocal > 1 && universe != nbDocumentsLocal) {
            documentArrayRef.length--;
            writeIntArrayEliasFano(bitStream, documentArrayRef, universe - 1 - (useStrictlyIncreasing ? nbDocumentsLocal : 0), useStrictlyIncreasing, baseDocId);
        }
    }
    
    public static void writeIntArrayEliasFano(BitOutputStream bitStream, IntsRef intArrayRef, int universe, boolean isStrictSequence) {
        writeIntArrayEliasFano(bitStream, intArrayRef, universe, isStrictSequence, 0);
    }
    public static void writeIntArrayEliasFano(BitOutputStream bitStream, IntsRef intArrayRef, int universe, boolean isStrictSequence, int baseDocId) {
        int[] intArray = intArrayRef.ints;
        int offset = intArrayRef.offset;
        int length = intArrayRef.length;
        int l = computeL(universe, length);
        // Low bits.
        int lowMask = (1 << l) - 1;
        for (int idDoc = 0; idDoc < length; idDoc++) {
            // Elias-Fano can work on any increasing sequence of numbers >= 0.
            // If we have a *strictly* increasing sequence of numbers >= 1,
            // we can substract (idDoc + 1) to each element to save space.
            integerencoding.BinaryEncoding.writeCodeBinary((intArray[offset + idDoc] - baseDocId - (isStrictSequence ? idDoc + 1 : 0)) & lowMask, bitStream, l);
        }
        // High bits.
        int previousHighNumber = 0;
        for (int idDoc = 0; idDoc < length; idDoc++) {
            // Elias-Fano can work on any increasing sequence of numbers >= 0.
            // If we have a *strictly* increasing sequence of numbers >= 1,
            // we can substract (idDoc + 1) to each element to save space.
            int highNumber = (intArray[offset + idDoc] - baseDocId - (isStrictSequence ? idDoc + 1 : 0)) >> l;
            int highGap = highNumber - previousHighNumber;
            integerencoding.UnaryEncoding.writeCodeUnary(highGap + 1, bitStream);
            previousHighNumber = highNumber;
        }
    }
    
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        // When reading, we do not need extra memory, we can work in-place on
        // gapArray.
        readDocumentArray(bitStream, gapArrayRef);
        Tools.documentArray2gapArray(gapArrayRef, gapArrayRef);
    }
    protected void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        if (nbDocumentsLocal > lowInts.length) {
            lowInts = new int[nbDocumentsLocal];
        }
        assert(documentArrayRef.offset == 0);
        readIntArrayEliasFano(bitStream, nbDocumentsLocal, documentArrayRef.ints, nbDocuments - (useStrictlyIncreasing ? nbDocumentsLocal : 0), lowInts, useStrictlyIncreasing);
    }
    
    /*
     * There is an optimization when working block by block. We need to retrieve
     * the maximum value for decoding.
     */
    @Override
    public void readGapArrayBlock(BitInputStream bitStream, IntsRef gapArrayRef) {
        readDocumentArrayBlock(bitStream, gapArrayRef, 0);
        Tools.documentArray2gapArray(gapArrayRef, gapArrayRef);
    }
    protected void readDocumentArrayBlock(BitInputStream bitStream, IntsRef documentArrayRef, int baseDocId) {
        // Remark: If needed, it would be possible to use the offset given in
        // documentArrayRef.offset. Here, we make it clear that we start at 0.
        documentArrayRef.offset = 0;
        int nbDocumentsLocal = documentArrayRef.length;
        if (nbDocumentsLocal > lowInts.length) {
            lowInts = new int[nbDocumentsLocal];
        }
        int universe = universeMethod.readCode(bitStream) + (nbDocumentsLocal - 1);
        // When reading, we do not need extra memory, we can work in-place on
        // gapArray.
        if (nbDocumentsLocal > 1) {
            if (universe == nbDocumentsLocal) {
                // When universe is nbGapsLocal, it means all the gaps are 1.
                for (int i = 0; i < nbDocumentsLocal - 1; i++) {
                    documentArrayRef.ints[i] = i + 1;
                }
            } else {
                readIntArrayEliasFano(bitStream, nbDocumentsLocal - 1, 0, documentArrayRef.ints, universe - 1 - (useStrictlyIncreasing ? nbDocumentsLocal : 0), baseDocId, lowInts, useStrictlyIncreasing);
            }
        }
        // The last int is universe.
        documentArrayRef.ints[nbDocumentsLocal - 1] = universe;
    }
    
    public static void readIntArrayEliasFano(BitInputStream bitStream, int nbInts, int[] intArray, int universe, int[] lowInts, boolean isStrictSequence) {
        readIntArrayEliasFano(bitStream, nbInts, 0, intArray, universe, 0, lowInts, isStrictSequence);
    }
    public static void readIntArrayEliasFano(BitInputStream bitStream, int nbInts, int offset, int[] intArray, int universe, int baseDocId, int[] lowInts, boolean isStrictSequence) {
        assert(lowInts.length >= nbInts);
        int l = computeL(universe, nbInts);
        // Low bits.
        for (int idDoc = 0; idDoc < nbInts; idDoc++) {
            lowInts[idDoc] = integerencoding.BinaryEncoding.readCodeBinary(bitStream, l);
        }
        // High bits.
        int highNumber = 0;
        for (int idDoc = 0; idDoc < nbInts; idDoc++) {
            int highGap = integerencoding.UnaryEncoding.readCodeUnary(bitStream) - 1;
            highNumber += highGap;
            intArray[offset + idDoc] = ((highNumber << l) | lowInts[idDoc]) + baseDocId + (isStrictSequence ? idDoc + 1 : 0);
        }
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 8; // useStrictlyIncreasing
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeBoolean(useStrictlyIncreasing);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        useStrictlyIncreasing = in.readBoolean();
    }

}
