/*
 * For each gapArray, here what is done:
 *    0. We split the gapArray by sub-gapArrays. The sizes of the sub-gapArrays
 * is computed optimally according to dynamic programming.
 *    1. With a GapArray method, we encode those sub-gapArrays.
 *
 * Remark: As a difference with MethodByBlockNoPadding.java, the sub-gapArrays
 * do not have constant size. As a difference with MethodByBlock.java, between
 * two consecutive sub-gapArrays, the padding is removed.
 *
 * Buchsbaum, Fowler, and Giancarlo, "Improving Table Compression with Combinatorial Optimization" (2003), p.830
 * 2.1. SOLUTIONS WITHOUT REORDERING. In the general case, irrespective of
 * whether combinatorial dependence is an equivalence relation, we can solve
 * Problem 2.1 by dynamic programming. Let E[i] be the cost of an optimal,
 * contiguous partition of variables x_1, ... , x_i. E[n] is thus the cost of a
 * solution to Problem 2.1. Define E[0] = 0; then, for 1 ≤ i ≤ n,
 * E[i] =  min E[j] + H(x_{j+1}, ..., x_i).
 *        0≤j<i
 * The actual partition with cost E[n] can be maintained by standard dynamic
 * programming backtracking.
 *
 * Ottaviano and Venturini, "Partitioned Elias-Fano Indexes" (2014), p.277
 * An optimal partition can be computed in Θ(n^2) time and space by solving a
 * variant of dynamic programming recurrence introduced in [2003].
 * => in fact, as is the case in this file, it is Θ(n^2) in time but linear in
 *    space.
 */
package gaparrayencoding;

import arrays.IntsRef;
import io.BitInputStream;
import io.BitOutputStream;
import static gaparrayencoding.MethodEliasFano.computeSizeEliasFano;

/**
 *
 * @author yann
 */
public class MethodEliasFanoOptBlock extends MethodEliasFano {
    
    private static MethodByElement universeMethod = new MethodVariableByte();
    private static MethodByElement nbDocumentsMethod = new MethodDelta();
    
    private static class OptimalPartition {
        
        public int indexBegin;
        public double cost;
        public OptimalPartition previousPartition;
        
        OptimalPartition(int indexBegin, double cost, OptimalPartition previousPartition) {
            this.indexBegin = indexBegin;
            this.cost = cost;
            this.previousPartition = previousPartition;
        }
        
        OptimalPartition() {
            this(-1, 0., null);
        }
        
    }
    
    private static class EFBlock {
        
        public IntsRef documentArrayRef;
        public double costNbDocuments;
        public double costEF;
        public double cost;
        public int universe;
        public int baseDocId;
        
        public EFBlock(int[] documentArray, int offset) {
            this.documentArrayRef = new IntsRef(documentArray, offset, 1);
            int nbDocuments = documentArrayRef.length; // 1 in this case
            this.baseDocId = offset == 0 ? 0 : documentArray[offset - nbDocuments];
            this.universe = documentArray[offset + nbDocuments - 1] - baseDocId;
            this.costNbDocuments = nbDocumentsMethod.computeSize(nbDocuments);
            this.costEF = universeMethod.computeSize(universe - (nbDocuments - 1));
            this.cost = costNbDocuments + costEF;
        }
        
        public void updateWithNewDocument() {
            int oldUniverse = universe;
            int oldNbDocuments = documentArrayRef.length;
            documentArrayRef.length++;
            int nbDocuments = documentArrayRef.length;
            int offset = documentArrayRef.offset;
            universe = documentArrayRef.ints[offset + nbDocuments - 1] - baseDocId - nbDocuments;
            costNbDocuments = nbDocumentsMethod.computeSize(nbDocuments);
            if (universe == 0) {
                // A universe of 0 means that oldUniverse is 0 also.
                // In that case, we can avoid recomputing the whole size.
            } else {
                int l = computeL(universe - 1, nbDocuments - 1);
                if (oldNbDocuments == 1 || oldUniverse == 0 || computeL(oldUniverse - 1, oldNbDocuments - 1) != l) {
                    // When newL != oldL, we have to recompute the whole size.
                    documentArrayRef.length--;
                    costEF =
                            universeMethod.computeSize(universe + 1) +
                            computeSizeEliasFano(documentArrayRef, universe - 1, true, baseDocId);
                    documentArrayRef.length++;
                } else {
                    // When newL == oldL, we can avoid recomputing the whole size.
                    int highNumber         = oldUniverse >> l;
                    int previousHighNumber = (documentArrayRef.ints[offset + oldNbDocuments - 2] - baseDocId - (oldNbDocuments - 1)) >> l;
                    int highGap = highNumber - previousHighNumber;
                    costEF +=
                            (universeMethod.computeSize(universe + 1) -
                            universeMethod.computeSize(oldUniverse + 1)) +
                            l + integerencoding.UnaryEncoding.computeSizeUnary(highGap + 1);
                }
            }
            cost = costNbDocuments + costEF;
        }
    }
    
    // At the iteration i of the algorithm, we define, for 0 ≤ j ≤ i,
    // optimalPartition[j] as the optimal partition, i.e. ths optimal splitting
    // of the documentArray { x_0, ..., x_{j-1} } into an arbitrary number of
    // blocks and applying Elias-Fano on those blocks, and its cost.
    // optimalPartitionCost[0] is defined as ( {}, 0 ).
    private OptimalPartition[] optimalPartition = new OptimalPartition[1];
    // At the iteration i of the algorithm, we define, for 0 ≤ j < i,
    // lastEFBlock[j] as the documentArray { x_j, ..., x_{i - 1} } and its
    // associated properties to easily compute its Elias-Fano cost.
    private EFBlock[] lastEFBlock = new EFBlock[0];
    
    /**
     * Creates a new instance of MethodEliasFanoOptBlock.
     */
    public MethodEliasFanoOptBlock() {
        super(1);
    }
    
    @Override
    protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
        int[] documentArray = documentArrayRef.ints;
        int nbDocumentsLocal = documentArrayRef.length;
        if (lastEFBlock.length < nbDocumentsLocal) {
            optimalPartition = new OptimalPartition[nbDocumentsLocal + 1];
            lastEFBlock = new EFBlock[nbDocumentsLocal];
        }
        optimalPartition[0] = new OptimalPartition();
        for (int i = 1; i <= nbDocumentsLocal; i++) {
            // Create the last block, the singleton block { x_{i-1} }.
            lastEFBlock[i - 1] = new EFBlock(documentArray, i - 1);
            // Add { x_{i-1} } to each of the other blocks.
            for (int j = 0; j < i - 1; j++) {
                lastEFBlock[j].updateWithNewDocument();
            }
            // Computation of E[i] = min E[j] + H(x_{j+1}, ..., x_i).
            //                      0≤j<i
            optimalPartition[i] = new OptimalPartition(0, lastEFBlock[0].cost, optimalPartition[0]);
            for (int j = 0; j < i; j++) {
                double jThCost = optimalPartition[j].cost + lastEFBlock[j].cost;
                if (jThCost < optimalPartition[i].cost) {
                    optimalPartition[i].indexBegin = j;
                    optimalPartition[i].cost = jThCost;
                    optimalPartition[i].previousPartition = optimalPartition[j];
                }
            }
        }
        return optimalPartition[nbDocumentsLocal].cost;
    }
    
    @Override
    public String getName() {
        return "EliasFanoOptBlock";
    }
    
    @Override
    protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        if (lastEFBlock.length < nbDocumentsLocal) {
            optimalPartition = new OptimalPartition[nbDocumentsLocal + 1];
            lastEFBlock = new EFBlock[nbDocumentsLocal];
        }
        int[] documentArray = documentArrayRef.ints;
        optimalPartition[0] = new OptimalPartition();
        for (int i = 1; i <= nbDocumentsLocal; i++) {
            // Create the last block, the singleton block { x_{i-1} }.
            lastEFBlock[i - 1] = new EFBlock(documentArray, i - 1);
            // Add { x_{i-1} } to each of the other blocks.
            for (int j = 0; j < i - 1; j++) {
                lastEFBlock[j].updateWithNewDocument();
            }
            // Computation of E[i] = min E[j] + H(x_{j+1}, ..., x_i).
            //                      0≤j<i
            optimalPartition[i] = new OptimalPartition(0, lastEFBlock[0].cost, optimalPartition[0]);
            for (int j = 0; j < i; j++) {
                double jThCost = optimalPartition[j].cost + lastEFBlock[j].cost;
                if (jThCost < optimalPartition[i].cost) {
                    optimalPartition[i].indexBegin = j;
                    optimalPartition[i].cost = jThCost;
                    optimalPartition[i].previousPartition = optimalPartition[j];
                }
            }
        }
        writePartition(bitStream, optimalPartition[nbDocumentsLocal], nbDocumentsLocal);
    }
    
    private void writePartition(BitOutputStream bitOutputStream, OptimalPartition partition, int previousIndexBegin) {
        if (partition.previousPartition != null) {
            int indexBegin = partition.indexBegin;
            // First, the recursion.
            writePartition(bitOutputStream, partition.previousPartition, indexBegin);
            // Then, write the block.
            // (this ensures that the partitions are written in ascending order,
            // because they are constructed in reverse order).
            int nbDocumentsLocal = previousIndexBegin - indexBegin;
            int baseDocId = indexBegin == 0 ? 0 : documentArrayRef.ints[indexBegin - 1];
            int universe = documentArrayRef.ints[previousIndexBegin - 1] - baseDocId - nbDocumentsLocal;
            nbDocumentsMethod.writeCode(nbDocumentsLocal, bitOutputStream);
            universeMethod.writeCode(universe + 1, bitOutputStream);
            if (nbDocumentsLocal > 1 && universe != 0) {
                documentArrayRef.offset = indexBegin;
                documentArrayRef.length = nbDocumentsLocal - 1;
                writeIntArrayEliasFano(bitOutputStream, documentArrayRef, universe - 1, true, baseDocId);
            }
        }
    }
    
    @Override
    protected void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        int indexBegin = 0;
        int baseDocId = 0;
        while (indexBegin < nbDocumentsLocal) {
            int nbDocumentsBlock = nbDocumentsMethod.readCode(bitStream);
            int universe = universeMethod.readCode(bitStream) - 1;
            if (nbDocumentsBlock > 1) {
                if (universe == 0) {
                    // When universe is 0, it means all the gaps are 1.
                    for (int i = 0; i < nbDocumentsBlock - 1; i++) {
                        documentArrayRef.ints[indexBegin + i] = baseDocId + i + 1;
                    }
                } else {
                    if (nbDocumentsBlock - 1 > lowInts.length) {
                        lowInts = new int[nbDocumentsBlock - 1];
                    }
                    readIntArrayEliasFano(bitStream, nbDocumentsBlock - 1, indexBegin, documentArrayRef.ints, universe - 1, baseDocId, lowInts, true);
                }
            }
            indexBegin += nbDocumentsBlock;
            baseDocId += universe + nbDocumentsBlock;
            documentArrayRef.ints[indexBegin - 1] = baseDocId;
        }
    }
    
}
