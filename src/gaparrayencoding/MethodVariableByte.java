/**
 * Index compression with the variable byte method.
 *
 * Thiel and Heaps, "Program design for retrospective searches on large data bases" (1972)
 */

package gaparrayencoding;

import io.BitInputStream;
import io.BitOutputStream;
import static compresslists.Tools.nbBitsPerByte;

/**
 *
 * @author yann
 */
public class MethodVariableByte extends MethodVariableXBits {
    
    /**
     * Creates a new instance of MethodVariableByte.
     */
    public MethodVariableByte() {
        super(nbBitsPerByte);
    }
    
    @Override
    public String getName() {
        return "VariableByte";
    }
    
    public static double computeSizeVariableByte(int x) {
        return MethodVariableXBits.computeSizeVariableXBits(x, nbBitsPerByte);
    }
    
    public static void writeCodeVariableByte(int x, BitOutputStream bitStream) {
        MethodVariableXBits.writeCodeVariableXBits(x, bitStream, nbBitsPerByte);
    }
    
    public static int readCodeVariableByte(BitInputStream bitStream) {
        return MethodVariableXBits.readCodeVariableXBits(bitStream, nbBitsPerByte);
    }

}
