/**
 * Method.java
 *
 * Created on 5 march 2020
 */

package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.DeltaEncoding;
import io.BitInputStreamFile;
import io.BitOutputStreamFile;
import io.DatasetInfo;
import io.DatasetReader;
import io.DatasetReaderASCII;
import io.DatasetReaderBinary;
import static compresslists.Tools.nbBitsPerByte;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public abstract class Method {
    
    // A number of methods need to know how many documents are in the set.
    public int nbDocuments;
    
    /*
     * Some methods need to be initialized according to the gaps it handles.
     */
    public final void init(DatasetReader datasetReader) {
        if (!needsInit()) {
            return;
        }
        datasetReader.restartReader();
        initBefore();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            init(gapArrayRef);
        }
        initAfter();
    }
    
    /*
     * Optimization: do not read the dataset if it is pointless.
     */
    public boolean needsInit() {
        try {
            return !this.getClass().getMethod("init", IntsRef.class).getDeclaringClass().equals(gaparrayencoding.Method.class);
        } catch (NoSuchMethodException e) {
            return true;
        }
    }
    
    public void initBefore() {}
    public void init(IntsRef gapArrayRef) {}
    public void initAfter() {}
    
    public abstract String getName();
    
    /*
     * Compute the size, in bits, that will be taken to code the gaps given by
     * the datasetReader.
     */
    public double computeSize(DatasetReader datasetReader) {
        computeSizeBefore();
        double size = computeSizeNeededInformation();
        datasetReader.restartReader();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            size += computeSize(datasetReader.getNextGapArray());
        }
        size += computeSizeAfter();
        return size;
    }
    /*
     * Compute the size, in bits, that will be taken to code the gaps given by
     * the datasetReader. Allows this function to be not fully computed and
     * restarted later. At each percent of the computation, it will output the
     * current size computed so far, e.g., if the last output looks like
     * METHOD_NAME (XXX%) = YYY MB (ZZZ bits),
     * you can continue the computation by calling
     * computeSize(datasetReader, XXX, ZZZ);
     * (note that when calling this function, no additional information on the
     * sizes may be computed --- unless one overrides this function and takes
     * care of the additional information properly)
     */
    public double computeSize(DatasetReader datasetReader, int percentageBegin, double sizeAlreadyComputed) {
        computeSizeBefore();
        double size = percentageBegin == 0 ? computeSizeNeededInformation() : sizeAlreadyComputed;
        int nbWords = datasetReader.getNbWords();
        int idWordBegin = Tools.ceilingDivision(nbWords * percentageBegin, 100);
        int nbWordsToHandle = nbWords - idWordBegin;
        int[] idWords = new int[nbWordsToHandle];
        for (int i = 0; i < nbWordsToHandle; i++) {
            idWords[i] = i + idWordBegin;
        }
        datasetReader.restartReader(idWords);
        int currentPercentage = percentageBegin - 1;
        for (int idWord : idWords) {
            if (idWord * 100 / nbWords > currentPercentage) {
                currentPercentage = idWord * 100 / nbWords;
                System.out.println(getName() + " (" + currentPercentage + "%) = " +
                        (size / 8000000.) + " MB (" + size + " bits)");
            }
            size += computeSize(datasetReader.getNextGapArray());
        }
        size += computeSizeAfter();
        double sizeLengths = computeSizeLengths(datasetReader, nbDocuments);
        double sizeFull = size + sizeLengths;
        System.out.println(getName() + " = " + (sizeFull / 8000000.) +
                " --- " + (size / 8000000.));
        return size;
    }
    
    public void computeSizeBefore() {}
    public abstract double computeSize(IntsRef gapArrayRef);
    /*
     * There are optimizations of some methods when working block by block, e.g.
     * the Interpolative method.
     */
    public double computeSizeBlock(IntsRef gapArrayRef) {
        return computeSize(gapArrayRef);
    }
    public double computeSizeAfter() { return 0.; }
    
    public double computeSizeLengths(DatasetReader datasetReader, int nbDocuments) {
        // Delta method to export lengths
        MethodDelta methodDelta = new MethodDelta();
        DatasetInfo datasetInfo = new DatasetInfo(datasetReader.getGapArrayLengthsArray(), nbDocuments);
        // Init is useless for the Delta method, but if we decide to change the
        // method used, we need to think about it...
        methodDelta.init(datasetInfo);
        double sizeDelta = methodDelta.computeSize(datasetInfo);
        return 64. + // nbGapArrays, nbDocuments
                Tools.nextMultiple((int)Math.ceil(sizeDelta), nbBitsPerByte); // the lengths
    }
    
    // WARNING: It is not guaranteed that all "theoretical sizes" are correct.
    protected boolean computeTheoreticalSize = true;
    
    /*
     * Compute the size, in bits, that will be taken to code these gaps,
     * then log the value and additional information.
     */
    public final double computeSize(DatasetReader datasetReader, int nbDocuments, long nbPointers, boolean hasToLog) {
        double size;
        double sizeLengths = computeSizeLengths(datasetReader, nbDocuments);
        if (computeTheoreticalSize) {
            size = computeSize(datasetReader);
        } else {
            String tmpFilename = "tmp_index.data";
            exportToFile(tmpFilename, datasetReader, nbDocuments);
            File tmpFile = new File(tmpFilename);
            size = tmpFile.length() * 8. - sizeLengths;
//            tmpFile.delete();
        }
        double sizeFull = size + sizeLengths;
        if (hasToLog) {
            System.out.print(getName() + " = " + (sizeFull / 8000000.) +
                    " --- " + (size / 8000000.));
            if (nbPointers > 0) {
                System.out.print(" --- " + (sizeFull / nbPointers) + " bits per pointer");
            }
            if (computeTheoreticalSize) {
                standardOutput();
            }
            System.out.println("");
        }
        return sizeFull;
    }
    public final double computeSize(DatasetReader datasetReader, int nbDocuments, boolean hasToLog) {
        return computeSize(datasetReader, nbDocuments, 0, hasToLog);
    }
    
    /*
     * Log additional information.
     */
    public void standardOutput() {}
    
    /*
     * Gap bound, as modified from the (too big) definition in:
     * Gupta, Hon, Shah, and Vitter, "Compressed Dictionaries: Space Measures, Data Sets, and Experiments", 2006
     * p. 161
     * (this is because we write currentGap - 1)
     */
    public static void standardOutputGapBound(DatasetReader datasetReader) {
        datasetReader.restartReader();
        double size = 0.;
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            int nbBits = 0;
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            int[] gapArray = gapArrayRef.ints;
            int nbGaps = gapArrayRef.length;
            int offset = gapArrayRef.offset;
            for (int idGap = 0; idGap < nbGaps; idGap++) {
                nbBits += Math.max(Tools.ceilingLog2(gapArray[offset + idGap]), 1);
            }
            size += (double)nbBits;
        }
        System.out.println("Gap bound = " + (size / 8000000.));
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of additional material needed by the method.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public double computeSizeNeededInformation() {
        return 0.0;
    }
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void exportNeededInformation(DataOutputStream out) throws IOException {}
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void importNeededInformation(DataInputStream in) throws IOException {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of the index file *with padding* from the gapArrays.
    // Padding is introduced, e.g.:
    //    * when a method outputs bit by bit, the export can only export byte
    //      by byte, at the end of a gapArray, between 0 and 7 bits can be
    //      wasted.
    //    * when a method packs different numbers in an int (e.g.,
    //      simple9), more bits can be wasted at the end of a gapArray (e.g.,
    //      between 0 and 27 bits for simple9).
    //    * when a method packs different numbers in a long (e.g.,
    //      simple8b), more bits can be wasted at the end of a gapArray (e.g.,
    //      between 0 and 59 bits for simple8b).
    //    * when a method packs different numbers in a 128-bit integer (e.g.,
    //      qmx), more bits can be wasted at the end of a gapArray (e.g.,
    //      between 0 and 127 bits for qmx).
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Writes the gap array in a dataOutputStream, by first encoding them with
     * the given compression method.
     */
    public abstract void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException;
    /*
     * There are optimizations of some methods when working block by block, e.g.
     * the Interpolative method.
     */
    public void writeGapArrayBlock(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        writeGapArray(dataOutputStream, gapArrayRef);
    }
    
    /*
     * Writes the gap array list in a dataOutputStream, by first encoding them
     * with the given compression method.
     */
    public void writeGapArrayList(DataOutputStream dataOutputStream, DatasetReader datasetReader) throws IOException {
        datasetReader.restartReader();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            writeGapArray(dataOutputStream, gapArrayRef);
        }
    }
    
    public void exportLengthsNeededToUncompress(DatasetReader datasetReader, DataOutputStream out) throws IOException {
        out.writeInt(datasetReader.getNbWords());
        BitOutputStreamFile bitStream = new BitOutputStreamFile(out);
        int[] gapArrayLengths = datasetReader.getGapArrayLengthsArray();
        for (int gapArrayLength : gapArrayLengths) {
            DeltaEncoding.writeCodeDelta(gapArrayLength, bitStream);
        }
        bitStream.close();
    }
    
    /*
     * Exports all the gapArrays.
     * The exported file will be named baseFileName + "." + getName(), and this
     * fileName will be returned.
     * N.B.: Could be final, only MethodRePair extends it.
     */
    public String exportToFile(String baseFileName, DatasetReader datasetReader, int nbDocuments) {
        String fileName = baseFileName + "." + getName();
        try (FileOutputStream fout = new FileOutputStream(fileName);
                DataOutputStream out = new DataOutputStream(fout)) {
            out.writeInt(nbDocuments);
            exportNeededInformation(out);
            exportLengthsNeededToUncompress(datasetReader, out);
            // Export the gap arrays
            writeGapArrayList(out, datasetReader);
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + fileName + "'.");
        }
        return fileName;
    }
    
    /*
     * Exports all the gapArrays in different files, according to batch limits.
     * The exported files will be named baseFilename + the id of the batch.
     */
    public void splitToFiles(String baseFilename, DatasetReader datasetReader, int nbDocuments, double[] batchLimits) {
        int nbBatches = batchLimits.length - 1;
        assert(batchLimits[0] == 0.);
        assert(batchLimits[nbBatches] == 1.);
        for (int i = 0; i < nbBatches; i++) {
            assert(batchLimits[i] < batchLimits[i + 1]);
        }
        double[] frequencyStarts = new double[nbBatches];
        double[] frequencyStops  = new double[nbBatches];
        for (int i = 0; i < nbBatches; i++) {
            frequencyStarts[i] = batchLimits[i];
            frequencyStops[i]  = batchLimits[i + 1];
        }
        try {
            FileOutputStream[] fouts = new FileOutputStream[nbBatches];
            DataOutputStream[] outs = new DataOutputStream[nbBatches];
            for (int i = 0; i < nbBatches; i++) {
                fouts[i] = new FileOutputStream(baseFilename + i + "." + getName());
                outs[i] = new DataOutputStream(fouts[i]);
            }
            // Equivalent of exportLengthsNeededToUncompress
            int[] gapArrayLengths = datasetReader.getGapArrayLengthsArray();
            for (int i = 0; i < nbBatches; i++) {
                DataOutputStream out = outs[i];
                out.writeInt(nbDocuments);
                exportNeededInformation(out);
                ArrayList<Integer> gapArrayLengthsList = new ArrayList<>();
                for (int gapArrayLength : gapArrayLengths) {
                    if (gapArrayLength > frequencyStarts[i] * nbDocuments && gapArrayLength <= frequencyStops[i] * nbDocuments) {
                        gapArrayLengthsList.add(gapArrayLength);
                    }
                }
                out.writeInt(gapArrayLengthsList.size());
                try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
                    for (int gapArrayLength : gapArrayLengthsList) {
                        DeltaEncoding.writeCodeDelta(gapArrayLength, bitStream);
                    }
                }
            }
            // Equivalent of writeGapArrayList
            datasetReader.restartReader();
            for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
                IntsRef gapArrayRef = datasetReader.getNextGapArray();
                int nbGaps = gapArrayRef.length;
                for (int i = 0; i < nbBatches; i++) {
                    if (nbGaps > frequencyStarts[i] * nbDocuments && nbGaps <= frequencyStops[i] * nbDocuments) {
                        writeGapArray(outs[i], gapArrayRef);
                        break;
                    }
                }
            }
            for (int i = 0; i < nbBatches; i++) {
                outs[i].close();
                fouts[i].close();
            }
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + baseFilename + "[XXX]." + getName() + "'.");
        }
    }
    
    /*
     * Merge all the gapArrays from different files.
     */
    public void mergeFiles(DatasetReader[] batchedDatasetReaders, String filename, int nbDocuments) {
        try (FileOutputStream fout = new FileOutputStream(filename);
                DataOutputStream out = new DataOutputStream(fout)) {
            out.writeInt(nbDocuments);
            exportNeededInformation(out);
            // Equivalent of exportLengthsNeededToUncompress
            int nbWords = 0;
            for (DatasetReader datasetReader : batchedDatasetReaders) {
                nbWords += datasetReader.getNbWords();
            }
            out.writeInt(nbWords);
            BitOutputStreamFile bitStream = new BitOutputStreamFile(out);
            for (DatasetReader datasetReader : batchedDatasetReaders) {
                int[] gapArrayLengths = datasetReader.getGapArrayLengthsArray();
                for (int gapArrayLength : gapArrayLengths) {
                    DeltaEncoding.writeCodeDelta(gapArrayLength, bitStream);
                }
            }
            bitStream.close();
            // Equivalent of writeGapArrayList
            for (DatasetReader datasetReader : batchedDatasetReaders) {
                writeGapArrayList(out, datasetReader);
            }
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    /*
     * Read a dataInputStream to extract gapArrayRef.length gaps encoded with
     * the given compression method.
     * Most methods need to know in advance the number of gaps to decode, and
     * will thus decode that much gaps in gapArrayRef.ints.
     * Some methods do not know in advance this number, and instead will write
     * the value of gapArrayRef.length, and decode that much gaps in
     * gapArrayRef.ints.
     */
    public abstract void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException;
    /*
     * There are optimizations of some methods when working block by block, e.g.
     * the Interpolative method.
     */
    public void readGapArrayBlock(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        readGapArray(dataInputStream, gapArrayRef);
    }
    
    public ArrayList<Integer> importLengthsNeededToUncompress(DataInputStream in) throws IOException {
        int nbGapArrays = in.readInt();
        ArrayList<Integer> gapArraySizes = new ArrayList<>();
        BitInputStreamFile bitStream = new BitInputStreamFile(in);
        for (int i = 0; i < nbGapArrays; i++) {
            int nbGapsLocal = DeltaEncoding.readCodeDelta(bitStream);
            gapArraySizes.add(nbGapsLocal);
        }
        bitStream.close();
        return gapArraySizes;
    }
    
    /*
     * Imports all the gapArrays from a file that contains the total number of
     * documents and the sizes of each gapArray.
     */
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                readGapArray(dataInputStream, intArrayRef);
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    ArrayList<Integer> lengthsNeededToUncompress = importLengthsNeededToUncompress(in);
                    nbWords = lengthsNeededToUncompress.size();
                    for (int nbGapsLocal : lengthsNeededToUncompress) {
                        updateStatisticsWithNewGapArray(nbGapsLocal);
                    }
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + filename + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Static ASCII methods --- to ease checkings.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Export to ASCII file.
     */
    public static void exportToFileASCII(String filename, DatasetReader datasetReader, int nbDocuments) {
        datasetReader.restartReader();
        try (FileWriter out = new FileWriter(filename)) {
            out.write("# Inverted index : for each word, the ids of documents that contain this word" +
                    " (preceded by the number of documents, in parentheses).\n");
            out.write("# In total, " + datasetReader.getNbWords() + " words.\n");
            out.write("# In total, " + nbDocuments + " documents.\n");
            for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
                IntsRef gapArrayRef = datasetReader.getNextGapArray();
                int[] gapArray = gapArrayRef.ints;
                int nbGaps = gapArrayRef.length;
                int offset = gapArrayRef.offset;
                out.write(idWord + "-th_name (" + nbGaps + ")");
                int idDocument = 0;
                for (int idGap = 0; idGap < nbGaps; idGap++) {
                    idDocument += gapArray[offset + idGap];
                    out.write(" " + idDocument);
                }
                out.write("\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    /*
     * Import from ASCII file.
     */
    public static DatasetReader importFromFileASCII(String filename) {
        return new DatasetReaderASCII(filename) {
            @Override
            protected void goToGapArrays() throws IOException {
                bufferedReader.readLine();
                bufferedReader.readLine();
                bufferedReader.readLine();
            }
            
            @Override
            protected void readGapArray(String line) {
                readDocumentArray(line);
                Tools.documentArray2gapArray(intArrayRef, intArrayRef);
            }
            
            @Override
            protected void readDocumentArray(String line) {
                String[] splitLine = line.split(" ");
                int nbDocumentsLocal = Integer.parseInt(splitLine[1].replaceAll("[^0-9]", ""));
                assert(splitLine.length == nbDocumentsLocal + 2);
                assert(intArrayRef.length == nbDocumentsLocal);
                for (int idDocument = 0; idDocument < nbDocumentsLocal; idDocument++) {
                    intArrayRef.ints[idDocument] = Integer.parseInt(splitLine[idDocument + 2]);
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileReader fileReader = new FileReader(filename);
                        BufferedReader br = new BufferedReader(fileReader)) {
                    String line;
                    br.readLine();
                    line = br.readLine();
                    nbWords = Integer.parseInt(line.replaceAll("[^0-9]", ""));
                    line = br.readLine();
                    nbDocuments = Integer.parseInt(line.replaceAll("[^0-9]", ""));
                    for (int i = 0; i < nbWords; i++) {
                        line = br.readLine();
                        String[] splitLine = line.split(" ");
                        assert(splitLine[0].equals(i + "-th_name"));
                        int nbDocumentsLocal = Integer.parseInt(splitLine[1].replaceAll("[^0-9]", ""));
                        assert(Integer.parseInt(splitLine[nbDocumentsLocal + 1]) <= nbDocuments);
                        updateStatisticsWithNewGapArray(nbDocumentsLocal);
                    }
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Unable to open file '" + filename + "'.");
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Testing only.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Export the lengthArrayOccurrences.
     */
    public static void exportLengthOccurrences(DatasetReader datasetReader, String filename) {
        int[] gapArrayLengthsArray = datasetReader.getGapArrayLengthsArray();
        int nbDocuments = datasetReader.getNbDocuments();
        DatasetInfo datasetInfo = new DatasetInfo(gapArrayLengthsArray, nbDocuments);
        MethodInformationTheoretic methodIT = new MethodInformationTheoretic(nbDocuments);
        methodIT.init(datasetInfo);
        
        IntsRef gapArrayLengthArrayRef = new IntsRef(gapArrayLengthsArray);
        long[] gapArrayOccurrences = Tools.gapOccurrences(gapArrayLengthArrayRef, nbDocuments);
        try (FileWriter out = new FileWriter(filename)) {
            out.write("IT method (in MB) : size of probas = " + (methodIT.computeSizeNeededInformation() / 8000000.) +
                    "; size of the encoded length array = " + (methodIT.computeSize(datasetInfo) / 8000000.) +
                    "\n");
            out.write(Arrays.toString(gapArrayOccurrences));
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
}
