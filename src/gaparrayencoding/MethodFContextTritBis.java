/**
 * MethodFContextTritBis.java
 * For each gapArray, here what is done:
 *    0. We note gapSizeArray the array such that
 *       gapSizeArray[i] = floor(log_2(gapArray[i])).
 *       We note tritList = tritList(gapSizeArray) and T the size of tritList.
 *    1. We code the first min(k+w, T) trits from tritList with an arithmetic
 *       contextual method --- where the context of the i-th trit is the
 *       min(i-1, kInit) previous trits.
 *    2. Then, a contextual arithmetic method encodes the trits
 *       trit_{k+w}, ... trit_T --- where the context is the previous k trits +
 *       the number of "2" in the w trits before them.
 *    3. We have now encoded what enables us to retrieve the "2" in
 *       tritList(gapArray). There remains to put the "{0,1}" from this trit
 *       list. We code them with an arithmetic contextual method --- where the
 *       context of the i-th bit is the min(i-1, kFinal) previous bits.
 * 
 * N.B.: MethodFContextTrit is in fact equal to MethodFContextTritBis with
 * kFinal = 0.
 */

package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequenceContextualEncoding;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import tritlistencoding.TritSequence;
import tritlistencoding.TritSequenceContextualEncoding;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodFContextTritBis extends MethodByArithmeticCoding {
    
    private final TritSequenceContextualEncoding tritSequenceEncoding;
    private final BitSequenceContextualEncoding bitSequenceEncoding;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    private double sizeFinal;
    
    // Parameters. Here, we have four parameters k, w, kInit, and kFinal.
    private int k;
    private int w;
    private int kInit;
    private int kFinal;
    
    private TritSequence tritSequence = new TritSequence();
    private TritSequence tl = new TritSequence();
    private BitSequence b01 = new BitSequence();
    
    /**
     * Creates a new instance of MethodFContextTritBis.
     */
    public MethodFContextTritBis(int k, int w, int kInit, int kFinal) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        this.kFinal = kFinal;
        tritSequenceEncoding = new TritSequenceContextualEncoding(k, w, kInit);
        bitSequenceEncoding = new BitSequenceContextualEncoding(Integer.MAX_VALUE, 0, kFinal);
    }
    
    public MethodFContextTritBis(int k, int w, int kInit) {
        this(k, w, kInit, kInit);
    }
    
    @Override
    public void initBefore() {
        tritSequenceEncoding.initBefore();
        bitSequenceEncoding.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        tritSequenceEncoding.initWithNewTritSequence(tl);
        bitSequenceEncoding.initWithNewBitSequence(b01);
    }
    
    @Override
    public void initAfter() {
        tritSequenceEncoding.initAfter();
        bitSequenceEncoding.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        double[] phase1Sizes = tritSequenceEncoding.computeSizes(tl);
        double[] phase2Sizes = bitSequenceEncoding.computeSizes(b01);
        
        double sizeInitialLocal = phase1Sizes[0];
        double sizeGenericLocal = phase1Sizes[1];
        double sizeFinalLocal   = phase2Sizes[0];
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        sizeFinal   += sizeFinalLocal;
        return sizeInitialLocal + sizeGenericLocal + sizeFinalLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeInitial = 0.;
        sizeGeneric = 0.;
        sizeFinal   = 0.;
        super.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        tl = new TritSequence();
        b01 = new BitSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) trits: " + (sizeInitial / 8000000) +
                " ; last trits: " + (sizeGeneric / 8000000) +
                " ; 01: " + (sizeFinal / 8000000) +
                ")");
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        tritSequenceEncoding.writeTritSequence(tl, encoder);
        bitSequenceEncoding.writeBitSequence(b01, encoder);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String getName() {
        return "ContextLengthTritBis_k" + k + "_w" + w + "_kI" + kInit + "_kF" + kFinal;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return tritSequenceEncoding.computeSizeNeededInformation() +
                bitSequenceEncoding.computeSizeNeededInformation();
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        tritSequenceEncoding.exportNeededInformation(out);
        bitSequenceEncoding.exportNeededInformation(out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        tritSequenceEncoding.importNeededInformation(in);
        bitSequenceEncoding.importNeededInformation(in);
    }
    
    /*
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        // Phase 1.
        tritSequenceEncoding.readTritSequence(nbGapsLocal, decoder, tl);
        
        // Phase 2.
        int nbBitsToRead = Tools.tritSequenceLength2nb01(tl);
        bitSequenceEncoding.readBitSequence(nbBitsToRead, decoder, false, b01);
        
        // Merge.
        MethodTrits.mergeTLAndB01(tl, b01, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }

}
