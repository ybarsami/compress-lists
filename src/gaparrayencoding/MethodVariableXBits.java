/**
 * This is essentially a file that factorizes the code of the variable byte
 * (X = 8) and the variable nibble (X = 4) methods. It can be used with other 
 * values of X, starting from 2.
 */

package gaparrayencoding;

import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class MethodVariableXBits extends MethodByElement {
    
    /*
     * Maximum 2^{nbBitsPerChunk - 1} values per chunk (excluding the continuation bit).
     */
    private int nbBitsPerChunk;
    
    /**
     * Creates a new instance of MethodVariableXBits.
     */
    public MethodVariableXBits(int nbBitsPerChunk) {
        if (nbBitsPerChunk < 2) {
            throw new RuntimeException("nbBitsPerChunk must be at least 2 to use the VariableXBits method.");
        }
        this.nbBitsPerChunk = nbBitsPerChunk;
    }
    
    @Override
    public String getName() {
        // Remark: It would be somehow more beautiful to have the name be
        // "Variable" + nbBitsPerChunk + "Bits". However, when retrieving the
        // method from its name (see, e.g., getDatasetReader() method from
        // Dataset.java), it is mandatory that "Method" + getName() always start
        // with the name of the java file in which the method is implemented.
        return "VariableXBits_" + nbBitsPerChunk;
    }
    
    public static double computeSizeVariableXBits(int x, int nbBitsPerChunk) {
        return Math.max(nbBitsPerChunk * Tools.ceilingDivision(Tools.ceilingLog2(x), nbBitsPerChunk - 1),
                nbBitsPerChunk);
    }
    
    @Override
    public double computeSize(int x) {
        return computeSizeVariableXBits(x, nbBitsPerChunk);
    }
    
    public static void writeCodeVariableXBits(int x, BitOutputStream bitStream, int nbBitsPerChunk) {
        int nbBits = Math.max(Tools.ceilingLog2(x), 1);
        int nbChunks = Tools.ceilingDivision(nbBits, nbBitsPerChunk - 1);
        int bitMask = 1 << (nbBits - 1);
        x = x - 1;
        
        // First chunk (includes padding)
        // Continuation bit
        bitStream.writeBit(nbChunks == 1);
        // Padding
        for (int j = nbBits; j < (nbBitsPerChunk - 1) * nbChunks; j++) {
            bitStream.writeBit(false);
        }
        // First useful bits
        for (int j = (nbBitsPerChunk - 1) * (nbChunks - 1); j < nbBits; j++) {
            bitStream.writeBit((x & bitMask) != 0);
            bitMask >>= 1;
        }
        
        // Regular chunks
        for (int i = 1; i < nbChunks; i++) {
            // Continuation bit
            bitStream.writeBit(i == nbChunks - 1);
            // Useful bits
            for (int j = 0; j < nbBitsPerChunk - 1; j++) {
                bitStream.writeBit((x & bitMask) != 0);
                bitMask >>= 1;
            }
        }
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        writeCodeVariableXBits(x, bitStream, nbBitsPerChunk);
    }
    
    public static int readCodeVariableXBits(BitInputStream bitStream, int nbBitsPerChunk) {
        int gap = 0;
        int continuationBit = 0;
        while (continuationBit == 0) {
            // Extract a chunk.
            continuationBit = bitStream.getNextBit();
            // Update the gap.
            for (int i = 0; i < nbBitsPerChunk - 1; i++) {
                int bitRead = bitStream.getNextBit();
                gap *= 2;
                gap += bitRead;
            }
        }
        // What is actually stored is gap - 1
        return gap + 1;
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return readCodeVariableXBits(bitStream, nbBitsPerChunk);
    }

}
