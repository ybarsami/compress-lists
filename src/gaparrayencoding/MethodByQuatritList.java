/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.DatasetReader;
import io.DatasetReaderBinary;
import quatritlistencoding.QuatritSequence;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodByQuatritList extends MethodByBitSequence {
    
    protected quatritlistencoding.Method qMethod;
    
    private QuatritSequence quatritSequence = new QuatritSequence();
    
    /**
     * Creates a new instance of MethodByQuatritList.
     */
    public MethodByQuatritList() {}
    public MethodByQuatritList(int nbDocuments, quatritlistencoding.Method qMethod) {
        this.nbDocuments = nbDocuments;
        this.qMethod = qMethod;
    }
    
    @Override
    public void initBefore() {
        qMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
        qMethod.init(quatritSequence);
    }
    
    @Override
    public void initAfter() {
        qMethod.initAfter();
    }
    
    public void init(String filename) {
        qMethod.init(filename);
    }
    
    @Override
    public double computeSizeLengths(DatasetReader datasetReader, int nbDocuments) {
        return 64.; // nbGapArrays, nbDocuments
    }
    
    @Override
    public void computeSizeBefore() {
        qMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
        return qMethod.computeSize(quatritSequence);
    }
    
    @Override
    public double computeSizeAfter() {
        quatritSequence = new QuatritSequence();
        return qMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        qMethod.standardOutput();
    }
    
    @Override
    public String getName() {
        return "ByQuatritList(" + qMethod.getName() + ")";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        qMethod.outputRemainingBits(bitStream);
    }
    
    @Override
    public final void flushRemainingBits() {
        qMethod.flushRemainingBits();
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
        qMethod.writeQuatritSequence(bitStream, quatritSequence);
    }
    
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        // Remark: gapArrayRef.length; is used only as output in this method,
        // not as input. Indeed, a '3' indicates the end of a gapArray.
        qMethod.readQuatritSequence(bitStream, 1, quatritSequence);
        Tools.tritSequence2intArray(quatritSequence, gapArrayRef);
    }
    
    public static final double computeSizeQuatritMethod(quatritlistencoding.Method qMethod) {
        return
                8. +
                qMethod.getClass().getName().length() * 16. +
                qMethod.computeSizeNeededInformation();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return computeSizeQuatritMethod(qMethod);
    }
   
    public static final void exportQuatritMethod(quatritlistencoding.Method qMethod, DataOutputStream out) throws IOException {
        String qMethodName = qMethod.getClass().getName();
        out.writeByte(qMethodName.length());
        out.writeChars(qMethodName);
        qMethod.exportNeededInformation(out);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        exportQuatritMethod(qMethod, out);
    }
    
    public static final quatritlistencoding.Method importQuatritMethod(DataInputStream in) throws IOException {
        byte qMethodNameLength = in.readByte();
        String qMethodName = "";
        for (int i = 0; i < qMethodNameLength; i++) {
            qMethodName += in.readChar();
        }
        try {
            Class methodClass = Class.forName(qMethodName);
            quatritlistencoding.Method qMethod = (quatritlistencoding.Method)methodClass.newInstance();
            qMethod.importNeededInformation(in);
            return qMethod;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
            System.out.println(qMethodName + " is not a valid quatritlistencoding method for CompressLists.");
            return null;
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        qMethod = importQuatritMethod(in);
    }
    
    @Override
    public void exportLengthsNeededToUncompress(DatasetReader datasetReader, DataOutputStream out) throws IOException {
        out.writeInt(datasetReader.getNbWords());
    }
    
    @Override
    public ArrayList<Integer> importLengthsNeededToUncompress(DataInputStream in) throws IOException {
        int nbGapArrays = in.readInt();
        ArrayList<Integer> nbGapArraysList = new ArrayList<>();
        for (int i = 0; i < nbGapArrays; i++) {
            nbGapArraysList.add(1); // Or any other number, it doesn't matter.
        }
        return nbGapArraysList;
    }
    
    // TODO: this is mostly a copy/paste from MethodByBitSequence.java.
    // Find a way to factorize the code.
    @Override
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            private BitInputStreamFile bitInputStream;
            
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
                bitInputStream = new BitInputStreamFile(dataInputStream);
                intArrayRef.ints = new int[nbDocuments];
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                readGapArray(bitInputStream, intArrayRef);
                assert(intArrayRef.length == gapArrayLengths.get(nbIntArraysRead));
                if (nbIntArraysRead == nbWords - 1) {
                    flushRemainingBits();
                    bitInputStream.close();
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream);
                        BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    nbWords = importLengthsNeededToUncompress(in).size();
                    intArrayRef.ints = new int[nbDocuments];
                    for (int idWord = 0; idWord < nbWords; idWord++) {
                        readGapArray(bitStream, intArrayRef);
                        updateStatisticsWithNewGapArray(intArrayRef.length);
                    }
                    flushRemainingBits();
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
}
