/**
 * Transforms each gap array into a trit sequence and codes the trits according
 * to their global observed frequency.
 * 
 * If useRealOccurrences is set to false, it codes each trit with
 * -log_2(1/3) = 1.584962501 bits instead --- slightly better than 8/5 = 1.6
 * which is what is done in MethodTrits. This would correspond to the fact that
 * each trit occurs with probability 1/3.
 * 
 * N.B.: Of course, it is equal to MethodAriCodeXTrits with X = 1.
 * 
 * N.B.: ArithmeticCodingForTrits is also equal to MethodContextTrit with
 * k = w = kInit = 0.
 */

package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;
import tritlistencoding.TritSequence;
import static compresslists.Tools.nbTrits;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodArithmeticTrits extends MethodByArithmeticCoding {
    
    public static enum OccurrenceType {
        OCCURRENCE_DENSITY("Density"),
        OCCURRENCE_REAL("Real"),
        OCCURRENCE_UNIFORM("Uniform");
        
        private String str;
        
        OccurrenceType(String str) {
            this.str = str;
        }
        
        public String getString() { return str; }
    };
    
    private OccurrenceType occurrenceType;
    
    // Size of the occurrence arrays.
    private final int arraySize = nbTrits;
    
    // Occurrence arrays.
    private long[] occurrences;
    private int[] occurrencesCumulative;
    private int[][] occurrencesCumulativeDensity;
    
    private double nbBitsPerTrit;
    
    private TritSequence tritSequence = new TritSequence();
    
    /**
     * Creates a new instance of MethodAriCodeTrits.
     */
    public MethodArithmeticTrits() {}
    public MethodArithmeticTrits(int nbDocuments, OccurrenceType occurrenceType) {
        this.nbDocuments = nbDocuments;
        setParameters(occurrenceType.ordinal());
    }
    public MethodArithmeticTrits(int nbDocuments) {
        this(nbDocuments, OccurrenceType.OCCURRENCE_DENSITY);
    }
    
    public final void setParameters(int occurrenceTypeOrdinal) {
        boolean foundOccType = false;
        for (OccurrenceType occType : OccurrenceType.values()) {
            if (occType.ordinal() == occurrenceTypeOrdinal) {
                this.occurrenceType = occType;
                foundOccType = true;
                break;
            }
        }
        if (!foundOccType) {
            throw new RuntimeException("Wrong ordinal for OccurrenceType : " + occurrenceTypeOrdinal + ".");
        }
        allocateOccurrences();
        if (occurrenceType == OccurrenceType.OCCURRENCE_UNIFORM) {
            OccurrenceTools.fillUniformOccurrences(occurrences);
        }
    }
    
    private void allocateOccurrences() {
        occurrences = new long[arraySize];
	occurrencesCumulative = new int[arraySize + 1];
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        if (occurrenceType == OccurrenceType.OCCURRENCE_REAL) {
            Tools.intArray2tritSequence(gapArrayRef, tritSequence);
            for (int i = 0; i < tritSequence.size(); i++) {
                occurrences[tritSequence.get(i)]++;
            }
        }
    }
    
    @Override
    public void initAfter() {
        if (occurrenceType == OccurrenceType.OCCURRENCE_DENSITY) {
            occurrencesCumulativeDensity = new int[nbDocuments + 1][arraySize + 1];
            for (int gapArraySize = 1; gapArraySize <= nbDocuments; gapArraySize++) {
                int myLog = Tools.ceilingLog2(Tools.ceilingDivision(nbDocuments, gapArraySize));
                long[] defaultOccurrences = new long[] {
                    Tools.ceilingDivision(myLog, 2),
                    Math.max(myLog / 2, 1L),
                    1L
                };
                OccurrenceTools.accumulate(defaultOccurrences, occurrencesCumulativeDensity[gapArraySize]);
            }
        } else {
            OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
            
            // Compute the number of bits needed per trit.
            nbBitsPerTrit = 0.;
            for (int i = 0; i < arraySize; i++) {
                double pi = (double)occurrences[i] / ((double)occurrencesCumulative[arraySize]);
                nbBitsPerTrit += -pi * Tools.log2(pi);
            }
        }
    }
    
    @Override
    public String getName() {
        return "ArithmeticTrits_" + this.occurrenceType.getString();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        // Get the trit list.
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        if (occurrenceType == OccurrenceType.OCCURRENCE_DENSITY) {
            int nbGapsLocal = gapArrayRef.length;
            // Do the encoding.
            for (int i = 0; i < tritSequence.size(); i++) {
                counter.update(occurrencesCumulativeDensity[nbGapsLocal], tritSequence.get(i));
            }
            return bitCounter.getNbBitsWrittenAndReset();
        } else {
            return tritSequence.size() * nbBitsPerTrit;
        }
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                8 + // (byte)occurrenceType.ordinal()
                (occurrenceType == OccurrenceType.OCCURRENCE_REAL ? 32 * arraySize : 0); // occurrences
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeByte((byte)occurrenceType.ordinal());
        if (occurrenceType == OccurrenceType.OCCURRENCE_REAL) {
            for (int i = 0; i < arraySize; i++) {
                out.writeInt((int) occurrences[i]);
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        setParameters(in.readByte());
        if (occurrenceType == OccurrenceType.OCCURRENCE_REAL) {
            for (int i = 0; i < arraySize; i++) {
                occurrences[i] = in.readInt();
            }
        }
        initAfter();
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        if (occurrenceType == OccurrenceType.OCCURRENCE_DENSITY) {
            occurrencesCumulative = occurrencesCumulativeDensity[nbGapsLocal];
        }
        // Get the trit list.
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        // Do the encoding.
        for (int i = 0; i < tritSequence.size(); i++) {
            encoder.update(occurrencesCumulative, tritSequence.get(i));
        }
    }
    
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        if (occurrenceType == OccurrenceType.OCCURRENCE_DENSITY) {
            occurrencesCumulative = occurrencesCumulativeDensity[nbGapsLocal];
        }
        tritSequence.reset();
        int nbGapsRead = 0;
        // Do the decoding.
        while (nbGapsRead < nbGapsLocal) {
            int trit = decoder.read(occurrencesCumulative);
            if (trit == 2) {
                nbGapsRead++;
            }
            tritSequence.add(trit);
        }
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }
    
}
