/*
 * For each gapArray, here what is done:
 *    0. We split the gapArray by sub-gapArrays of size BLOCK_SIZE.
 *    1. With a GapArray method, we encode those sub-gapArrays.
 *
 * Remark: As a difference with MethodByBlock.java, between two consecutive
 * sub-gapArrays, the padding is removed.
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodByBlockNoPadding extends MethodByBitSequence {
    
    private final int BLOCK_SIZE = 128;
    private MethodByBitSequence gapMethod;
    
    private final int[] subGapArray = new int[BLOCK_SIZE];
    
    /**
     * Creates a new instance of MethodByBlockNoPadding.
     */
    public MethodByBlockNoPadding() {}
    public MethodByBlockNoPadding(MethodByBitSequence gapMethod) {
        this.gapMethod = gapMethod;
    }
    
    @Override
    public void initBefore() {
        gapMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            gapMethod.init(new IntsRef(gapArrayRef.ints, offset + idBlock * BLOCK_SIZE, curBlockSize));
        }
    }
    
    @Override
    public void initAfter() {
        gapMethod.initAfter();
    }
    
    @Override
    public void computeSizeBefore() {
        gapMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        double size = 0.;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            // We call computeSizeBlock and not computeSize to enable block
            // optimizations.
            double sizeLocal = gapMethod.computeSizeBlock(
                    new IntsRef(gapArrayRef.ints, offset + idBlock * BLOCK_SIZE, curBlockSize));
            size += sizeLocal;
        }
        return size;
    }
    
    @Override
    public double computeSizeAfter() {
        return gapMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        gapMethod.standardOutput();
    }
    
    @Override
    public String getName() {
        return "ByBlockNoPadding(" + gapMethod.getName() + ")";
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return MethodByBlock.computeSizeGapMethod(gapMethod);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        MethodByBlock.exportGapMethod(gapMethod, out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        gapMethod = (MethodByBitSequence)MethodByBlock.importGapMethod(in);
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitOutputStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            // We call writeGapArrayBlock and not writeGapArray to enable block
            // optimizations.
            gapMethod.writeGapArrayBlock(
                    bitOutputStream,
                    new IntsRef(gapArrayRef.ints, offset + idBlock * BLOCK_SIZE, curBlockSize));
        }
    }
    
    @Override
    public final void readGapArray(BitInputStream bitInputStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int nbBlocks = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        int idGap = 0;
        for (int idBlock = 0; idBlock < nbBlocks; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            // We call readGapArrayBlock and not readGapArray to enable block
            // optimizations.
            IntsRef subGapArrayRef = new IntsRef(subGapArray, 0, curBlockSize);
            gapMethod.readGapArrayBlock(bitInputStream, subGapArrayRef);
            
            for (int i = 0; i < curBlockSize; i++) {
                int gap = subGapArray[i];
                gapArrayRef.ints[idGap++] = gap;
            }
        }
    }
    
}
