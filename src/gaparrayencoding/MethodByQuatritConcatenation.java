/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.BitInputStream;
import io.BitInputStreamArray;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.DatasetReader;
import io.DatasetReaderBinary;
import quatritlistencoding.QuatritSequence;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodByQuatritConcatenation extends MethodByBitSequence {
    
    protected quatritlistencoding.Method qMethod;
    
    private QuatritSequence quatritSequence = new QuatritSequence();
    private QuatritSequence concatenatedSequence = new QuatritSequence();
    
    /**
     * Creates a new instance of MethodByQuatritConcatenation.
     */
    public MethodByQuatritConcatenation() {}
    public MethodByQuatritConcatenation(int nbDocuments, quatritlistencoding.Method qMethod) {
        this.nbDocuments = nbDocuments;
        this.qMethod = qMethod;
    }
    
    @Override
    public void initBefore() {
        qMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
        try {
            concatenatedSequence.concatenate(quatritSequence);
        } catch (IndexOutOfBoundsException e) {
            qMethod.init(concatenatedSequence);
            concatenatedSequence = quatritSequence;
        }
    }
    
    @Override
    public void initAfter() {
        qMethod.init(concatenatedSequence);
        concatenatedSequence.reset();
        qMethod.initAfter();
    }
    
    @Override
    public double computeSizeLengths(DatasetReader datasetReader, int nbDocuments) {
        datasetReader.restartReader();
        int nbQuatritSequenceConcatenated = 0;
        int currentSize = 0;
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
            int quatritSequenceSize = quatritSequence.size();
            try {
                currentSize = Math.addExact(currentSize, quatritSequenceSize);
            } catch (IndexOutOfBoundsException e) {
                nbQuatritSequenceConcatenated++;
                currentSize = quatritSequenceSize;
            }
        }
        nbQuatritSequenceConcatenated++;
        return 32. + // nbDocuments
                32. * (1. + nbQuatritSequenceConcatenated); // nbThreesList
    }
    
    @Override
    public void computeSizeBefore() {
        qMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
        try {
            concatenatedSequence.concatenate(quatritSequence);
            return 0.;
        } catch (IndexOutOfBoundsException e) {
            double size = qMethod.computeSize(concatenatedSequence);
            concatenatedSequence = quatritSequence;
            return size;
        }
    }
    
    @Override
    public double computeSizeAfter() {
        double size = qMethod.computeSize(concatenatedSequence);
        concatenatedSequence = new QuatritSequence();
        return size + qMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        qMethod.standardOutput();
    }
    
    @Override
    public String getName() {
        return "ByQuatritConcatenation(" + qMethod.getName() + ")";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        qMethod.writeQuatritSequence(bitStream, concatenatedSequence);
        concatenatedSequence = new QuatritSequence();
        qMethod.outputRemainingBits(bitStream);
    }
    
    @Override
    public final void flushRemainingBits() {
        qMethod.flushRemainingBits();
    }
    
    @Override
    public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
        try {
            concatenatedSequence.concatenate(quatritSequence);
        } catch (IndexOutOfBoundsException e) {
            qMethod.writeQuatritSequence(bitStream, concatenatedSequence);
            concatenatedSequence = quatritSequence;
        }
    }
    
    /*
     * When we read a unique gapArray, the number of threes is 1 !
     */
    @Override
    public int[] bitSequence2gapArray(BitSequence bitSequence, int nbGapsLocal) {
        BitInputStreamArray bitInputStream = new BitInputStreamArray(bitSequence);
        IntsRef gapArrayRef = new IntsRef(new int[nbGapsLocal], 0, 1);
        readGapArray(bitInputStream, gapArrayRef);
        return gapArrayRef.ints;
    }
    
    private int nbThreesRead = 0;
    private int indexLastThree = -1;
    
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbThrees = gapArrayRef.length;
        // The first time we call readGapArray, we need to read a concatenated
        // list of gapArrays.
        if (nbThreesRead == 0) {
            qMethod.readQuatritSequence(bitStream, nbThrees, concatenatedSequence);
        }
        // We extract a gapArray from the concatenation.
        int indexThree = indexLastThree + 1;
        while (concatenatedSequence.get(indexThree) != 3) {
            indexThree++;
        }
        QuatritSequence.sequenceCopy(concatenatedSequence, indexLastThree + 1, indexThree, quatritSequence);
        int nbTwos = quatritSequence.numberOfTwos();
        gapArrayRef.length = nbTwos;
        Tools.tritSequence2intArray(quatritSequence, gapArrayRef);
        indexLastThree = indexThree;
        nbThreesRead++;
        // When we finished the concatenation, we reset everything.
        if (nbThreesRead == nbThrees) {
            nbThreesRead = 0;
            indexLastThree = -1;
            concatenatedSequence = new QuatritSequence();
        }
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return MethodByQuatritList.computeSizeQuatritMethod(qMethod);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        MethodByQuatritList.exportQuatritMethod(qMethod, out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        qMethod = MethodByQuatritList.importQuatritMethod(in);
    }
    
    @Override
    public void exportLengthsNeededToUncompress(DatasetReader datasetReader, DataOutputStream out) throws IOException {
        datasetReader.restartReader();
        int currentQuatritSequenceSize = 0;
        int currentNbThrees = 0;
        int nbQuatritSequenceConcatenated = 0;
        ArrayList<Integer> nbThreesList = new ArrayList<>();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            Tools.intArray2quatritSequence(gapArrayRef, quatritSequence);
            int quatritSequenceSize = quatritSequence.size();
            try {
                currentQuatritSequenceSize = Math.addExact(currentQuatritSequenceSize, quatritSequenceSize);
                currentNbThrees++;
            } catch (IndexOutOfBoundsException e) {
                nbQuatritSequenceConcatenated++;
                nbThreesList.add(currentNbThrees);
                currentQuatritSequenceSize = quatritSequenceSize;
                currentNbThrees = 1;
            }
        }
        nbQuatritSequenceConcatenated++;
        nbThreesList.add(currentNbThrees);
        out.writeInt(nbQuatritSequenceConcatenated);
        for (int nbThrees : nbThreesList) {
            out.writeInt(nbThrees);
        }
    }
    
    @Override
    public ArrayList<Integer> importLengthsNeededToUncompress(DataInputStream in) throws IOException {
        int nbQuatritSequenceConcatenated = in.readInt();
        ArrayList<Integer> nbThreesList = new ArrayList<>();
        for (int i = 0; i < nbQuatritSequenceConcatenated; i++) {
            int nbThrees = in.readInt();
            for (int j = 0; j < nbThrees; j++) {
                // It is necessary that when reading a gapArray, we know how
                // much gapArrays were concatenated.
                nbThreesList.add(nbThrees);
            }
        }
        return nbThreesList;
    }
    
    // TODO: this is mostly a copy/paste from MethodByBitSequence.java.
    // Find a way to factorize the code.
    @Override
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            private BitInputStreamFile bitInputStream;
            private ArrayList<Integer> nbThreesList;
            
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
                bitInputStream = new BitInputStreamFile(dataInputStream);
                intArrayRef.ints = new int[nbDocuments];
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                intArrayRef.length = nbThreesList.get(nbIntArraysRead);
                readGapArray(bitInputStream, intArrayRef);
                assert(intArrayRef.length == gapArrayLengths.get(nbIntArraysRead));
                if (nbIntArraysRead == nbWords - 1) {
                    flushRemainingBits();
                    bitInputStream.close();
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream);
                        BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    nbThreesList = importLengthsNeededToUncompress(in);
                    nbWords = nbThreesList.size();
                    intArrayRef.ints = new int[nbDocuments];
                    for (int nbThrees : nbThreesList) {
                        intArrayRef.length = nbThrees;
                        readGapArray(bitStream, intArrayRef);
                        updateStatisticsWithNewGapArray(intArrayRef.length);
                    }
                    flushRemainingBits();
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
}
