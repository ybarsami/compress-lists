/**
 * Index compression with the Simple8b method.
 * It is the same idea than the Simple-9 method, on 64-bit words.
 *
 * Anh and Moffat, "Index compression using 64-bit words" (2010)
 */

package gaparrayencoding;

import arrays.IntsRef;
import io.DatasetReader;
import io.DatasetReaderBinary;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodSimple8b extends Method {
    
    // For writing
    private int[] intsToWrite = new int[codeNum[firstNonExactSelectorValue]];
    private int nbIntsToWrite = 0;
    
    // For reading
    private int[] intsRead = new int[codeNum[0]];
    private int nbIntsRead = 0;
    
    public void outputRemainingInts(LongOutputStream longOutputStream) {
        if (nbIntsToWrite == 0) {
            return;
        }
        // mainloop handles the case where it is possible to choose the
        // selector in [2; maxSelectorValue - 1 = 14].
        // Here, it is not possible that we need to use the first two selector
        // values.
        mainloop: for (int selector = firstNonExactSelectorValue; selector < maxSelectorValue; selector++) {
            // All the remaining ints will be treated in only one long.
            int compressedNum = nbIntsToWrite;
            // We check that all numbers are within the range of the selector.
            long res = 0L;
            int b = bitLength[selector];
            for (int i = 0; i < compressedNum; i++) {
                long numberToEncode = (long)intsToWrite[i] - 1L;
                if (numberToEncode >= maxValue[selector])
                    continue mainloop;
                res = (res << b) + numberToEncode;
            }
            // Padding with 0s at the end is necessary.
            res <<= (codeNum[selector] - compressedNum) * b;
            // Adding the selector.
            res |= (long)selector << nbBitsForInts;
            longOutputStream.writeLong(res);
            nbIntsToWrite = 0;
            return;
        }
        // Here, it is not possible that we need to use the last selector value,
        // the number would already have been written.
        assert(nbIntsToWrite == 0);
    }
    
    public void flushRemainingInts() {
        nbIntsRead = 0;
    }
    
    
    /**
     * Creates a new instance of MethodSimple8b.
     */
    public MethodSimple8b() {}
    
    @Override
    public String getName() {
        return "Simple8b";
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        ArrayList<Long> arrayList = longArrayListOfGapArray(gapArrayRef);
        return 64 * arrayList.size();
    }
    
    public final ArrayList<Long> longArrayListOfGapArray(IntsRef gapArrayRef) {
        ArrayList<Long> arrayList = new ArrayList<>();
        writeGapArray(arrayList, gapArrayRef);
        return arrayList;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the file from the gapArrays
    // Adapted from Simple9 by D. Lemire.
    // https://github.com/lemire/JavaFastPFOR/blob/master/src/main/java/me/lemire/integercompression/Simple9.java
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) {
        LongOutputStream longOutputStream = new LongOutputStreamFile(dataOutputStream);
        writeGapArray(longOutputStream, gapArrayRef);
        outputRemainingInts(longOutputStream);
    }
    
    public void writeGapArray(ArrayList<Long> arrayList, IntsRef gapArrayRef) {
        LongOutputStream longOutputStream = new LongOutputStreamList(arrayList);
        writeGapArray(longOutputStream, gapArrayRef);
        outputRemainingInts(longOutputStream);
    }
    
    public void writeGapArray(LongOutputStream longOutputStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        // Merge the current gapArray with the gaps still in cache.
        final int nbGapsToWrite = nbIntsToWrite + nbGapsLocal;
        final int[] newGapArray;
        if (nbIntsToWrite > 0) {
            newGapArray = new int[nbGapsToWrite];
            System.arraycopy(intsToWrite,      0, newGapArray, 0,             nbIntsToWrite);
            System.arraycopy(gapArrayRef.ints, 0, newGapArray, nbIntsToWrite, nbGapsLocal);
        } else {
            newGapArray = gapArrayRef.ints;
        }
        try {
            int nbGapsWritten = 0;
            outer: while (nbGapsWritten < nbGapsToWrite) {
                // mainloop handles the case where it is possible to choose the
                // selector in [0; maxSelectorValue - 1 = 14].
                mainloop: for (int selector = 0; selector < maxSelectorValue; selector++) {
                    int compressedNum = Math.min(nbGapsToWrite - nbGapsWritten, codeNum[selector]);
                    // For selectors 0 and 1, we must enforce *exactly*
                    // codeNum[selector] values.
                    if (selector <= 1 && compressedNum != codeNum[selector]) {
                        continue;
                    }
                    // For other selectors, we check that all numbers are within
                    // the range.
                    long res = 0L;
                    int b = bitLength[selector];
                    for (int i = 0; i < compressedNum; i++) {
                        long numberToEncode = (long)newGapArray[nbGapsWritten + i] - 1L;
                        if (numberToEncode >= maxValue[selector])
                            continue mainloop;
                        res = (res << b) + numberToEncode;
                    }
                    // If there are less numbers than can be accomodated, put
                    // them in cache for further treatment.
                    if (compressedNum != codeNum[selector]) {
                        for (int i = 0; i < compressedNum; i++) {
                            intsToWrite[i] = newGapArray[nbGapsWritten + i];
                        }
                        nbIntsToWrite = compressedNum;
                        return;
                    }
                    // Adding the selector.
                    res |= (long)selector << nbBitsForInts;
                    longOutputStream.writeLong(res);
                    nbGapsWritten += compressedNum;
                    nbIntsToWrite = 0;
                    continue outer;
                }
                // If the value to encode is >= 2^30 (30 = bitLength[14]), then
                // we have to use the last selector 15. Because gapArray is an
                // int array, the gap values are always < 2^32 so it is always
                // possible to encode a gap as a single 60-bit number, so this
                // exception should never be raised.
                final int selector = maxSelectorValue;
                int b = bitLength[selector];
                long numberToEncode = (long)newGapArray[nbGapsWritten] - 1L;
                if (numberToEncode >= maxValue[selector]) {
                    throw new RuntimeException("Cannot encode " + numberToEncode +
                            "; maximum value is 2^" + b + " - 1 = " + (2^b - 1) + ".");
                }
                longOutputStream.writeLong(numberToEncode | ((long)selector << nbBitsForInts));
                nbGapsWritten++;
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public final void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) {
        LongInputStreamFile longInputStream = new LongInputStreamFile(dataInputStream);
        readGapArray(longInputStream, gapArrayRef);
        flushRemainingInts();
    }
    
    public final void readGapArray(ArrayList<Long> arrayList, IntsRef gapArrayRef) {
        LongInputStreamList longInputStream = new LongInputStreamList(arrayList);
        readGapArray(longInputStream, gapArrayRef);
        flushRemainingInts();
    }
    
    public void readGapArray(LongInputStream longStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int nbGapsRead = 0;
        // First, read the gaps previously in cache.
        int nbGapsToReadFromCache = Math.min(nbGapsLocal, nbIntsRead);
        while (nbGapsRead < nbGapsToReadFromCache) {
            gapArrayRef.ints[nbGapsRead] = intsRead[nbGapsRead];
            nbGapsRead++;
        }
        // Then, shift the intsRead array.
        // Remark: This shift could be saved if we instead work on this array as
        // a circular one.
        if (nbGapsToReadFromCache > 0) {
            nbIntsRead -= nbGapsToReadFromCache;
            for (int i = 0; i < nbIntsRead; i++) {
                intsRead[i] = intsRead[i + nbGapsToReadFromCache];
            }
        }
        // Then, continue reading ints from the stream if needed.
        try {
            while (nbGapsRead < nbGapsLocal) {
                long val = longStream.getNextLong();
                int selector = (int)(val >>> nbBitsForInts);
                switch (selector) {
                    case 0:
                    case 1: {
                        final int howmany = Math.min(nbGapsLocal - nbGapsRead, codeNum[selector]);
                        for (int k = 0; k < howmany; k++) {
                            gapArrayRef.ints[nbGapsRead++] = 1;
                        }
                        // Finally, add the remaining ints in the cache.
                        for (int k = howmany; k < codeNum[selector]; k++) {
                            intsRead[nbIntsRead++] = 1;
                        }
                    }
                    break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15: {
                        int b = bitLength[selector];
                        int howmany = Math.min(nbGapsLocal - nbGapsRead, codeNum[selector]);
                        int wastedBits = nbBitsForInts % b;
                        for (int k = 0; k < howmany; k++) {
                            gapArrayRef.ints[nbGapsRead++] = 1 + (int)((val << (b * k + wastedBits + 64 - nbBitsForInts)) >>> (64 - b));
                        }
                        // Finally, add the remaining ints in the cache.
                        for (int k = howmany; k < codeNum[selector]; k++) {
                            intsRead[nbIntsRead++] = 1 + (int)((val << (b * k + wastedBits + 64 - nbBitsForInts)) >>> (64 - b));
                        }
                    }
                    break;
                    default: {
                        // As long as nbBitsForInts is 60, any long value shifted nbBitsForInts bits to the right,
                        // i.e., val >>> nbBitsForInts, will be in [0; 15]. This exception can thus not be
                        // thrown by the current code, even if we are reading a list of longs not created
                        // by writeGapArray.
                        throw new RuntimeException("Inconsistant value " + selector + " for the selector; values should be in the range [0; " + maxSelectorValue + "].");
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
    
    private final static int bitLength[] = { 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 15, 20, 30, 60 };
    private final static int codeNum[] = { 240, 120, 60, 30, 20, 15, 12, 10, 8, 7, 6, 5, 4, 3, 2, 1 };
    private final static long maxValue[] = Arrays
            .stream(bitLength)
            .mapToLong(i -> 1L << i)
            .toArray();
    
    private final static int nbBitsForInts = 60;
    private final static int firstNonExactSelectorValue = 2;
    private final static int maxSelectorValue = bitLength.length - 1;
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of the index file *without padding* from the gapArrays.
    // Padding is avoided because when all the gapArrays can be treated as one
    // unique gapArray.
    // For example, the Interpolative method does not allow such a treatment.
    // When a method does not allow such a treatment, the methods with and
    // without padding output exactly the same index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeGapArrayList(DataOutputStream dataOutputStream, DatasetReader datasetReader) {
        LongOutputStreamFile longOutputStream = new LongOutputStreamFile(dataOutputStream);
        writeGapArrayList(longOutputStream, datasetReader);
    }
    
    public void writeGapArrayList(LongOutputStream longOutputStream, DatasetReader datasetReader) {
        datasetReader.restartReader();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            writeGapArray(longOutputStream, gapArrayRef);
        }
        outputRemainingInts(longOutputStream);
    }
    
    // TODO: this is mostly a copy/paste from Method.java.
    // Find a way to factorize the code.
    @Override
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            private LongInputStreamFile longInputStream;
            
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
                longInputStream = new LongInputStreamFile(dataInputStream);
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                readGapArray(longInputStream, intArrayRef);
                if (nbIntArraysRead == nbWords - 1) {
                    flushRemainingInts();
                    longInputStream.close();
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    ArrayList<Integer> lengthsNeededToUncompress = importLengthsNeededToUncompress(in);
                    nbWords = lengthsNeededToUncompress.size();
                    for (int nbGapsLocal : lengthsNeededToUncompress) {
                        updateStatisticsWithNewGapArray(nbGapsLocal);
                    }
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // LongInputStream similar to BitInputStream but only used in this file.
    ////////////////////////////////////////////////////////////////////////////
    
    public static abstract class LongInputStream {
        
        /*
         * Get the next long from this long stream.
         * Throws IndexOutOfBoundsException when the end of stream is reached.
         */
       public abstract long getNextLong() throws IndexOutOfBoundsException;
        
    }
    
    public static class LongInputStreamList extends LongInputStream {
        
        final ArrayList<Long> arrayList;
        int nbLongsRead;
        
        /**
         * Creates a new instance of LongInputStreamList.
         */
        public LongInputStreamList(ArrayList<Long> arrayList) {
            this.arrayList = arrayList;
            nbLongsRead = 0;
        }
        
        @Override
        public long getNextLong() throws IndexOutOfBoundsException {
            return arrayList.get(nbLongsRead++);
        }

    }
    
    public static class LongInputStreamFile extends LongInputStream implements AutoCloseable {
        
        // The LongInputStream here comes from an external file, converted to a
        // data input stream.
        private final DataInputStream dataInputStream;
        
        /**
         * Creates a new instance of LongInputStreamFile.
         */
        public LongInputStreamFile(DataInputStream dataInputStream) {
            this.dataInputStream = dataInputStream;
        }
        
        /*
         * Get the next long from this long stream.
         */
        @Override
        public long getNextLong() throws IndexOutOfBoundsException {
            try {
                return dataInputStream.readLong();
            } catch (IOException e) {
                throw new IndexOutOfBoundsException();
            }
        }
        
        /**
         * Closes this stream and the underlying DataInputStream.
         * @throws IOException if an I/O exception occurred.
         */
        @Override
        public void close() throws IOException {
            dataInputStream.close();
        }
        
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // LongOutputStream similar to BitOutputStream but only used in this file.
    ////////////////////////////////////////////////////////////////////////////
    
    public static abstract class LongOutputStream {
        
        /*
         * Write a long to this long stream.
         */
        public abstract void writeLong(long value) throws IndexOutOfBoundsException;
        
    }
    
    public static class LongOutputStreamList extends LongOutputStream {
        
        final ArrayList<Long> arrayList;
        
        /**
         * Creates a new instance of LongOutputStreamList.
         */
        public LongOutputStreamList(ArrayList<Long> arrayList) {
            this.arrayList = arrayList;
        }
        
        @Override
        public void writeLong(long value) throws IndexOutOfBoundsException {
            arrayList.add(value);
        }

    }
    
    public static class LongOutputStreamFile extends LongOutputStream implements AutoCloseable {
        
        // The LongOutputStream here comes from an external file, converted to a data
        // output stream.
        private final DataOutputStream dataOutputStream;
        
        /**
         * Creates a new instance of LongOutputStreamFile.
         */
        public LongOutputStreamFile(DataOutputStream dataOutputStream) {
            this.dataOutputStream = dataOutputStream;
        }
        
        /*
         * Write a long to this long stream.
         */
        @Override
        public void writeLong(long value) throws IndexOutOfBoundsException {
            try {
                dataOutputStream.writeLong(value);
            } catch (IOException e) {
                throw new IndexOutOfBoundsException();
            }
        }
        
        /**
         * Closes this stream and the underlying DataOutputStream.
         * @throws IOException if an I/O exception occurred.
         */
        @Override
        public void close() throws IOException {
            dataOutputStream.close();
        }
        
    }

}
