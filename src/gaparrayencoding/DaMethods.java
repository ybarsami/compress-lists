/*
 * The new methods for our paper.
 */
package gaparrayencoding;

import bitlistencoding.BitSequenceMarkovHandler;
import compresslists.CompressLists.BitContextParameters;
import compresslists.CompressLists.ContextParameters;
import compresslists.Tools;

/**
 *
 * @author yann
 */
public class DaMethods {
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods that work directly on the BitVector
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Contextual method on the BitVector.
     */
    public static class MethodBitVectorContext extends MethodByBitVector {
        
        public MethodBitVectorContext(BitContextParameters contextParameters) {
            super(
                    new bitlistencoding.MethodContext(contextParameters));
        }
    }
    public static class MethodBitVectorAdaptiveContext extends MethodByBitVector {
        
        public MethodBitVectorAdaptiveContext(BitContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
            super(
                    new bitlistencoding.MethodAdaptiveContext(contextParameters, nbDocuments, firstGapArraySize, adaptToCurrentDensity));
        }
        
        public final void setParameters(int nbDocuments, int firstGapArraySize) {
            ((bitlistencoding.MethodAdaptiveContext)bMethod).setParameters(nbDocuments, firstGapArraySize);
        }
    }
    /*
     * Only take into account global frequencies of 0 and 1 in the bit vector.
     * Combined with batching, gives a bound on what can be achieved.
     */
    public static class MethodBitVectorIT extends MethodByBitVector {
        
        public MethodBitVectorIT() {
            super(
                    new bitlistencoding.MethodContext(new BitContextParameters(0, 0, 0)));
        }
    }
    /*
     * Contextual method on the BitSequence created by the delta encoding.
     *
     * XXX: For testing purposes only.
     */
    public static class MethodDeltaContext extends MethodByBitComposition {
        
        public MethodDeltaContext(BitContextParameters contextParameters) {
            super(
                    new MethodDelta(),
                    new bitlistencoding.MethodContext(contextParameters));
        }
    }
    
    /*
     * Computations with static probabilities for the whole index or lower bound
     * (on the index size) of the models presented in the following paper:
     * Bookstein, Klein, and Raita, "Modeling Word Occurrences for the Compression of Concordances" (1997)
     */
    public static class MethodBookstein extends MethodByBitVector {
        
        public MethodBookstein(BitSequenceMarkovHandler.Model model, boolean testingLowerBound) {
            super(
                    new bitlistencoding.MethodBookstein(model, testingLowerBound));
        }
        public MethodBookstein(BitSequenceMarkovHandler.Model model) {
            super(
                    new bitlistencoding.MethodBookstein(model));
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods that work directly on the trit list
    ////////////////////////////////////////////////////////////////////////////
    
    public static class MethodTritListContext extends MethodByTritList {
        
        public MethodTritListContext(ContextParameters contextParameters) {
            super(
                    new tritlistencoding.MethodContext(contextParameters));
        }
        
        public final void setParameters(ContextParameters contextParameters) {
            ((tritlistencoding.MethodContext)tMethod).setParameters(contextParameters);
        }
    }
    
    public static class MethodTritConcatenateContext extends MethodByTritConcatenation {
        
        public MethodTritConcatenateContext(ContextParameters contextParameters, int nbDocuments) {
            super(nbDocuments,
                    new tritlistencoding.MethodContext(contextParameters));
        }
        
        public final void setParameters(ContextParameters contextParameters) {
            ((tritlistencoding.MethodContext)tMethod).setParameters(contextParameters);
        }
    }
    
    public static class MethodTritListAdaptiveContext extends MethodByTritList {
        
        public MethodTritListAdaptiveContext(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
            super(
                    new tritlistencoding.MethodAdaptiveContext(contextParameters, nbDocuments, firstGapArraySize, adaptToCurrentDensity));
        }
        
        public final void setParameters(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize) {
            ((tritlistencoding.MethodAdaptiveContext)tMethod).setParameters(contextParameters, nbDocuments, firstGapArraySize);
        }
    }
    
    public static class MethodTritConcatenateAdaptiveContext extends MethodByTritConcatenation {
        
        public MethodTritConcatenateAdaptiveContext(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
            super(nbDocuments,
                    new tritlistencoding.MethodAdaptiveContext(contextParameters, nbDocuments, firstGapArraySize, adaptToCurrentDensity));
        }
        
        public final void setParameters(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize) {
            ((tritlistencoding.MethodAdaptiveContext)tMethod).setParameters(contextParameters, nbDocuments, firstGapArraySize);
        }
    }
    
    public static class MethodQuatritConcatenateContext extends MethodByQuatritConcatenation {
        
        public MethodQuatritConcatenateContext(ContextParameters contextParameters, int nbDocuments) {
            super(nbDocuments,
                    new quatritlistencoding.MethodContext(contextParameters));
        }
        
        public final void setParameters(ContextParameters contextParameters) {
            ((quatritlistencoding.MethodContext)qMethod).setParameters(contextParameters);
        }
    }
    
    public static class MethodQuatritListContext extends MethodByQuatritList {
        
        public MethodQuatritListContext(ContextParameters contextParameters, int nbDocuments) {
            super(nbDocuments,
                    new quatritlistencoding.MethodContext(contextParameters));
        }
        
        public final void setParameters(ContextParameters contextParameters) {
            ((quatritlistencoding.MethodContext)qMethod).setParameters(contextParameters);
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods that work on the trit list separated in B2 / B01.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Two versions of the simplified code of MethodContextBit.
     */
    public static class MethodSplitContextContext extends MethodBySplitB2B01 {
        
        public MethodSplitContextContext(int k, int w, int kInit, int kFinal) {
            super(
                    new bitlistencoding.MethodContext(new BitContextParameters(k, w, kInit)),
                    new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal)));
        }
    }
    public static class MethodSplitContextContext2 extends MethodByTritList {
        
        public MethodSplitContextContext2(int k, int w, int kInit, int kFinal) {
            super(
                    new tritlistencoding.MethodBySplitB2B01(
                        new bitlistencoding.MethodContext(new BitContextParameters(k, w, kInit)),
                        new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal))));
        }
    }
    
    public static class MethodSplitContextInterp extends MethodBySplitB2B01 {
        
        public MethodSplitContextInterp(BitContextParameters contextParameters, int nbDocuments) {
            super(
                    new bitlistencoding.MethodContext(contextParameters),
                    new bitlistencoding.MethodByGapArray(new MethodInterpolative(nbDocuments)));
        }
    }
    
    public static class MethodSplitInterpContext extends MethodBySplitB2B01 {
        
        public MethodSplitInterpContext(int nbDocuments, int kFinal) {
            super(
                    new bitlistencoding.MethodByGapArray(new MethodInterpolative(nbDocuments)),
                    new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal)));
        }
    }
    
    public static class MethodSplitGammaContext extends MethodBySplitB2B01 {
        
        public MethodSplitGammaContext(int kFinal) {
            super(
                    new bitlistencoding.MethodByGapArray(new MethodGamma()),
                    new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal)));
        }
    }
    
    public static class MethodSplitITContext extends MethodBySplitB2B01 {
        
        public MethodSplitITContext(int nbDocuments, int kFinal) {
            super(
                    new bitlistencoding.MethodByGapArray(new MethodInformationTheoretic(Tools.ceilingLog2(nbDocuments))),
                    new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal)));
        }
    }
    
    public static class MethodSplitInterpOptimisticContext extends MethodBySplitB2B01 {
        
        public MethodSplitInterpOptimisticContext(int nbDocuments, int kFinal) {
            super(
                    new bitlistencoding.MethodByGapArray(new MethodInterpolative.MethodDynamicInterpolativeOptimistic(nbDocuments)),
                    new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal)));
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // One level of splitting TL / B01.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Two versions of the simplified code of MethodFContextTritBis.
     */
    public static class MethodTSplitContextContext extends MethodBySplitTLB01 {
        
        public MethodTSplitContextContext(ContextParameters contextParameters, int kFinal) {
            super(
                    new tritlistencoding.MethodContext(contextParameters),
                    new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal)));
        }
    }
    public static class MethodTSplitContextContext2 extends MethodByTritList {
        
        public MethodTSplitContextContext2(ContextParameters contextParameters, int kFinal) {
            super(
                    new tritlistencoding.MethodBySplitTLB01(
                        new tritlistencoding.MethodContext(contextParameters),
                        new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal))));
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Two levels of splitting TL / B01.
    ////////////////////////////////////////////////////////////////////////////
    
    public static class MethodTTSplit extends MethodByTritList {
        
        public MethodTTSplit(ContextParameters contextParameters, int kFinal) {
            super(
                    new tritlistencoding.MethodBySplitTLB01(
                        new tritlistencoding.MethodBySplitTLB01(
                            new tritlistencoding.MethodContext(contextParameters),
                            new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal))),
                        new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal))));
        }
    }
    
    public static class MethodTTSplitInterpOpt extends MethodByTritList {
        
        public MethodTTSplitInterpOpt(int nbDocuments, int kFinal) {
            super(
                    new tritlistencoding.MethodBySplitTLB01(
                        new tritlistencoding.MethodBySplitB2B01(
                            new bitlistencoding.MethodByGapArray(new MethodInterpolative.MethodDynamicInterpolativeOptimistic(nbDocuments)),
                            new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal))),
                        new bitlistencoding.MethodContext(new BitContextParameters(Integer.MAX_VALUE, 0, kFinal))));
        }
    }
}
