/**
 * Index compression with the interpolative method.
 * We here use the refinements presented in Sections 4 and 6.
 *
 * Moffat and Stuiver, "Exploiting Clustering in Inverted File Compression" (1996)
 */

package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;
import io.DatasetReader;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodInterpolative extends MethodByBitSequence {
    
    /*
     * Moffat and Stuiver, "Exploiting Clustering in Inverted File Compression" (1996), pp. 6--7
     * Moreover, the assumption that BC_len(x) = 1 + log_2(x) is pessimistic.
     * One simple heuristic to improve compression is to arrange the binary
     * codes so that some number of codewords are one bit shorter and are
     * allocated to the more likely symbols --- which are those in the middle of
     * the range. For example, in the example inverted list one of the coding
     * steps requires Binary_Code(17,14,20). If the codes for 14 through 20 are
     * allocated as 000,001,010,11,011,100,101 respectively then a two bit code
     * suffices. Of course, there would be no saving were (16,14,20) to be the
     * triple coded; but it is reasonable to suppose that the middle pointer of
     * a set of pointers is more likely to have a value near the “middle” of the
     * given range than it is to be near either of the extremities of the range.
     * In general, when there are r possible values being coded,
     * 2^(ceiling(log_2(r)) - r of the codewords can be floor(log_2(r)) bits
     * rather than ceiling(log_2(r)) bits long. Note that there will always be
     * an even number of longer codewords, so the codeword allocation can always
     * be symmetric.
     */
    private boolean useMinimalBinaryCodes;
    
    /* 
     * Moffat and Stuiver, "Exploiting Clustering in Inverted File Compression" (1996), p. 8
     * The first refinement is easiest to illustrate with an example. Assume
     * that some inverted list contains 10 pointers. The recursive description
     * of Interpolative_Code given earlier first codes the 5th pointer, and then
     * recurses on one list of four pointers and one list of five. These two
     * lists in turn give rise to recursive calls with f equal to 1, 2, 2, and
     * 2. Now suppose instead that the first pointer processed in the original
     * list was the 8th. Then the left recursion is on seven pointers, and leads
     * to a simple recursion pattern as per the example given earlier in Section
     * 3. The right recursion is on two pointers, and is the only two-pointer
     * recursion in the entire process. Because the middle values of each range
     * are favoured by the shorter codewords, two-pointer recursions --- coding
     * a pointer that on average can be expected to lie one third of the way
     * through the range --- are, on average, slightly more expensive for what
     * they achieve than are three- or one-pointer recursions. The same argument
     * applies to four-pointer recursions. Hence minimising the number of
     * “non 2^k - 1” recursions is desirable; and is achieved by always coding
     * the pointer that occupies the largest power of two location in the list,
     * and accepting an early large “non-balanced”) recursion to avoid many
     * small non-balanced recursions later in the process.
     */
    private boolean useBalancedRecursion;
    
    /* 
     * Moffat and Stuiver, "Exploiting Clustering in Inverted File Compression" (1996), p. 9
     * The second refinement arises from the observation that at the base of the
     * sequence of recursive calls, when f = 1, it is no longer appropriate to
     * assume that middle values are more likely. Instead it should be the outer
     * regions of the range that are allocated the shorter codewords, since if
     * there is any clustering the document pointer can be expected to lie close
     * to one or the other of the two bounding pointers; and if there is no
     * clustering then the codeword allocation is immaterial. Hence, when f = 1,
     * we reverse the allocation of codewords, and favour the extremities with
     * the one-bit-shorter codes.
     */
    private boolean useReorderedCodelengths;
    
    protected IntsRef documentArrayRef = new IntsRef(0);
    
    /*
     * The method that is used to encode the maximum value, in case we use the
     * block or dynamic interpolative method.
     */
    protected static final MethodByElement maxValueMethod = new MethodVariableByte();
    
    /**
     * Creates a new instance of MethodInterpolative.
     */
    public MethodInterpolative() {}
    public MethodInterpolative(int nbDocuments, boolean useMinimalBinaryCodes,
            boolean useBalancedRecursion, boolean useReorderedCodelengths) {
        this.nbDocuments = nbDocuments;
        this.useMinimalBinaryCodes = useMinimalBinaryCodes;
        this.useBalancedRecursion = useBalancedRecursion;
        this.useReorderedCodelengths = useReorderedCodelengths;
    }
    public MethodInterpolative(int nbDocuments) {
        this(nbDocuments, true, true, true);
    }
    
    /*
     * To be used when there is no way of capturing a global "nbDocuments" that
     * can be used as a bound on all the integers to encode.
     */
    public static class MethodDynamicInterpolative extends MethodInterpolative {
        
        public MethodDynamicInterpolative(int nbDocuments) {
            super(nbDocuments);
        }
        
        @Override
        public String getName() {
            return "DynamicInterpolative";
        }
        
        @Override
        protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
            return computeSizeBlockDocumentArray(documentArrayRef);
        }
        
        @Override
        protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef) {
            writeDocumentArrayBlock(bitStream, documentArrayRef);
        }
        
        @Override
        protected void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef) {
            readDocumentArrayBlock(bitStream, documentArrayRef);
        }
    }
    
    /*
     * Optimistic bound on what can be achieved if we use the dynamic
     * interpolative method --- as if the maxValueMethod was always giving an
     * optimal encoding.
     * WARNING: If used to produce an index, will produce the standard
     * interpolative index, as it is not a real method..
     */
    public static class MethodDynamicInterpolativeOptimistic extends MethodInterpolative {
        
        public MethodDynamicInterpolativeOptimistic(int nbDocuments) {
            super(nbDocuments);
        }
        
        @Override
        public String getName() {
            return "DynamicInterpolativeOptimistic";
        }
        
        @Override
        protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
            return computeSizeInterpolative(documentArrayRef, documentArrayRef.ints[documentArrayRef.length - 1]);
        }
    }
    
    @Override
    public String getName() {
        return "Interpolative";
    }
    
    /*
     * As an optimization when we use a DatasetReader that stores document
     * arrays instead of gap arrays, we do not read a document array, transform
     * it into a gap array, then transform it back into a document array...
     * (this is essentially a copy / paste from Method.java)
     * TODO: create specific classes for methods that work on document arrays.
     */
    @Override
    public double computeSize(DatasetReader datasetReader) {
        computeSizeBefore();
        double size = computeSizeNeededInformation();
        datasetReader.restartReader();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            size += computeSizeDocumentArray(datasetReader.getNextDocumentArray());
        }
        size += computeSizeAfter();
        return size;
    }
    
    @Override
    public final double computeSize(IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        return computeSizeDocumentArray(documentArrayRef);
    }
    protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
        return computeSizeInterpolative(documentArrayRef, nbDocuments,
                    useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
    }
    
    private double sizeMaxValue;
    private double sizeGaps;
    @Override
    public void computeSizeBefore() {
        sizeMaxValue = 0.;
        sizeGaps = 0.;
    }
    /*
     * There is an optimization when working block by block. We need to store
     * the maximum value for decoding.
     */
    @Override
    public final double computeSizeBlock(IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        return computeSizeBlockDocumentArray(documentArrayRef);
    }
    protected double computeSizeBlockDocumentArray(IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        int maxValue = documentArrayRef.ints[nbDocumentsLocal - 1];
        // We know that maxValue is always >= length, so as an optimization to
        // take less memory, we write maxValue - (length - 1). It will be
        // possible to retrieve the value because the length is known at
        // decoding time.
        double sizeMaxValueLocal = maxValueMethod.computeSize(maxValue - (nbDocumentsLocal - 1));
        documentArrayRef.length--;
        double sizeGapsLocal = computeSizeInterpolative(documentArrayRef, maxValue - 1,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
        documentArrayRef.length++;
        sizeMaxValue += sizeMaxValueLocal;
        sizeGaps += sizeGapsLocal;
        return sizeMaxValueLocal + sizeGapsLocal;
    }
    
    @Override
    public void standardOutput() {
        if (sizeMaxValue > 0) {
            System.out.print(" (maxValues: " + (sizeMaxValue / 8000000) +
                    " ; gaps: " + (sizeGaps / 8000000) +
                    ")");
        }
    }
    
    public static int computeSizeInterpolative(IntsRef documentArrayRef, int nbDocuments) {
        return computeSizeInterpolative(documentArrayRef, nbDocuments, true, true, true);
    }
    public static int computeSizeInterpolative(IntsRef documentArrayRef, int nbDocuments,
            boolean useMinimalBinaryCodes, boolean useBalancedRecursion, boolean useReorderedCodelengths) {
        return computeSizeInterpolative(documentArrayRef.ints, documentArrayRef.length, 1, nbDocuments,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        writeDocumentArray(bitStream, documentArrayRef);
    }
    protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef) {
        writeListCodeInterpolative(bitStream, documentArrayRef.ints, documentArrayRef.length, 1, nbDocuments,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
    }
    /*
     * There is an optimization when working block by block. We need to store
     * the maximum value for decoding.
     */
    @Override
    public final void writeGapArrayBlock(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.gapArray2documentArray(gapArrayRef, documentArrayRef);
        writeDocumentArrayBlock(bitStream, documentArrayRef);
    }
    protected void writeDocumentArrayBlock(BitOutputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        int maxValue = documentArrayRef.ints[nbDocumentsLocal - 1];
        // We know that maxValue is always >= length, so as an optimization to
        // take less memory, we write maxValue - (length - 1). It will be
        // possible to retrieve the value because the length is known at
        // decoding time.
        maxValueMethod.writeCode(maxValue - (nbDocumentsLocal - 1), bitStream);
        writeListCodeInterpolative(bitStream, documentArrayRef.ints, nbDocumentsLocal - 1, 1, maxValue - 1,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
    }
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        readDocumentArray(bitStream, gapArrayRef);
        Tools.documentArray2gapArray(gapArrayRef, gapArrayRef);
    }
    protected void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        ArrayList<Integer> documentList = new ArrayList<>();
        readCodeListInterpolative(bitStream, documentList, nbDocumentsLocal, 1, nbDocuments,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
        // When reading, we do not need extra memory, we can work in-place on
        // gapArray.
        for (int idDoc = 0; idDoc < nbDocumentsLocal; idDoc++) {
            documentArrayRef.ints[idDoc] = documentList.get(idDoc);
        }
    }
    
    /*
     * There is an optimization when working block by block. We need to retrieve
     * the maximum value for decoding.
     */
    @Override
    public final void readGapArrayBlock(BitInputStream bitStream, IntsRef gapArrayRef) {
        readDocumentArrayBlock(bitStream, gapArrayRef);
        Tools.documentArray2gapArray(gapArrayRef, gapArrayRef);
    }
    protected void readDocumentArrayBlock(BitInputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        // Do not forget to add (length - 1) to retrieve maxValue.
        int maxValue = maxValueMethod.readCode(bitStream) + nbDocumentsLocal - 1;
        ArrayList<Integer> documentList = new ArrayList<>();
        readCodeListInterpolative(bitStream, documentList, nbDocumentsLocal - 1, 1, maxValue - 1,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
        // When reading, we do not need extra memory, we can work in-place on
        // gapArray. We retrieve the (length - 1) first gaps.
        for (int idDoc = 0; idDoc < nbDocumentsLocal - 1; idDoc++) {
            documentArrayRef.ints[idDoc] = documentList.get(idDoc);
        }
        // The last document is maxValue.
        documentArrayRef.ints[nbDocumentsLocal - 1] = maxValue;
    }
    
    /*
     * Moffat and Stuiver, "Exploiting Clustering in Inverted File Compression" (1996), p. 5
     * The process of calculating ranges and codes is captured by the following
     * pseudo-code. Function Binary_Code(x, lo, hi) is assumed to encode a
     * number lo <= x <= hi in some appropriate manner. The simplest mechanism
     * for doing this (as assumed above) requires ceiling(log_2(hi - lo + 1))
     * bits. Other mechanisms are also possible, and are discussed below. The
     * operation “+” in step 5 denotes concatenation of codewords.
     *
     * Interpolative_Code(L, f, lo, hi)
     *     1. Let L[O... (f - 1)] be a sorted list of f document numbers, all
     *        in the range lo... hi.
     *     2. If f = 0 then return the empty string.
     *     3. If f = 1 then return Binary_Code(L[O], lo, hi).
     *     4. Otherwise, calculate
     *        (a) h <-- f div 2.
     *        (b) m <-- L[h].
     *        (c) L1 <-- L[O... (h - 1)].
     *        (d) L2 <-- L[(h + 1)... (f - 1)].
     *     5. Return Binary_Code(m, lo + h, hi - (f - h - 1)) +
     *               Interpolative_Code(L1, h, lo, m - 1) +
     *               Interpolative_Code(L2, f - h - 1, m + 1, hi).
     */
    public static void writeListCodeInterpolative(BitOutputStream bitStream,
            int[] documentArray, int f, int lo, int hi) {
        writeListCodeInterpolative(bitStream, documentArray, f, lo, hi, true, true, true);
    }
    public static void writeListCodeInterpolative(BitOutputStream bitStream,
            int[] documentArray, int f, int lo, int hi,
            boolean useMinimalBinaryCodes, boolean useBalancedRecursion, boolean useReorderedCodelengths) {
        switch (f) {
            case 0:
                return;
            case 1:
                if (useMinimalBinaryCodes) {
                    if (useReorderedCodelengths) {
                        BinaryEncoding.writeCodeBinaryOuterShorter(documentArray[0], bitStream, lo, hi);
                    } else {
                        BinaryEncoding.writeCodeBinaryInnerShorter(documentArray[0], bitStream, lo, hi);
                    }
                } else {
                    BinaryEncoding.writeCodeBinaryNaive(documentArray[0], bitStream, lo, hi);
                }
                return;
            default:
                int h = useBalancedRecursion ? Tools.pow(2, Tools.ilog2(f)) - 1 : f / 2;
                int m = documentArray[h];
                // fromIndex is inclusive, toIndex is exclusive.
                int[] L1 = Arrays.copyOfRange(documentArray, 0, h);
                int[] L2 = Arrays.copyOfRange(documentArray, h + 1, f);
                if (useMinimalBinaryCodes) {
                    BinaryEncoding.writeCodeBinaryInnerShorter(m, bitStream, lo + h, hi - (f - h - 1));
                } else {
                    BinaryEncoding.writeCodeBinaryNaive(m, bitStream, lo + h, hi - (f - h - 1));
                }
                writeListCodeInterpolative(bitStream, L1, h, lo, m - 1,
                        useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
                writeListCodeInterpolative(bitStream, L2, f - h - 1, m + 1, hi,
                        useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
                break;
        }
    }
    private static int computeSizeInterpolative(int[] documentArray, int f, int lo, int hi,
            boolean useMinimalBinaryCodes, boolean useBalancedRecursion, boolean useReorderedCodelengths) {
        switch (f) {
            case 0:
                return 0;
            case 1:
                if (useMinimalBinaryCodes) {
                    if (useReorderedCodelengths) {
                        return BinaryEncoding.computeSizeCodeBinaryOuterShorter(documentArray[0], lo, hi);
                    } else {
                        return BinaryEncoding.computeSizeCodeBinaryInnerShorter(documentArray[0], lo, hi);
                    }
                } else {
                    return BinaryEncoding.computeSizeCodeBinaryNaive(documentArray[0], lo, hi);
                }
            default:
                int h = useBalancedRecursion ? Tools.pow(2, Tools.ilog2(f)) - 1 : f / 2;
                int m = documentArray[h];
                // fromIndex is inclusive, toIndex is exclusive.
                int[] L1 = Arrays.copyOfRange(documentArray, 0, h);
                int[] L2 = Arrays.copyOfRange(documentArray, h + 1, f);
                int size = 0;
                if (useMinimalBinaryCodes) {
                    size += BinaryEncoding.computeSizeCodeBinaryInnerShorter(m, lo + h, hi - (f - h - 1));
                } else {
                    size += BinaryEncoding.computeSizeCodeBinaryNaive(m, lo + h, hi - (f - h - 1));
                }
                size += computeSizeInterpolative(L1, h, lo, m - 1,
                        useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
                size += computeSizeInterpolative(L2, f - h - 1, m + 1, hi,
                        useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
                return size;
        }
    }
    
    private static void readCodeListInterpolative(BitInputStream bitInputStream,
            ArrayList<Integer> documentList, int f, int lo, int hi,
            boolean useMinimalBinaryCodes, boolean useBalancedRecursion, boolean useReorderedCodelengths) {
        switch (f) {
            case 0:
                return;
            case 1:
                documentList.add(useMinimalBinaryCodes
                        ? useReorderedCodelengths
                            ? BinaryEncoding.readCodeBinaryOuterShorter(bitInputStream, lo, hi)
                            : BinaryEncoding.readCodeBinaryInnerShorter(bitInputStream, lo, hi)
                        : BinaryEncoding.readCodeBinaryNaive(bitInputStream, lo, hi));
                break;
            default:
                int h = useBalancedRecursion ? Tools.pow(2, Tools.ilog2(f)) - 1 : f / 2;
                int m = useMinimalBinaryCodes
                        ? BinaryEncoding.readCodeBinaryInnerShorter(bitInputStream, lo + h, hi - (f - h - 1))
                        : BinaryEncoding.readCodeBinaryNaive(bitInputStream, lo + h, hi - (f - h - 1));
                readCodeListInterpolative(bitInputStream, documentList, h, lo, m - 1,
                        useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
                documentList.add(m);
                readCodeListInterpolative(bitInputStream, documentList, f - h - 1, m + 1, hi,
                        useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
                break;
        }
    }
    
    public static final ArrayList<Integer> readCodeListInterpolative(BitInputStream bitInputStream,
            int f, int lo, int hi) {
        return readCodeListInterpolative(bitInputStream, f, lo, hi, true, true, true);
    }
    public static final ArrayList<Integer> readCodeListInterpolative(BitInputStream bitInputStream,
            int f, int lo, int hi,
            boolean useMinimalBinaryCodes, boolean useBalancedRecursion, boolean useReorderedCodelengths) {
        ArrayList<Integer> documentList = new ArrayList<>();
        readCodeListInterpolative(bitInputStream, documentList, f, lo, hi,
                useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths);
        return documentList;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 3 * 8; // useMinimalBinaryCodes, useBalancedRecursion, useReorderedCodelengths
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeBoolean(useMinimalBinaryCodes);
        out.writeBoolean(useBalancedRecursion);
        out.writeBoolean(useReorderedCodelengths);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        useMinimalBinaryCodes = in.readBoolean();
        useBalancedRecursion = in.readBoolean();
        useReorderedCodelengths = in.readBoolean();
    }

}
