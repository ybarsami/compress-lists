/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaparrayencoding;

import arrays.IntsRef;
import io.BitInputStreamFile;
import io.BitOutputStreamFile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodGolombLocal extends Method {
    
    // Converts gapArray lengths to parameters.
    private int[] length2parameter;
    
    /**
     * Creates a new instance of MethodGolombLocal.
     */
    public MethodGolombLocal() {}
    public MethodGolombLocal(int nbDocuments) {
        this.nbDocuments = nbDocuments;
        length2parameter = new int[nbDocuments + 1];
    }
    
    @Override
    public String getName() {
        return "GolombLocal";
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int parameter = length2parameter[nbGapsLocal];
        if (parameter == 0) {
            double p = (double)nbGapsLocal / (double)nbDocuments;
            int b = MethodGolombGlobal.golombParameterFromFrequency(p);
            length2parameter[nbGapsLocal] = b;
        }
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int b = length2parameter[nbGapsLocal];
        
        double nbBits = 0;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            nbBits += MethodGolombGlobal.computeSizeGolomb(gapArray[offset + idGap], b);
        }
        return nbBits;
    }
    
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int b = length2parameter[nbGapsLocal];
        
        try (BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(dataOutputStream)) {
            for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
                MethodGolombGlobal.writeCodeGolomb(gapArray[offset + idGap], bitOutputStream, b);
            }
        }
    }
    
    @Override
    public void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int b = length2parameter[nbGapsLocal];
        
        BitInputStreamFile bitInputStream = new BitInputStreamFile(dataInputStream);
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            // Extract a gap.
            int gap = MethodGolombGlobal.readCodeGolomb(bitInputStream, b);
            // Add the gap to the gap array.
            gapArrayRef.ints[idGap] = gap;
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        length2parameter = new int[nbDocuments + 1];
    }
    
}
