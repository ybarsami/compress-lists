/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.DatasetReader;
import io.DatasetReaderBinary;
import tritlistencoding.TritSequence;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodByTritConcatenation extends MethodByBitSequence {
    
    protected tritlistencoding.Method tMethod;
    
    private TritSequence tritSequence = new TritSequence();
    private TritSequence concatenatedSequence = new TritSequence();
    
    /**
     * Creates a new instance of MethodByTritConcatenation.
     */
    public MethodByTritConcatenation() {}
    public MethodByTritConcatenation(int nbDocuments, tritlistencoding.Method tMethod) {
        this.nbDocuments = nbDocuments;
        this.tMethod = tMethod;
    }
    
    /*
     * Small modification from Tools.intArray2tritSequence: here, we add
     * nbDocuments to the first element, to mark the break from the previous
     * tritSequence in the concatenation.
     */
    private void intArray2tritSequence(IntsRef intArrayRef, TritSequence tritSequence) {
        int[] intArray = intArrayRef.ints;
        int length = intArrayRef.length;
        int offset = intArrayRef.offset;
        tritSequence.reset();
        MethodTrits.writeCodeTrits(intArray[offset] + nbDocuments, tritSequence);
        for (int i = 1; i < length; i++) {
            MethodTrits.writeCodeTrits(intArray[offset + i], tritSequence);
        }
    }
    
    /*
     * Further optimization: here, we override init(IntsRef), so needsInit()
     * from Method.java will return true. Here, we return false if the tMethod
     * does not override init(TritSequence).
     */
    @Override
    public boolean needsInit() {
        try {
            return !tMethod.getClass().getMethod("init", TritSequence.class).getDeclaringClass().equals(tritlistencoding.Method.class);
        } catch (NoSuchMethodException e) {
            return true;
        }
    }
    
    @Override
    public void initBefore() {
        tMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        intArray2tritSequence(gapArrayRef, tritSequence);
        try {
            concatenatedSequence.concatenate(tritSequence);
        } catch (IndexOutOfBoundsException e) {
            tMethod.init(concatenatedSequence);
            concatenatedSequence = tritSequence;
        }
    }
    
    @Override
    public void initAfter() {
        tMethod.init(concatenatedSequence);
        concatenatedSequence.reset();
        tMethod.initAfter();
    }
    
    @Override
    public double computeSizeLengths(DatasetReader datasetReader, int nbDocuments) {
        datasetReader.restartReader();
        int nbTritSequenceConcatenated = 0;
        int currentSize = 0;
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            intArray2tritSequence(gapArrayRef, tritSequence);
            int tritSequenceSize = tritSequence.size();
            try {
                currentSize = Math.addExact(currentSize, tritSequenceSize);
            } catch (IndexOutOfBoundsException e) {
                nbTritSequenceConcatenated++;
                currentSize = tritSequenceSize;
            }
        }
        nbTritSequenceConcatenated++;
        return 32. + // nbDocuments
                32. * (1. + 2. * nbTritSequenceConcatenated); // gapArrayConcatenatedSizes + nbGapsConcatenated
    }
    
    @Override
    public void computeSizeBefore() {
        tMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        intArray2tritSequence(gapArrayRef, tritSequence);
        try {
            concatenatedSequence.concatenate(tritSequence);
            return 0.;
        } catch (IndexOutOfBoundsException e) {
            double size = tMethod.computeSize(concatenatedSequence);
            concatenatedSequence = tritSequence;
            return size;
        }
    }
    
    @Override
    public double computeSizeAfter() {
        double size = tMethod.computeSize(concatenatedSequence);
        concatenatedSequence = new TritSequence();
        return size + tMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        tMethod.standardOutput();
    }
    
    @Override
    public String getName() {
        return "ByTritConcatenation(" + tMethod.getName() + ")";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        tMethod.writeTritSequence(bitStream, concatenatedSequence);
        concatenatedSequence = new TritSequence();
        tMethod.outputRemainingBits(bitStream);
    }
    
    @Override
    public final void flushRemainingBits() {
        tMethod.flushRemainingBits();
    }
    
    @Override
    public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        intArray2tritSequence(gapArrayRef, tritSequence);
        try {
            concatenatedSequence.concatenate(tritSequence);
        } catch (IndexOutOfBoundsException e) {
            tMethod.writeTritSequence(bitStream, concatenatedSequence);
            concatenatedSequence = tritSequence;
        }
    }
    
    private int nbGapArraysRead = 0;
    private int nbGapsConcatenatedRead = 0;
    private int indexStartNextGapArray = 0;
    private IntsRef gapArrayConcatenatedRef = new IntsRef(0);
    
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int gapArrayConcatenatedSize = gapArrayRef.length;
        // The first time we call readGapArray, we need to read a concatenated
        // list of gapArrays.
        if (nbGapArraysRead == 0) {
            tMethod.readTritSequence(bitStream, gapArrayConcatenatedSize, concatenatedSequence);
            Tools.tritSequence2intArray(concatenatedSequence, gapArrayConcatenatedRef);
        }
        // We extract a gapArray from the concatenation.
        int[] gapArrayConcatenated = gapArrayConcatenatedRef.ints;
        gapArrayRef.ints[0] = gapArrayConcatenated[indexStartNextGapArray] - nbDocuments;
        int indexStopGapArray = 1;
        indexStartNextGapArray++;
        while (indexStartNextGapArray < gapArrayConcatenatedSize && gapArrayConcatenated[indexStartNextGapArray] < nbDocuments) {
            gapArrayRef.ints[indexStopGapArray] = gapArrayConcatenated[indexStartNextGapArray];
            indexStartNextGapArray++;
            indexStopGapArray++;
        }
        nbGapArraysRead++;
        nbGapsConcatenatedRead += indexStopGapArray;
        // When we finished the concatenation, we reset everything.
        if (nbGapsConcatenatedRead == gapArrayConcatenatedSize) {
            nbGapArraysRead = 0;
            nbGapsConcatenatedRead = 0;
            indexStartNextGapArray = 0;
            gapArrayConcatenatedRef = new IntsRef(0);
        }
        gapArrayRef.length = indexStopGapArray;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return MethodByTritList.computeSizeTritMethod(tMethod);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        MethodByTritList.exportTritMethod(tMethod, out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        tMethod = MethodByTritList.importTritMethod(in);
    }
    
    @Override
    public void exportLengthsNeededToUncompress(DatasetReader datasetReader, DataOutputStream out) throws IOException {
        datasetReader.restartReader();
        int currentTritSequenceSize = 0;
        int currentGapArraySize = 0;
        int currentNbGapArrays = 0;
        int nbTritSequenceConcatenated = 0;
        ArrayList<Integer> toBeWritten = new ArrayList<>();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            int nbGaps = gapArrayRef.length;
            intArray2tritSequence(gapArrayRef, tritSequence);
            int tritSequenceSize = tritSequence.size();
            try {
                currentTritSequenceSize = Math.addExact(currentTritSequenceSize, tritSequenceSize);
                currentGapArraySize += nbGaps;
                currentNbGapArrays++;
            } catch (IndexOutOfBoundsException e) {
                nbTritSequenceConcatenated++;
                toBeWritten.add(currentGapArraySize);
                toBeWritten.add(currentNbGapArrays);
                currentTritSequenceSize = tritSequenceSize;
                currentGapArraySize = nbGaps;
                currentNbGapArrays = 1;
            }
        }
        nbTritSequenceConcatenated++;
        toBeWritten.add(currentGapArraySize);
        toBeWritten.add(currentNbGapArrays);
        out.writeInt(nbTritSequenceConcatenated);
        for (int i : toBeWritten) {
            out.writeInt(i);
        }
    }
    
    @Override
    public ArrayList<Integer> importLengthsNeededToUncompress(DataInputStream in) throws IOException {
        int nbTritSequenceConcatenated = in.readInt();
        ArrayList<Integer> gapArrayConcatenatedSizes = new ArrayList<>();
        for (int i = 0; i < nbTritSequenceConcatenated; i++) {
            int gapArrayConcatenatedSize = in.readInt();
            int nbGapArraysLocal = in.readInt();
            for (int j = 0; j < nbGapArraysLocal; j++) {
                // It is necessary that when reading a gapArray, we know how
                // much gapArrays were concatenated.
                gapArrayConcatenatedSizes.add(gapArrayConcatenatedSize);
            }
        }
        return gapArrayConcatenatedSizes;
    }
    
    // TODO: this is mostly a copy/paste from MethodByBitSequence.java.
    // Find a way to factorize the code.
    @Override
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            private BitInputStreamFile bitInputStream;
            private ArrayList<Integer> gapArrayConcatenatedSizes;
            
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
                bitInputStream = new BitInputStreamFile(dataInputStream);
                intArrayRef.ints = new int[nbDocuments];
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                intArrayRef.length = gapArrayConcatenatedSizes.get(nbIntArraysRead);
                readGapArray(bitInputStream, intArrayRef);
                assert(intArrayRef.length == gapArrayLengths.get(nbIntArraysRead));
                if (nbIntArraysRead == nbWords - 1) {
                    flushRemainingBits();
                    bitInputStream.close();
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream);
                        BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    gapArrayConcatenatedSizes = importLengthsNeededToUncompress(in);
                    nbWords = gapArrayConcatenatedSizes.size();
                    intArrayRef.ints = new int[nbDocuments];
                    for (int gapArrayConcatenatedSize : gapArrayConcatenatedSizes) {
                        intArrayRef.length = gapArrayConcatenatedSize;
                        readGapArray(bitStream, intArrayRef);
                        updateStatisticsWithNewGapArray(intArrayRef.length);
                    }
                    flushRemainingBits();
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
}
