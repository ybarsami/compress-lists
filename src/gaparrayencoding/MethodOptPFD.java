/**
 * Index compression with the OptPFD method.
 * 
 * Yan, Ding, and Suel, "Inverted Index Compression and Query Processing with Optimized Document Ordering" (2009)
 * 
 * Implementation from the java library JavaFastPFOR.
 * https://github.com/lemire/JavaFastPFOR
 */

package gaparrayencoding;

import me.lemire.integercompression.OptPFD;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.VariableByte;

/**
 *
 * @author yann
 */
public class MethodOptPFD extends MethodByJavaFastPFOR {
    
    /**
     * Creates a new instance of MethodOptPFD.
     */
    public MethodOptPFD() {
        // It uses a block size of 128 integers.
        super(new Composition(new OptPFD(), new VariableByte()));
    }
    
}
