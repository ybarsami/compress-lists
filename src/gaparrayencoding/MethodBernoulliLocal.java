/*
 * Index compression with the local Bernoulli method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 121
 */
package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import static gaparrayencoding.MethodBernoulliGlobal.bernoulliProbabilities;

/**
 *
 * @author yann
 */
public class MethodBernoulliLocal extends MethodByArithmeticCoding {
    
    // Size of the occurrence arrays.
    private int arraySize;
    
    // Occurrence arrays.
    private double[][] probabilities;
    private long[] occurrences;
    private int[] occurrencesCumulative;
    
    /**
     * Creates a new instance of MethodBernoulliLocal.
     */
    public MethodBernoulliLocal(int nbDocuments) {
        this.nbDocuments = nbDocuments;
        allocateOccurrences();
    }
    
    private void allocateOccurrences() {
        arraySize = nbDocuments + 1;
        probabilities = new double[arraySize][];
        occurrences = new long[arraySize];
	occurrencesCumulative = new int[arraySize + 1];
    }
    
    @Override
    public String getName() {
        return "BernoulliLocal";
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        // Only allocate useful parts of the array. One could alternatively
        // allocate the full array and avoid the tests...
        if (probabilities[nbGapsLocal] == null) {
            double p = (double)nbGapsLocal / (double)nbDocuments;
            // Compute the geometric distribution.
            probabilities[nbGapsLocal] = bernoulliProbabilities(p, nbDocuments);
        }
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        
        // Convert the probabilities to occurrences.
        OccurrenceTools.convertProbasToOccurrences(probabilities[nbGapsLocal], occurrences);
        
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Do the encoding.
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            counter.update(occurrencesCumulative, gapArrayRef.ints[offset + idGap]);
        }
        return bitCounter.getNbBitsWrittenAndReset();
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        
        // Convert the probabilities to occurrences.
        OccurrenceTools.convertProbasToOccurrences(probabilities[nbGapsLocal], occurrences);
        
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Do the encoding.
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            encoder.update(occurrencesCumulative, gapArrayRef.ints[offset + idGap]);
        }
    }
    
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        
        // Convert the probabilities to occurrences.
        OccurrenceTools.convertProbasToOccurrences(probabilities[nbGapsLocal], occurrences);
        
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Do the decoding.
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            int gap = decoder.read(occurrencesCumulative);
            gapArrayRef.ints[idGap] = gap;
        }
    }
    
}
