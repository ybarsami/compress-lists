/**
 * Index compression with the FastPFOR method.
 * 
 * Lemire and Boytsov, "Decoding billions of integers per second through vectorization" (2013)
 * 
 * Implementation from the java library JavaFastPFOR.
 * https://github.com/lemire/JavaFastPFOR
 */

package gaparrayencoding;

import me.lemire.integercompression.FastPFOR128;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.VariableByte;

/**
 *
 * @author yann
 */
public class MethodFastPFOR extends MethodByJavaFastPFOR {
    
    /**
     * Creates a new instance of MethodFastPFOR.
     */
    public MethodFastPFOR() {
        // It uses a block size of 128 integers (instead of the 256 block size
        // of FastPFOR, because we expect our arrays to be smaller).
        super(new Composition(new FastPFOR128(), new VariableByte()));
    }

}
