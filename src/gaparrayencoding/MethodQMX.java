/*
 * Index compression with the QMX method.
 * It is essentially the same idea than the Simple-9 and Simple8b methods, on 128-bit words.
 *
 * Trotman, "Compression, SIMD, and Postings Lists" (2014)
 *
 * A version of BinPacking where we pack into a 128-bit integer the following:
 *  256  0-bit words
 *  128  1-bit words 
 *   64	 2-bit words
 *   40  3-bit words
 *   32  4-bit words
 *   24  5-bit words
 *   20  6-bit words
 *   16  8-bit words
 *   12 10-bit words
 *    8 16-bit words
 *    4 32-bit words
 *  or pack into two 128-bit words (i.e. 256 bits) the following:
 *   36  7-bit words
 *   28  9-bit words
 *   20 12-bit words
 *   12 21-bit words
 *
 * This gives us 15 possible combinations. The combination is stored in the top
 * 4 bits of a selector byte. The bottom 4 bits of the selector store a
 * run-length (the number of such sequences seen in a row).
 *
 * The selectors are stored first, then the 128-bit (or 256-bit) packed binary
 * values --- this is different from the original implementation.
 *
 * NOTE: this is a Java implementation, of the CODEC that can be found on:
 * http://www.cs.otago.ac.nz/homepages/andrew/papers/QMX.zip
 * We wrote this Java implementation from scratch without thinking about time
 * efficiency, we are only interested in the index size.
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.DatasetReader;
import io.DatasetReaderBinary;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodQMX extends Method {
    
    // In the inverted index file, selectors are exported and imported as bytes.
    // However, to ease the use of selectors that should be unsigned, we treat
    // them as Integers (it does not change the memory consumption anyway).
    private ArrayList<Integer> selectors;
    private final int nbBitsForRunLength = 4;
    private final int maxRunLength = 1 << nbBitsForRunLength;
    
    private final BigInteger TWO_TO_64 = BigInteger.ONE.shiftLeft(64);
    
    /**
     * Creates a new instance of MethodQMX.
     */
    public MethodQMX() {}
    
    @Override
    public String getName() {
        return "QMX";
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        selectors = new ArrayList<>();
        binaryPacking = new ArrayList<>();
        treatGapArray(gapArrayRef);
        treatRemainingInts();
        return 8 * selectors.size() + 64 * binaryPacking.size();
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the byte array + long array from the gapArrays
    // Adapted from Simple9 by D. Lemire.
    // https://github.com/lemire/JavaFastPFOR/blob/master/src/main/java/me/lemire/integercompression/Simple9.java
    ////////////////////////////////////////////////////////////////////////////
    
    private int[] intsToWrite = new int[codeNum[firstNonExactCombinationValue]];
    private int nbIntsToWrite = 0;
    private ArrayList<Long> binaryPacking;
    
    private void treatRemainingInts() {
        if (nbIntsToWrite == 0) {
            return;
        }
        // mainloop handles the case where it is possible to choose the
        // combination in [1; maxCombinationValue].
        // Here, it is not possible that we need to use the first combination
        // value.
        mainloop: for (int combination = firstNonExactCombinationValue; combination <= maxCombinationValue; combination++) {
            // All the remaining ints will be treated in only one long.
            int compressedNum = nbIntsToWrite;
            // We check that all numbers are within the range of the combination.
            BigInteger res = BigInteger.ZERO;
            int b = bitLength[combination];
            for (int i = 0; i < compressedNum; i++) {
                long numberToEncode = (long)intsToWrite[i] - 1L;
                if (numberToEncode >= maxValue[combination])
                    continue mainloop;
                res = res.shiftLeft(b);
                res = res.add(BigInteger.valueOf(numberToEncode));
            }
            // Padding with 0s at the end is necessary.
            res = res.shiftLeft((codeNum[combination] - compressedNum) * b);
            // Adding the combination.
            addCombination(combination);
            // Adding 2 or 4 longs, depending on the combination.
            addLongs(res, combination);
            nbIntsToWrite = 0;
            return;
        }
        // It is not possible to encode values >= 2^32 (32 = bitLength[14]),
        // but it is not a problem because gapArray is an int array.
        assert(nbIntsToWrite == 0);
    }
    
    private void addCombination(int combination) {
        if (selectors.isEmpty()) {
            selectors.add(combination << nbBitsForRunLength);
        } else {
            int lastPosition = selectors.size() - 1;
            int lastSelector = selectors.get(lastPosition);
            int lastCombination = lastSelector >>> nbBitsForRunLength;
            if (lastCombination == combination) {
                if (Integer.remainderUnsigned(lastSelector, maxRunLength) == maxRunLength - 1) {
                    selectors.add(combination << nbBitsForRunLength);
                } else {
                    selectors.set(lastPosition, lastSelector + 1);
                }
            } else {
                selectors.add(combination << nbBitsForRunLength);
            }
        }
    }
    
    private void addLongs(BigInteger res, int combination) {
        int nbLongValues = (combination == 0)
                ? 0
                : (combination == 7 || combination == 9 || combination == 11 || combination == 13)
                ? 4
                : 2;
        for (int i = 0; i < nbLongValues; i++) {
            long myLong = res.remainder(TWO_TO_64).longValue();
            binaryPacking.add(myLong);
            res = res.shiftRight(64);
        }
    }
    
    private void treatGapArray(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        // Merge the current gapArray with the gaps still in cache.
        final int nbGapsToWrite = nbIntsToWrite + nbGapsLocal;
        final int[] newGapArray;
        if (nbIntsToWrite > 0) {
            newGapArray = new int[nbGapsToWrite];
            System.arraycopy(intsToWrite, 0, newGapArray, 0,             nbIntsToWrite);
            System.arraycopy(gapArray,    0, newGapArray, nbIntsToWrite, nbGapsLocal);
        } else {
            newGapArray = gapArray;
        }
        try {
            int nbGapsWritten = 0;
            outer: while (nbGapsWritten < nbGapsToWrite) {
                // mainloop handles the case where it is possible to choose the
                // combination in [0; maxCombinationValue].
                mainloop: for (int combination = 0; combination <= maxCombinationValue; combination++) {
                    int compressedNum = Math.min(nbGapsToWrite - nbGapsWritten, codeNum[combination]);
                    // For combination 0, we must enforce *exactly*
                    // codeNum[combination] values.
                    if (combination == 0 && compressedNum != codeNum[combination]) {
                        continue;
                    }
                    // For other combinations, we check that all numbers are within
                    // the range.
                    BigInteger res = BigInteger.ZERO;
                    int b = bitLength[combination];
                    for (int i = 0; i < compressedNum; i++) {
                        long numberToEncode = (long)newGapArray[nbGapsWritten + i] - 1L;
                        if (numberToEncode >= maxValue[combination])
                            continue mainloop;
                        res = res.shiftLeft(b);
                        res = res.add(BigInteger.valueOf(numberToEncode));
                    }
                    // If there are less numbers than can be accomodated, put
                    // them in cache for further treatment.
                    if (compressedNum != codeNum[combination]) {
                        for (int i = 0; i < compressedNum; i++) {
                            intsToWrite[i] = newGapArray[nbGapsWritten + i];
                        }
                        nbIntsToWrite = compressedNum;
                        return;
                    }
                    // Adding the combination.
                    addCombination(combination);
                    // Adding 0, 2 or 4 longs, depending on the combination.
                    addLongs(res, combination);
                    nbGapsWritten += compressedNum;
                    nbIntsToWrite = 0;
                    continue outer;
                }
                // It is not possible to encode values >= 2^32 (32 = bitLength[14]),
                // but it is not a problem because gapArray is an int array.
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
    
    private void exportData(DataOutputStream dataOutputStream) throws IOException {
        for (int selector : selectors) {
            dataOutputStream.writeByte((byte)selector);
        }
        for (long packing : binaryPacking) {
            dataOutputStream.writeLong(packing);
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the gapArrays from the byte array + long array
    // Adapted from Simple9 by D. Lemire.
    // https://github.com/lemire/JavaFastPFOR/blob/master/src/main/java/me/lemire/integercompression/Simple9.java
    ////////////////////////////////////////////////////////////////////////////
    
    private int[] intsRead = new int[codeNum[0]];
    private int nbIntsRead = 0;
    
    private int readCombination() {
        if (selectors.isEmpty()) {
            throw new RuntimeException("Trying to decode too many integers from this data stream.");
        } else {
            int nextSelector = selectors.get(0);
            int nextCombination = nextSelector >>> nbBitsForRunLength;
            if (Integer.remainderUnsigned(nextSelector, maxRunLength) == 0) {
                selectors.remove(0);
            } else {
                selectors.set(0, nextSelector - 1);
            }
            return nextCombination;
        }
    }
    
    private void importGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        int nbGapsLocal = gapArrayRef.length;
        int nbGapsRead = 0;
        // First, read the gaps previously in cache.
        int nbGapsToReadFromCache = Math.min(nbGapsLocal, nbIntsRead);
        while (nbGapsRead < nbGapsToReadFromCache) {
            gapArrayRef.ints[nbGapsRead] = intsRead[nbGapsRead];
            nbGapsRead++;
        }
        // Then, shift the intsRead array.
        // Remark: This shift could be saved if we instead work on this array as
        // a circular one.
        if (nbGapsToReadFromCache > 0) {
            nbIntsRead -= nbGapsToReadFromCache;
            for (int i = 0; i < nbIntsRead; i++) {
                intsRead[i] = intsRead[i + nbGapsToReadFromCache];
            }
        }
        // Then, continue reading ints from the stream if needed.
        while (nbGapsRead < nbGapsLocal) {
            int combination = readCombination();
            if (combination == 0) {
                final int howmany = Math.min(nbGapsLocal - nbGapsRead, codeNum[combination]);
                for (int k = 0; k < howmany; k++) {
                    gapArrayRef.ints[nbGapsRead++] = 1;
                }
                // Finally, add the remaining ints in the cache.
                for (int k = howmany; k < codeNum[combination]; k++) {
                    intsRead[nbIntsRead++] = 1;
                }
            } else if (combination > maxCombinationValue) {
                throw new RuntimeException("Inconsistant value " + combination + " for the combination; values should be in the range [0; " + maxCombinationValue + "].");
            } else {
                int nbLongValues = (combination == 7 || combination == 9 || combination == 11 || combination == 13)
                        ? 4
                        : 2;
                BigInteger val = BigInteger.ZERO;
                long[] myLongs = new long[nbLongValues];
                for (int i = 0; i < nbLongValues; i++) {
                    myLongs[nbLongValues - 1 - i] = dataInputStream.readLong();
                }
                for (int i = 0; i < nbLongValues; i++) {
                    val = val.shiftLeft(64);
                    val = val.add(Tools.long2bigInteger(myLongs[i]));
                }
                int nbBitsForVal = 64 * nbLongValues;
                int b = bitLength[combination];
                int howmany = Math.min(nbGapsLocal - nbGapsRead, codeNum[combination]);
                // Here, nbBitsForVal % b does not give the right result, because
                // in the QMX algorithms, sometimes less values are put than the
                // theoretical maximum.
                int wastedBits = nbBitsForVal - b * codeNum[combination];
                for (int k = 0; k < howmany; k++) {
                    gapArrayRef.ints[nbGapsRead++] = 1 + val.shiftRight(nbBitsForVal - b - wastedBits - b * k).remainder(twoToB[b]).intValue();
                }
                // Finally, add the remaining ints in the cache.
                for (int k = howmany; k < codeNum[combination]; k++) {
                    intsRead[nbIntsRead++] = 1 + val.shiftRight(nbBitsForVal - b - wastedBits - b * k).remainder(twoToB[b]).intValue();
                }
            }
        }
    }
    
    private void flushRemainingInts() {
        nbIntsRead = 0;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of the index file *with padding* from the gapArrays.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        selectors = new ArrayList<>();
        binaryPacking = new ArrayList<>();
        treatGapArray(gapArrayRef);
        treatRemainingInts();
        exportData(dataOutputStream);
    }
    
    @Override
    public final void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        int nbGapsLocal = gapArrayRef.length;
        selectors = new ArrayList<>();
        int nbGapsInSelectors = 0;
        while (nbGapsInSelectors < nbGapsLocal) {
            int selector = Tools.byte2int(dataInputStream.readByte());
            selectors.add(selector);
            int runLength = Integer.remainderUnsigned(selector, maxRunLength);
            int combination = selector >>> nbBitsForRunLength;
            nbGapsInSelectors += (runLength + 1) * codeNum[combination];
        }
        importGapArray(dataInputStream, gapArrayRef);
        flushRemainingInts();
    }
    
    private final static int bitLength[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 16, 21, 32 };
    private final static int codeNum[] = { 256, 128, 64, 40, 32, 24, 20, 36, 16, 28, 12, 20, 8, 12, 4 };
    private final static long maxValue[] = Arrays
            .stream(bitLength)
            .mapToLong(i -> 1L << i)
            .toArray();
    private final static BigInteger twoToB[] = Arrays
            .stream(maxValue)
            .mapToObj(BigInteger::valueOf)
            .toArray(BigInteger[]::new);
    
    private final static int firstNonExactCombinationValue = 1;
    private final static int maxCombinationValue = bitLength.length - 1;
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of the index file *without padding* from the gapArrays.
    // Padding is avoided because when all the gapArrays can be treated as one
    // unique gapArray.
    // For example, the Interpolative method does not allow such a treatment.
    // When a method does not allow such a treatment, the methods with and
    // without padding output exactly the same index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeGapArrayList(DataOutputStream dataOutputStream, DatasetReader datasetReader) throws IOException {
        datasetReader.restartReader();
        selectors = new ArrayList<>();
        binaryPacking = new ArrayList<>();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            treatGapArray(gapArrayRef);
        }
        treatRemainingInts();
        exportData(dataOutputStream);
    }
    
    // TODO: this is mostly a copy/paste from Method.java.
    // Find a way to factorize the code.
    @Override
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
                // Read the selectors
                selectors = new ArrayList<>();
                int nbGapsInSelectors = 0;
                while (nbGapsInSelectors < nbPointers) {
                    int selector = Tools.byte2int(dataInputStream.readByte());
                    selectors.add(selector);
                    int runLength = Integer.remainderUnsigned(selector, maxRunLength);
                    int combination = selector >>> nbBitsForRunLength;
                    nbGapsInSelectors += (runLength + 1) * codeNum[combination];
                }
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                importGapArray(dataInputStream, intArrayRef);
                if (nbIntArraysRead == nbWords - 1) {
                    flushRemainingInts();
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    ArrayList<Integer> lengthsNeededToUncompress = importLengthsNeededToUncompress(in);
                    nbWords = lengthsNeededToUncompress.size();
                    for (int nbGapsLocal : lengthsNeededToUncompress) {
                        updateStatisticsWithNewGapArray(nbGapsLocal);
                    }
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }
    
}
