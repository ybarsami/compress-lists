/**
 * We first compute the trit sequence, then convert it to a bit sequence to
 * store it in a file. To do the conversion, we use the fact that 3**5 = 243
 * and 2**8 = 256, so we store 5 trits on 8 bits (1 byte).
 */

package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;
import tritlistencoding.TritSequence;
import static compresslists.Tools.*;

/**
 *
 * @author yann
 */
public class MethodTrits extends MethodByBitSequence {
    
    // For writing
    private int currentByte      = 0;
    protected int nbTritsWritten = 0;
    
    // For reading
    protected int[] currentTrits = new int[nbTritsPerByte];
    protected int nbCurrentTritsRead = nbTritsPerByte;
    
    private TritSequence tritSequence = new TritSequence();
    
    /**
     * Creates a new instance of MethodTrits.
     */
    public MethodTrits() {}
    
    @Override
    public String getName() {
        return "Trits";
    }
    
    @Override
    public void outputRemainingBits(BitOutputStream bitStream) {
        if (nbTritsWritten != 0) {
            for (int i = 0; i < nbTritsPerByte - nbTritsWritten; i++) {
                currentByte *= nbTrits;
            }
            BinaryEncoding.writeCodeBinary(currentByte, bitStream, nbBitsPerByte);
        }
        currentByte    = 0;
        nbTritsWritten = 0;
    }
    
    @Override
    public void flushRemainingBits() {
        currentTrits = new int[nbTritsPerByte];
        nbCurrentTritsRead = nbTritsPerByte;
    }
    
    @Override
    public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        writeTritSequence(bitStream, tritSequence);
    }
    
    public void writeTritSequence(BitOutputStream bitStream, TritSequence tritSequence) {
        // Converts the trits to bits (5 trits per byte).
        for (int i = 0; i < tritSequence.size(); i++) {
            currentByte *= nbTrits;
            currentByte += tritSequence.get(i);
            nbTritsWritten = (nbTritsWritten + 1) % nbTritsPerByte;
            if (nbTritsWritten == 0) {
                BinaryEncoding.writeCodeBinary(currentByte, bitStream, nbBitsPerByte);
                currentByte = 0;
            }
        }
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        // Formula is 8/5 * floor(log2(gapList(get(i))) + 8/5
        int nbNeededTrits = 0;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            nbNeededTrits += ilog2(gapArray[offset + idGap]) + 1;
        }
        return ((double) nbNeededTrits) * 8.0 / 5.0;
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        return super.computeSizeAfter();
    }
    
    public void readTritSequence(BitInputStream bitStream, int nbGapsLocal, TritSequence tritSequence) {
        tritSequence.reset();
        int nbGapsTreated = 0;
        while (nbGapsTreated < nbGapsLocal) {
            // If there are no more trits to read, read a new byte from the stream.
            if (nbCurrentTritsRead == nbTritsPerByte) {
                int[] currentBits = new int[nbBitsPerByte];
                for (int i = 0; i < nbBitsPerByte; i++) {
                    currentBits[i] = bitStream.getNextBit();
                }
                nbCurrentTritsRead = 0;
                int byteRead = byte2int(bitArray2byte(currentBits));
                for (int i = 0; i < nbTritsPerByte; i++) {
                    currentTrits[nbTritsPerByte - 1 - i] = byteRead % nbTrits;
                    byteRead /= nbTrits;
                }
            }
            // Extract a trit.
            int tritRead = currentTrits[nbCurrentTritsRead++];
            tritSequence.add(tritRead);
            if (tritRead == 2) {
                // If the trit is 2, it means we finished reading a gap.
                nbGapsTreated++;
            }
        }
    }
    
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        readTritSequence(bitStream, nbGapsLocal, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods useful for context encodings.
    ////////////////////////////////////////////////////////////////////////////
    
    public static int readCodeTrits(TritSequence tritSequence, int indexBegin) {
        // integer starts at 1 and not 0 because what is actually stored is
        // integer - 2^{ilog2(integer)}
        int integer = 1;
        int tritIndex = indexBegin;
        while (true) {
            // Extract a trit.
            int tritRead = tritSequence.get(tritIndex++);
            if (tritRead == 2) {
                // If the trit is 2, we finished reading the integer.
                return integer;
            } else {
                // Else, update the integer.
                integer *= 2;
                integer += tritRead;
            }
        }
    }
    
    /*
     * The trits code represents the number x as 1 + floor(log x) trits:
     *   - the first floor(log x) trits represent x - 2^{floor(x)} in binary
     *   - the last trit is 2, to mark stops between two successive numbers.
     * We here store the trits in an ArrayIntList (where we only use values
     * 0, 1 and 2).
     */
    public static void writeCodeTrits(int x, TritSequence buffer) {
        int nbBitsToWrite = ilog2(x);
        int bitMask = 1 << (nbBitsToWrite - 1);
        for (int j = 0; j < nbBitsToWrite; j++) {
            buffer.add((x & bitMask) == 0 ? 0 : 1);
            bitMask >>= 1;
        }   
        buffer.add(2);
    }
    
    /*
     * A tritList is of the form (b_{0,0}, ... b_{0,k_0-1}, 2, b_{1,0}, ..., b_{1,k_1-1}, 2, [...], b_{l-1,0}, ..., b{l-1,k_{l-1}-1}, 2)
     * Where the b_{i,j} are 0 or 1, and where "1b_{i,0}...b_{i,k_i-1}" is the
     * binary encoding of the i-th int (ints are always >= 1 so there is a "1"
     * at the beginning, which is implicit in the representation). We here
     * compute l intSizes (where l is the number of ints), where
     * intSize[i] = k_i + 1, the number of bits used for the binary encoding of
     * the i-th int --- that is, floor(log_2(int_i)) + 1. Then we concatenate
     * the trit codes of those intSizes in the array.
     */
    public static void tritSequence2tl(TritSequence tritSequence, TritSequence tl) {
        tl.reset();
        int nbBitsForGap = 1;
        for (int i = 0; i < tritSequence.size(); i++) {
            if (tritSequence.get(i) == 2) {
                writeCodeTrits(nbBitsForGap, tl);
                nbBitsForGap = 1;
            } else {
                nbBitsForGap++;
            }
        }
    }
    
    /*
     * A trit sequence is of the form (t_0, ... t_{n-1}) where the t_i are 0, 1 or 2.
     * We here replace all "0" and "1" by "0" and all "2" by "1", in the same
     * order as the original one.
     */
    public static void tritSequence2b2(TritSequence tritSequence, BitSequence b2) {
        b2.reset();
        for (int i = 0; i < tritSequence.size(); i++) {
            int trit = tritSequence.get(i);
            if (trit == 2) {
                b2.add(1);
            } else {
                b2.add(0);
            }
        }
    }
    
    /*
     * A trit sequence is of the form (t_0, ... t_{n-1}) where the t_i are 0, 1 or 2.
     * We here get rid of the "2" and output a list containing exactly the
     * "0" and "1" of the trit sequence, in the same order as the original one.
     */
    public static void tritSequence2b01(TritSequence tritSequence, BitSequence b01) {
        b01.reset();
        for (int i = 0; i < tritSequence.size(); i++) {
            int trit = tritSequence.get(i);
            if (trit != 2) {
                b01.add(trit);
            }
        }
    }
    
    public static void mergeB2AndB01(BitSequence b2, BitSequence b01, TritSequence tritSequence) {
        tritSequence.reset();
        int indexB01 = 0;
        for (int i = 0; i < b2.size(); i++) {
            int bit = b2.get(i);
            if (bit == 1) {
                tritSequence.add(2);
            } else {
                tritSequence.add(b01.get(indexB01));
                indexB01++;
            }
        }
    }
    
    public static void mergeTLAndB01(TritSequence tl, BitSequence b01, TritSequence tritSequence) {
        tritSequence.reset();
        int indexBit = 0;
        int indexTrit = 0;
        int length = tl.numberOfTwos();
        for (int i = 0; i < length; i++) {
            int integer = MethodTrits.readCodeTrits(tl, indexTrit);
            // integer now stores 1 + the size in bits of the i-th gap, as it
            // is stored in a trit sequence, cf. Tools.tritSequence2intSizeArray.
            for (int j = 0; j < integer - 1; j++) {
                tritSequence.add(b01.get(indexBit));
                indexBit++;
            }
            tritSequence.add(2);
            indexTrit = tl.indexOfNextTwo(indexTrit) + 1;
        }
    }
    
}
