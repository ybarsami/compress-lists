/**
 * Index compression with the binary method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 114
 * For a text of N documents and an index containing f pointers, the total
 * space required with a naive representation is f * ceiling(log N) bits,
 * provided that pointers are stored in a minimal number of bits.
 */

package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodBinary extends MethodByElement {
    
    private int nbBitsPerBinaryNumber;
    
    /**
     * Creates a new instance of MethodBinary.
     */
    public MethodBinary(int nbBitsPerBinaryNumber) {
        setParameter(nbBitsPerBinaryNumber);
    }
    public MethodBinary() {
        this(Tools.nbBitsPerInt);
    }
    
    public final void setParameter(int nbBitsPerBinaryNumber) {
        this.nbBitsPerBinaryNumber = nbBitsPerBinaryNumber;
    }
    
    @Override
    public void initBefore() {
        nbBitsPerBinaryNumber = 0;
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            if (Tools.ceilingLog2(gapArray[idGap]) > nbBitsPerBinaryNumber) {
                nbBitsPerBinaryNumber = Tools.ceilingLog2(gapArray[offset + idGap]);
            }
        }
    }
    
    @Override
    public String getName() {
        return "Binary";
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 32; // nbBitsPerBinaryNumber
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(nbBitsPerBinaryNumber);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        setParameter(in.readInt());
    }
    
    @Override
    public double computeSize(int x) {
        return (double) nbBitsPerBinaryNumber;
    }
    
    // As a small optimization, here computeSize(int[]) is overriden to do a
    // simple multiplication instead of length additions.
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        return (double) (gapArrayRef.length * nbBitsPerBinaryNumber);
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        BinaryEncoding.writeCodeBinary(x - 1, bitStream, nbBitsPerBinaryNumber);
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return BinaryEncoding.readCodeBinary(bitStream, nbBitsPerBinaryNumber) + 1;
    }
    
}
