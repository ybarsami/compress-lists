/**
 * Index compression with the Simple16 method.
 * It is a refinement from the Simple-9 method that wastes less bits.
 *
 * Zhang, Long, and Suel, "Performance of Compressed Inverted List Caching in Search Engines" (2008)
 * 
 * Implementation from the java library JavaFastPFOR.
 * https://github.com/lemire/JavaFastPFOR
 */

package gaparrayencoding;

import me.lemire.integercompression.Simple16;

/**
 *
 * @author yann
 */
public class MethodSimple16 extends MethodByJavaFastPFOR {
    
    /**
     * Creates a new instance of MethodSimple16.
     */
    public MethodSimple16() {
        super(new Simple16());
    }
}
