/**
 * Transforms each gap array into a trit sequence and codes the trits by chunks
 * of X trits, according to their global observed frequency.
 */

package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;
import tritlistencoding.TritSequence;
import static compresslists.Tools.nbTrits;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodArithmeticXTrits extends MethodByArithmeticCoding {
    
    // Number of trits processed at a time.
    private int nbTritsByChunk;
    private boolean useRealOccurrences;
    
    // Size of the occurrence arrays.
    private int arraySize;
    
    // Occurrence arrays.
    private long[] occurrences;
    private int[] occurrencesCumulative;
    
    private int currentTrituplet = 0;
    private int nbTritsWritten   = 0;
    
    private double nbBitsPerTrituplet;
    
    private TritSequence tritSequence = new TritSequence();
    
    /**
     * Creates a new instance of MethodAriCodeXTrits.
     */
    public MethodArithmeticXTrits() {}
    public MethodArithmeticXTrits(int nbTritsByChunk, boolean useRealOccurrences) {
        setParameters(nbTritsByChunk, useRealOccurrences);
    }
    public MethodArithmeticXTrits(int nbTritsByChunk) {
        this(nbTritsByChunk, true);
    }
    
    public final void setParameters(int nbTritsByChunk, boolean useRealOccurrences) {
        if (nbTritsByChunk <= 0) {
            throw new RuntimeException("nbTritsByChunk must be > 0.");
        }
        this.useRealOccurrences = useRealOccurrences;
        this.nbTritsByChunk = nbTritsByChunk;
        arraySize = Tools.pow(nbTrits, nbTritsByChunk);
        allocateOccurrences();
        if (!useRealOccurrences) {
            OccurrenceTools.fillUniformOccurrences(occurrences);
        }
    }
    
    private void allocateOccurrences() {
        occurrences = new long[arraySize];
	occurrencesCumulative = new int[arraySize + 1];
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        // A symbol is a nbTritsByChunk-uplet of trits.
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        // Fill the last chunk.
        for (int i = 0; i < tritSequence.size() % nbTritsByChunk; i++) {
            tritSequence.add(2);
        }
        int size = tritSequence.size() / nbTritsByChunk;
//        int[] trituplet = new int[nbTritsByChunk];
        for (int i = 0; i < size; i++) {
            int tritindex = 0;
            for (int j = 0; j < nbTritsByChunk; j++) {
//                trituplet[j] = fullTritSequence.get(nbTritsByChunk * i + j);
                tritindex *= nbTrits;
                tritindex += tritSequence.get(nbTritsByChunk * i + j);
            }
            occurrences[tritindex]++;
        }
    }
    
    @Override
    public void initAfter() {
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Compute the number of bits needed per trituplet.
        nbBitsPerTrituplet = 0.;
        for (int i = 0; i < arraySize; i++) {
            double pi = (double)occurrences[i] / ((double)occurrencesCumulative[arraySize]);
            nbBitsPerTrituplet += -pi * Tools.log2(pi);
        }
    }
    
    @Override
    public String getName() {
        return "ArithmeticXTrits_" + nbTritsByChunk;
    }
    
    @Override
    public void outputRemainingBits() {
        if (nbTritsWritten != 0) {
            for (int i = 0; i < nbTritsByChunk - nbTritsWritten; i++) {
                currentTrituplet *= nbTrits;
                currentTrituplet += 2;
            }
            encoder.update(occurrencesCumulative, currentTrituplet);
            nbTritsWritten   = 0;
            currentTrituplet = 0;
        }
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        return Tools.ceilingDivision(tritSequence.size(), nbTritsByChunk) * nbBitsPerTrituplet;
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                32 + // nbTritsByChunk
                8 + // useRealOccurrences
                (useRealOccurrences ? 32 * arraySize : 0); // occurrences
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(nbTritsByChunk);
        out.writeBoolean(useRealOccurrences);
        if (useRealOccurrences) {
            for (int i = 0; i < arraySize; i++) {
                out.writeInt((int) occurrences[i]);
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        nbTritsByChunk = in.readInt();
        useRealOccurrences = in.readBoolean();
        setParameters(nbTritsByChunk, useRealOccurrences);
        if (useRealOccurrences) {
            for (int i = 0; i < arraySize; i++) {
                occurrences[i] = in.readInt();
            }
        }
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
    }
    
    private int writeNextTrituplet(TritSequence tritSequence, int startingIndex) {
        int nbTritsToRead = nbTritsByChunk - nbTritsWritten;
        int nbTritsRead = 0;
        for (int i = startingIndex; i < Math.min(startingIndex + nbTritsToRead, tritSequence.size()); i++) {
            currentTrituplet *= nbTrits;
            currentTrituplet += tritSequence.get(i);
            nbTritsRead++;
        }
        nbTritsWritten = (nbTritsWritten + nbTritsRead) % nbTritsByChunk;
        if (nbTritsWritten == 0) {
            encoder.update(occurrencesCumulative, currentTrituplet);
            currentTrituplet = 0;
        }
        return startingIndex + nbTritsRead;
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        // Get the trit list.
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        // Do the encoding.
        int tritSequenceIndex = 0;
        while (tritSequenceIndex < tritSequence.size()) {
            tritSequenceIndex = writeNextTrituplet(tritSequence, tritSequenceIndex);
        }
    }
    
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        tritSequence.reset();
        int nbGapsRead = 0;
        // Do the decoding.
        while (nbGapsRead < nbGapsLocal) {
            int trit = decoder.read(occurrencesCumulative);
            if (trit == 2) {
                nbGapsRead++;
            }
            tritSequence.add(trit);
        }
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }

}
