/**
 * MethodBySplitTLB01.java
 * For each gapArray, here what is done:
 *    0. We note gapSizeArray the array such that
 *       gapSizeArray[i] = floor(log_2(gapArray[i])).
 *       We note tritList = tritList(gapSizeArray).
 *    1. With a TritSequence method, we encode tritList.
 *    2. We have now encoded what enables us to retrieve the "2" in
 *       tritList(gapArray). There remains to put the "{0,1}" from this trit
 *       list, i.e. b01. We code b01 with a BitSequence method.
 * 
 * Remark: This is essentially the code extracted from MethodFContextTrit.
 * 
 * Remark: To allow more flexibility, it is better to use the same Method in the
 * tritlistencoding package.
 */

package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;
import tritlistencoding.TritSequence;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodBySplitTLB01 extends MethodByBitSequence {
    
    private tritlistencoding.Method tlMethod;
    private bitlistencoding.Method b01Method;
    
    // To separate the different sizes in the index.
    private double sizetl;
    private double sizeb01;
    
    private TritSequence tritSequence = new TritSequence();
    private TritSequence tl = new TritSequence();
    private BitSequence b01 = new BitSequence();
    
    /**
     * Creates a new instance of MethodBySplitTLB01.
     */
    public MethodBySplitTLB01() {}
    public MethodBySplitTLB01(tritlistencoding.Method tlMethod, bitlistencoding.Method b01Method) {
        this.tlMethod  = tlMethod;
        this.b01Method = b01Method;
    }
    
    @Override
    public void initBefore() {
        tlMethod.initBefore();
        b01Method.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        tlMethod.init(tl);
        b01Method.init(b01);
    }
    
    @Override
    public void initAfter() {
        tlMethod.initAfter();
        b01Method.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        double sizetlLocal  = tlMethod.computeSize(tl);
        double sizeb01Local = b01Method.computeSize(b01);
        
        sizetl  += sizetlLocal;
        sizeb01 += sizeb01Local;
        return sizetlLocal + sizeb01Local;
    }
    
    @Override
    public void computeSizeBefore() {
        sizetl  = 0.;
        sizeb01 = 0.;
        tlMethod.computeSizeBefore();
        b01Method.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        tl = new TritSequence();
        b01 = new BitSequence();
        return tlMethod.computeSizeAfter() + b01Method.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; tl: " + (sizetl / 8000000) +
                " ; b01: " + (sizeb01 / 8000000) +
                ")");
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        tlMethod.writeTritSequence(bitStream, tl);
        b01Method.writeBitSequence(bitStream, b01);
    }
    
    @Override
    public String getName() {
        return "BySplitTLB01(" + tlMethod.getName() + "-" + b01Method.getName() + ")";
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                MethodByTritList.computeSizeTritMethod(tlMethod) +
                MethodByBitVector.computeSizeBitMethod(b01Method);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        MethodByTritList.exportTritMethod(tlMethod, out);
        MethodByBitVector.exportBitMethod(b01Method, out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        tlMethod = MethodByTritList.importTritMethod(in);
        b01Method = MethodByBitVector.importBitMethod(in);
    }
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        // Phase 1.
        tlMethod.readTritSequence(bitStream, nbGapsLocal, tl);
        
        // Phase 2.
        int nbBitsToRead = Tools.tritSequenceLength2nb01(tl);
        b01Method.readBitSequence(bitStream, nbBitsToRead, false, b01);
        
        // Merge.
        MethodTrits.mergeTLAndB01(tl, b01, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }

}
