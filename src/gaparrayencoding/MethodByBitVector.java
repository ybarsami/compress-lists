/*
 * MethodByBitVector.java
 * For each gapArray, here what is done:
 *    0. We note bitVector = bitVector(gapArray).
 *    1. With a BitSequence method, we encode bitVector.
 */
package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodByBitVector extends MethodByBitSequence {
    
    protected bitlistencoding.Method bMethod;
    
    private BitSequence bitVector = new BitSequence();
    
    /**
     * Creates a new instance of MethodByBitVector.
     */
    public MethodByBitVector() {}
    public MethodByBitVector(bitlistencoding.Method bMethod) {
        this.bMethod = bMethod;
    }
    
    /*
     * Further optimization: here, we override init(IntsRef), so needsInit()
     * from Method.java will return true. Here, we return false if the bMethod
     * does not override init(BitSequence).
     */
    @Override
    public boolean needsInit() {
        try {
            return !bMethod.getClass().getMethod("init", BitSequence.class).getDeclaringClass().equals(bitlistencoding.Method.class);
        } catch (NoSuchMethodException e) {
            return true;
        }
    }
    
    @Override
    public void initBefore() {
        bMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.gapArray2bitVector(gapArrayRef, bitVector);
        bMethod.init(bitVector);
    }
    
    @Override
    public void initAfter() {
        bMethod.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.gapArray2bitVector(gapArrayRef, bitVector);
        return bMethod.computeSize(bitVector);
    }
    
    @Override
    public void computeSizeBefore() {
        bMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        bitVector = new BitSequence();
        return bMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        bMethod.standardOutput();
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.gapArray2bitVector(gapArrayRef, bitVector);
        bMethod.writeBitSequence(bitStream, bitVector);
    }
    
    @Override
    public String getName() {
        return "ByBitVector(" + bMethod.getName() + ")";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        bitVector = new BitSequence();
        bMethod.outputRemainingBits(bitStream);
    }
    
    @Override
    public final void flushRemainingBits() {
        bMethod.flushRemainingBits();
    }
    
    public static final double computeSizeBitMethod(bitlistencoding.Method bMethod) {
        return
                8. +
                bMethod.getClass().getName().length() * 16. +
                bMethod.computeSizeNeededInformation();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return computeSizeBitMethod(bMethod);
    }
    
    public static final void exportBitMethod(bitlistencoding.Method bMethod, DataOutputStream out) throws IOException {
        String bMethodName = bMethod.getClass().getName();
        out.writeByte(bMethodName.length());
        out.writeChars(bMethodName);
        bMethod.exportNeededInformation(out);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        exportBitMethod(bMethod, out);
    }
    
    public static final bitlistencoding.Method importBitMethod(DataInputStream in) throws IOException {
        byte bMethodNameLength = in.readByte();
        String bMethodName = "";
        for (int i = 0; i < bMethodNameLength; i++) {
            bMethodName += in.readChar();
        }
        try {
            Class methodClass = Class.forName(bMethodName);
            bitlistencoding.Method bMethod = (bitlistencoding.Method)methodClass.newInstance();
            bMethod.importNeededInformation(in);
            return bMethod;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
            System.out.println(bMethodName + " is not a valid bitlistencoding method for CompressLists.");
            return null;
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        bMethod = importBitMethod(in);
    }
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        bMethod.readBitSequence(bitStream, nbGapsLocal, bitVector);
        Tools.bitVector2gapArray(bitVector, gapArrayRef);
    }
    
}
