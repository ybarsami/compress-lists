/*
 * MethodByTritList.java
 * For each gapArray, here what is done:
 *    0. We note tritList = tritList(gapArray).
 *    1. With a TritSequence method, we encode tritList.
 */
package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;
import tritlistencoding.TritSequence;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodByTritList extends MethodByBitSequence {
    
    protected tritlistencoding.Method tMethod;
    
    private TritSequence tritSequence = new TritSequence();
    
    /**
     * Creates a new instance of MethodByTritList.
     */
    public MethodByTritList() {}
    public MethodByTritList(tritlistencoding.Method tMethod) {
        this.tMethod = tMethod;
    }
    
    /*
     * Further optimization: here, we override init(IntsRef), so needsInit()
     * from Method.java will return true. Here, we return false if the tMethod
     * does not override init(TritSequence).
     */
    @Override
    public boolean needsInit() {
        try {
            return !tMethod.getClass().getMethod("init", TritSequence.class).getDeclaringClass().equals(tritlistencoding.Method.class);
        } catch (NoSuchMethodException e) {
            return true;
        }
    }
    
    @Override
    public void initBefore() {
        tMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        tMethod.init(tritSequence);
    }
    
    @Override
    public void initAfter() {
        tMethod.initAfter();
    }
    
    @Override
    public void computeSizeBefore() {
        tMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        return tMethod.computeSize(tritSequence);
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        return tMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        tMethod.standardOutput();
    }
    
    @Override
    public String getName() {
        return "ByTritList(" + tMethod.getName() + ")";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        tMethod.outputRemainingBits(bitStream);
    }
    
    @Override
    public final void flushRemainingBits() {
        tMethod.flushRemainingBits();
    }
    
    public static final double computeSizeTritMethod(tritlistencoding.Method tMethod) {
        return
                8. +
                tMethod.getClass().getName().length() * 16. +
                tMethod.computeSizeNeededInformation();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return computeSizeTritMethod(tMethod);
    }
   
    public static final void exportTritMethod(tritlistencoding.Method tMethod, DataOutputStream out) throws IOException {
        String tMethodName = tMethod.getClass().getName();
        out.writeByte(tMethodName.length());
        out.writeChars(tMethodName);
        tMethod.exportNeededInformation(out);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        exportTritMethod(tMethod, out);
    }
    
    public static final tritlistencoding.Method importTritMethod(DataInputStream in) throws IOException {
        byte tMethodNameLength = in.readByte();
        String tMethodName = "";
        for (int i = 0; i < tMethodNameLength; i++) {
            tMethodName += in.readChar();
        }
        try {
            Class methodClass = Class.forName(tMethodName);
            tritlistencoding.Method tMethod = (tritlistencoding.Method)methodClass.newInstance();
            tMethod.importNeededInformation(in);
            return tMethod;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
            System.out.println(tMethodName + " is not a valid tritlistencoding method for CompressLists.");
            return null;
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        tMethod = importTritMethod(in);
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        tMethod.writeTritSequence(bitStream, tritSequence);
    }
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        tMethod.readTritSequence(bitStream, nbGapsLocal, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }
    
    /*
    @Override
    public void exportToFile(String filename, ArrayList<int[]> gapArrayList, int nbDocuments) {
        ArrayList<TritSequence> tList = convert(gapArrayList);
        tMethod.exportToFile(filename, tList, nbDocuments);
    }
    
    @Override
    public DatasetInfo importFromFile(String filename) {
        return tMethod.importFromFile(filename);
    }
    */
    
}
