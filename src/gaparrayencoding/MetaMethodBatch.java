/**
 * MetaMethodBatch.java
 */

package gaparrayencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.Dataset;
import io.DatasetInfo;
import io.DatasetReader;
import static compresslists.CompressLists.*;
import static gaparrayencoding.DaMethods.*;

import com.martiansoftware.jsap.JSAPResult;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author yann
 */
public class MetaMethodBatch extends Method {
    
    private int nbBatches;
    private double[] frequencyStarts;
    private double[] frequencyStops;
    private Method[] methodByBatch;
    
    private double size;
    private double sizeOccurrences;
    
    private boolean testOnInverted;
    private ArrayList<Method> methodsToTest;
    private int nbMethods;
    private String[] nameByMethod;
    private double[] sizeTotalByMethod;
    private double[] sizeOccurrencesByMethod;
    
    private boolean hasToLog = true;
    
    /**
     * Creates a new instance of MetaMethodBatch.
     *
     * frequencyDelimiters.length must be one more than the number of batches you want to test.
     * This function will tests the batches:
     * [fd[0]; fd[1]), [fd[1]; fd[2]), ... [fd[length-2]; fd[length-1]).
     *
     * You might want to change the methods tested. It is line 108.
     *
     * At each batch, it will print the delimiters, and print the various statistics of each tested method.
     */
    public MetaMethodBatch(DatasetInfo datasetInfo, double[] frequencyDelimiters, boolean useABatchForSingletonGapArrays, String[] subMethods, JSAPResult config) {
        this.nbDocuments = datasetInfo.getNbDocuments();
        initMethodsToTest(subMethods, config, datasetInfo);
        ArrayList<int[]> gapArrayList = datasetInfo.gapArrayList;
        
        // Singleton arrays are those whose frequency is equal to 1 / nbDocuments.
        // We here choose 1.5 to avoid numerical error in the division.
        double frequencyStopForSingletonArrays = 1.5 / ((double)nbDocuments);
        // We use the batch for the singleton arrays only if the associated
        // frequency is within the range of the wanted frequencies.
        useABatchForSingletonGapArrays &= frequencyDelimiters[0] < frequencyStopForSingletonArrays;
        // One less interval than the number of delimiters.
        assert(frequencyDelimiters.length >= 2);
        nbBatches = frequencyDelimiters.length - 1;
        // We add one batch for the gapArrays which contain only 1 gap.
        if (useABatchForSingletonGapArrays) {
            nbBatches++;
        }
        frequencyStarts = new double[nbBatches];
        frequencyStops  = new double[nbBatches];
        methodByBatch = new Method[nbBatches];
        ArrayList<ArrayList<int[]>> gapArrayListByBatch = new ArrayList<>();
        // The first batch --- distinguish when we have one batch for the
        // gapArrays containing only 1 gap.
        if (useABatchForSingletonGapArrays) {
            frequencyStarts[0] = frequencyDelimiters[0];
            frequencyStops[0]  = frequencyStopForSingletonArrays;
            gapArrayListByBatch.add(new ArrayList<>());
            frequencyStarts[1] = frequencyStopForSingletonArrays;
            frequencyStops[1]  = frequencyDelimiters[1];
            gapArrayListByBatch.add(new ArrayList<>());
        } else {
            frequencyStarts[0] = frequencyDelimiters[0];
            frequencyStops[0]  = frequencyDelimiters[1];
            gapArrayListByBatch.add(new ArrayList<>());
        }
        // The regular batches.
        for (int i = 1; i < frequencyDelimiters.length - 1; i++) {
            final int index = useABatchForSingletonGapArrays ? i + 1 : i;
            frequencyStarts[index] = frequencyDelimiters[i];
            frequencyStops[index]  = frequencyDelimiters[i + 1];
            gapArrayListByBatch.add(new ArrayList<>());
        }
        
        // Separation of the full gapArrayList by batch.
        for (int[] gapArray : gapArrayList) {
            if (gapArray.length < frequencyStarts[0] * nbDocuments) {
                continue;
            }
            for (int i = 0; i < nbBatches; i++) {
                if (gapArray.length <= frequencyStops[i] * nbDocuments) {
                    gapArrayListByBatch.get(i).add(gapArray);
                    break;
                }
            }
        }
        
        size = 0.;
        sizeOccurrences = 0.;
        for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
            // The normal methods
            final ArrayList<int[]> gapArrayList_Real = gapArrayListByBatch.get(idBatch);
            if (gapArrayList_Real.isEmpty()) {
                continue;
            }
            final DatasetInfo datasetInfo_Real = new DatasetInfo(gapArrayList_Real, nbDocuments);
            int nbRealMethods = testOnInverted ? nbMethods / 2 : nbMethods;
            for (int idMethod = 0; idMethod < nbRealMethods; idMethod++) {
                setParametersAndInit(methodsToTest.get(idMethod), datasetInfo_Real);
            }
            
            // The methods on inverted gap arrays (associated to the inverted bit vectors)
            DatasetInfo datasetInfo_Inverted = null;
            if (testOnInverted) {
                final ArrayList<int[]> gapArrayList_Inverted = invertGapArrayList(gapArrayList_Real, nbDocuments);
                datasetInfo_Inverted = new DatasetInfo(gapArrayList_Inverted, nbDocuments);
                for (int idMethod = nbRealMethods; idMethod < 2 * nbRealMethods; idMethod++) {
                    setParametersAndInit(methodsToTest.get(idMethod), datasetInfo_Inverted);
                }
            }
            
            // Testing the best method.
            if (hasToLog) {
                System.out.println("--- Testing " +
                        (useABatchForSingletonGapArrays && idBatch == 0
                                ? "singleton gap arrays"
                                : "frequencies in " + (idBatch == 0 ? "[" : "(") +
                                        frequencyStarts[idBatch] +
                                        ", " + frequencyStops[idBatch] + "]") +
                        " ---");
            }
            int indexBestMethod = 0;
            double[] sizeTotalByMethodLocal = new double[nbMethods];
            double[] sizeOccurrencesByMethodLocal = new double[nbMethods];
            for (int idMethod = 0; idMethod < nbRealMethods; idMethod++) {
                // For empty gapArrays, set the size to 0.
                sizeTotalByMethodLocal[idMethod] = datasetInfo_Real.getNbWords() == 0
                        ? 0.
                        : methodsToTest.get(idMethod).computeSize(datasetInfo_Real, nbDocuments, hasToLog) - 32.; // nbDocuments can be stored only once
                sizeOccurrencesByMethodLocal[idMethod] = datasetInfo_Real.getNbWords() == 0
                        ? 0.
                        : methodsToTest.get(idMethod).computeSizeNeededInformation();
                sizeTotalByMethod[idMethod] += sizeTotalByMethodLocal[idMethod];
                sizeOccurrencesByMethod[idMethod] += sizeOccurrencesByMethodLocal[idMethod];
            }
            for (int idMethod = nbRealMethods; idMethod < nbMethods; idMethod++) {
                // For empty gapArrays, set the size to 0.
                sizeTotalByMethodLocal[idMethod] = datasetInfo_Inverted.getNbWords() == 0
                        ? 0.
                        : methodsToTest.get(idMethod).computeSize(datasetInfo_Inverted, nbDocuments, hasToLog) - 32.; // nbDocuments can be stored only once
                sizeOccurrencesByMethodLocal[idMethod] = datasetInfo_Inverted.getNbWords() == 0
                        ? 0.
                        : methodsToTest.get(idMethod).computeSizeNeededInformation();
                sizeTotalByMethod[idMethod] += sizeTotalByMethodLocal[idMethod];
                sizeOccurrencesByMethod[idMethod] += sizeOccurrencesByMethodLocal[idMethod];
            }
            double costBestMethod = sizeTotalByMethodLocal[0];
            for (int idMethod = 1; idMethod < nbMethods; idMethod++) {
                double costMethod = sizeTotalByMethodLocal[idMethod];
                if (costMethod < costBestMethod) {
                    costBestMethod = costMethod;
                    indexBestMethod = idMethod;
                }
            }
            size += costBestMethod;
            sizeOccurrences += sizeOccurrencesByMethodLocal[indexBestMethod];
            
            // Saving the best method.
            methodByBatch[idBatch] = methodsToTest.get(indexBestMethod);
            if (hasToLog) {
                System.out.println("   The best method was " + methodByBatch[idBatch].getName() +
                        (indexBestMethod >= nbRealMethods ? " (inverted list)" : "") + " : " +
                        (costBestMethod / 8000000.) + " MB.");
                for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
                    if (methodsToTest.get(idMethod) instanceof MethodInterpolative && idMethod != indexBestMethod) {
                        double sizeInterpolative = sizeTotalByMethodLocal[idMethod];
                        System.out.println("   Gains w.r.t. Interpolative : " +
                                (100*(sizeInterpolative - costBestMethod) / sizeInterpolative) +
                                "%");
                        break;
                    }
                }
            }
        }
    }
    public MetaMethodBatch(DatasetInfo datasetInfo, double[] frequencyDelimiters, String[] subMethods, JSAPResult config) {
        this(datasetInfo, frequencyDelimiters, true, subMethods, config);
    }
    public MetaMethodBatch(Dataset[] batchedDatasets, double[] frequencyDelimiters, String[] subMethods, JSAPResult config, DatasetReader globalDatasetReader) {
        this.nbDocuments = globalDatasetReader.getNbDocuments();
        initMethodsToTest(subMethods, config, globalDatasetReader);
        
        // One less interval than the number of delimiters.
        assert(frequencyDelimiters.length >= 2);
        nbBatches = frequencyDelimiters.length - 1;
        assert(batchedDatasets.length == nbBatches);
        frequencyStarts = new double[nbBatches];
        frequencyStops  = new double[nbBatches];
        methodByBatch = new Method[nbBatches];
        for (int i = 0; i < frequencyDelimiters.length - 1; i++) {
            frequencyStarts[i] = frequencyDelimiters[i];
            frequencyStops[i]  = frequencyDelimiters[i + 1];
        }
        
        // Sizes of some datasets (in MB), computed with the Interpolative method.
        HashMap<String, Double> mapSizesInterpolative = new HashMap<>();
        // Gov2 batches
        mapSizesInterpolative.put("gov2-bisection-batch0.Delta", 464.733744375);
        mapSizesInterpolative.put("gov2-bisection-batch0-sortedByDensity.Delta", 464.733744375);
        mapSizesInterpolative.put("gov2-bisection-batch1.Delta", 247.837156375);
        mapSizesInterpolative.put("gov2-bisection-batch1-sortedByDensity.Delta", 247.837156375);
        mapSizesInterpolative.put("gov2-bisection-batch2.Delta", 516.237109625);
        mapSizesInterpolative.put("gov2-bisection-batch2-sortedByDensity.Delta", 516.237109625);
        mapSizesInterpolative.put("gov2-bisection-batch3.Delta", 296.529291625);
        mapSizesInterpolative.put("gov2-bisection-batch3-sortedByDensity.Delta", 296.529291625);
        mapSizesInterpolative.put("gov2-bisection-batch4.Delta", 323.097382875);
        mapSizesInterpolative.put("gov2-bisection-batch4-sortedByDensity.Delta", 323.097382875);
        mapSizesInterpolative.put("gov2-bisection-batch5.Delta", 112.65986275);
        mapSizesInterpolative.put("gov2-bisection-batch5-sortedByDensity.Delta", 112.65986275);
        mapSizesInterpolative.put("gov2-bisection-batch6.Delta", 50.1860385);
        mapSizesInterpolative.put("gov2-bisection-batch6-sortedByDensity.Delta", 50.1860385);
        // ClueWeb09 batches
        mapSizesInterpolative.put("clueweb09-bisection-batch0.Delta", 1249.281186875);
        mapSizesInterpolative.put("clueweb09-bisection-batch1.Delta", 886.24818775);
        mapSizesInterpolative.put("clueweb09-bisection-batch2.Delta", 2189.17358275);
        mapSizesInterpolative.put("clueweb09-bisection-batch3.Delta", 1508.944694125);
        mapSizesInterpolative.put("clueweb09-bisection-batch4.Delta", 1561.985634375);
        mapSizesInterpolative.put("clueweb09-bisection-batch5.Delta", 620.91130475);
        mapSizesInterpolative.put("clueweb09-bisection-batch6.Delta", 408.574437875);
        
        size = 0.;
        sizeOccurrences = 0.;
        for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
            // The methods to test.
            final DatasetReader datasetReader = batchedDatasets[idBatch].getDatasetReader();
            for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
                setParametersAndInit(methodsToTest.get(idMethod), datasetReader);
            }
            
            // Precomputed value for Interpolative.
            double sizeInBitsInterpolative = 0.;
            for (String s : mapSizesInterpolative.keySet()) {
                if (batchedDatasets[idBatch].getDatasetName().equals(s)) {
                    sizeInBitsInterpolative = mapSizesInterpolative.get(s) * 8000000.;
                }
            }
            
            // Testing the best method.
            if (hasToLog) {
                System.out.println("--- Testing frequencies in " + (idBatch == 0 ? "[" : "(") +
                                frequencyStarts[idBatch] +
                                ", " + frequencyStops[idBatch] + "]" +
                        " ---");
            }
            int indexBestMethod = 0;
            double[] sizeTotalByMethodLocal = new double[nbMethods];
            double[] sizeOccurrencesByMethodLocal = new double[nbMethods];
            for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
                // For empty gapArrays, set the size to 0.
                // For Interpolative, use the precomputed value if it exists.
                sizeTotalByMethodLocal[idMethod] = datasetReader.getNbWords() == 0
                        ? 0.
                        : methodsToTest.get(idMethod) instanceof MethodInterpolative && sizeInBitsInterpolative > 0
                        ? sizeInBitsInterpolative
                        : methodsToTest.get(idMethod).computeSize(datasetReader, nbDocuments, hasToLog) - 32.; // nbDocuments can be stored only once
                sizeOccurrencesByMethodLocal[idMethod] = datasetReader.getNbWords() == 0
                        ? 0.
                        : methodsToTest.get(idMethod).computeSizeNeededInformation();
                sizeTotalByMethod[idMethod] += sizeTotalByMethodLocal[idMethod];
                sizeOccurrencesByMethod[idMethod] += sizeOccurrencesByMethodLocal[idMethod];
            }
            double costBestMethod = sizeTotalByMethodLocal[0];
            for (int idMethod = 1; idMethod < nbMethods; idMethod++) {
                double costMethod = sizeTotalByMethodLocal[idMethod];
                if (costMethod < costBestMethod) {
                    costBestMethod = costMethod;
                    indexBestMethod = idMethod;
                }
            }
            
            // Saving the best method.
            methodByBatch[idBatch] = methodsToTest.get(indexBestMethod);
            size += costBestMethod;
            sizeOccurrences += sizeOccurrencesByMethodLocal[indexBestMethod];
            
            // Print statistics
            if (hasToLog) {
                System.out.println("   The best method was " + methodByBatch[idBatch].getName() + " : " +
                        (costBestMethod / 8000000.) + " MB.");
                for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
                    if (methodsToTest.get(idMethod) instanceof MethodInterpolative && idMethod != indexBestMethod) {
                        double sizeInterpolative = sizeTotalByMethodLocal[idMethod];
                        System.out.println("Gains w.r.t. Interpolative : " +
                                (100*(sizeInterpolative - costBestMethod) / sizeInterpolative) +
                                "%");
                        break;
                    }
                }
            }
        }
    }
    
    public static void setParametersAndInit(Method method, DatasetReader datasetReader) {
        int localNbWords = datasetReader.getNbWords();
        int localNbDocuments = datasetReader.getNbDocuments();
        long localNbPointers = datasetReader.getNbPointers();
        ContextParameters contextParameters = new ContextParameters(localNbDocuments, localNbPointers);
        int firstGapArraySize = datasetReader.getGapArrayLength(0);
        ContextParameters adaptiveContextParameters = AdaptiveContextParameters(localNbPointers);
        if (method instanceof MethodBinary) {
            ((MethodBinary)method).setParameter(Tools.ceilingLog2(localNbDocuments));
        } else if (method instanceof MethodBernoulliGlobal) {
            ((MethodBernoulliGlobal)method).setParameter(localNbWords, localNbDocuments, localNbPointers);
        } else if (method instanceof MethodGolombGlobal) {
            ((MethodGolombGlobal)method).setParameter(localNbWords, localNbDocuments, localNbPointers);
        } else if (method instanceof MethodRice) {
            ((MethodRice)method).setParameter(localNbWords, localNbDocuments, localNbPointers);
        } else if (method instanceof MethodTritListContext) {
            ((MethodTritListContext)method).setParameters(contextParameters);
        } else if (method instanceof MethodTritConcatenateContext) {
            ((MethodTritConcatenateContext)method).setParameters(contextParameters);
        } else if (method instanceof MethodQuatritListContext) {
            ((MethodQuatritListContext)method).setParameters(contextParameters);
        } else if (method instanceof MethodQuatritConcatenateContext) {
            ((MethodQuatritConcatenateContext)method).setParameters(contextParameters);
        } else if (method instanceof MethodTritListAdaptiveContext) {
            ((MethodTritListAdaptiveContext)method).setParameters(
                    adaptiveContextParameters, localNbDocuments, firstGapArraySize);
        } else if (method instanceof MethodTritConcatenateAdaptiveContext) {
            ((MethodTritConcatenateAdaptiveContext)method).setParameters(
                    adaptiveContextParameters, localNbDocuments, firstGapArraySize);
        } else if (method instanceof MethodBitVectorAdaptiveContext) {
            ((MethodBitVectorAdaptiveContext)method).setParameters(
                    localNbDocuments, firstGapArraySize);
        }
        method.init(datasetReader);
    }
    
    private void initMethodsToTest(String[] subMethods, JSAPResult config, DatasetReader datasetReader) {
        // The meta method can only work with theoretical right now.
        computeTheoreticalSize = true;
        // The different methods to test. testOnInverted also tests the methods on the inverted gapList.
        testOnInverted = false;
        int nbTimesToAddEachMethod = testOnInverted ? 2 : 1;
        methodsToTest = new ArrayList<>();
        for (int i = 0; i < nbTimesToAddEachMethod; i++) {
            addMethods(subMethods, config, datasetReader, methodsToTest);
        }
        nbMethods = methodsToTest.size();
        nameByMethod = new String[nbMethods];
        sizeTotalByMethod = new double[nbMethods];
        sizeOccurrencesByMethod = new double[nbMethods];
        for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
            nameByMethod[idMethod] = methodsToTest.get(idMethod).getName();
        }
    }
    
    public static int[] invertGapArray(int[] gapArray, int nbDocuments) {
        int length = nbDocuments - gapArray.length;
        final int[] invertedGapArray = new int[length];
        if (length == 0) {
            return invertedGapArray;
        }
        int indexReal = 0;
        int indexInverted = 0;
        boolean lastGapIs1 = true;
        boolean isFirstGap = true;
        while (indexReal < gapArray.length && indexInverted < length) {
            if (gapArray[indexReal] == 1) {
                int currentGap = (isFirstGap ? 1 : 2);
                while (indexReal < gapArray.length && gapArray[indexReal] == 1) {
                    indexReal++;
                    currentGap++;
                }
                invertedGapArray[indexInverted++] = currentGap;
                lastGapIs1 = true;
            } else {
                if (!lastGapIs1) {
                    invertedGapArray[indexInverted++] = 2;
                }
                for (int nbGaps = (isFirstGap ? 1 : 2); nbGaps < gapArray[indexReal]; nbGaps++) {
                    invertedGapArray[indexInverted++] = 1;
                }
                indexReal++;
                lastGapIs1 = false;
            }
            isFirstGap = false;
        }
        if (indexInverted < length) {
            if (!lastGapIs1) {
                invertedGapArray[indexInverted++] = 2;
            }
            while (indexInverted < length) {
                invertedGapArray[indexInverted++] = 1;
            }
        }
        return invertedGapArray;
    }
    
    public static ArrayList<int[]> invertGapArrayList(ArrayList<int[]> gapArrayList, int nbDocuments) {
        final ArrayList<int[]> invertedList = new ArrayList<>();
        for (int[] gapArray : gapArrayList) {
            invertedList.add(invertGapArray(gapArray, nbDocuments));
        }
        return invertedList;
    }
    
    @Override
    public String getName() {
        return "MetaBatch";
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(nbBatches);
        out.writeDouble(frequencyStarts[0]);
        for (int i = 0; i < nbBatches; i++) {
            out.writeDouble(frequencyStops[i]);
        }
        // according to the chosen method
        // TODO: does not work as written because of automatic parameters
        // that are only those of the last batch
        for (int i = 0; i < nbBatches; i++) {
            methodByBatch[i].exportNeededInformation(out);
        }
    }
    
    @Override
    public double computeSizeNeededInformation() {
        // TODO: does not work as written because of automatic parameters that
        // are only those of the last batch
        /*
        double methodByBatchSize = 0.;
        for (int i = 0; i < nbBatches; i++) {
            methodByBatchSize += methodByBatch[i].computeSizeNeededInformation();
        }
        */
        return 32. + // nbBatches
                (1. + nbBatches) * 32. + // first frequencyStart + all the frequencyStops
                sizeOccurrences; // methodByBatchSize; // according to the chosen method
    }
    
    @Override
    public double computeSizeLengths(DatasetReader datasetReader, int nbDocuments) {
        return 32.; // nbDocuments has to be stored once
    }
    
    /*
     * This method does not really belong to the gaparrayencoding methods...
     * For now, it can only work globally.
     *
     * XXX: DO NOT CALL the 3 next methods !
     */
    @Override
    public final double computeSize(IntsRef gapArrayRef) {
        return 0.0;
    }
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) {}
    @Override
    public void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) {}
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(DatasetReader datasetReader) {
        return size;
    }
    
    /*
     * It will print the best sum (= sum of the best method on each batch --- one should
     * take into account bits for the selection of the method at each batch, not taken into account here),
     * and also the sum of each of the tested method (as if we used the same method on each batch).
     */
    @Override
    public void standardOutput() {
        System.out.println(" (occurrences: " + (sizeOccurrences / 8000000.) +
                " ; gaps: " + ((size - sizeOccurrences) / 8000000.) +
                ")");
        for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
            if (methodsToTest.get(idMethod) instanceof MethodInterpolative) {
                double sizeInterpolative = sizeTotalByMethod[idMethod];
                System.out.println("Gains w.r.t. Interpolative : " +
                        (100*(sizeInterpolative - size) / sizeInterpolative) +
                        "%");
                break;
            }
        }
        System.out.println("   For reference, sizes of each method if taken alone:");
        for (int idMethod = 0; idMethod < nbMethods; idMethod++) {
            System.out.print("   " + nameByMethod[idMethod] + " = " + (sizeTotalByMethod[idMethod] / 8000000.) +
                " (occurrences: " + (sizeOccurrencesByMethod[idMethod] / 8000000.) +
                " ; gaps: " + ((sizeTotalByMethod[idMethod] - sizeOccurrencesByMethod[idMethod]) / 8000000.) +
                ")" + (idMethod == nbMethods - 1 ? "" : "\n"));
        }
    }
    
    // TODO: when writeGapArrayList is modified...
    @Override
    public DatasetReader importFromFile(String filename) {
        return super.importFromFile(filename);
    }
    
    @Override
    public final void writeGapArrayList(DataOutputStream dataOutputStream, DatasetReader datasetReader) {
        // TODO
        // convert the gap array list to gap array list by batches, then apply
        // the correct methodByBatch method.
    }
    
}
