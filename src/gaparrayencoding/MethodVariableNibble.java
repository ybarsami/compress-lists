/**
 * Index compression with the variable nibble method.
 * (it is essentially a variation of the variable byte method where the chunks
 * are 4 bits long instead of being 8 bits long).
 *
 * Scholer, Williams, Yiannis, and Zobel, "Compression of inverted indexes for fast query evaluation" (2002)
 * Blandford, Blelloch, Cardoze, and Kadow, "Compact Representations of Simplicial Meshes in Two and Three Dimensions" (2003)
 * (there are probably earlier references).
 */

package gaparrayencoding;

import io.BitInputStream;
import io.BitOutputStream;
import static compresslists.Tools.nbBitsPerByte;

/**
 *
 * @author yann
 */
public class MethodVariableNibble extends MethodVariableXBits {
    
    /**
     * Creates a new instance of MethodVariableNibble.
     */
    public MethodVariableNibble() {
        super(nbBitsPerByte / 2);
    }
    
    @Override
    public String getName() {
        return "VariableNibble";
    }
    
    public static double computeSizeVariableNibble(int x) {
        return MethodVariableXBits.computeSizeVariableXBits(x, nbBitsPerByte / 2);
    }
    
    public static void writeCodeVariableNibble(int x, BitOutputStream bitStream) {
        MethodVariableXBits.writeCodeVariableXBits(x, bitStream, nbBitsPerByte / 2);
    }
    
    public static int readCodeVariableNibble(BitInputStream bitStream) {
        return MethodVariableXBits.readCodeVariableXBits(bitStream, nbBitsPerByte / 2);
    }

}
