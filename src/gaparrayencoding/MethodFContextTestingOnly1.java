/**
 * MethodFContextTestingOnly1.java
 * For each gapArray, here what is done:
 *    0. We note gapSizeArray the array such that
 *       gapSizeArray[i] = floor(log_2(gapArray[i])).
 *       We note bitList = bitList(gapSizeArray) and T the size of bitList.
 *    1. We code the run lengths of "0" and "1" in bitList with an arithmetic
 *       method --- nbDocuments + ceiling(log_2(nbDocuments)) symbols.
 *    2. We have now encoded what enables us to retrieve the "2" in
 *       tritList(gapArray). There remains to put the "{0,1}" from this trit
 *       list. We code them with an arithmetic contextual method --- where the
 *       context of the i-th bit is the min(i-1, k01) previous bits.
 * 
 * Difference from FContext1-2: instead of working on tritList(gapSizeArray), we
 * work on bitList(gapSizeArray).
 * 
 * NB: This method was designed just for testing purposes, and is now discarded
 * in tests as it takes more space than MethodFContextTestingOnly2 which, in
 * turn, takes more space than MethodContextBit --- those two methods also work
 * on bitList(gapSizeArray).
 */

package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStreamFile;
import io.BitOutputStreamFile;
import tritlistencoding.TritSequence;
import static compresslists.Tools.nbBits;
import static compresslists.Tools.nbBitsPerByte;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public class MethodFContextTestingOnly1 extends MethodByArithmeticCoding {
    
    // Size of the occurrence arrays.
    private int contextSize01;
    private int nbSymbols;
    
    // Occurrences arrays.
    private long[] occurrences;
    private int[] occurrencesCumulative;
    private long[][] occurrences01;
    private int[][] occurrencesCumulative01;
    
    // To separate the different sizes in the index.
    private double sizeArit;
    private double size01;
    
    // Paarmeters.
    private int k01;
    
    private int nbBitsPerOccurrence;
    private double nbBitsPerRun;
    
    private TritSequence tritSequence = new TritSequence();
    private BitSequence bitSequence = new BitSequence();
    
    /**
     * Creates a new instance of MethodFContextTestingOnly1.
     */
    public MethodFContextTestingOnly1(int k01, int nbDocuments) {
        this.nbDocuments = nbDocuments;
        setParameter(k01);
    }
    
    public final void setParameter(int k01) {
        this.k01 = k01;
        // We can have a list of "1" as long as nbDocuments.
        // The list of "0" can only be as long as ceiling(log_2(nbDocuments)).
        nbSymbols = nbDocuments + Tools.ceilingLog2(nbDocuments);
        contextSize01 = nbBits * (1 - (1 << (k01 + 1))) / (1 - nbBits);
    }
    
    @Override
    public void initBefore() {
        allocateOccurrences();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        ArrayList<Object> nullData = new ArrayList<>();
        doSomethingWithGapArray(gapArrayRef,
                new UpdateOccurrenceArray(), nullData,
                new Update01OccurrenceTensor(), nullData);
    }
    
    @Override
    public void initAfter() {
        convertToOccurrenceIntervals();
        
        // Computes the number of bits necessary to store all the occurrences.
        int nbBitsMax = 0;
        for (int runLength = 0; runLength < nbSymbols; runLength++) {
            if (occurrences[runLength] > 0 && Tools.ceilingLog2((int)occurrences[runLength]) > nbBitsMax) {
                nbBitsMax = Tools.ceilingLog2((int)occurrences[runLength]);
            }
        }
        nbBitsPerOccurrence = nbBitsMax;
        
        // Occurrences of "1".
        // System.out.println(Arrays.toString(Arrays.copyOfRange(occurrences, 0, nbDocuments)));
        // Occurrences of "0".
        // System.out.println(Arrays.toString(Arrays.copyOfRange(occurrences, nbDocuments, arraySize)));
        
        // Compute the number of bits needed per run.
        nbBitsPerRun = 0.;
        for (int runLength = 0; runLength < nbSymbols; runLength++) {
            double pi = (double)occurrences[runLength] / ((double)occurrencesCumulative[nbSymbols]);
            nbBitsPerRun += -pi * Tools.log2(pi);
        }
    }
    
    private void convertToOccurrenceIntervals() {
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        OccurrenceTools.ensureTotalAndAccumulate(occurrences01, occurrencesCumulative01);
    }
    
    public interface MyFunction {
        public void execute(ArrayList<Object> data);
    }
    
    /*
     * This is the general function that takes a gapArray as parameter, and does
     * two things:
     *     1. a gapSize phase (function methodGapSize, with parameters myObjectsGapSize),
     *        to encode the bits of the tritList(gapSizeArray) where {0,1} -> 0 and {2} -> 1.
     *     2. a bitList phase (function methodBitList, with parameters myObjectsBitList),
     *        which encodes the list of {0,1} that is inside tritList(gapArray).
     */
    public final void doSomethingWithGapArray(IntsRef gapArrayRef,
            MyFunction methodGapSize, ArrayList<Object> myObjectsGapSize,
            MyFunction methodBitList, ArrayList<Object> myObjectsBitList) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        Tools.tritSequence2intSizeArray(tritSequence, gapArrayRef);
        // Here, the gapArray is in fact the gapSizeArray
        
        // Phase 1.
        Tools.gapArray2bitVector(gapArrayRef, bitSequence);
        int nbBitsWritten = 0;
        int nbBitsToEncode = bitSequence.size();
        int currentRunBit = 0;
        int currentRunSize = 0;
        while (nbBitsWritten < nbBitsToEncode) {
            int currentBit = bitSequence.get(nbBitsWritten);
            if (currentBit == currentRunBit) {
                currentRunSize++;
            } else {
                if (currentRunSize > 0) {
                    ArrayList<Object> data = new ArrayList<>();
                    data.add(currentRunBit); // currentRunBit will always be data.get(0)
                    data.add(currentRunSize); // currentRunSize will always be data.get(1)
                    data.addAll(myObjectsGapSize); // then we add the other parameters --- if there are.
                    methodGapSize.execute(data);
                }
                currentRunBit = currentBit;
                currentRunSize = 1;
            }
            nbBitsWritten++;
        }
        if (currentRunSize > 0) {
            // Encode the last run.
            ArrayList<Object> data = new ArrayList<>();
            data.add(currentRunBit); // currentRunBit will always be data.get(0)
            data.add(currentRunSize); // currentRunSize will always be data.get(1)
            data.addAll(myObjectsGapSize); // then we add the other parameters --- if there are.
            methodGapSize.execute(data);
        }
        
        // Phase 2.
        nbBitsWritten = 0;
        MethodTrits.tritSequence2b01(tritSequence, bitSequence);
        nbBitsToEncode = bitSequence.size();
        // index01 is a bijection between k01-uplets of bits and { 0, ..., 2^k01 - 1 }.
        int index01 = 0;
        int nbIndexes01Before = 0;
        while (nbBitsWritten < nbBitsToEncode) {
            int currentBit = bitSequence.get(nbBitsWritten);
            ArrayList<Object> data = new ArrayList<>();
            data.add(index01); // index01 will always be data.get(0)
            data.add(currentBit); // currentBit will always be data.get(1)
            data.addAll(myObjectsBitList); // then we add the other parameters --- if there are.
            methodBitList.execute(data);
            nbBitsWritten++;
            if (nbBitsWritten <= k01) {
                nbIndexes01Before = nbBits * (1 - (1 << nbBitsWritten)) / (1 - nbBits);
            }
            // Update the index for the next iteration.
            index01 = nbIndexes01Before + ((nbBits * index01 + currentBit) % (1 << k01));
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the general function: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Size of the general arithmetic method. Automatically updated when we change the occurrences used.
     */
    public class UpdateSizeArithmeticCode implements MyFunction {
        @Override
        public void execute(ArrayList<Object> data) {
            // In this function, we don't need currentRunBit which is data.get(0)
            // In this function, we don't need currentRunSize which is data.get(1)
            double[] oldSize = (double[])data.get(2);
            oldSize[0] += nbBitsPerRun;
        }
    }
    
    /*
     * Size of the bit list array.
     */
    public class UpdateSizeBitListCode implements MyFunction {
        @Override
        public void execute(ArrayList<Object> data) {
            int index01 = (int)data.get(0);
            // In this function, we don't need currentBit which is data.get(1)
            double[] oldSize = (double[])data.get(2);
            for (int bit = 0; bit < nbBits; bit++) {
                double pz = ((double)occurrences01[index01][bit]) / ((double)occurrencesCumulative01[index01][nbBits]);
                oldSize[0] += -pz * Tools.log2(pz);
            }
        }
    }
    
    /*
     * The final function which uses the previous ones. We store the result of the
     * size in an array of one double --- to be updated when passed as parameter.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        double[] resultArit = new double[] { 0.0 };
        ArrayList<Object> dataArit = new ArrayList<>();
        dataArit.add(resultArit);
        double[] result01 = new double[] { 0.0 };
        ArrayList<Object> data01 = new ArrayList<>();
        data01.add(result01);
        doSomethingWithGapArray(gapArrayRef,
                new UpdateSizeArithmeticCode(), dataArit,
                new UpdateSizeBitListCode(), data01);
        double sizeAritLocal = ((double[])dataArit.get(0))[0];
        double size01Local = ((double[])data01.get(0))[0];
        sizeArit += sizeAritLocal;
        size01 += size01Local;
        return sizeAritLocal + size01Local;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeArit = 0.;
        size01   = 0.;
        super.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        bitSequence = new BitSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; gapSizeArray: " + (sizeArit / 8000000) +
                " ; 01: " + (size01 / 8000000) +
                ")");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the general function: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Write the code according to the general arithmetic method. Automatically updated when we change the occurrences used.
     */
    public class WriteArithmeticCode implements MyFunction {
        @Override
        public void execute(ArrayList<Object> data) {
            int currentRunBit = (int)data.get(0);
            int currentRunSize = (int)data.get(1);
            int metaIndex = currentRunBit == 1
                    ? currentRunSize - 1
                    : nbDocuments + currentRunSize - 1;
            encoder.update(occurrencesCumulative, metaIndex);
        }
    }
    
    /*
     * Write the code according to the 01 arithmetic method. Automatically updated when we change the occurrences used.
     */
    public class WriteBitListCode implements MyFunction {
        @Override
        public void execute(ArrayList<Object> data) {
            int index01 = (int)data.get(0);
            int currentBit = (int)data.get(1);
            encoder.update(occurrencesCumulative01[index01], currentBit);
        }
    }
    
    /*
     * The final function which uses the previous ones. We store the result of the
     * index in the buffer.
     */
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        ArrayList<Object> nullData = new ArrayList<>();
        doSomethingWithGapArray(gapArrayRef,
                new WriteArithmeticCode(), nullData,
                new WriteBitListCode(), nullData);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the general function: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Update the occurrences for the general arithmetic method. Automatically updated when we change the context used.
     */
    public class UpdateOccurrenceArray implements MyFunction {
        @Override
        public void execute(ArrayList<Object> data) {
            int currentRunBit = (int)data.get(0);
            int currentRunSize = (int)data.get(1);
            int metaIndex = currentRunBit == 1
                    ? currentRunSize - 1
                    : nbDocuments + currentRunSize - 1;
            occurrences[metaIndex]++;
        }
    }
    
    /*
     * Update the occurrences for the 01 arithmetic method.
     */
    public class Update01OccurrenceTensor implements MyFunction {
        @Override
        public void execute(ArrayList<Object> data) {
            int index01 = (int)data.get(0);
            int currentBit = (int)data.get(1);
            occurrences01[index01][currentBit]++;
        }
    }
    
    private void allocateOccurrences() {
	occurrences = new long[nbSymbols];
	occurrences01 = OccurrenceTools.allocateOccurrences(contextSize01, nbBits);
	occurrencesCumulative = new int[nbSymbols + 1];
	occurrencesCumulative01 = OccurrenceTools.allocateOccurrencesCumulative(contextSize01, nbBits);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String getName() {
        return "ContextLengthTestingOnly1_kF" + k01;
    }
    
    private int getNbBitsWritten() {
        return GammaEncoding.computeSizeGamma(nbBitsPerOccurrence) +
                nbSymbols * nbBitsPerOccurrence;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 32 + // k01
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte) + // occurrenceMatrix
                contextSize01 * 32; // occurrence01Tensor
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeInt(k01);
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            // export occurrenceMatrix (for the run lengths)
            for (int runLength = 0; runLength < nbSymbols; runLength++) {
                BinaryEncoding.writeCodeBinary((int)occurrences[runLength], bitStream, nbBitsPerOccurrence);
            }
            // Padding
            bitStream.close();
            // export occurrence01Tensor
            for (int context = 0; context < contextSize01; context++) {
                for (int bit = 0; bit < nbBits; bit++) {
                    out.writeInt((int)occurrences01[context][bit]);
                }
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            setParameter(in.readInt());
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            allocateOccurrences();
            // import occurrenceMatrix (for the run lengths)
            for (int runLength = 0; runLength < nbSymbols; runLength++) {
                occurrences[runLength] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
            }
            // import occurrence01Tensor
            for (int context = 0; context < contextSize01; context++) {
                for (int bit = 0; bit < nbBits; bit++) {
                    occurrences01[context][bit] = in.readInt();
                }
            }
            convertToOccurrenceIntervals();
        }
    }
    
    /*
     * This is the reverse of the updateBitSequenceWithGapArray function. It reads nbGapsLocal
     * gaps in the buffer BitStream, according to the inverse of the encoding function.
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        bitSequence.reset();
        int nbGapsRead = 0;
        int nbBitsRead = 0;
        while (nbGapsRead < nbGapsLocal) {
            int currentSymbol = decoder.read(occurrencesCumulative);
            if (currentSymbol < nbDocuments) {
                final int nbOneRead = currentSymbol + 1;
                for (int i = 0; i < nbOneRead; i++) {
                    bitSequence.add(1);
                }
                nbBitsRead += nbOneRead;
                nbGapsRead += nbOneRead;
            } else {
                final int nbZeroRead = currentSymbol + 1 - nbDocuments;
                for (int i = 0; i < nbZeroRead; i++) {
                    bitSequence.add(0);
                }
                nbBitsRead += nbZeroRead;
            }
        }
        Tools.bitVector2gapArray(bitSequence, gapArrayRef);
        // Here, the gapArray is in fact the gapSizeArray
        tritSequence.reset();
        nbBitsRead = 0;
        // index01 is a bijection between k01-uplets of bits and { 0, ..., 2^k01 - 1 }.
        int index01 = 0;
        int nbIndexes01Before = 0;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            // gapSizeArray[i] stores 1 + the size in bits of the i-th gap, as
            // it should be stored in a trit sequence, cf. Tools.tritSequence2intSizeArray.
            for (int j = 0; j < gapArrayRef.ints[idGap] - 1; j++) {
                int currentBit = decoder.read(occurrencesCumulative01[index01]);
                nbBitsRead++;
                if (nbBitsRead <= k01) {
                    nbIndexes01Before = nbBits * (1 - (1 << nbBitsRead)) / (1 - nbBits);
                }
                tritSequence.add(currentBit);
                // Update the index for the next iteration.
                index01 = nbIndexes01Before + ((nbBits * index01 + currentBit) % (1 << k01));
            }
            tritSequence.add(2);
        }
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }

}
