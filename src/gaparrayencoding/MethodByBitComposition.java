/*
 * MethodByBitComposition.java
 * For each gapArray, here what is done:
 *    0. We note bitSequence = gapMethod.toBitSequence(gapArray).
 *    1. With a BitSequence method, we encode bitSequence.
 *
 * XXX: Not invertible! For testing purposes only.
 */
package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodByBitComposition extends MethodByBitSequence {
    
    protected MethodByBitSequence gapMethod;
    protected bitlistencoding.Method bMethod;
    
    private BitSequence bitSequence = new BitSequence();
    
    /**
     * Creates a new instance of MethodByBitComposition.
     */
    public MethodByBitComposition() {}
    public MethodByBitComposition(MethodByBitSequence gapMethod, bitlistencoding.Method bMethod) {
        this.gapMethod = gapMethod;
        this.bMethod = bMethod;
        try {
            if (!gapMethod.getClass().getMethod("init", IntsRef.class).getDeclaringClass().equals(Method.class)) {
                throw new RuntimeException("MethodByBitComposition cannot be used with a gapMethod that needs an initialization.");
            }
        } catch (NoSuchMethodException e) {}
    }
    
    /*
     * Further optimization: here, we override init(IntsRef), so needsInit()
     * from Method.java will return true. Here, we return false if the bMethod
     * does not override init(BitSequence).
     */
    @Override
    public boolean needsInit() {
        try {
            return !bMethod.getClass().getMethod("init", BitSequence.class).getDeclaringClass().equals(bitlistencoding.Method.class);
        } catch (NoSuchMethodException e) {
            return true;
        }
    }
    
    @Override
    public void initBefore() {
        bMethod.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        bitSequence = gapMethod.gapArray2bitSequence(gapArrayRef);
        bMethod.init(bitSequence);
    }
    
    @Override
    public void initAfter() {
        bMethod.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        bitSequence = gapMethod.gapArray2bitSequence(gapArrayRef);
        int nbOnes = bitSequence.numberOfOnes();
        return
//                MethodVariableByte.computeSizeVariableByte(nbOnes) +
                bMethod.computeSize(bitSequence);
    }
    
    @Override
    public void computeSizeBefore() {
        bMethod.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        bitSequence = new BitSequence();
        return bMethod.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        bMethod.standardOutput();
    }
    
    @Override
    public final void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        bitSequence = gapMethod.gapArray2bitSequence(gapArrayRef);
        int nbOnes = bitSequence.numberOfOnes();
//        MethodVariableByte.writeCodeVariableByte(nbOnes, bitStream);
        bMethod.writeBitSequence(bitStream, bitSequence);
    }
    
    @Override
    public String getName() {
        return "ByBitComposition(" + gapMethod.getName() + ", " + bMethod.getName() + ")";
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        bitSequence = new BitSequence();
        bMethod.outputRemainingBits(bitStream);
    }
    
    @Override
    public final void flushRemainingBits() {
        bMethod.flushRemainingBits();
    }
    
    public static final double computeSizeGapMethod(MethodByBitSequence gapMethod) {
        return
                8. +
                gapMethod.getClass().getName().length() * 16. +
                gapMethod.computeSizeNeededInformation();
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                computeSizeGapMethod(gapMethod) +
                MethodByBitVector.computeSizeBitMethod(bMethod);
    }
    
    public static final void exportGapMethod(MethodByBitSequence gapMethod, DataOutputStream out) throws IOException {
        String gapMethodName = gapMethod.getClass().getName();
        out.writeByte(gapMethodName.length());
        out.writeChars(gapMethodName);
        gapMethod.exportNeededInformation(out);
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        exportGapMethod(gapMethod, out);
        MethodByBitVector.exportBitMethod(bMethod, out);
    }
    
    public static final MethodByBitSequence importGapMethod(DataInputStream in) throws IOException {
        byte gapMethodNameLength = in.readByte();
        String gapMethodName = "";
        for (int i = 0; i < gapMethodNameLength; i++) {
            gapMethodName += in.readChar();
        }
        try {
            Class methodClass = Class.forName(gapMethodName);
            MethodByBitSequence gapMethod = (MethodByBitSequence)methodClass.newInstance();
            gapMethod.importNeededInformation(in);
            return gapMethod;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
            System.out.println(gapMethodName + " is not a valid method by bit sequence for CompressLists.");
            return null;
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        gapMethod = importGapMethod(in);
        bMethod = MethodByBitVector.importBitMethod(in);
    }
    
    @Override
    public final void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int nbOnes = nbGapsLocal;//MethodVariableByte.readCodeVariableByte(bitStream);
        bMethod.readBitSequence(bitStream, nbOnes, bitSequence);
        int[] gapArray = gapMethod.bitSequence2gapArray(bitSequence, nbGapsLocal);
        gapArrayRef.ints = gapArray;
    }
    
}
