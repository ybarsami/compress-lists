/**
 * Index compression with the delta method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 117
 * A further development is the \delta code, in which the prefix indicating
 * the number of binary suffix bits is represented by the \gamma code rather
 * than the unary code. Taking the same example of x = 9, the unary prefix
 * of 1110 coding 4 is replaced by 11000, the \gamma code for 4. That is,
 * the \delta code for x = 9 is 11000 001.
 */

package gaparrayencoding;

import integerencoding.DeltaEncoding;
import io.BitInputStream;
import io.BitOutputStream;

/**
 *
 * @author yann
 */
public class MethodDelta extends MethodByElement {
    
    /**
     * Creates a new instance of MethodDelta.
     */
    public MethodDelta() {}
    
    @Override
    public String getName() {
        return "Delta";
    }
    
    @Override
    public double computeSize(int x) {
        return DeltaEncoding.computeSizeDelta(x);
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        DeltaEncoding.writeCodeDelta(x, bitStream);
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return DeltaEncoding.readCodeDelta(bitStream);
    }

}
