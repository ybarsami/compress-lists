/*
 * For each gapArray, here what is done:
 *    0. We split the gapArray by sub-gapArrays. The sizes of the sub-gapArrays
 * is computed optimally according to dynamic programming.
 *    1. With a GapArray method, we encode those sub-gapArrays.
 *
 * Remark: As a difference with MethodByBlockNoPadding.java, the sub-gapArrays
 * do not have constant size. As a difference with MethodByBlock.java, between
 * two consecutive sub-gapArrays, the padding is removed.
 *
 * Buchsbaum, Fowler, and Giancarlo, "Improving Table Compression with Combinatorial Optimization" (2003), p.830
 * 2.1. SOLUTIONS WITHOUT REORDERING. In the general case, irrespective of
 * whether combinatorial dependence is an equivalence relation, we can solve
 * Problem 2.1 by dynamic programming. Let E[i] be the cost of an optimal,
 * contiguous partition of variables x_1, ... , x_i. E[n] is thus the cost of a
 * solution to Problem 2.1. Define E[0] = 0; then, for 1 ≤ i ≤ n,
 * E[i] =  min E[j] + H(x_{j+1}, ..., x_i).
 *        0≤j<i
 * The actual partition with cost E[n] can be maintained by standard dynamic
 * programming backtracking.
 *
 * Ottaviano and Venturini, "Partitioned Elias-Fano Indexes" (2014), p.277
 * An optimal partition can be computed in Θ(n^2) time and space by solving a
 * variant of dynamic programming recurrence introduced in [2003].
 * => in fact, as is the case in this file, it is Θ(n^2) in time but linear in
 *    space.
 */
package gaparrayencoding;

import arrays.IntsRef;
import integerencoding.BinaryEncoding;
import integerencoding.UnaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;
import static gaparrayencoding.MethodEliasFano.computeSizeEliasFano;

/**
 *
 * @author yann
 */
public class MethodQuasiSuccinctOptBlock extends MethodQuasiSuccinct {
    
    private static MethodByElement nbDocumentsMethod = new MethodDelta();
    
    private static class OptimalPartition {
        
        public int indexBegin;
        public double cost;
        public boolean eliasFanoWasChosen;
        public OptimalPartition previousPartition;
        
        OptimalPartition(int indexBegin, double cost, boolean eliasFanoWasChosen, OptimalPartition previousPartition) {
            this.indexBegin = indexBegin;
            this.cost = cost;
            this.eliasFanoWasChosen = eliasFanoWasChosen;
            this.previousPartition = previousPartition;
        }
        
        OptimalPartition() {
            this(-1, 0., true, null);
        }
        
    }
    
    private static class EFBlock {
        // A block of integers encoded with the Quasi-Succinct indices is as
        // follows:
        // * first, we encode the number of documents in the block
        // * then, a bit tells whether the Elias-Fano or the BitVector is better
        //   to encode the block
        // * finally, we encode with Elias-Fano or BitVector
        public IntsRef documentArrayRef;
        public double costNbDocuments;
        public double costEF;
        public double costBV;
        // Hence, cost is always computed as:
        // * cost of encoding(number of document) +
        // * 1 +
        // * min(cost of Elias-Fano, cost of BitVector).
        public double cost;
        // The baseDocId of the block is equal to the last docId of the previous
        // block (or 0 if this is the first block).
        public int baseDocId;
        // The universe of the block is equal to the last docId to encode minus
        // baseDocId.
        public int universe;
        
        public EFBlock(int[] documentArray, int offset) {
            int nbDocuments = 1;
            this.documentArrayRef = new IntsRef(documentArray, offset, nbDocuments);
            this.baseDocId = offset == 0 ? 0 : documentArray[offset - 1];
            this.universe = documentArray[offset + nbDocuments - 1] - baseDocId - nbDocuments;
            this.costNbDocuments = nbDocumentsMethod.computeSize(nbDocuments);
            this.costEF = universeMethod.computeSize(universe + 1);
            this.costBV = UnaryEncoding.computeSizeUnary(documentArray[offset] - baseDocId);
            this.cost = 1. + costNbDocuments + Math.min(costEF, costBV);
        }
        
        public void updateWithNewDocument() {
            int oldUniverse = universe;
            int oldNbDocuments = documentArrayRef.length;
            documentArrayRef.length++;
            int nbDocuments = documentArrayRef.length;
            int offset = documentArrayRef.offset;
            universe = documentArrayRef.ints[offset + nbDocuments - 1] - baseDocId - nbDocuments;
            costNbDocuments = nbDocumentsMethod.computeSize(nbDocuments);
            if (universe == 0) {
                // A universe of 0 means that oldUniverse is 0 also.
                // In that case, we can avoid computing costEF, it is 0.
            } else {
                int l = computeL(universe - 1, nbDocuments - 1);
                if (oldNbDocuments == 1 || oldUniverse == 0 || computeL(oldUniverse - 1, oldNbDocuments - 1) != l) {
                    // When newL != oldL, we have to recompute the whole size.
                    documentArrayRef.length--;
                    costEF =
                            universeMethod.computeSize(universe + 1) +
                            computeSizeEliasFano(documentArrayRef, universe - 1, true, baseDocId);
                    documentArrayRef.length++;
                } else {
                    // When newL == oldL, we can avoid recomputing the whole size.
                    int highNumber         = oldUniverse >> l;
                    int previousHighNumber = (documentArrayRef.ints[offset + oldNbDocuments - 2] - baseDocId - (oldNbDocuments - 1)) >> l;
                    int highGap = highNumber - previousHighNumber;
                    costEF +=
                            (universeMethod.computeSize(universe + 1) -
                            universeMethod.computeSize(oldUniverse + 1)) +
                            l + integerencoding.UnaryEncoding.computeSizeUnary(highGap + 1);
                }
            }
            costBV += UnaryEncoding.computeSizeUnary(documentArrayRef.ints[offset + nbDocuments - 1] - documentArrayRef.ints[offset + nbDocuments - 2]);
            cost = 1. + costNbDocuments + Math.min(costEF, costBV);
        }
    }
    
    // At the iteration i of the algorithm, we define, for 0 ≤ j ≤ i,
    // optimalPartition[j] as the optimal partition, i.e. ths optimal splitting
    // of the documentArray { x_0, ..., x_{j-1} } into an arbitrary number of
    // blocks and applying Elias-Fano on those blocks, and its cost.
    // optimalPartitionCost[0] is defined as ( {}, 0 ).
    private OptimalPartition[] optimalPartition = new OptimalPartition[1];
    // At the iteration i of the algorithm, we define, for 0 ≤ j < i,
    // lastEFBlock[j] as the documentArray { x_j, ..., x_{i - 1} } and its
    // associated properties to easily compute its Elias-Fano cost.
    private EFBlock[] lastEFBlock = new EFBlock[0];
    
    /**
     * Creates a new instance of MethodQuasiSuccinctOptBlock.
     */
    public MethodQuasiSuccinctOptBlock() {
        super(1);
    }
    
    private void computeOptimalPartition(IntsRef documentArrayRef) {
        int[] documentArray = documentArrayRef.ints;
        int nbDocumentsLocal = documentArrayRef.length;
        if (lastEFBlock.length < nbDocumentsLocal) {
            optimalPartition = new OptimalPartition[nbDocumentsLocal + 1];
            lastEFBlock = new EFBlock[nbDocumentsLocal];
        }
        optimalPartition[0] = new OptimalPartition();
        for (int i = 1; i <= nbDocumentsLocal; i++) {
            // Create the last block, the singleton block { x_{i-1} }.
            lastEFBlock[i - 1] = new EFBlock(documentArray, i - 1);
            // Add { x_{i-1} } to each of the other blocks.
            for (int j = 0; j < i - 1; j++) {
                lastEFBlock[j].updateWithNewDocument();
            }
            // Computation of E[i] = min E[j] + H(x_{j+1}, ..., x_i).
            //                      0≤j<i
            boolean eliasFanoWasChosen = lastEFBlock[0].costEF <= lastEFBlock[0].costBV;
            optimalPartition[i] = new OptimalPartition(0, lastEFBlock[0].cost, eliasFanoWasChosen, optimalPartition[0]);
            for (int j = 0; j < i; j++) {
                double jThCost = optimalPartition[j].cost + lastEFBlock[j].cost;
                if (jThCost < optimalPartition[i].cost) {
                    optimalPartition[i].indexBegin = j;
                    optimalPartition[i].cost = jThCost;
                    optimalPartition[i].eliasFanoWasChosen = lastEFBlock[j].costEF <= lastEFBlock[j].costBV;
                    optimalPartition[i].previousPartition = optimalPartition[j];
                }
            }
        }
    }
    
    @Override
    protected double computeSizeDocumentArray(IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        computeOptimalPartition(documentArrayRef);
        return optimalPartition[nbDocumentsLocal].cost;
    }
    
    @Override
    public double computeSizeAfter() {
        documentArrayRef = new IntsRef(0);
        return super.computeSizeAfter();
    }
    
    @Override
    public String getName() {
        return "QuasiSuccinctOptBlock";
    }
    
    @Override
    protected void writeDocumentArray(BitOutputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        computeOptimalPartition(documentArrayRef);
        writePartition(bitStream, optimalPartition[nbDocumentsLocal], nbDocumentsLocal);
    }
    
    private void writePartition(BitOutputStream bitOutputStream, OptimalPartition partition, int previousIndexBegin) {
        if (partition.previousPartition != null) {
            int indexBegin = partition.indexBegin;
            // First, the recursion.
            writePartition(bitOutputStream, partition.previousPartition, indexBegin);
            // Then, write the block.
            // (this ensures that the partitions are written in ascending order,
            // because they are constructed in reverse order).
            int nbDocumentsLocal = previousIndexBegin - indexBegin;
            int baseDocId = indexBegin == 0 ? 0 : documentArrayRef.ints[indexBegin - 1];
            int universe = documentArrayRef.ints[previousIndexBegin - 1] - baseDocId - nbDocumentsLocal;
            nbDocumentsMethod.writeCode(nbDocumentsLocal, bitOutputStream);
            documentArrayRef.offset = indexBegin;
            documentArrayRef.length = nbDocumentsLocal;
            BinaryEncoding.writeCodeBinary(partition.eliasFanoWasChosen ? 1 : 0, bitOutputStream, 1);
            if (partition.eliasFanoWasChosen) {
                universeMethod.writeCode(universe + 1, bitOutputStream);
                if (nbDocumentsLocal > 1 && universe != 0) {
                    documentArrayRef.length--;
                    writeIntArrayEliasFano(bitOutputStream, documentArrayRef, universe - 1, true, baseDocId);
                }
            } else {
                methodBitVector.writeDocumentArray(bitOutputStream, documentArrayRef, baseDocId);
            }
        }
    }
    
    @Override
    protected void readDocumentArray(BitInputStream bitStream, IntsRef documentArrayRef) {
        int nbDocumentsLocal = documentArrayRef.length;
        int indexBegin = 0;
        int baseDocId = 0;
        while (indexBegin < nbDocumentsLocal) {
            int nbDocumentsBlock = nbDocumentsMethod.readCode(bitStream);
            boolean eliasFanoWasChosen = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            if (eliasFanoWasChosen) {
                int universe = universeMethod.readCode(bitStream) - 1;
                if (nbDocumentsBlock > 1) {
                    if (universe == 0) {
                        // When universe is 0, it means all the gaps are 1.
                        for (int i = 0; i < nbDocumentsBlock - 1; i++) {
                            documentArrayRef.ints[indexBegin + i] = baseDocId + i + 1;
                        }
                    } else {
                        if (nbDocumentsBlock - 1 > lowInts.length) {
                            lowInts = new int[nbDocumentsBlock - 1];
                        }
                        readIntArrayEliasFano(bitStream, nbDocumentsBlock - 1, indexBegin, documentArrayRef.ints, universe - 1, baseDocId, lowInts, true);
                    }
                }
                baseDocId += universe + nbDocumentsBlock;
                documentArrayRef.ints[indexBegin + nbDocumentsBlock - 1] = baseDocId;
            } else {
                IntsRef gapArrayRefBV = new IntsRef(nbDocumentsBlock);
                methodBitVector.readGapArray(bitStream, gapArrayRefBV);
                int deltaBaseDocId = gapArrayRefBV.ints[0];
                documentArrayRef.ints[indexBegin] = gapArrayRefBV.ints[0] + baseDocId;
                for (int i = 1; i < nbDocumentsBlock; i++) {
                    deltaBaseDocId += gapArrayRefBV.ints[i];
                    documentArrayRef.ints[indexBegin + i] = gapArrayRefBV.ints[i] + documentArrayRef.ints[indexBegin + i - 1] ;
                }
                baseDocId += deltaBaseDocId;
            }
            indexBegin += nbDocumentsBlock;
        }
    }
    
}
