/*
 * Index compression with a method derived from the Packed+ANS2 method.
 *
 * Moffat & Petri, "Index Compression Using Byte-Aligned ANS Coding and Two-Dimensional Contexts" (2018)
 *
 * Remark: The differences with the original Packed+ANS2 method are:
 * 1. we use arithmetic coding, and not ANS coding.
 * 2. we did not optimize the RAM usage --- there is still a huge array of size
 *    2^S[S.length - 1] --- here, 2^26 --- allocated for the probabilities
 *    (hence, we do not use ``ANSmsb'' in section 3.3).
 * 3. we keep all the 16 * 17 / 2 = 136 contexts instead of only 64. The use of
 *    64 contexts in the original paper is done in order to keep the number of
 *    bits used for the context as low as 6 (instead of 8), thus gaining 2 bits
 *    per block. Here, we code the selectors for contexts with an arithmetic
 *    coder, which takes less bits per block (e.g.: between 4.5 and 4.9 bits per
 *    selector on mail datasets, 4.6 bits per selector on Gov2), and permits to
 *    distinguish more contexts, thus gaining space also on the compression of
 *    the gaps.
 */
package gaparrayencoding;

import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStreamFile;
import io.BitOutputStreamFake;
import io.BitOutputStreamFile;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodPackedArithmetic extends MethodByArithmeticCoding {
    
    private BitOutputStreamFake bitCounter = new BitOutputStreamFake();
    private ArithmeticEncoder counter;
    
    // To count the number of blocks used.
    private double size;
    private int nbBlocks;
    
    // Moffat & Petri, "Index Compression Using Byte-Aligned ANS Coding and Two-Dimensional Contexts" (2018)
    // Equation (1), p. 406
    // WARNING: This choice of S is only valid if all gap values are <= 2^26.
    // Remark: in the paper, this array has in fact a 25 as last element
    // (instead 26).
    // Remark: in ans_packed_util.hpp, this array has in fact a 32 as last
    // element (instead 26, not in addition to it).
    // Remark: For the dataset ClueWeb09 sorted by url from the IC2014 datasets,
    // the last value must be at least 26 as there is a value > 2^25.
    private final static int[] S = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 19, 22, 26 };
    private final static int maxValue[] = Arrays
            .stream(S)
            .map(i -> 1 << i)
            .toArray();
    
    // Size of the occurrence arrays.
    private final int arraySize = nbSelectorValues();
    
    // Occurrence arrays.
    // First, count the occurrences of the couples (median, max)
    private long[] occurrencesMedMax;
    private int[] occurrencesCumulativeMedMax;
    // Then, count the occurrences, for each couple (median, max), of the gaps
    // which have a given magnitude.
    private long[][] occurrencesMergedMagnitude;
    // The previous occurrence counts are converted for all values to encode
    private int[][] occurrencesCumulativeGaps;
    
    private int nbBitsPerOccurrence;
    
    // The block size is set to 128 in the paper, but to 256 in their
    // implementation.
    private final int BLOCK_SIZE = 128;
    private final int[] subGapArray = new int[BLOCK_SIZE];
    
    /**
     * Creates a new instance of MethodPackedArithmetic.
     */
    public MethodPackedArithmetic() {}
    
    @Override
    public String getName() {
        return "PackedArithmetic_" + BLOCK_SIZE;
    }
    
    private int findSelector(int n) {
        for (int i = 0; i < S.length; i++) {
            if (n <= maxValue[i]) {
                return i;
            }
        }
        throw new RuntimeException(
                n + " is too big a value for the current choice of S = " +
                Arrays.toString(S) + ".");
    }
    
    @Override
    public void initBefore() {
        allocateOccurrenceArrays();
    }
    
    private void allocateOccurrenceArrays() {
        occurrencesMedMax = new long[arraySize];
	occurrencesCumulativeMedMax = new int[arraySize + 1];
        occurrencesMergedMagnitude = new long[arraySize][];
        for (int selector = 0; selector < arraySize; selector++) {
            int selectorMax = computeSelectorMax(selector);
            occurrencesMergedMagnitude[selector] = new long[S[selectorMax] + 1];
        }
        occurrencesCumulativeGaps = new int[arraySize][];
        for (int selector = 0; selector < arraySize; selector++) {
            int selectorMax = computeSelectorMax(selector);
            occurrencesCumulativeGaps[selector] = new int[maxValue[selectorMax] + 1];
        }
    }
    
    /*
     * Given a gapArray [g_0, ... g_{length-1}], compute the maximum value of
     * the g_i's and their median value.
     * This gives us S.length * S.length / 2 different couples (max:med),
     * because med <= max.
     * We output a selector value inside S.length^2 because it leads to easier
     * formulae for the bijection (max, med) <-> selector.
     * If one cares about memory consumption of the occurrences arrays (that are
     * allocated as arrays of arrays, of size S.length^2 * 2^S[S.length - 1],
     * it is possible to change the three following functions.
     */
    private int computeSelector(int[] gapArray, int length) {
        Arrays.sort(gapArray, 0, length);
        int max = gapArray[length - 1];
        int med = length % 2 == 0
                ? (gapArray[length / 2 - 1] + gapArray[length / 2]) / 2
                : gapArray[length / 2];
        int selectorMax = findSelector(max);
        int selectorMed = findSelector(med);
        return selectorMax * S.length + selectorMed;
    }
    
    private int computeSelectorMax(int selector) {
        return selector / S.length;
    }
    
    private int nbSelectorValues() {
        return S.length * S.length;
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocksLocal = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocksLocal; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            for (int i = 0; i < curBlockSize; i++) {
                subGapArray[i] = gapArrayRef.ints[offset + idBlock * BLOCK_SIZE + i];
            }
            int selector = computeSelector(subGapArray, curBlockSize);
            occurrencesMedMax[selector]++;
            if (selector > 0) {
                // If selector is 0, all the gaps are necessarily 1. No need to count.
                for (int i = 0; i < curBlockSize; i++) {
                    occurrencesMergedMagnitude[selector][Tools.ceilingLog2(subGapArray[i])]++;
                }
            }
        }
    }
    
    @Override
    public void initAfter() {
        // If selector is 0, all the gaps are necessarily 1. No need to count.
        occurrencesMergedMagnitude[0][0] = 1;
        // At this point, occurrencesMergedMagnitude[selector][magnitude]
        // represents the total number of occurrences of gaps having a given
        // magnitude, in a given selector.
        // We transform it into uniform occurrences among the different gaps
        // having this magnitude.
        for (int selector = 1; selector < arraySize; selector++) {
            int selectorMax = computeSelectorMax(selector);
            for (int magnitude = 0; magnitude <= S[selectorMax]; magnitude++) {
                int maxGapInMagnitude = 1 << magnitude;
                int minGapInMagnitude = magnitude == 0 ? 1 : (1 << (magnitude - 1)) + 1;
                int nbGapsInMagnitude = maxGapInMagnitude - minGapInMagnitude + 1;
                occurrencesMergedMagnitude[selector][magnitude] = Tools.ceilingDivision(
                        occurrencesMergedMagnitude[selector][magnitude],
                        nbGapsInMagnitude);
            }
        }
        nbBitsPerOccurrence = OccurrenceTools.getNbBitsMax(occurrencesMergedMagnitude);
        convertToOccurrenceIntervals();
    }
    
    private void convertToOccurrenceIntervals() {
        OccurrenceTools.ensureTotalAndAccumulate(occurrencesMedMax, occurrencesCumulativeMedMax);
        for (int selector = 0; selector < arraySize; selector++) {
            OccurrenceTools.ensureTotal(occurrencesMergedMagnitude[selector]);
        }
        for (int selector = 0; selector < arraySize; selector++) {
            int selectorMax = computeSelectorMax(selector);
            occurrencesCumulativeGaps[selector][0] = 0;
            for (int magnitude = 0; magnitude <= S[selectorMax]; magnitude++) {
                int maxGapInMagnitude = 1 << magnitude;
                int minGapInMagnitude = magnitude == 0 ? 1 : (1 << (magnitude - 1)) + 1;
                for (int gap = minGapInMagnitude; gap <= maxGapInMagnitude; gap++) {
                    occurrencesCumulativeGaps[selector][gap] = Math.addExact(
                            (int)occurrencesMergedMagnitude[selector][magnitude],
                            occurrencesCumulativeGaps[selector][gap - 1]);
                }
            }
        }
    }
    
    @Override
    public void computeSizeBefore() {
        int nbStateBits = DEFAULT_STATE_BITS;
        counter = new ArithmeticEncoder(nbStateBits, bitCounter);
        nbBlocks = 0;
        size = 0.;
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocksLocal = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        nbBlocks += nbBlocksLocal;
        for (int idBlock = 0; idBlock < nbBlocksLocal; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            for (int i = 0; i < curBlockSize; i++) {
                subGapArray[i] = gapArrayRef.ints[offset + idBlock * BLOCK_SIZE + i];
            }
            int selector = computeSelector(subGapArray, curBlockSize);
            counter.update(occurrencesCumulativeMedMax, selector);
            for (int i = 0; i < curBlockSize; i++) {
                counter.update(occurrencesCumulativeGaps[selector], gapArrayRef.ints[offset + idBlock * BLOCK_SIZE + i] - 1);
            }
        }
        double sizeLocal = bitCounter.getNbBitsWrittenAndReset();
        size += sizeLocal;
        return sizeLocal;
    }
    
    @Override
    public double computeSizeAfter() {
        if (counter == null) {
            throw new RuntimeException("Unsupported call to computeSizeAfter without prior call to computeSizeBefore.");
        }
        counter.finish();
        counter = null;
        double sizeAfter = bitCounter.getNbBitsWrittenAndReset();
        size += sizeAfter;
        return sizeAfter;
    }
    
    @Override
    public void standardOutput() {
        double sizeSelectors = 0.;
        for (int i = 0; i < arraySize; i++) {
            double pi = (double)occurrencesMedMax[i] / ((double)occurrencesCumulativeMedMax[arraySize]);
            sizeSelectors += -pi * Tools.log2(pi);
        }
        sizeSelectors *= nbBlocks;
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; selectors: " + (sizeSelectors / 8000000) +
                " --- " + (sizeSelectors / nbBlocks) + " bits per selector" +
                " ; gaps: " + ((size - sizeSelectors) / 8000000) +
                ")");
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                32 * arraySize + // occurrencesMedMax
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence) +
                nbBitsPerOccurrence * S.length * Arrays.stream(S).map(x -> x + 1).reduce(0, Math::addExact); // occurrencesMagnitude
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        for (int i = 0; i < arraySize; i++) {
            out.writeInt((int) occurrencesMedMax[i]);
        }
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            for (int selector = 0; selector < arraySize; selector++) {
                int selectorMax = computeSelectorMax(selector);
                for (int j = 0; j < S[selectorMax] + 1; j++) {
                    BinaryEncoding.writeCodeBinary((int)occurrencesMergedMagnitude[selector][j], bitStream, nbBitsPerOccurrence);
                }
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        for (int i = 0; i < arraySize; i++) {
            occurrencesMedMax[i] = in.readInt();
        }
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            for (int selector = 0; selector < arraySize; selector++) {
                int selectorMax = computeSelectorMax(selector);
                for (int j = 0; j < S[selectorMax] + 1; j++) {
                    occurrencesMergedMagnitude[selector][j] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                }
            }
        }
        convertToOccurrenceIntervals();
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        int nbBlocksLocal = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        for (int idBlock = 0; idBlock < nbBlocksLocal; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            for (int i = 0; i < curBlockSize; i++) {
                subGapArray[i] = gapArrayRef.ints[offset + idBlock * BLOCK_SIZE + i];
            }
            int selector = computeSelector(subGapArray, curBlockSize);
            encoder.update(occurrencesCumulativeMedMax, selector);
            for (int i = 0; i < curBlockSize; i++) {
                encoder.update(occurrencesCumulativeGaps[selector], gapArrayRef.ints[offset + idBlock * BLOCK_SIZE + i] - 1);
            }
        }
    }
    
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int nbBlocksLocal = Tools.ceilingDivision(nbGapsLocal, BLOCK_SIZE);
        int idGap = 0;
        for (int idBlock = 0; idBlock < nbBlocksLocal; idBlock++) {
            int curBlockSize = ((idBlock + 1) * BLOCK_SIZE <= nbGapsLocal)
                ? BLOCK_SIZE
                : (nbGapsLocal % BLOCK_SIZE);
            
            int selector = decoder.read(occurrencesCumulativeMedMax);
            for (int i = 0; i < curBlockSize; i++) {
                gapArrayRef.ints[idGap++] = decoder.read(occurrencesCumulativeGaps[selector]) + 1;
            }
        }
    }
    
}
