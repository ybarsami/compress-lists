/**
 * MethodFContextTrit.java
 * For each gapArray, here what is done:
 *    0. We note gapSizeArray the array such that
 *       gapSizeArray[i] = floor(log_2(gapArray[i])).
 *       We note tritList = tritList(gapSizeArray) and T the size of tritList.
 *    1. We code the first min(k+w, T) trits from tritList with an arithmetic
 *       contextual method --- where the context of the i-th trit is the
 *       min(i-1, kInit) previous trits.
 *    2. Then, a contextual arithmetic method encodes the trits
 *       trit_{k+w}, ... trit_T --- where the context is the previous k trits +
 *       the number of "2" in the w trits before them.
 *    3. We have now encoded what enables us to retrieve the "2" in
 *       tritList(gapArray). There remains to put the "{0,1}" from this trit
 *       list. We code them with an arithmetic method --- taking only into
 *       account the occurrences of those bits, without context.
 * 
 * NB: This is the implementation the closest to Andre's initial code.
 * 
 * General remarks that remain true for all the FContext methods:
 * 1) The initialization (coding of the first trits of each gapSizeArray) could
 * also be done by treating gapSize by gapSize until we reach k+w trits.
 * 2) Compressing the last list of 0,1 with an arithmetic coder (using the real
 * frequencies of 0 and 1, or using a context) is only slightly better than
 * considering uniform frequencies of 0 and 1 (each 1/2).
 * 3) When using an arithmetic coding method, it is not easy to mix it with a
 * non-arithmetic method without losing space --- because when decoding, we do
 * not know when we can stop reading the bits!
 */

package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import tritlistencoding.TritSequence;
import tritlistencoding.TritSequenceContextualEncoding;
import static compresslists.Tools.nbBits;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodFContextTrit extends MethodByArithmeticCoding {
    
    private final TritSequenceContextualEncoding tritSequenceEncoding;
    
    private long[] occurrences01;
    private int[] occurrencesCumulative01;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    private double sizeFinal;
    
    // Parameters. Here, we have three parameters k, w, and kInit.
    private int k;
    private int w;
    private int kInit;
    
    private TritSequence tritSequence = new TritSequence();
    private TritSequence tl = new TritSequence();
    private BitSequence b01 = new BitSequence();
    
    /**
     * Creates a new instance of MethodFContextTrit.
     */
    public MethodFContextTrit(int k, int w, int kInit) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        tritSequenceEncoding = new TritSequenceContextualEncoding(k, w, kInit);
    }
    
    @Override
    public void initBefore() {
        tritSequenceEncoding.initBefore();
        allocateOccurrences();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        tritSequenceEncoding.initWithNewTritSequence(tl);
        for (int i = 0; i < b01.size(); i++) {
            occurrences01[b01.get(i)]++;
        }
    }
    
    @Override
    public void initAfter() {
        tritSequenceEncoding.initAfter();
        OccurrenceTools.ensureTotalAndAccumulate(occurrences01, occurrencesCumulative01);
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        // Phase 1.
        double[] phase1Sizes = tritSequenceEncoding.computeSizes(tl);
        
        // Phase 2.
        double sizeFinalLocal = 0.;
        double nbBits01 = 0.;
        for (int bit = 0; bit < nbBits; bit++) {
            double pi = ((double)occurrences01[bit]) / ((double)occurrencesCumulative01[nbBits]);
            nbBits01 += -pi * Tools.log2(pi);
        }
        sizeFinalLocal += b01.size() * nbBits01;
        
        double sizeInitialLocal = phase1Sizes[0];
        double sizeGenericLocal = phase1Sizes[1];
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        sizeFinal   += sizeFinalLocal;
        return sizeInitialLocal + sizeGenericLocal + sizeFinalLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeInitial = 0.;
        sizeGeneric = 0.;
        sizeFinal   = 0.;
        super.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        tl = new TritSequence();
        b01 = new BitSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) trits: " + (sizeInitial / 8000000) +
                " ; last trits: " + (sizeGeneric / 8000000) +
                " ; 01: " + (sizeFinal / 8000000) +
                ")");
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        
        // Phase 1.
        tritSequenceEncoding.writeTritSequence(tl, encoder);
        
        // Phase 2.
        for (int i = 0; i < b01.size(); i++) {
            encoder.update(occurrencesCumulative01, b01.get(i));
        }
    }
    
    private void allocateOccurrences() {
        occurrences01 = new long[nbBits];
	occurrencesCumulative01 = new int[nbBits + 1];
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String getName() {
        return "ContextLengthTrit_k" + k + "_w" + w + "_kI" + kInit;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return tritSequenceEncoding.computeSizeNeededInformation() +
                nbBits * 32; // occurrenceArray
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        tritSequenceEncoding.exportNeededInformation(out);
        // export occurrenceArray (for 01)
        for (int bit = 0; bit < nbBits; bit++) {
            out.writeInt((int)occurrences01[bit]);
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        tritSequenceEncoding.importNeededInformation(in);
        allocateOccurrences();
        // import occurrenceArray (for 01)
        for (int bit = 0; bit < nbBits; bit++) {
            occurrences01[bit] = in.readInt();
        }
        OccurrenceTools.ensureTotalAndAccumulate(occurrences01, occurrencesCumulative01);
    }
    
    /*
     * This is the reverse of the updateBitSequenceWithGapArray function. It reads nbGapsLocal
     * gaps in the buffer BitStream, according to the inverse of the encoding function.
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        // Phase 1.
        tritSequenceEncoding.readTritSequence(nbGapsLocal, decoder, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
        // Here, the gapArray is in fact the gapSizeArray
        
        // Phase 2.
        tritSequence.reset();
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            // gapSizeArray[i] stores 1 + the size in bits of the i-th gap, as
            // it should be stored in a trit sequence, cf. Tools.tritSequence2intSizeArray.
            for (int j = 0; j < gapArrayRef.ints[idGap] - 1; j++) {
                int currentBit = decoder.read(occurrencesCumulative01);
                tritSequence.add(currentBit);
            }
            tritSequence.add(2);
        }
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }

}
