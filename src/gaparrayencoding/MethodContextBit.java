/**
 * MethodContextBit.java
 * For each gapArray, here what is done:
 *    0. We note gapSizeArray the array such that
 *       gapSizeArray[i] = floor(log_2(gapArray[i])).
 *       We note bitList = bitList(gapSizeArray) and T the size of bitList.
 *    1. We code the first min(k+w, T) bits from bitList with an arithmetic
 *       contextual method --- where the context of the i-th bit is the
 *       min(i-1, kInit) previous bits.
 *    2. Then, a contextual arithmetic method encodes the bits
 *       bit_{k+w}, ... bit_T --- where the context is the previous k bits +
 *       the number of "1" in the w bits before them.
 *    3. We have now encoded what enables us to retrieve the "2" in
 *       tritList(gapArray). There remains to put the "{0,1}" from this trit
 *       list. We code them with an arithmetic contextual method --- where the
 *       context of the i-th bit is the min(i-1, kFinal) previous bits.
 * 
 * Difference from Context1-2: instead of working on tritList(gapSizeArray), we
 * work on bitList(gapSizeArray).
 */

package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequenceContextualEncoding;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import tritlistencoding.TritSequence;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodContextBit extends MethodByArithmeticCoding {
    
    private final BitSequenceContextualEncoding phase1Encoding;
    private final BitSequenceContextualEncoding phase2Encoding;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    private double sizeFinal;
    
    // Parameters. Here, we have four parameters k, w, kInit, and kFinal.
    private int k;
    private int w;
    private int kInit;
    private int kFinal;
    
    private TritSequence tritSequence = new TritSequence();
    private BitSequence b2 = new BitSequence();
    private BitSequence b01 = new BitSequence();
    
    /**
     * Creates a new instance of MethodContextBit.
     */
    public MethodContextBit(int k, int w, int kInit, int kFinal) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        this.kFinal = kFinal;
        phase1Encoding = new BitSequenceContextualEncoding(k, w, kInit);
        phase2Encoding = new BitSequenceContextualEncoding(Integer.MAX_VALUE, 0, kFinal);
    }
    
    public MethodContextBit(int k, int w, int kInit) {
        this(k, w, kInit, kInit);
    }
    
    @Override
    public void initBefore() {
        phase1Encoding.initBefore();
        phase2Encoding.initBefore();
    }
    
    @Override
    public void init(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2b2(tritSequence, b2);
        phase1Encoding.initWithNewBitSequence(b2);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        phase2Encoding.initWithNewBitSequence(b01);
    }
    
    @Override
    public void initAfter() {
        phase1Encoding.initAfter();
        phase2Encoding.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same gapArrays when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2b2(tritSequence, b2);
        double[] phase1Sizes = phase1Encoding.computeSizes(b2);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        double[] phase2Sizes = phase2Encoding.computeSizes(b01);
        
        double sizeInitialLocal = phase1Sizes[0];
        double sizeGenericLocal = phase1Sizes[1];
        double sizeFinalLocal   = phase2Sizes[0];
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        sizeFinal   += sizeFinalLocal;
        return sizeInitialLocal + sizeGenericLocal + sizeFinalLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeInitial = 0.;
        sizeGeneric = 0.;
        sizeFinal   = 0.;
        super.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        b2 = new BitSequence();
        b01 = new BitSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) bits: " + (sizeInitial / 8000000) +
                " ; last bits: " + (sizeGeneric / 8000000) +
                " ; 01: " + (sizeFinal / 8000000) +
                ")");
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2b2(tritSequence, b2);
        phase1Encoding.writeBitSequence(b2, encoder);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        phase2Encoding.writeBitSequence(b01, encoder);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public String getName() {
        return "ContextBit_k" + k + "_w" + w + "_kI" + kInit + "_kF" + kFinal;
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return phase1Encoding.computeSizeNeededInformation() +
                phase2Encoding.computeSizeNeededInformation();
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        phase1Encoding.exportNeededInformation(out);
        phase2Encoding.exportNeededInformation(out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        phase1Encoding.importNeededInformation(in);
        phase2Encoding.importNeededInformation(in);
    }
    
    /*
     * This is the reverse of the updateBitSequenceWithGapArray function. It reads nbGapsLocal
     * gaps in the buffer BitStream, according to the inverse of the encoding function.
     * First, it uses arithmetic decoding of the initial bits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining bits, and finally it
     * uses arithmetic decoding of the bits from the original trit list.
     */
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        // Phase 1.
        phase1Encoding.readBitSequence(nbGapsLocal, decoder, b2);
        
        // Phase 2.
        int nbBitsToRead = b2.size() - b2.numberOfOnes();
        phase2Encoding.readBitSequence(nbBitsToRead, decoder, false, b01);
        
        // Merge.
        MethodTrits.mergeB2AndB01(b2, b01, tritSequence);
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some testing
    ////////////////////////////////////////////////////////////////////////////
    
    public final void exportProbas(FileWriter out) throws IOException {
        phase1Encoding.exportProbas(out);
        phase2Encoding.exportProbas(out);
    }
    
}
