/**
 * MethodByBitSequence.java
 */

package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import integerencoding.DeltaEncoding;
import io.BitInputStream;
import io.BitInputStreamArray;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamArray;
import io.BitOutputStreamFile;
import io.DatasetReader;
import io.DatasetReaderBinary;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public abstract class MethodByBitSequence extends Method {
    
    /*
     * Some methods keep some bits to be written at the end.
     */
    public void outputRemainingBits(BitOutputStream bitStream) {}
    
    /*
     * All the MethodByBitSequence are obliged to read byte by byte. When
     * considering padding, the last bits of the last byte read can be
     * discarded. But when we don't use padding, we have to keep them.
     */
    public void flushRemainingBits() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of the index file *with padding* from the gapArrays.
    // Padding is introduced, e.g.:
    //    * when a method outputs bit by bit, the export can only export byte
    //      by byte, at the end of a gapArray, between 0 and 7 bits can be
    //      wasted.
    //    * when a method packs different numbers in an int (e.g.,
    //      simple9), at the end of a gapArray, between 0 and 27 bits can be
    //      wasted.
    //    * when a method packs different numbers in a long (e.g.,
    //      simple8b), at the end of a gapArray, between 0 and 59 bits can be
    //      wasted.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Output a BitSequence encoding the numbers included in gapArray.
     */
    public final BitSequence gapArray2bitSequence(int[] gapArray) {
        return gapArray2bitSequence(new IntsRef(gapArray));
    }
    public final BitSequence gapArray2bitSequence(IntsRef gapArrayRef) {
        BitSequence bitSequence = new BitSequence();
        BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(bitSequence);
        writeGapArray(bitOutputStream, gapArrayRef);
        outputRemainingBits(bitOutputStream);
        return bitSequence;
    }
    
    /*
     * Output the gapArray encoded in the given BitSequence.
     * WARNING: This is to be updated when, e.g., gapArrayRef.length is needed
     * as input as a number different from the number of gaps to read. This is
     * the case in MethodByQuatritConcatenation.
     */
    public int[] bitSequence2gapArray(BitSequence bitSequence, int nbGapsLocal) {
        BitInputStreamArray bitInputStream = new BitInputStreamArray(bitSequence);
        IntsRef gapArrayRef = new IntsRef(nbGapsLocal);
        readGapArray(bitInputStream, gapArrayRef);
        return gapArrayRef.ints;
    }
    
    /*
     * Conceptually, this method could be final. However, it is not final
     * because in MethodByArithmeticCoding it is overridden to make clear that
     * it should not be used.
     */
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        try (BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(dataOutputStream)) {
            writeGapArray(bitOutputStream, gapArrayRef);
            outputRemainingBits(bitOutputStream);
        }
    }
    @Override
    public void writeGapArrayBlock(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        try (BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(dataOutputStream)) {
            writeGapArrayBlock(bitOutputStream, gapArrayRef);
            outputRemainingBits(bitOutputStream);
        }
    }
    
    public abstract void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef);
    public void writeGapArrayBlock(BitOutputStream bitStream, IntsRef gapArrayRef) {
        writeGapArray(bitStream, gapArrayRef);
    }
    
    /*
     * Conceptually, this method could be final. However, it is not final
     * because in MethodByArithmeticCoding it is overridden to make clear that
     * it should not be used.
     */
    @Override
    public void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        BitInputStreamFile bitInputStream = new BitInputStreamFile(dataInputStream);
        readGapArray(bitInputStream, gapArrayRef);
        flushRemainingBits();
    }
    @Override
    public void readGapArrayBlock(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        BitInputStreamFile bitInputStream = new BitInputStreamFile(dataInputStream);
        readGapArrayBlock(bitInputStream, gapArrayRef);
        flushRemainingBits();
    }
    
    public abstract void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef);
    public void readGapArrayBlock(BitInputStream bitStream, IntsRef gapArrayRef) {
        readGapArray(bitStream, gapArrayRef);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of the index file *without padding* from the gapArrays.
    // Padding is avoided because when all the gapArrays can be treated as one
    // unique gapArray.
    // For example, the Interpolative method does not allow such a treatment.
    // When a method does not allow such a treatment, the methods with and
    // without padding output exactly the same index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeGapArrayList(DataOutputStream dataOutputStream, DatasetReader datasetReader) throws IOException {
        try (BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(dataOutputStream)) {
            writeGapArrayList(bitOutputStream, datasetReader);
        }
    }
    
    public final void writeGapArrayList(BitOutputStream bitOutputStream, DatasetReader datasetReader) {
        datasetReader.restartReader();
        for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
            IntsRef gapArrayRef = datasetReader.getNextGapArray();
            writeGapArray(bitOutputStream, gapArrayRef);
        }
        outputRemainingBits(bitOutputStream);
    }
    
    /*
     * Exports all the gapArrays in different files, according to batch limits.
     * The exported files will be named baseFilename + the id of the batch.
     * Remark: Always use a DatasetReader that supports efficient skips for this
     * method to be efficient.
     */
    @Override
    public void splitToFiles(String baseFileName, DatasetReader datasetReader, int nbDocuments, double[] batchLimits) {
        int nbBatches = batchLimits.length - 1;
        assert(batchLimits[0] == 0.);
        assert(batchLimits[nbBatches] == 1.);
        for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
            assert(batchLimits[idBatch] < batchLimits[idBatch + 1]);
        }
        double[] frequencyStarts = new double[nbBatches];
        double[] frequencyStops  = new double[nbBatches];
        for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
            frequencyStarts[idBatch] = batchLimits[idBatch];
            frequencyStops[idBatch]  = batchLimits[idBatch + 1];
        }
        // Separation of the gap arrays by batch
        ArrayList<ArrayList<Integer>> batches = new ArrayList<>();
        int[] gapArrayLengths = datasetReader.getGapArrayLengthsArray();
        for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
            ArrayList<Integer> batch = new ArrayList<>();
            // WARNING: if datasetReader is sub-dataset, the idWord set here
            // will not correspond to datasetReder.gapArrayLength(idWord).
            // TODO: make it work if datasetReader is a sub-dataset.
            for (int idWord = 0; idWord < gapArrayLengths.length; idWord++) {
                int gapArrayLength = gapArrayLengths[idWord];
                if (gapArrayLength > frequencyStarts[idBatch] * nbDocuments && gapArrayLength <= frequencyStops[idBatch] * nbDocuments) {
                    batch.add(idWord);
                }
            }
            batches.add(batch);
        }
        // Export each batch in a separate file
        for (int idBatch = 0; idBatch < nbBatches; idBatch++) {
            try (FileOutputStream fout = new FileOutputStream(baseFileName + "-batch" + idBatch + "." + getName());
                    DataOutputStream out = new DataOutputStream(fout);
                    BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
                int[] batch = batches.get(idBatch)
                        .stream()
                        .mapToInt(Integer::intValue)
                        .toArray();
                // Equivalent of exportLengthsNeededToUncompress
                out.writeInt(nbDocuments);
                exportNeededInformation(out);
                out.writeInt(batch.length);
                for (int idWord : batch) {
                    // WARNING: do not call datasetReder.gapArrayLength(idWord)
                    // instead, it may be different if datasetReader is a
                    // sub-dataset.
                    // TODO: make it work if datasetReader is a sub-dataset.
                    DeltaEncoding.writeCodeDelta(gapArrayLengths[idWord], bitStream);
                }
                bitStream.close();
                // Equivalent of writeGapArrayList
                // WARNING: batch is supposed to be in ascending order for the
                // loop.
                // TODO: make it work if datasetReader is a sub-dataset.
                datasetReader.restartReader(batch);
                for (int idWord : batch) {
                    IntsRef gapArrayRef = datasetReader.getNextGapArray();
                    writeGapArray(bitStream, gapArrayRef);
                }
                outputRemainingBits(bitStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /*
     * Extract only the gap arrays marked in cluster, and output them into an
     * index file.
     */
    public void extractToFile(String baseFileName, DatasetReader datasetReader, int nbDocuments, int[] cluster) {
        String filename = baseFileName + "." + getName();
        try (FileOutputStream fout = new FileOutputStream(filename);
                DataOutputStream out = new DataOutputStream(fout);
                BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            // Equivalent of exportLengthsNeededToUncompress
            out.writeInt(nbDocuments);
            exportNeededInformation(out);
            out.writeInt(cluster.length);
            for (int idWord: cluster) {
                DeltaEncoding.writeCodeDelta(datasetReader.getGapArrayLength(idWord), bitStream);
            }
            bitStream.close();
            // Equivalent of writeGapArrayList
            // WARNING: cluster is supposed to be in ascending order for
            // the loop. It has been done in MetaMethodCluster.computeClustersFromFile.
            datasetReader.restartReader(cluster);
            for (int idWord : cluster) {
                IntsRef gapArrayRef = datasetReader.getNextGapArray();
                writeGapArray(bitStream, gapArrayRef);
            }
            outputRemainingBits(bitStream);
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    // TODO: there are two passes over the dataset reader that make the same
    // computation; do one pass, output two files and merge them at the end.
    public void intersectClustersReferences(String baseFileName, int nbDocuments, DatasetReader datasetReader, DatasetReader refListReader, ArrayList<int[]> clusters, boolean shrinkResiduals) {
        int nbClusters = refListReader.getNbWords();
        if (clusters.size() != nbClusters) {
            throw new AssertionError();
        }
        refListReader.restartReader();
        for (int idCluster = 0; idCluster < nbClusters; idCluster++) {
            try (FileOutputStream fout = new FileOutputStream(baseFileName + (shrinkResiduals ? "-shrinked" : "") + "-part" + idCluster + "." + getName());
                    DataOutputStream out = new DataOutputStream(fout);
                    BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
                int[] cluster = clusters.get(idCluster);
                // Equivalent of exportLengthsNeededToUncompress
                out.writeInt(nbDocuments);
                exportNeededInformation(out);
                // The reference list
                // WARNING: The .csv is filled with the doc ids, and has to be
                // read "as if" it was a gap array.
                IntsRef referenceListRef = refListReader.getNextGapArray();
                int[] referenceList = referenceListRef.ints;
                int referenceListLength = referenceListRef.length;
                // SHRINK
                int[] nbDocumentsToShrink = new int[referenceList[referenceListLength - 1] + 1];
                int idDoc = 1;
                for (int shrink = 0; shrink < referenceListLength; shrink++) {
                    while (idDoc < referenceList[shrink]) {
                        nbDocumentsToShrink[idDoc++] = shrink;
                    }
                }
                // END SHRINK
                // Compute the sizes
                ArrayList<Integer> gapArrayLengthsList = new ArrayList<>();
                gapArrayLengthsList.add(referenceListLength);
                datasetReader.restartReader(cluster);
                for (int idWord : cluster) {
                    IntsRef documentArrayRef = datasetReader.getNextDocumentArray();
                    int[] documentArray = documentArrayRef.ints;
                    int nbDocumentsLocal = documentArrayRef.length;
                    assert(documentArrayRef.offset == 0);
                    int[] map = Tools.sortedIntersectIds(documentArray, nbDocumentsLocal, referenceList);
                    // We can work in-place on the map array.
                    IntsRef mapRef = new IntsRef(map);
                    Tools.documentArray2gapArray(mapRef, mapRef);
                    if (map.length > 0) {
                        gapArrayLengthsList.add(map.length);
                    }
                    int[] residual = Tools.sortedIntersectNot(documentArray, nbDocumentsLocal, referenceList);
                    // We can work in-place on the residual array.
                    IntsRef residualRef = new IntsRef(residual);
                    Tools.documentArray2gapArray(residualRef, residualRef);
                    if (residual.length > 0) {
                        gapArrayLengthsList.add(residual.length);
                    }
                }
                // Export the sizes
                out.writeInt(gapArrayLengthsList.size());
                for (int gapArrayLength : gapArrayLengthsList) {
                    DeltaEncoding.writeCodeDelta(gapArrayLength, bitStream);
                }
                bitStream.close();
                // Equivalent of writeGapArrayList
                IntsRef referenceGapArrayRef = new IntsRef(0);
                Tools.documentArray2gapArray(new IntsRef(referenceList), referenceGapArrayRef);
                writeGapArray(bitStream, referenceGapArrayRef);
                datasetReader.restartReader(cluster);
                for (int idWord : cluster) {
                    IntsRef documentArrayRef = datasetReader.getNextDocumentArray();
                    int[] documentArray = documentArrayRef.ints;
                    int nbDocumentsLocal = documentArrayRef.length;
                    assert(documentArrayRef.offset == 0);
                    int[] map = Tools.sortedIntersectIds(documentArray, nbDocumentsLocal, referenceList);
                    // We can work in-place on the map array.
                    IntsRef mapRef = new IntsRef(map);
                    Tools.documentArray2gapArray(mapRef, mapRef);
                    if (map.length > 0) {
                        writeGapArray(bitStream, mapRef);
                    }
                    int[] residual = Tools.sortedIntersectNot(documentArray, nbDocumentsLocal, referenceList);
                    if (shrinkResiduals) {
                        for (int i = 0; i < residual.length; i++) {
                            residual[i] -= residual[i] >= nbDocumentsToShrink.length
                                    ? nbDocumentsToShrink[nbDocumentsToShrink.length - 1]
                                    : nbDocumentsToShrink[residual[i]];
                        }
                    }
                    // We can work in-place on the residual array.
                    IntsRef residualRef = new IntsRef(residual);
                    Tools.documentArray2gapArray(residualRef, residualRef);
                    if (residual.length > 0) {
                        writeGapArray(bitStream, residualRef);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /*
     * Merge all the gapArrays from different files.
     */
    @Override
    public void mergeFiles(DatasetReader[] batchedDatasetReaders, String filename, int nbDocuments) {
        try (FileOutputStream fout = new FileOutputStream(filename);
                DataOutputStream out = new DataOutputStream(fout);
                BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(out)) {
            out.writeInt(nbDocuments);
            exportNeededInformation(out);
            // Equivalent of exportLengthsNeededToUncompress
            int nbWords = 0;
            for (DatasetReader datasetReader : batchedDatasetReaders) {
                nbWords += datasetReader.getNbWords();
            }
            out.writeInt(nbWords);
            for (DatasetReader datasetReader : batchedDatasetReaders) {
                int[] gapArrayLengths = datasetReader.getGapArrayLengthsArray();
                for (int gapArrayLength : gapArrayLengths) {
                    DeltaEncoding.writeCodeDelta(gapArrayLength, bitOutputStream);
                }
            }
            bitOutputStream.close();
            // Equivalent of writeGapArrayList
            for (DatasetReader datasetReader : batchedDatasetReaders) {
                datasetReader.restartReader();
                for (int idWord = 0; idWord < datasetReader.getNbWords(); idWord++) {
                    IntsRef gapArrayRef = datasetReader.getNextGapArray();
                    writeGapArray(bitOutputStream, gapArrayRef);
                }
            }
            outputRemainingBits(bitOutputStream);
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    // TODO: this is mostly a copy/paste from Method.java.
    // Find a way to factorize the code.
    @Override
    public DatasetReader importFromFile(String filename) {
        return new DatasetReaderBinary(filename) {
            private BitInputStreamFile bitInputStream;
            
            @Override
            protected void goToGapArrays() throws IOException {
                dataInputStream.readInt();
                importNeededInformation(dataInputStream);
                importLengthsNeededToUncompress(dataInputStream);
                bitInputStream = new BitInputStreamFile(dataInputStream);
            }
            
            @Override
            protected void readNextGapArray() throws IOException {
                readGapArray(bitInputStream, intArrayRef);
                if (nbIntArraysRead == nbWords - 1) {
                    flushRemainingBits();
                    bitInputStream.close();
                }
            }
            
            @Override
            protected void collectStatistics() {
                try (FileInputStream fileInputStream = new FileInputStream(filename);
                        DataInputStream in = new DataInputStream(fileInputStream)) {
                    nbDocuments = in.readInt();
                    importNeededInformation(in);
                    ArrayList<Integer> lengthsNeededToUncompress = importLengthsNeededToUncompress(in);
                    nbWords = lengthsNeededToUncompress.size();
                    for (int nbGapsLocal : lengthsNeededToUncompress) {
                        updateStatisticsWithNewGapArray(nbGapsLocal);
                    }
                } catch (IOException e) {
                    throw new RuntimeException("IOException in '" + filename + "'.");
                }
            }
        };
    }

}
