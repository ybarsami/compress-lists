/**
 * Abstract class to ease the use of CODECs from the java library JavaFastPFOR
 * inside this library.
 * 
 * https://github.com/lemire/JavaFastPFOR
 * 
 * NB: It is possible not to store the size of the compressedInts array.
 * [same thing as in MethodAfor3]
 * At least two possible ways to do it:
 *     1. Manage a buffer of bytes (when we read too much bytes, they are kept
 *        and used when decompressing the next gapArray)
 *     2. Use a java.io.PushbackInputStream or a java.io.RandomAccessFile.
 * 
 * Alternatively, it could be possible not to store the size of the gapArray.
 * It means that we should have a way of knowing, given an array of compressed
 * ints, the maximum size of the uncompressed array.
 */

package gaparrayencoding;

import arrays.IntsRef;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.IntegerCODEC;

/**
 *
 * @author yann
 */
public class MethodByJavaFastPFOR extends Method {
    
    // Possible alternative is to use SkippableIntegerCODEC and
    // headless(Un)Compress because we manually deal with the size anyway.
    private IntegerCODEC integerCODEC;
    
    private int[] compressedInts = new int[0];
    private int nbCompressedInts = 0;
    
    /**
     * Creates a new instance of MethodByJavaFastPFOR.
     */
    public MethodByJavaFastPFOR(IntegerCODEC integerCODEC) {
        this.integerCODEC = integerCODEC;
    }
    
    @Override
    public String getName() {
        return integerCODEC.toString();
    }
    
    private void compress(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        IntWrapper inpos = new IntWrapper(offset);
        IntWrapper outpos = new IntWrapper();
        if (compressedInts.length < nbGapsLocal + 1) {
            compressedInts = new int[nbGapsLocal + 1]; // this buffer has enough size
        }
        integerCODEC.compress(gapArray, inpos, nbGapsLocal, compressedInts, outpos);
        nbCompressedInts = outpos.intValue();
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        compress(gapArrayRef);
        return
                32. + // we also store the length of the compressed array
                32. * nbCompressedInts; // the (int[]) compressed array
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the file from the gapArrays
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public void writeGapArray(DataOutputStream dataOutputStream, IntsRef gapArrayRef) throws IOException {
        compress(gapArrayRef);
        dataOutputStream.writeInt(nbCompressedInts);
        for (int i = 0; i < nbCompressedInts; i++) {
            dataOutputStream.writeInt(compressedInts[i]);
        }
    }
    
    @Override
    public void readGapArray(DataInputStream dataInputStream, IntsRef gapArrayRef) throws IOException {
        int nbGapsLocal = gapArrayRef.length;
        nbCompressedInts = dataInputStream.readInt();
        if (compressedInts.length < nbCompressedInts) {
            compressedInts = new int[nbCompressedInts];
        }
        for (int i = 0; i < nbCompressedInts; i++) {
            compressedInts[i] = dataInputStream.readInt();
        }
        IntWrapper inpos = new IntWrapper();
        IntWrapper outpos = new IntWrapper();
        integerCODEC.uncompress(compressedInts, inpos, nbCompressedInts, gapArrayRef.ints, outpos);
        assert(outpos.intValue() == nbGapsLocal);
    }
    
    /*
    // This would be available with a method that outputs, given an array of
    // compressed ints, the maximum size of the uncompressed array.
    
    @Override
    public double computeSizeLengths(ArrayList<int[]> gapArrayList, int nbDocuments) {
        return 64.; // nbGapArrays, nbDocuments
    }
    @Override
    public double computeSizeLengths(DatasetReader datasetReader, int nbDocuments) {
        return 64.; // nbGapArrays, nbDocuments
    }
    
    @Override
    public void exportLengthsNeededToUncompress(ArrayList<int[]> gapArrayList, DataOutputStream out) throws IOException {
        out.writeInt(gapArrayList.size());
    }
    @Override
    public void exportLengthsNeededToUncompress(DatasetReader datasetReader, DataOutputStream out) throws IOException {
        out.writeInt(datasetReader.getNbWords());
    }
    
    @Override
    public ArrayList<Integer> importLengthsNeededToUncompress(DataInputStream in) throws IOException {
        int nbGapArrays = in.readInt();
        ArrayList<Integer> nbGapArraysList = new ArrayList<>();
        for (int i = 0; i < nbGapArrays; i++) {
            nbGapArraysList.add(1); // Or any other number, it doesn't matter.
        }
        return nbGapArraysList;
    }
    
    // TODO: when importLengthsNeededToUncompress is modified...
    @Override
    public DatasetReader importReaderFromFile(String filename) {
        return super.importReaderFromFile(filename);
    }
    */
    
}
