/*
 * Index compression with the global Bernoulli method.
 *
 * Witten, Moffat, and Bell, "Managing Gigabytes" (1999), p. 119
 * One obvious way to parameterize the model and perhaps obtain better
 * compression is to make use of the actual density of pointers in the inverted
 * file. Suppose that the total number of pointers to be stored (the quantity f
 * in Table 3.1) is known. Dividing this by the number of index terms, and then
 * by the number of documents, gives a probability ot f /(N * n) that any
 * randomly selected document contains any randomly chosen term. The pointer
 * occurrences can then be modeled as a Bernoulli process with this probability,
 * by assuming that the f pointers of the inverted file are randomly selected
 * from the n * N possible word-document pairs in the collection. For example,
 * using the information contained in Table 3.1, the probability that any
 * randomly selected word of Bible appears in any randomly chosen verse is
 * calculated as 701,412/(31,101 * 8,965) = 0.0025, assuming that words are
 * scattered completely uniformly across verses.
 * Making this assumption, the chance of a gap of size x is the probability of
 * having x - 1 nonoccurrences of that particular word, each of probability
 * (1 - p), followed by one occurrence of probability p, which is
 * Pr[x] = (1 - p)^{x - 1} * p. This is called the geometric distribution and is
 * equivalent to modeling each possible term-document pair as appearing
 * independently with probability p. If arithmetic coding is to be used, the
 * required cumulative probabilities can be calculated by summing this
 * distribution:
 *
 * low-bound  = \sum_{i = 1}^{x - 1} (1 - p)^{i - 1} * p = 1 - (1 - p)^{x - 1}
 *
 * high-bound = \sum_{i = 1}^{x    } (1 - p)^{i - 1} * p = 1 - (1 - p)^{x    }
 */
package gaparrayencoding;

import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodBernoulliGlobal extends MethodByArithmeticCoding {
    
    // The parameter.
    public double p;
    
    // Size of the occurrence arrays.
    private int arraySize;
    
    // Occurrence arrays.
    private long[] occurrences;
    private int[] occurrencesCumulative;
    
    private double nbBitsPerGap;
    
    /**
     * Creates a new instance of MethodBernoulliGlobal.
     */
    public MethodBernoulliGlobal() {}
    public MethodBernoulliGlobal(int nbDocuments, double p) {
        this.nbDocuments = nbDocuments;
        setParameter(p);
    }
    public MethodBernoulliGlobal(int nbWords, int nbDocuments, long nbPointers) {
        this.nbDocuments = nbDocuments;
        setParameter(nbWords, nbDocuments, nbPointers);
    }
    
    public final void setParameter(int nbWords, int nbDocuments, long nbPointers) {
        setParameter((double)nbPointers / ((double)nbWords * (double)nbDocuments));
    }
    public final void setParameter(double p) {
        this.p = p;
        arraySize = nbDocuments + 1;
        allocateOccurrences();
        
        // Compute the geometric distribution.
        double[] probabilities = bernoulliProbabilities(p, nbDocuments);
        
        // Convert the probabilities to occurrences.
        OccurrenceTools.convertProbasToOccurrences(probabilities, occurrences);
        
        // Convert the occurrences to occurrences intervals.
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        
        // Compute the number of bits needed per gap.
        nbBitsPerGap = 0.;
        for (int i = 1; i < arraySize; i++) {
            double pi = probabilities[i];
            nbBitsPerGap += -pi * Tools.log2(pi);
        }
    }
    
    private void allocateOccurrences() {
        occurrences = new long[arraySize];
	occurrencesCumulative = new int[arraySize + 1];
    }
    
    public final static double[] bernoulliProbabilities(double p, int nbDocuments) {
        int arraySize = nbDocuments + 1;
        
        // Compute the geometric distribution.
        double[] probabilities = new double[arraySize];
        probabilities[1] = p;
        for (int i = 2; i < arraySize; i++) {
            probabilities[i] = (1. - p) * probabilities[i - 1];
        }
        
        return probabilities;
    }
    
    @Override
    public String getName() {
        return "BernoulliGlobal_" + p;
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        return nbGapsLocal * nbBitsPerGap;
    }
    
    @Override
    public final void writeGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            encoder.update(occurrencesCumulative, gapArrayRef.ints[offset + idGap]);
        }
    }
    
    @Override
    public final void readGapArray(IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            int gap = decoder.read(occurrencesCumulative);
            gapArrayRef.ints[idGap] = gap;
        }
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 64; // p
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeDouble(p);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        setParameter(in.readDouble());
    }
    
}
