/**
 * Index compression with the zeta method.
 *
 * Boldi and Vigna, "The WebGraph Framework II: Codes For The World-Wide Web" (2003)
 * Given a fixed positive integer k, the shrinking factor, a positive integer x
 * in the interval [2^{hk}, 2^{(h+1)k} - 1] is Zeta_k-coded by writing h + 1 in
 * unary, followed by a minimal binary coding of x - 2^{hk} in the interval
 * [0, 2^{(h+1)k} - 2^{hk} - 1].
 * Note that for our purposes the minimal binary coding of x in the interval
 * [0, z - 1] is defined as follows: let s = ceiling(log_2(z)); if x < 2^s - z,
 * then x is coded using the x-th binary word of length s - 1 (in
 * lexicographical order); otherwise, x is coded using the (x - z + 2^s)-th
 * binary word of length s.
 * 
 * Remark: Zeta(1) = Gamma.
 */

package gaparrayencoding;

import integerencoding.ZetaEncoding;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodZeta extends MethodByElement {
    
    // The shrinking factor
    private int k;
    
    /**
     * Creates a new instance of MethodZeta.
     */
    public MethodZeta() {}
    public MethodZeta(int k) {
        this.k = k;
    }
    
    @Override
    public String getName() {
        return "Zeta_" + k;
    }
    
    @Override
    public double computeSize(int x) {
        return ZetaEncoding.computeSizeZeta(x, k);
    }
    
    @Override
    public void writeCode(int x, BitOutputStream bitStream) {
        ZetaEncoding.writeCodeZeta(x, bitStream, k);
    }
    
    @Override
    public int readCode(BitInputStream bitStream) {
        return ZetaEncoding.readCodeZeta(bitStream, k);
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return 32; // k
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        out.writeInt(k);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        k = in.readInt();
    }

}
