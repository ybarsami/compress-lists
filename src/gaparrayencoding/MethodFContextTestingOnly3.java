/**
 * MethodFContextTestingOnly3.java
 * For each gapArray, here what is done:
 *    0. We note gapSizeArray the array such that
 *       gapSizeArray[i] = floor(log_2(gapArray[i])).
 *       We note tritList = tritList(gapSizeArray).
 *       We note nbMaxBitsForGapSize = floor(log_2(floor(log_2(nbDocuments)) + 1)).
 *       This is the maximum number of bits required to encode a gapSize.
 *    1. We code the trits of tritList with a trits method, except that when we
 *       encounter nbMaxBitsForGapSize 0/1 in a row, we do not encode the 2
 *       which must follow them. There must be a 2 in this scenario because
 *       gapsize is in [0; floor(log_2(nbDocuments))], then to encode it we add
 *       1 and we use the trit method again, resulting in a number of bits used
 *       in [0; nbMaxBitsForGapSize]. So when we have nbMaxBitsForGapSize 0/1 in
 *       a row, we cannot have one more 0/1, it is necessarily the end of the
 *       encoding of the gapSize.
 *    2. We have now encoded what enables us to retrieve the "2" in
 *       tritList(gapArray). There remains to put the "{0,1}" from this trit
 *       list. We just append them at the end.
 *       As a small improvement, we first use the potentially wasted space in
 *       the last byte (and write the {0,1} as trits), then write the rest of
 *       the {0,1} bit by bit.
 * 
 * Difference from FContext1-2: instead of using an arithmetic coder for
 * tritList(gapSizeArray), we use plain trits.
 * 
 * NB: This method was designed just for testing purposes, and is now discarded
 * in tests as it takes more space than the others.
 */

package gaparrayencoding;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import compresslists.Tools;
import io.BitInputStream;
import io.BitOutputStream;
import tritlistencoding.TritSequence;
import static compresslists.Tools.bitArray2byte;
import static compresslists.Tools.byte2int;
import static compresslists.Tools.nbBitsPerByte;
import static compresslists.Tools.nbTrits;
import static compresslists.Tools.nbTritsPerByte;

/**
 *
 * @author yann
 */
public class MethodFContextTestingOnly3 extends MethodTrits {
    
    private final int nbMaxBitsForGapSize;
    
    // To separate the different sizes in the index.
    private double sizeGapSize;
    private double size01;
    
    private TritSequence tritSequence = new TritSequence();
    private TritSequence tl = new TritSequence();
    private TritSequence tritSequenceToWrite = new TritSequence();
    private BitSequence b01 = new BitSequence();
    
    /**
     * Creates a new instance of MethodFContextTestingOnly3.
     */
    public MethodFContextTestingOnly3(int nbDocuments) {
        nbMaxBitsForGapSize = Tools.ilog2(Tools.ilog2(nbDocuments) + 1);
    }
    
    @Override
    public String getName() {
        return "ContextLengthTestingOnly3";
    }
    
    @Override
    public void writeGapArray(BitOutputStream bitStream, IntsRef gapArrayRef) {
        Tools.intArray2tritSequence(gapArrayRef, tritSequence);
        MethodTrits.tritSequence2tl(tritSequence, tl);
        tritSequenceToWrite.reset();
        int nbBitsInARow = 0;
        for (int i = 0; i < tl.size(); i++) {
            int trit = tl.get(i);
            if (trit == 2) {
                if (nbBitsInARow < nbMaxBitsForGapSize) {
                    tritSequenceToWrite.add(2);
                }
                nbBitsInARow = 0;
            } else {
                tritSequenceToWrite.add(trit);
                nbBitsInARow++;
            }
        }
        writeTritSequence(bitStream, tritSequenceToWrite);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        if (nbTritsWritten == 0) {
            for (int i = 0; i < b01.size(); i++) {
                bitStream.writeBit(b01.get(i));
            }
        } else {
            int nbBitsWriteableAsTrits = nbTritsPerByte - nbTritsWritten;
            TritSequence bitListAsTrits = new TritSequence();
            for (int i = 0; i < Math.min(b01.size(), nbBitsWriteableAsTrits); i++) {
                bitListAsTrits.add(b01.get(i));
            }
            if (b01.size() < nbBitsWriteableAsTrits) {
                writeTritSequence(bitStream, bitListAsTrits);
                outputRemainingBits(bitStream);
            } else {
                writeTritSequence(bitStream, bitListAsTrits);
                // Here, we are sure that we have no remaining bits to output !
                for (int i = nbBitsWriteableAsTrits; i < b01.size(); i++) {
                    bitStream.writeBit(b01.get(i));
                }
            }
        }
    }
    
    @Override
    public double computeSize(IntsRef gapArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        // Formula is 8/5 * floor(log2(gapList(get(i))) + 8/5
        int nbNeededTrits = 0;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            int gapSize = Tools.ilog2(gapArray[offset + idGap]) + 1;
            int nbTritsForGapSize = Tools.ilog2(gapSize);
            nbNeededTrits += nbTritsForGapSize + (nbTritsForGapSize == nbMaxBitsForGapSize ? 0 : 1);
        }
        sizeGapSize += ((double)nbNeededTrits) * 8.0 / 5.0;
        
        int nbNeededBits01 = 0;
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            nbNeededBits01 += Tools.ilog2(gapArray[offset + idGap]);;
        }
        size01 += (double)nbNeededBits01;
        return ((double)(nbNeededTrits)) * 8.0 / 5.0 + (double)nbNeededBits01;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeGapSize = 0.;
        size01      = 0.;
    }
    
    @Override
    public double computeSizeAfter() {
        tritSequence = new TritSequence();
        tl = new TritSequence();
        tritSequenceToWrite = new TritSequence();
        b01 = new BitSequence();
        return super.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (trits for the gapsizes: " + (sizeGapSize / 8000000) +
                " ; 01: " + (size01 / 8000000) +
                ")");
    }
    
    @Override
    public void readTritSequence(BitInputStream bitStream, int nbGapsLocal, TritSequence tritSequence) {
        tritSequence.reset();
        int nbGapsTreated = 0;
        int nbBitsInARow = 0;
        while (nbGapsTreated < nbGapsLocal) {
            // If there are no more trits to read, read a new byte from the stream.
            if (nbCurrentTritsRead == nbTritsPerByte) {
                int[] currentBits = new int[nbBitsPerByte];
                for (int i = 0; i < nbBitsPerByte; i++) {
                    currentBits[i] = bitStream.getNextBit();
                }
                nbCurrentTritsRead = 0;
                int byteRead = byte2int(bitArray2byte(currentBits));
                for (int i = 0; i < nbTritsPerByte; i++) {
                    currentTrits[nbTritsPerByte - 1 - i] = byteRead % nbTrits;
                    byteRead /= nbTrits;
                }
            }
            // Extract a trit.
            int tritRead = currentTrits[nbCurrentTritsRead++];
            tritSequence.add(tritRead);
            nbBitsInARow++;
            if (tritRead == 2) {
                // If the trit is 2, it means we finished reading a gap.
                nbGapsTreated++;
                nbBitsInARow = 0;
            } else if (nbBitsInARow == nbMaxBitsForGapSize) {
                // If we have read nbMaxBitsForGapSize trits, the next one is an
                // unwritten 2, and it also means we finished reading a gap.
                tritSequence.add(2);
                nbGapsTreated++;
                nbBitsInARow = 0;
            }
        }
        // Extract last trits.
        while (nbCurrentTritsRead < nbTritsPerByte) {
            tritSequence.add(currentTrits[nbCurrentTritsRead++]);
        }
    }
    
    public static void readGapArray(TritSequence gapSizeTritSequence, BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        int[] gapSizeArray = new int[nbGapsLocal];
        int nbGapsTreated = 0;
        int tritIndex = 0;
        // Gap starts at 1 and not 0 because what is actually stored is gap - 2^{ilog2(gap)}
        int gap = 1;
        while (nbGapsTreated < nbGapsLocal) {
            // Extract a trit.
            int tritRead = gapSizeTritSequence.get(tritIndex++);
            assert(tritRead >= 0 && tritRead <= 2);
            if (tritRead == 2) {
                // If the trit is 2, add the gap to the array.
                gapSizeArray[nbGapsTreated++] = gap;
                gap = 1;
            } else {
                // Else, update the gap.
                gap *= 2;
                gap += tritRead;
            }
        }
        TritSequence tritSequence = new TritSequence();
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            // gapSizeArray[i] stores 1 + the size in bits of the i-th gap, as
            // it should be stored in a trit sequence, cf. Tools.tritSequence2intSizeArray.
            for (int j = 0; j < gapSizeArray[idGap] - 1; j++) {
                int currentBit = tritIndex < gapSizeTritSequence.size()
                        ? gapSizeTritSequence.get(tritIndex++)
                        : bitStream.getNextBit();
                tritSequence.add(currentBit);
            }
            tritSequence.add(2);
        }
        Tools.tritSequence2intArray(tritSequence, gapArrayRef);
    }
    
    @Override
    public void readGapArray(BitInputStream bitStream, IntsRef gapArrayRef) {
        int nbGapsLocal = gapArrayRef.length;
        readTritSequence(bitStream, nbGapsLocal, tritSequence);
        readGapArray(tritSequence, bitStream, gapArrayRef);
    }

}
