/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tritlistencoding;

/**
 *
 * @author yann
 */
public class LongTritSequenceMultipleReader extends LongTritSequenceMultipleHandler implements LongTritSequenceAbstractReader {
    
    public LongTritSequenceMultipleReader(int k, int[] wList, int kInit, boolean merge01ForContext) {
        super(k, wList, kInit, merge01ForContext);
    }
    
    public LongTritSequenceMultipleReader(int k, int[] wList, int kInit) {
        this(k, wList, kInit, false);
    }
    
    @Override
    public void init(TritSequence tritSequence) {
        init();
        this.tritSequence = tritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbTritsHandled < tritSequence.size();
    }
    
}
