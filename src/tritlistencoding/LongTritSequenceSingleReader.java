/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tritlistencoding;

/**
 *
 * @author yann
 */
public class LongTritSequenceSingleReader extends LongTritSequenceSingleHandler implements LongTritSequenceAbstractReader {
    
    public LongTritSequenceSingleReader(int k, int w, int kInit, boolean merge01ForContext) {
        super(k, w, kInit, merge01ForContext);
    }
    
    public LongTritSequenceSingleReader(int k, int w, int kInit) {
        this(k, w, kInit, false);
    }
    
    @Override
    public void init(TritSequence tritSequence) {
        init();
        this.tritSequence = tritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbTritsHandled < tritSequence.size();
    }
    
}
