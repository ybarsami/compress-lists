/**
 * This is essentially the code extracted from TritSequenceContextualEncoding
 * and composed with gaparraylistencoding.MethodByArithmeticCoding.
 */

package tritlistencoding;

import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import compresslists.CompressLists.ContextParameters;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFake;
import io.BitOutputStreamFile;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;
import static arithmeticcode.ArithmeticCoderBase.MAX_OCC_BITS;
import static arithmeticcode.ArithmeticCoderBase.STATE_BITS;
import static compresslists.Tools.nbBitsPerByte;
import static compresslists.Tools.nbTrits;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodContext extends Method {
    
    // Arithmetic encoder and decoder.
    protected ArithmeticEncoder encoder;
    protected ArithmeticDecoder decoder;
    
    private BitOutputStreamFake bitCounter = new BitOutputStreamFake();
    private ArithmeticEncoder counter;
    
    // To handle trit sequences
    private TritSequenceAbstractReader tritSequenceReader;
    private TritSequenceAbstractWriter tritSequenceWriter;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    
    // Size of the occurrence arrays.
    private int contextSize;
    private int contextSizeInit;
    private int nbSymbolsForContext;
    private final int nbSymbols = nbTrits;
    
    // Occurrences arrays.
    private long[][] occurrences;
    private int[][] occurrencesCumulative;
    private long[][] occurrencesInit;
    private int[][] occurrencesCumulativeInit;
    
    // Parameters.
    private int k;
    private int[] wList;
    private int kInit;
    
    private int nbBitsPerOccurrence;
    private boolean merge01ForContext;
    private boolean merge01ForProbas;
    private boolean writeAllProbas;
    private int nbWrittenProbas;
    
    /**
     * Creates a new instance of MethodContext.
     */
    public MethodContext() {}
    public MethodContext(ContextParameters contextParameters) {
        setParameters(contextParameters);
    }
    
    /*
     * A ContextParameters object does not store any boolean named
     * writeAllProbas. Instead, if nbBitsPerOccurrence is set to a value <= 0,
     * this means that we will use exact probabilities.
     */
    public final void setParameters(ContextParameters contextParameters) {
        this.k = contextParameters.k;
        this.wList = contextParameters.wList;
        this.kInit = contextParameters.kInit;
        this.merge01ForContext = contextParameters.merge01ForContext;
        this.merge01ForProbas = contextParameters.merge01ForProbas;
        this.writeAllProbas = contextParameters.nbBitsPerOccurrence <= 0;
        if (!writeAllProbas) {
            this.nbBitsPerOccurrence = contextParameters.nbBitsPerOccurrence;
            if (nbBitsPerOccurrence > MAX_OCC_BITS[DEFAULT_STATE_BITS]) {
                throw new RuntimeException("Maximum number of bits for occurrences is " +
                        MAX_OCC_BITS[DEFAULT_STATE_BITS] + " for a 64-bit arithmetic coder.");
            }
        }
        nbSymbolsForContext = merge01ForContext ? nbTrits - 1 : nbTrits;
        nbWrittenProbas = merge01ForProbas ? nbSymbols - 1 : nbSymbols;
        if (!writeAllProbas) {
            nbWrittenProbas--;
        }
        contextSize = Math.multiplyExact(
                Arrays.stream(wList).map(x -> x + 1).reduce(1, Math::multiplyExact),
                Tools.pow(nbSymbolsForContext, k));
        contextSizeInit = nbSymbols * (1 - Tools.pow(nbSymbolsForContext, kInit + 1)) / (1 - nbSymbolsForContext);
        if (wList.length == 1) {
            int w = wList[0];
            tritSequenceReader = new TritSequenceSingleReader(k, w, kInit, merge01ForContext);
            tritSequenceWriter = new TritSequenceSingleWriter(k, w, kInit, merge01ForContext);
        } else {
            tritSequenceReader = new TritSequenceMultipleReader(k, wList, kInit, merge01ForContext);
            tritSequenceWriter = new TritSequenceMultipleWriter(k, wList, kInit, merge01ForContext);
        }
    }
    
    @Override
    public void initBefore() {
        allocateOccurrenceArrays();
    }
    
    @Override
    public void init(TritSequence tritSequence) {
        tritSequenceReader.init(tritSequence);
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getInitialIndex();
            // Update the occurrences
            occurrencesInit[index][currentTrit]++;
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getGenericIndex();
            // Update the occurrences
            occurrences[index][currentTrit]++;
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
    }
    
    @Override
    public void initAfter() {
        if (merge01ForProbas) {
            merge0and1Probas();
        }
        convertToOccurrenceIntervals();
        nbBitsPerOccurrence = OccurrenceTools.getNbBitsMax(occurrences);
    }
    
    @Override
    public String getName() {
        return "Context" +
                (merge01ForContext ? "_Merge01Context" : "") +
                (merge01ForProbas ? "_Merge01Probas" : "") +
                (writeAllProbas ? "_ExactProbas" : "_ProbasOn" + nbBitsPerOccurrence + "Bits") +
                "_k" + k +
                (wList.length == 1
                    ? "_w" + wList[0]
                    : "_wList" + Arrays.stream(wList).mapToObj(Integer::toString).reduce("", (s1, s2) -> s1 + "-" + s2)) +
                "_kI" + kInit;
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeTritSequence.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.finish();
        encoder = null;
    }
    
    @Override
    public void flushRemainingBits() {
        decoder = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the trit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Warning: for this to work, we need to pass the same lists when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public final double computeSize(TritSequence tritSequence) {
        tritSequenceReader.init(tritSequence);
        
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getInitialIndex();
            // Update the size
            counter.update(occurrencesCumulativeInit[index], currentTrit);
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        double sizeInitialLocal = bitCounter.getNbBitsWrittenAndReset();
        
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getGenericIndex();
            // Update the size
            counter.update(occurrencesCumulative[index], currentTrit);
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
        double sizeGenericLocal = bitCounter.getNbBitsWrittenAndReset();
        
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        return sizeInitialLocal + sizeGenericLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        int nbStateBits = writeAllProbas
                ? DEFAULT_STATE_BITS
                : STATE_BITS[nbBitsPerOccurrence];
        counter = new ArithmeticEncoder(nbStateBits, bitCounter);
        sizeInitial = 0.;
        sizeGeneric = 0.;
    }
    
    @Override
    public double computeSizeAfter() {
        if (counter == null) {
            throw new RuntimeException("Unsupported call to computeSizeAfter without prior call to computeSizeBefore.");
        }
        counter.finish();
        counter = null;
        double sizeAfter = bitCounter.getNbBitsWrittenAndReset();
        sizeGeneric += sizeAfter;
        return sizeAfter;
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) trits: " + (sizeInitial / 8000000) +
                " ; last trits: " + (sizeGeneric / 8000000) +
                ")");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the trit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeTritSequence(BitOutputStream bitStream, TritSequence tritSequence) {
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = writeAllProbas
                    ? DEFAULT_STATE_BITS
                    : STATE_BITS[nbBitsPerOccurrence];
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        writeTritSequence(tritSequence);
    }
    
    private void writeTritSequence(TritSequence tritSequence) {
        tritSequenceReader.init(tritSequence);
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getInitialIndex();
            // Encode the trit
            encoder.update(occurrencesCumulativeInit[index], currentTrit);
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getGenericIndex();
            // Encode the trit
            encoder.update(occurrencesCumulative[index], currentTrit);
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the trit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    private void merge0and1Probas() {
        // Merge 0 and 1
        for (int context = 0; context < contextSizeInit; context++) {
            long occ01 = occurrencesInit[context][0] + occurrencesInit[context][1];
            occurrencesInit[context][0] = Tools.ceilingDivision(occ01, 2L);
            occurrencesInit[context][1] = Tools.ceilingDivision(occ01, 2L);
        }
        for (int context = 0; context < contextSize; context++) {
            long occ01 = occurrences[context][0] + occurrences[context][1];
            occurrences[context][0] = Tools.ceilingDivision(occ01, 2L);
            occurrences[context][1] = Tools.ceilingDivision(occ01, 2L);
        }
    }
    
    private void allocateOccurrenceArrays() {
	occurrences     = OccurrenceTools.allocateOccurrences(contextSize, nbSymbols);
	occurrencesInit = OccurrenceTools.allocateOccurrences(contextSizeInit, nbSymbols);
        occurrencesCumulative     = OccurrenceTools.allocateOccurrencesCumulative(contextSize, nbSymbols);
        occurrencesCumulativeInit = OccurrenceTools.allocateOccurrencesCumulative(contextSizeInit, nbSymbols);
    }
    
    private void convertToOccurrenceIntervals() {
        if (writeAllProbas) {
            OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
            OccurrenceTools.ensureTotalAndAccumulate(occurrencesInit, occurrencesCumulativeInit);
        } else {
            OccurrenceTools.normalizeAndAccumulate(occurrences, occurrencesCumulative, nbBitsPerOccurrence);
            OccurrenceTools.normalizeAndAccumulate(occurrencesInit, occurrencesCumulativeInit, nbBitsPerOccurrence);
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private long getNbBitsWritten() {
        return Math.addExact(
                GammaEncoding.computeSizeGamma(k + 1) +
                GammaEncoding.computeSizeGamma(wList.length + 1) +
                Arrays.stream(wList).map(x -> GammaEncoding.computeSizeGamma(x + 1)).reduce(0, Math::addExact) +
                GammaEncoding.computeSizeGamma(kInit + 1) +
                3 + // 3 booleans
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence),
                Math.multiplyExact((long)contextSize, nbWrittenProbas * nbBitsPerOccurrence));
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return Math.addExact(
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte), // parameters + occurrenceMatrix
                Math.multiplyExact(contextSizeInit, nbWrittenProbas * 32)); // occurrenceInitTensor
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            GammaEncoding.writeCodeGamma(k + 1, bitStream);
            GammaEncoding.writeCodeGamma(wList.length + 1, bitStream);
            for (int w : wList) {
                GammaEncoding.writeCodeGamma(w + 1, bitStream);
            }
            GammaEncoding.writeCodeGamma(kInit + 1, bitStream);
            BinaryEncoding.writeCodeBinary(merge01ForContext ? 1 : 0, bitStream, 1);
            BinaryEncoding.writeCodeBinary(merge01ForProbas ? 1 : 0, bitStream, 1);
            BinaryEncoding.writeCodeBinary(writeAllProbas ? 1 : 0, bitStream, 1);
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            int symbolStart = merge01ForProbas ? 1 : 0;
            int symbolStop  = writeAllProbas ? nbSymbols : nbSymbols - 1;
            // export occurrenceMatrix
            for (int context = 0; context < contextSize; context++) {
                for (int symbol = symbolStart; symbol < symbolStop; symbol++) {
                    BinaryEncoding.writeCodeBinary((int)occurrences[context][symbol], bitStream, nbBitsPerOccurrence);
                }
            }
            // Padding
            bitStream.close();
            // export occurrenceInitTensor
            for (int context = 0; context < contextSizeInit; context++) {
                for (int symbol = symbolStart; symbol < symbolStop; symbol++) {
                    out.writeInt((int)occurrencesInit[context][symbol]);
                }
            }
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            k = GammaEncoding.readCodeGamma(bitStream) - 1;
            int wListLength = GammaEncoding.readCodeGamma(bitStream) - 1;
            wList = new int[wListLength];
            for (int i = 0; i < wListLength; i++) {
                wList[i] = GammaEncoding.readCodeGamma(bitStream) - 1;
            }
            kInit = GammaEncoding.readCodeGamma(bitStream) - 1;
            merge01ForContext = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            merge01ForProbas = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            writeAllProbas = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            setParameters(new ContextParameters(k, wList, kInit, merge01ForContext, merge01ForProbas, writeAllProbas ? -1 : nbBitsPerOccurrence));
            allocateOccurrenceArrays();
            int symbolStart = merge01ForProbas ? 1 : 0;
            int symbolStop  = writeAllProbas ? nbSymbols : nbSymbols - 1;
            // import occurrenceMatrix
            for (int context = 0; context < contextSize; context++) {
                for (int symbol = symbolStart; symbol < symbolStop; symbol++) {
                    occurrences[context][symbol] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                }
            }
            // import occurrenceInitTensor
            for (int context = 0; context < contextSizeInit; context++) {
                for (int symbol = symbolStart; symbol < symbolStop; symbol++) {
                    occurrencesInit[context][symbol] = in.readInt();
                }
            }
            if (merge01ForProbas) {
                // copy the proba of symbol 0 from symbol 1
                for (int context = 0; context < contextSize; context++) {
                    occurrences[context][0] = occurrences[context][1];
                }
                for (int context = 0; context < contextSizeInit; context++) {
                    occurrencesInit[context][0] = occurrencesInit[context][1];
                }
            }
            if (!writeAllProbas) {
                // deduce the proba of symbol 2 from symbol 0, 1 and nbBitsPerOccurrence
                for (int context = 0; context < contextSize; context++) {
                    occurrences[context][2] = (1 << nbBitsPerOccurrence) - 1;
                    occurrences[context][2] -= occurrences[context][0];
                    occurrences[context][2] -= occurrences[context][1];
                }
                for (int context = 0; context < contextSizeInit; context++) {
                    occurrencesInit[context][2] = (1 << nbBitsPerOccurrence) - 1;
                    occurrencesInit[context][2] -= occurrencesInit[context][0];
                    occurrencesInit[context][2] -= occurrencesInit[context][1];
                }
            }
            convertToOccurrenceIntervals();
        }
    }
    
    @Override
    public final void readTritSequence(BitInputStream bitStream, int nbTritsToRead, TritSequence tritSequence) {
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = writeAllProbas
                    ? DEFAULT_STATE_BITS
                    : STATE_BITS[nbBitsPerOccurrence];
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        readTritSequence(nbTritsToRead, tritSequence);
    }
    
    /*
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    private void readTritSequence(int nbTwosToRead, TritSequence tritSequence) {
        tritSequenceWriter.init(nbTwosToRead, tritSequence);
        while (tritSequenceWriter.isInInitialState()) {
            int index = tritSequenceWriter.getInitialIndex();
            // Decode a trit
            int currentTrit = decoder.read(occurrencesCumulativeInit[index]);
            // Update the index for the next iteration.
            tritSequenceWriter.addTrit(currentTrit);
            tritSequenceWriter.updateInitialIndex();
        }
        while (tritSequenceWriter.isInGenericState()) {
            int index = tritSequenceWriter.getGenericIndex();
            // Decode a trit
            int currentTrit = decoder.read(occurrencesCumulative[index]);
            // Update the indexes for the next iteration.
            tritSequenceWriter.addTrit(currentTrit);
            tritSequenceWriter.updateGenericIndex();
        }
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some testing
    ////////////////////////////////////////////////////////////////////////////
    
    public final void exportProbas(FileWriter out) {
        try {
            if (merge01ForContext) {
                out.write("Occurrence init tensor\n");
                for (int context = 0; context < contextSizeInit; context++) {
                    out.write(((double)occurrencesInit[context][0] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
                    out.write(((double)occurrencesInit[context][2] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
                }
                out.write("Occurrence matrix\n");
                for (int context = 0; context < contextSize; context++) {
                    out.write(((double)occurrences[context][0] / (double)occurrencesCumulative[context][nbSymbols]) + " ");
                    out.write(((double)occurrences[context][2] / (double)occurrencesCumulative[context][nbSymbols]) + " ");
                }
                out.write("\n");
            } else {
                out.write("Occurrence init tensor\n");
                for (int context = 0; context < contextSizeInit; context++) {
                    for (int symbol = 0; symbol < nbSymbols; symbol++) {
                        out.write(((double)occurrencesInit[context][symbol] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
                    }
                }
                out.write("Occurrence matrix\n");
                for (int context = 0; context < contextSize; context++) {
                    for (int symbol = 0; symbol < nbSymbols; symbol++) {
                        out.write(((double)occurrences[context][symbol] / (double)occurrencesCumulative[context][nbSymbols]) + " ");
                    }
                }
                out.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static int getInitialContextIndex(int[] context, int kInitContext) {
        return 0;
    }
    
    public static int getContextIndex(int[] context, int kContext, int wContext, int nbTwosBefore) {
        assert(kContext == context.length);
        int currentIndex = 0;
        for (int i = 0; i < kContext; i++) {
            currentIndex *= nbTrits;
            currentIndex += context[i];
        }
        return (wContext + 1) * currentIndex + nbTwosBefore;
    }

}
