/**
 * MethodBySplitB2B01.java
 * For each trit sequence, here what is done:
 *    0. We note b2 = bitList(trit sequence).
 *    1. With a BitSequence method, we encode b2.
 *    2. We have now encoded what enables us to retrieve the "2" in the trit
 *       sequence. There remains to put the "{0,1}" from this trit sequence,
 *       i.e. b01. We code b01 with another BitSequence method.
 * 
 * Remark: This is essentially the code extracted from MethodContextBit.
 */

package tritlistencoding;

import bitlistencoding.BitSequence;
import gaparrayencoding.MethodTrits;
import io.BitInputStream;
import io.BitOutputStream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author yann
 */
public class MethodBySplitB2B01 extends Method {
    
    private final bitlistencoding.Method b2Method;
    private final bitlistencoding.Method b01Method;
    
    // To separate the different sizes in the index.
    private double sizeb2;
    private double sizeb01;
    
    private BitSequence b2 = new BitSequence();
    private BitSequence b01 = new BitSequence();
    
    /**
     * Creates a new instance of MethodBySplitB2B01.
     */
    public MethodBySplitB2B01(bitlistencoding.Method b2Method, bitlistencoding.Method b01Method) {
        this.b2Method  = b2Method;
        this.b01Method = b01Method;
    }
    
    @Override
    public void initBefore() {
        b2Method.initBefore();
        b01Method.initBefore();
    }
    
    @Override
    public void init(TritSequence tritSequence) {
        MethodTrits.tritSequence2b2(tritSequence, b2);
        b2Method.init(b2);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        b01Method.init(b01);
    }
    
    @Override
    public void initAfter() {
        b2Method.initAfter();
        b01Method.initAfter();
    }
    
    /*
     * Warning: for this to work, we need to pass the same trit sequences when
     * computing the cost than the ones passed to initialize the class.
     */
    @Override
    public double computeSize(TritSequence tritSequence) {
        MethodTrits.tritSequence2b2(tritSequence, b2);
        double sizeb2Local  = b2Method.computeSize(b2);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        double sizeb01Local = b01Method.computeSize(b01);
        
        sizeb2  += sizeb2Local;
        sizeb01 += sizeb01Local;
        return sizeb2Local + sizeb01Local;
    }
    
    @Override
    public void computeSizeBefore() {
        sizeb2  = 0.;
        sizeb01 = 0.;
        b2Method.computeSizeBefore();
        b01Method.computeSizeBefore();
    }
    
    @Override
    public double computeSizeAfter() {
        b2 = new BitSequence();
        b01 = new BitSequence();
        return b2Method.computeSizeAfter() + b01Method.computeSizeAfter();
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; b2: " + (sizeb2 / 8000000));
        b2Method.standardOutput();
        System.out.print(
                " ; b01: " + (sizeb01 / 8000000));
        b01Method.standardOutput();
        System.out.print(
                ")");
    }
    
    @Override
    public final void writeTritSequence(BitOutputStream bitStream, TritSequence tritSequence) {
        MethodTrits.tritSequence2b2(tritSequence, b2);
        b2Method.writeBitSequence(bitStream, b2);
        MethodTrits.tritSequence2b01(tritSequence, b01);
        b01Method.writeBitSequence(bitStream, b01);
    }
    
    @Override
    public String getName() {
        return "Split(" + b2Method.getName() + "-" + b01Method.getName() + ")";
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return b2Method.computeSizeNeededInformation() +
                b01Method.computeSizeNeededInformation();
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        b2Method.exportNeededInformation(out);
        b01Method.exportNeededInformation(out);
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        b2Method.importNeededInformation(in);
        b01Method.importNeededInformation(in);
    }
    
    @Override
    public final void readTritSequence(BitInputStream bitStream, int nbGapsLocal, TritSequence tritSequence) {
        // Phase 1.
        b2Method.readBitSequence(bitStream, nbGapsLocal, b2);
        
        // Phase 2.
        int nbBitsToRead = b2.size() - b2.numberOfOnes();
        b01Method.readBitSequence(bitStream, nbBitsToRead, false, b01);
        
        // Merge.
        MethodTrits.mergeB2AndB01(b2, b01, tritSequence);
    }
    
}
