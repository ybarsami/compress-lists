/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tritlistencoding;

/**
 *
 * @author yann
 */
public class TritSequenceMultipleWriter extends TritSequenceMultipleHandler implements TritSequenceAbstractWriter {
    
    private int nbTwosRead;
    private int nbTwosToRead;
    
    public TritSequenceMultipleWriter(int k, int[] wList, int kInit, boolean merge01ForContext) {
        super(k, wList, kInit, merge01ForContext);
    }
    
    public TritSequenceMultipleWriter(int k, int[] wList, int kInit) {
        this(k, wList, kInit, false);
    }
    
    @Override
    public void init(int nbTwosToRead, TritSequence tritSequence) {
        init();
        this.nbTwosToRead = nbTwosToRead;
        nbTwosRead = 0;
        tritSequence.reset();
        this.tritSequence = tritSequence;
    }
    
    @Override
    public void addTrit(int trit) {
        tritSequence.add(trit);
        if (trit == 2) {
            nbTwosRead++;
        }
    }
    
    @Override
    public TritSequence getTritSequence() {
        return tritSequence;
    }
    
    @Override
    public boolean isStillActive() {
        return nbTwosRead < nbTwosToRead;
    }
    
}
