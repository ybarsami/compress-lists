/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tritlistencoding;

/**
 *
 * @author yann
 */
public interface TritSequenceAbstractWriter extends TritSequenceAbstractHandler {
    
    public void init(int nbTwosToRead, TritSequence tritSequence);
    
    public void addTrit(int trit);
    
    public TritSequence getTritSequence();
    
}
