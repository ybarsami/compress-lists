/**
 * This is essentially the code extracted from TritSequenceContextualEncoding
 * and composed with gaparraylistencoding.MethodByArithmeticCoding.
 */

package tritlistencoding;

import arithmeticcode.ArithmeticCoderBase;
import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.ContextualOccurrenceTable;
import arithmeticcode.ContextualOccurrenceTableExtendedInt;
import arithmeticcode.ContextualOccurrenceTableFlatInt;
import arithmeticcode.ContextualOccurrenceTableSimpleInt;
import arithmeticcode.ContextualOccurrenceTableSparse;
import compresslists.CompressLists.ContextParameters;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFake;
import io.BitOutputStreamFile;
import static arithmeticcode.ArithmeticCoderBase.DEFAULT_STATE_BITS;
import static arithmeticcode.ArithmeticCoderBase.MAX_OCC_BITS;
import static arithmeticcode.ContextualOccurrenceTable.*;
import static compresslists.Tools.nbBitsPerByte;
import static compresslists.Tools.nbTrits;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class MethodAdaptiveContext extends Method {
    
    // Arithmetic encoder and decoder.
    protected ArithmeticEncoder encoder;
    protected ArithmeticDecoder decoder;
    
    private BitOutputStreamFake bitCounter = new BitOutputStreamFake();
    private ArithmeticEncoder counter;
    
    // To handle trit sequences
    private LongTritSequenceAbstractReader tritSequenceReader;
    private LongTritSequenceAbstractWriter tritSequenceWriter;
    
    // To separate the different sizes in the index.
    private double sizeInitial;
    private double sizeGeneric;
    
    // Size of the occurrence arrays.
    private long contextSize;
    private long contextSizeInit;
    private int nbSymbolsForContext;
    private final int nbSymbols = nbTrits;
    
    public static enum OccurrenceType {
        OCCURRENCE_DENSITY("Density"),
        OCCURRENCE_UNIFORM("Uniform");
        
        private String str;
        
        OccurrenceType(String str) {
            this.str = str;
        }
        
        public String getString() { return str; }
    };
    
    private OccurrenceType occurrenceType = OccurrenceType.OCCURRENCE_UNIFORM;
    
    // Occurrences arrays.
    private int occurrenceTableType = TABLE_SIMPLE_INT;
    private boolean adaptToCurrentDensity;
    private ContextualOccurrenceTable occurrenceTable;
    private ContextualOccurrenceTable occurrenceTableInit;
    
    // Parameters.
    private int k;
    private int[] wList;
    private int kInit;
    private int nbDocuments;
    private int maxBitsOcc;
    private int firstGapArraySize;
    private int currentGapArraySize;
    
    private boolean merge01ForContext;
    
    /**
     * Creates a new instance of MethodAdaptiveContext.
     */
    public MethodAdaptiveContext() {}
    public MethodAdaptiveContext(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
        setParameters(contextParameters, nbDocuments, firstGapArraySize, adaptToCurrentDensity);
    }
    
    public final void setParameters(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize) {
        this.k = contextParameters.k;
        this.wList = contextParameters.wList;
        this.kInit = contextParameters.kInit;
        this.merge01ForContext = contextParameters.merge01ForContext;
        nbSymbolsForContext = merge01ForContext ? nbTrits - 1 : nbTrits;
        contextSize = Math.multiplyExact(
                Arrays.stream(wList).map(x -> x + 1).reduce(1, Math::multiplyExact),
                Tools.pow((long)nbSymbolsForContext, (long)k));
        contextSizeInit = nbSymbols * (1L - Tools.pow((long)nbSymbolsForContext, (long)(kInit + 1))) / (1L - nbSymbolsForContext);
        if (wList.length == 1) {
            int w = wList[0];
            tritSequenceReader = new LongTritSequenceSingleReader(k, w, kInit, merge01ForContext);
            tritSequenceWriter = new LongTritSequenceSingleWriter(k, w, kInit, merge01ForContext);
        } else {
            tritSequenceReader = new LongTritSequenceMultipleReader(k, wList, kInit, merge01ForContext);
            tritSequenceWriter = new LongTritSequenceMultipleWriter(k, wList, kInit, merge01ForContext);
        }
        this.maxBitsOcc = contextParameters.nbBitsPerOccurrence > 0
                ? contextParameters.nbBitsPerOccurrence
                : MAX_OCC_BITS[DEFAULT_STATE_BITS];
        this.nbDocuments = nbDocuments;
        this.firstGapArraySize = firstGapArraySize;
    }
    public final void setParameters(ContextParameters contextParameters, int nbDocuments, int firstGapArraySize, boolean adaptToCurrentDensity) {
        setParameters(contextParameters, nbDocuments, firstGapArraySize);
        this.adaptToCurrentDensity = adaptToCurrentDensity;
    }
    
    @Override
    public String getName() {
        return "AdaptiveContext" +
                (merge01ForContext ? "_Merge01Context" : "") +
                (adaptToCurrentDensity ? "_AdaptedInit" : "_ConstantInit") +
                "_maxBitsOcc" + maxBitsOcc +
                "_k" + k +
                (wList.length == 1
                    ? "_w" + wList[0]
                    : "_wList" + Arrays.stream(wList).mapToObj(Integer::toString).reduce("", (s1, s2) -> s1 + "-" + s2)) +
                "_kI" + kInit + 
                "_Initial" + occurrenceType.getString();
    }
    
    @Override
    public final void outputRemainingBits(BitOutputStream bitStream) {
        deallocateOccurrenceArrays();
        if (encoder == null) {
            throw new RuntimeException("Unsupported call to outputRemainingBits without prior call to writeTritSequence.");
        } else if(!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        encoder.finish();
        encoder = null;
    }
    
    @Override
    public void flushRemainingBits() {
        deallocateOccurrenceArrays();
        decoder = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the trit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final double computeSize(TritSequence tritSequence) {
        if (adaptToCurrentDensity) {
            int nbTwos = tritSequence.numberOfTwos();
            if (nbTwos != currentGapArraySize) {
                currentGapArraySize = nbTwos;
                setDefaultOccurrences();
            }
        }
        
        tritSequenceReader.init(tritSequence);
        
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            long index = tritSequenceReader.getInitialIndex();
            // Update the size
            counter.update(occurrenceTableInit.getOccurrencesCumulative(index), currentTrit);
            occurrenceTableInit.incrementOccurrence(index, currentTrit);
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        double sizeInitialLocal = bitCounter.getNbBitsWrittenAndReset();
        
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            long index = tritSequenceReader.getGenericIndex();
            // Update the size
            counter.update(occurrenceTable.getOccurrencesCumulative(index), currentTrit);
            occurrenceTable.incrementOccurrence(index, currentTrit);
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
        double sizeGenericLocal = bitCounter.getNbBitsWrittenAndReset();
        
        sizeInitial += sizeInitialLocal;
        sizeGeneric += sizeGenericLocal;
        return sizeInitialLocal + sizeGenericLocal;
    }
    
    @Override
    public void computeSizeBefore() {
        int nbStateBits = ArithmeticCoderBase.STATE_BITS[maxBitsOcc];
        counter = new ArithmeticEncoder(nbStateBits, bitCounter);
        sizeInitial = 0.;
        sizeGeneric = 0.;
        if (occurrenceTable == null) {
            allocateOccurrenceArrays();
        }
    }
    
    @Override
    public double computeSizeAfter() {
        if (counter == null) {
            throw new RuntimeException("Unsupported call to computeSizeAfter without prior call to computeSizeBefore.");
        }
        counter.finish();
        counter = null;
        deallocateOccurrenceArrays();
        double sizeAfter = bitCounter.getNbBitsWrittenAndReset();
        sizeGeneric += sizeAfter;
        return sizeAfter;
    }
    
    @Override
    public void standardOutput() {
        System.out.print(" (occurrences: " + (computeSizeNeededInformation() / 8000000) +
                " ; initial (k+w) trits: " + (sizeInitial / 8000000) +
                " ; last trits: " + (sizeGeneric / 8000000) +
                ")");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the trit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    @Override
    public final void writeTritSequence(BitOutputStream bitStream, TritSequence tritSequence) {
        if (occurrenceTable == null) {
            allocateOccurrenceArrays();
        }
        // We make sure:
        // 1. Not to create a new encoder each time.
        // 2. That we are using the same bitStream at each write.
        if (encoder == null) {
            int nbStateBits = ArithmeticCoderBase.STATE_BITS[maxBitsOcc];
            encoder = new ArithmeticEncoder(nbStateBits, bitStream);
        } else if (!encoder.checkBitOutputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole encoding.");
        }
        writeTritSequence(tritSequence);
    }
    
    private void writeTritSequence(TritSequence tritSequence) {
        if (adaptToCurrentDensity) {
            int nbTwos = tritSequence.numberOfTwos();
            if (nbTwos != currentGapArraySize) {
                currentGapArraySize = nbTwos;
                setDefaultOccurrences();
            }
        }
        tritSequenceReader.init(tritSequence);
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            long index = tritSequenceReader.getInitialIndex();
            // Encode the trit
            encoder.update(occurrenceTableInit.getOccurrencesCumulative(index), currentTrit);
            occurrenceTableInit.incrementOccurrence(index, currentTrit);
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            long index = tritSequenceReader.getGenericIndex();
            // Encode the trit
            encoder.update(occurrenceTable.getOccurrencesCumulative(index), currentTrit);
            occurrenceTable.incrementOccurrence(index, currentTrit);
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the trit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    private void allocateOccurrenceArrays() {
        switch (occurrenceTableType) {
            case TABLE_SPARSE:
                occurrenceTable     = new ContextualOccurrenceTableSparse(contextSize, nbSymbols, maxBitsOcc);
                occurrenceTableInit = new ContextualOccurrenceTableSparse(contextSizeInit, nbSymbols, maxBitsOcc);
                break;
            case TABLE_SIMPLE_INT:
                long maxContextSize = Math.max(contextSize, contextSizeInit);
                if (Math.multiplyExact(maxContextSize, nbSymbols + 1) <= Integer.MAX_VALUE) {
                    occurrenceTable     = new ContextualOccurrenceTableFlatInt((int)contextSize, nbSymbols, maxBitsOcc);
                    occurrenceTableInit = new ContextualOccurrenceTableFlatInt((int)contextSizeInit, nbSymbols, maxBitsOcc);
                } else /*if (maxContextSize > Integer.MAX_VALUE)*/ {
                    occurrenceTable     = new ContextualOccurrenceTableExtendedInt(contextSize, nbSymbols, maxBitsOcc);
                    occurrenceTableInit = new ContextualOccurrenceTableExtendedInt(contextSizeInit, nbSymbols, maxBitsOcc);
                /*
                } else {
                    occurrenceTable     = new ContextualOccurrenceTableSimpleInt((int)contextSize, nbSymbols, maxBitsOcc);
                    occurrenceTableInit = new ContextualOccurrenceTableSimpleInt((int)contextSizeInit, nbSymbols, maxBitsOcc);
                */
                }
                break;
            default:
                throw new RuntimeException("This occurrenceTableType is not handled.");
        }
        currentGapArraySize = firstGapArraySize;
        setDefaultOccurrences();
    }
    
    private void setDefaultOccurrences() {
        long[] defaultOccurrences;
        switch (occurrenceType) {
            case OCCURRENCE_DENSITY:
                int myLog = Tools.ceilingLog2(Tools.ceilingDivision(nbDocuments, currentGapArraySize));
                defaultOccurrences = new long[] {
                    Tools.ceilingDivision(myLog, 2),
                    Math.max(myLog / 2, 1L),
                    1L
                };
                break;
            case OCCURRENCE_UNIFORM:
                defaultOccurrences = new long[] { 1L, 1L, 1L };
                break;
            default:
                throw new RuntimeException("This occurrenceType is not handled.");
        }
        occurrenceTable.setDefaultOccurrences(defaultOccurrences);
        occurrenceTableInit.setDefaultOccurrences(defaultOccurrences);
    }
    
    private void deallocateOccurrenceArrays() {
        occurrenceTable = null;
        occurrenceTableInit = null;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private int getNbBitsWritten() {
        return
                3 * 32 + // nbDocuments, firstGapArraySize, maxBitsOcc
                GammaEncoding.computeSizeGamma(k + 1) +
                GammaEncoding.computeSizeGamma(wList.length + 1) +
                Arrays.stream(wList).map(x -> GammaEncoding.computeSizeGamma(x + 1)).reduce(0, Math::addExact) +
                GammaEncoding.computeSizeGamma(kInit + 1) +
                2; // 2 booleans
    }
    
    @Override
    public double computeSizeNeededInformation() {
        return
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte); // parameters
    }
    
    @Override
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeInt(nbDocuments);
            out.writeInt(firstGapArraySize);
            out.writeInt(maxBitsOcc);
            GammaEncoding.writeCodeGamma(k + 1, bitStream);
            GammaEncoding.writeCodeGamma(wList.length + 1, bitStream);
            for (int w : wList) {
                GammaEncoding.writeCodeGamma(w + 1, bitStream);
            }
            GammaEncoding.writeCodeGamma(kInit + 1, bitStream);
            BinaryEncoding.writeCodeBinary(merge01ForContext ? 1 : 0, bitStream, 1);
            BinaryEncoding.writeCodeBinary(adaptToCurrentDensity ? 1 : 0, bitStream, 1);
        }
    }
    
    @Override
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            nbDocuments = in.readInt();
            firstGapArraySize = in.readInt();
            maxBitsOcc = in.readInt();
            k = GammaEncoding.readCodeGamma(bitStream) - 1;
            int wListLength = GammaEncoding.readCodeGamma(bitStream) - 1;
            wList = new int[wListLength];
            for (int i = 0; i < wListLength; i++) {
                wList[i] = GammaEncoding.readCodeGamma(bitStream) - 1;
            }
            kInit = GammaEncoding.readCodeGamma(bitStream) - 1;
            merge01ForContext = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            adaptToCurrentDensity = BinaryEncoding.readCodeBinary(bitStream, 1) == 1;
            setParameters(new ContextParameters(k, wList, kInit, merge01ForContext, false, maxBitsOcc), nbDocuments, firstGapArraySize, adaptToCurrentDensity);
        }
    }
    
    @Override
    public final void readTritSequence(BitInputStream bitStream, int nbTritsToRead, TritSequence tritSequence) {
        if (occurrenceTable == null) {
            allocateOccurrenceArrays();
        }
        // We make sure:
        // 1. Not to create a new decoder each time, this will consume initialization bits.
        // 2. That we are using the same bitStream at each read.
        if (decoder == null) {
            int nbStateBits = ArithmeticCoderBase.STATE_BITS[maxBitsOcc];
            decoder = new ArithmeticDecoder(nbStateBits, bitStream);
        } else if (!decoder.checkBitInputStreamEquality(bitStream)) {
            throw new RuntimeException("Arithmetic coding must use the same stream for the whole decoding.");
        }
        readTritSequence(nbTritsToRead, tritSequence);
    }
    
    /*
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    private void readTritSequence(int nbTwosToRead, TritSequence tritSequence) {
        if (adaptToCurrentDensity) {
            if (nbTwosToRead != currentGapArraySize) {
                currentGapArraySize = nbTwosToRead;
                setDefaultOccurrences();
            }
        }
        tritSequenceWriter.init(nbTwosToRead, tritSequence);
        while (tritSequenceWriter.isInInitialState()) {
            long index = tritSequenceWriter.getInitialIndex();
            // Decode a trit
            int currentTrit = decoder.read(occurrenceTableInit.getOccurrencesCumulative(index));
            occurrenceTableInit.incrementOccurrence(index, currentTrit);
            // Update the index for the next iteration.
            tritSequenceWriter.addTrit(currentTrit);
            tritSequenceWriter.updateInitialIndex();
        }
        while (tritSequenceWriter.isInGenericState()) {
            long index = tritSequenceWriter.getGenericIndex();
            // Decode a trit
            int currentTrit = decoder.read(occurrenceTable.getOccurrencesCumulative(index));
            occurrenceTable.incrementOccurrence(index, currentTrit);
            // Update the indexes for the next iteration.
            tritSequenceWriter.addTrit(currentTrit);
            tritSequenceWriter.updateGenericIndex();
        }
    }

}
