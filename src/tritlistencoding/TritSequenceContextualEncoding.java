/**
 * Encodes a set of trit list.
 * 
 * For each trit list, here what is done:
 *    0. We note T the size of trit list. The trits of this list are noted
 *       trit_1, trit_2, ... trit_T.
 *    1. We code the first min(k+w, T) trits with an arithmetic contextual
 *       method --- where the context of the i-th trit is the min(i-1, kInit)
 *       previous trits.
 *    2. Then, a contextual arithmetic method encodes the trits
 *       trit_{k+w}, ... trit_T --- where the context is the previous k trits +
 *       the number of "2" in the w trits before them.
 */

package tritlistencoding;

import arithmeticcode.ArithmeticDecoder;
import arithmeticcode.ArithmeticEncoder;
import arithmeticcode.OccurrenceTools;
import arrays.IntsRef;
import compresslists.Tools;
import integerencoding.BinaryEncoding;
import integerencoding.GammaEncoding;
import io.BitInputStreamFile;
import io.BitOutputStreamFile;
import static compresslists.Tools.nbBitsPerByte;
import static compresslists.Tools.nbTrits;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class TritSequenceContextualEncoding {
    
    // To handle trit sequences
    private final TritSequenceSingleReader tritSequenceReader;
    private final TritSequenceSingleWriter tritSequenceWriter;
    
    // Size of the occurrence arrays.
    private final int contextSize;
    private final int contextSizeInit;
    private final int nbSymbolsForContext;
    private final int nbSymbols = nbTrits;
    
    // Occurrences arrays.
    private long[][] occurrences;
    private int[][] occurrencesCumulative;
    private long[][] occurrencesInit;
    private int[][] occurrencesCumulativeInit;
    
    // Parameters.
    private int k;
    private int w;
    private int kInit;
    
    private int nbBitsPerOccurrence;
    private boolean merge01ForContext;
    private boolean merge01ForProbas;
    
    /**
     * Creates a new instance of TritSequenceContextualEncoding.
     */
    public TritSequenceContextualEncoding(int k, int w, int kInit, boolean merge01ForContext, boolean merge01ForProbas) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        this.merge01ForContext = merge01ForContext;
        this.merge01ForProbas = merge01ForProbas;
        nbSymbolsForContext = merge01ForContext ? nbTrits - 1 : nbTrits;
        contextSize = (w + 1) * Tools.pow(nbSymbolsForContext, k);
        contextSizeInit = nbSymbols * (1 - Tools.pow(nbSymbolsForContext, kInit + 1)) / (1 - nbSymbolsForContext);
        tritSequenceReader = new TritSequenceSingleReader(k, w, kInit, merge01ForContext);
        tritSequenceWriter = new TritSequenceSingleWriter(k, w, kInit, merge01ForContext);
    }
    
    public TritSequenceContextualEncoding(int k, int w, int kInit) {
        this(k, w, kInit, false, false);
    }
    
    public void initBefore() {
        allocateOccurrenceArrays();
    }
    
    public void initWithNewTritSequence(TritSequence tritSequence) {
        collectOccurrences(tritSequence,
                k, w, kInit, merge01ForContext,
                occurrences, occurrencesInit);
    }
    
    public void initAfter() {
        if (merge01ForProbas) {
            merge0and1Probas();
        }
        convertToOccurrenceIntervals();
        nbBitsPerOccurrence = OccurrenceTools.getNbBitsMax(occurrences);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // First use of the trit list handler: to compute the size of the encoding.
    ////////////////////////////////////////////////////////////////////////////
    
    public final double[] computeSizes(TritSequence tritSequence) {
        tritSequenceReader.init(tritSequence);
        double sizeInitial = 0.;
        double sizeGeneric = 0.;
        
        while (tritSequenceReader.isInInitialState()) {
            int index = tritSequenceReader.getInitialIndex();
            // Update the size
            for (int trit = 0; trit < nbSymbols; trit++) {
                double pz = ((double)occurrencesInit[index][trit]) / ((double)occurrencesCumulativeInit[index][nbSymbols]);
                sizeInitial += -pz * Tools.log2(pz);
            }
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        while (tritSequenceReader.isInGenericState()) {
            int index = tritSequenceReader.getGenericIndex();
            // Update the size
            for (int trit = 0; trit < nbSymbols; trit++) {
                double pz = ((double)occurrences[index][trit]) / ((double)occurrencesCumulative[index][nbSymbols]);
                sizeGeneric += -pz * Tools.log2(pz);
            }
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
        
        return new double[] { sizeInitial, sizeGeneric };
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Second use of the trit list handler: to actually create the index.
    ////////////////////////////////////////////////////////////////////////////
    
    public final void writeTritSequence(TritSequence tritSequence,
            ArithmeticEncoder encoder) {
        tritSequenceReader.init(tritSequence);
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getInitialIndex();
            // Encode the trit
            encoder.update(occurrencesCumulativeInit[index], currentTrit);
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getGenericIndex();
            // Encode the trit
            encoder.update(occurrencesCumulative[index], currentTrit);
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Third use of the trit list handler: to initialize the occurrences used by the arithmetic encoder.
    ////////////////////////////////////////////////////////////////////////////
    
    private void merge0and1Probas() {
        // Merge 0 and 1
        for (int context = 0; context < contextSizeInit; context++) {
            long occ01 = occurrencesInit[context][0] + occurrencesInit[context][1];
            occurrencesInit[context][0] = Tools.ceilingDivision(occ01, 2L);
            occurrencesInit[context][1] = Tools.ceilingDivision(occ01, 2L);
        }
        for (int context = 0; context < contextSize; context++) {
            long occ01 = occurrences[context][0] + occurrences[context][1];
            occurrences[context][0] = Tools.ceilingDivision(occ01, 2L);
            occurrences[context][1] = Tools.ceilingDivision(occ01, 2L);
        }
    }
    
    public static final void collectOccurrences(TritSequence tritSequence,
            int k, int w, int kInit, boolean merge01ForContext,
            long[][] occurrences, long[][] occurrencesInit) {
        TritSequenceSingleReader tritSequenceReader = new TritSequenceSingleReader(k, w, kInit, merge01ForContext);
        tritSequenceReader.init(tritSequence);
        while (tritSequenceReader.isInInitialState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getInitialIndex();
            // Update the occurrences
            occurrencesInit[index][currentTrit]++;
            // Update the index for the next iteration.
            tritSequenceReader.updateInitialIndex();
        }
        while (tritSequenceReader.isInGenericState()) {
            int currentTrit = tritSequenceReader.getCurrentTrit();
            int index = tritSequenceReader.getGenericIndex();
            // Update the occurrences
            occurrences[index][currentTrit]++;
            // Update the indexes for the next iteration.
            tritSequenceReader.updateGenericIndex();
        }
    }
    
    private void allocateOccurrenceArrays() {
	occurrences     = OccurrenceTools.allocateOccurrences(contextSize, nbSymbols);
	occurrencesInit = OccurrenceTools.allocateOccurrences(contextSizeInit, nbSymbols);
        occurrencesCumulative     = OccurrenceTools.allocateOccurrencesCumulative(contextSize, nbSymbols);
        occurrencesCumulativeInit = OccurrenceTools.allocateOccurrencesCumulative(contextSizeInit, nbSymbols);
    }
    
    private void convertToOccurrenceIntervals() {
        OccurrenceTools.ensureTotalAndAccumulate(occurrences, occurrencesCumulative);
        OccurrenceTools.ensureTotalAndAccumulate(occurrencesInit, occurrencesCumulativeInit);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Overriden function from the Method abstract class.
    ////////////////////////////////////////////////////////////////////////////
    
    private long getNbBitsWritten() {
        return
                GammaEncoding.computeSizeGamma(nbBitsPerOccurrence) +
                Math.multiplyExact((long)contextSize, nbSymbols * nbBitsPerOccurrence);
    }
    
    public double computeSizeNeededInformation() {
        return 3 * 32 + Math.addExact( // k, w, kInit
                Tools.nextMultiple(getNbBitsWritten(), nbBitsPerByte), // occurrenceMatrix
                Math.multiplyExact(contextSizeInit, 32)); // occurrenceInitTensor
    }
    
    public void exportNeededInformation(DataOutputStream out) throws IOException {
        try (BitOutputStreamFile bitStream = new BitOutputStreamFile(out)) {
            out.writeInt(k);
            out.writeInt(w);
            out.writeInt(kInit);
            GammaEncoding.writeCodeGamma(nbBitsPerOccurrence, bitStream);
            if (merge01ForProbas) {
                // export occurrenceMatrix
                for (int context = 0; context < contextSize; context++) {
                    BinaryEncoding.writeCodeBinary((int)occurrences[context][0], bitStream, nbBitsPerOccurrence);
                    BinaryEncoding.writeCodeBinary((int)occurrences[context][2], bitStream, nbBitsPerOccurrence);
                }
                // Padding
                bitStream.close();
                // export occurrenceInitTensor
                for (int context = 0; context < contextSizeInit; context++) {
                    out.writeInt((int)occurrencesInit[context][0]);
                    out.writeInt((int)occurrencesInit[context][2]);
                }
            } else {
                // export occurrenceMatrix
                for (int context = 0; context < contextSize; context++) {
                    for (int symbol = 0; symbol < nbSymbols; symbol++) {
                        BinaryEncoding.writeCodeBinary((int)occurrences[context][symbol], bitStream, nbBitsPerOccurrence);
                    }
                }
                // Padding
                bitStream.close();
                // export occurrenceInitTensor
                for (int context = 0; context < contextSizeInit; context++) {
                    for (int symbol = 0; symbol < nbSymbols; symbol++) {
                        out.writeInt((int)occurrencesInit[context][symbol]);
                    }
                }
            }
        }
    }
    
    public void importNeededInformation(DataInputStream in) throws IOException {
        try (BitInputStreamFile bitStream = new BitInputStreamFile(in)) {
            k = in.readInt();
            w = in.readInt();
            kInit = in.readInt();
            nbBitsPerOccurrence = GammaEncoding.readCodeGamma(bitStream);
            allocateOccurrenceArrays();
            if (merge01ForProbas) {
                // import occurrenceMatrix
                for (int context = 0; context < contextSize; context++) {
                    int occ01 = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                    occurrences[context][0] = occ01;
                    occurrences[context][1] = occ01;
                    occurrences[context][2] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                }
                // import occurrenceInitTensor
                for (int context = 0; context < contextSizeInit; context++) {
                    int occ01 = in.readInt();
                    occurrencesInit[context][0] = occ01;
                    occurrencesInit[context][1] = occ01;
                    occurrencesInit[context][2] = in.readInt();
                }
            } else {
                // import occurrenceMatrix
                for (int context = 0; context < contextSize; context++) {
                    for (int symbol = 0; symbol < nbSymbols; symbol++) {
                        occurrences[context][symbol] = BinaryEncoding.readCodeBinary(bitStream, nbBitsPerOccurrence);
                    }
                }
                // import occurrenceInitTensor
                for (int context = 0; context < contextSizeInit; context++) {
                    for (int symbol = 0; symbol < nbSymbols; symbol++) {
                        occurrencesInit[context][symbol] = in.readInt();
                    }
                }
            }
            convertToOccurrenceIntervals();
        }
    }
    
    /*
     * First, it uses arithmetic decoding of the initial trits until it reaches k+w trits,
     * then it uses arithmetic decoding of the remaining trits.
     */
    public final void readTritSequence(int nbTwosToRead, ArithmeticDecoder decoder, TritSequence tritSequence) {
        tritSequenceWriter.init(nbTwosToRead, tritSequence);
        while (tritSequenceWriter.isInInitialState()) {
            int index = tritSequenceWriter.getInitialIndex();
            // Decode a trit
            int currentTrit = decoder.read(occurrencesCumulativeInit[index]);
            // Update the index for the next iteration.
            tritSequenceWriter.addTrit(currentTrit);
            tritSequenceWriter.updateInitialIndex();
        }
        while (tritSequenceWriter.isInGenericState()) {
            int index = tritSequenceWriter.getGenericIndex();
            // Decode a trit
            int currentTrit = decoder.read(occurrencesCumulative[index]);
            // Update the indexes for the next iteration.
            tritSequenceWriter.addTrit(currentTrit);
            tritSequenceWriter.updateGenericIndex();
        }
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Some testing
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Getting the true probabilities for the example given in our paper.
     */
    public void testing(ArrayList<TritSequence> tritSequenceList) {
        // For testing purposes only
        boolean outputProbas = false;
        if (outputProbas) {
            try (FileWriter out = new FileWriter("context_probas.txt")) {
                out.write("Method Context with k = " + k + ", w = " + w + " and kInit = " + kInit + "\n");
                exportProbas(out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        boolean outputTritSequences = false;
        if (outputTritSequences) {
            for (TritSequence tritSequence : tritSequenceList) {
                System.out.println(tritSequence.toString());
            }
        }
        boolean printProbas = false;
        if (printProbas) {
            int kContext = 1;
            int wContext = 2;
            int kInitContext = 2;
            int[] gapArray = {4,1,1,3,5,2};
            TritSequence tritSequence = new TritSequence();
            Tools.intArray2tritSequence(new IntsRef(gapArray), tritSequence);
            System.out.println(tritSequence.toString());
            // It gives "002221201202".
            System.out.println("Initial context");
            {
                int iTrit = 0;
                while (iTrit < kContext + wContext) {
                    int contextSizeLocal = iTrit < kInitContext ? iTrit : kInitContext;
                    int[] context = new int[contextSizeLocal];
                    for (int iContext = 0; iContext < contextSizeLocal; iContext++) {
                        context[iContext] = tritSequence.get(iTrit - contextSizeLocal + iContext);
                    }
                    int indexContext = getInitialContextIndex(context, kInitContext);
                    double[] probas = new double[nbSymbols];
                    int index = nbTrits * (1 - Tools.pow(nbSymbolsForContext, contextSizeLocal)) / (1 - nbSymbolsForContext) + indexContext;
                    for (int trit = 0; trit < nbSymbols; trit++) {
                        probas[trit] = (double)occurrencesInit[index][trit] / (double)occurrencesCumulativeInit[index][nbSymbols];
                    }
                    System.out.println(Arrays.toString(probas));
                    iTrit++;
                }
            }
            System.out.println("General context");
            for (int iTrit = kContext + wContext; iTrit < tritSequence.size(); iTrit++) {
                int[] context = new int[kContext];
                for (int iContext = 0; iContext < kContext; iContext++) {
                    context[iContext] = tritSequence.get(iTrit - kContext + iContext);
                }
                int nbTwosBefore = 0;
                for (int iContext = 0; iContext < wContext; iContext++) {
                    if (tritSequence.get(iTrit - kContext - wContext + iContext) == 2) {
                        nbTwosBefore++;
                    }
                }
                int indexContext = getContextIndex(context, kContext, wContext, nbTwosBefore);
                double[] probas = new double[nbSymbols];
                for (int trit = 0; trit < nbSymbols; trit++) {
                    probas[trit] = (double)occurrences[indexContext][trit] / (double)occurrencesCumulative[indexContext][nbSymbols];
                }
                System.out.println(Arrays.toString(probas));
            }
        }
    }
    
    public final void exportProbas(FileWriter out) {
        try {
            if (merge01ForContext) {
                out.write("Occurrence init tensor\n");
                for (int context = 0; context < contextSizeInit; context++) {
                    out.write(((double)occurrencesInit[context][0] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
                    out.write(((double)occurrencesInit[context][2] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
                }
                out.write("Occurrence matrix\n");
                for (int context = 0; context < contextSize; context++) {
                    out.write(((double)occurrences[context][0] / (double)occurrencesCumulative[context][nbSymbols]) + " ");
                    out.write(((double)occurrences[context][2] / (double)occurrencesCumulative[context][nbSymbols]) + " ");
                }
                out.write("\n");
            } else {
                out.write("Occurrence init tensor\n");
                for (int context = 0; context < contextSizeInit; context++) {
                    for (int trit = 0; trit < nbSymbols; trit++) {
                        out.write(((double)occurrencesInit[context][trit] / (double)occurrencesCumulativeInit[context][nbSymbols]) + " ");
                    }
                }
                out.write("Occurrence matrix\n");
                for (int context = 0; context < contextSize; context++) {
                    for (int trit = 0; trit < nbSymbols; trit++) {
                        out.write(((double)occurrences[context][trit] / (double)occurrencesCumulative[context][nbSymbols]) + " ");
                    }
                }
                out.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static int getInitialContextIndex(int[] context, int kInitContext) {
        return 0;
    }
    
    public static int getContextIndex(int[] context, int kContext, int wContext, int nbTwosBefore) {
        assert(kContext == context.length);
        int currentIndex = 0;
        for (int i = 0; i < kContext; i++) {
            currentIndex *= nbTrits;
            currentIndex += context[i];
        }
        return (wContext + 1) * currentIndex + nbTwosBefore;
    }

}
