/*
 * Remark: tritlistencoding.Method is the TritSequence equivalent of
 * gaparraylistencoding.MethodByBitSequence.
 * bitlistencoding.Method is also doing the same stuff.
 *
 * N.B.: It is possible to handle the similarities with templates.
 */
package tritlistencoding;

import arrays.IntsRef;
import compresslists.Tools;
import io.BitInputStream;
import io.BitInputStreamFile;
import io.BitOutputStream;
import io.BitOutputStreamFile;
import io.DatasetInfo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author yann
 */
public abstract class Method {
    
    /*
     * Some methods need to be initialized according to the trit sequence list
     * it handles.
     */
    public void initBefore() {}
    public void init(TritSequence tritSequence) {}
    public void initAfter() {}
    
    public abstract String getName();
    
    /*
     * Compute the size, in bits, that will be taken to code this trit sequence.
     */
    public void computeSizeBefore() {}
    public abstract double computeSize(TritSequence tritSequence);
    public double computeSizeAfter() { return 0.; }
    
    /*
     * Compute the size, in bits, that will be taken to code this trit sequence
     * list.
     */
    public double computeSize(ArrayList<TritSequence> tritSequenceList) {
        double size = computeSizeNeededInformation();
        for (TritSequence tritSequence : tritSequenceList) {
            size += computeSize(tritSequence);
        }
        return size;
    }
    
    /*
     * Log additional information.
     */
    public void standardOutput() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Manipulation of additional material needed by the method.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public double computeSizeNeededInformation() {
        return 0.0;
    }
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void exportNeededInformation(DataOutputStream out) throws IOException {}
    
    /*
     * Some methods need some additional information to decode the index.
     */
    public void importNeededInformation(DataInputStream in) throws IOException {}
    
    /*
     * Some methods keep some bits to be written at the end.
     */
    public void outputRemainingBits(BitOutputStream bitStream) {}
    
    /*
     * All the MethodByBitSequence are obliged to read byte by byte. When
     * considering padding, the last bits of the last byte read can be
     * discarded. But when we don't use padding, we have to keep them.
     */
    public void flushRemainingBits() {}
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Creation of the file from the trit sequences
    // Some method add a padding so that each trit sequence starts at the
    // beginning of a byte in the file --- thus no trit sequence can be encoded
    // by less than a byte.
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Exports all the trit sequences.
     * The exported file will be named
     * filenameBegin + "_" + getName() + extension
     */
    public final void exportToFile(String filenameBegin, String extension, ArrayList<TritSequence> tritSequenceList, int nbDocuments) {
        exportToFile(filenameBegin + "_" + getName() + extension, tritSequenceList, nbDocuments);
    }
    
    public abstract void writeTritSequence(BitOutputStream bitStream, TritSequence tritSequence);
    
    public void writeTritSequenceList(BitOutputStream bitStream, ArrayList<TritSequence> tritSequenceList) {
        for (TritSequence tritSequence : tritSequenceList) {
            writeTritSequence(bitStream, tritSequence);
        }
    }
    
    /*
     * Writes the trit sequence list in a dataOutputStream, by first encoding
     * them with the given compression method.
     */
    public final void writeTritSequenceList(DataOutputStream dataOutputStream, ArrayList<TritSequence> tritSequenceList) throws IOException {
        try (BitOutputStreamFile bitOutputStream = new BitOutputStreamFile(dataOutputStream)) {
            writeTritSequenceList(bitOutputStream, tritSequenceList);
            outputRemainingBits(bitOutputStream);
        }
    }
    
    /*
     * Exports all the trit sequences.
     * The exported file will be named filename.
     * Some schemes split the index in two files, and in that case it is
     * possible to store only in one of those files the sizes. Hence, to gain
     * memory, we allow not to store the sizes for the other file.
     */
    public final void exportToFile(String filename, ArrayList<TritSequence> tritSequenceList, int nbDocuments, boolean exportSizes) {
        try (FileOutputStream fout = new FileOutputStream(filename);
                DataOutputStream out = new DataOutputStream(fout)) {
            if (exportSizes) {
                out.writeInt(tritSequenceList.size());
                out.writeInt(nbDocuments);
            }
            exportNeededInformation(out);
            if (exportSizes) {
                for (TritSequence tritSequence : tritSequenceList) {
                    out.writeInt(tritSequence.numberOfTwos());
                }
            }
            writeTritSequenceList(out, tritSequenceList);
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    public final void exportToFile(String filename, ArrayList<TritSequence> tritSequenceList, int nbDocuments) {
        exportToFile(filename, tritSequenceList, nbDocuments, true);
    }
    
    public abstract void readTritSequence(BitInputStream bitStream, int nbTwosToRead, TritSequence tritSequence);
    
    public ArrayList<TritSequence> readTritSequenceList(BitInputStream bitStream, ArrayList<Integer> nbTwosToReadList) {
        ArrayList<TritSequence> tritSequenceList = new ArrayList<>();
        for (int nbTwosToRead : nbTwosToReadList) {
            TritSequence tritSequence = new TritSequence();
            readTritSequence(bitStream, nbTwosToRead, tritSequence);
            tritSequenceList.add(tritSequence);
        }
        return tritSequenceList;
    }
    
    /*
     * Reads the gap array list from a dataInputStream, by first decoding them
     * with the given compression method.
     */
    public final ArrayList<TritSequence> readTritSequenceList(DataInputStream dataInputStream, ArrayList<Integer> nbTwosToReadList) {
        BitInputStreamFile bitInputStream = new BitInputStreamFile(dataInputStream);
        ArrayList<TritSequence> tritSequenceList = readTritSequenceList(bitInputStream, nbTwosToReadList);
        flushRemainingBits();
        return tritSequenceList;
    }
    
    /*
     * Imports all the trit sequences from a file that contains the total number
     * of documents and the number of ones of each trit sequence.
     */
    public final DatasetInfo importFromFile(String filename) {
        ArrayList<int[]> gapArrayList = new ArrayList<>();
        int nbGapArrays = 0;
        int nbDocuments = 0;
        try (FileInputStream fin = new FileInputStream(filename);
                DataInputStream in = new DataInputStream(fin)) {
            nbGapArrays = in.readInt();
            nbDocuments = in.readInt();
            importNeededInformation(in);
            ArrayList<Integer> nbTwosToReadList = new ArrayList<>();
            for (int i = 0; i < nbGapArrays; i++) {
                int tritSequenceSize = in.readInt();
                nbTwosToReadList.add(tritSequenceSize);
            }
            ArrayList<TritSequence> tritSequenceList = readTritSequenceList(in, nbTwosToReadList);
            for (int i = 0; i < nbGapArrays; i++) {
                TritSequence tritSequence = tritSequenceList.get(i);
                IntsRef gapArrayRef = new IntsRef(nbTwosToReadList.get(i));
                Tools.tritSequence2intArray(tritSequence, gapArrayRef);
                gapArrayList.add(gapArrayRef.ints);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open file '" + filename + "'.");
        } catch (IOException e) {
            throw new RuntimeException("IOException in file '" + filename + "'.");
        }
        return new DatasetInfo(gapArrayList, nbDocuments);
    }

}
