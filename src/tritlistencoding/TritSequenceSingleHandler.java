/*
 * Handles a list of trits so that there is:
 *     a) an initial pass
 *     b) a generic pass
 */
package tritlistencoding;

import compresslists.Tools;
import static compresslists.Tools.nbTrits;

/**
 *
 * @author yann
 */
public abstract class TritSequenceSingleHandler implements TritSequenceAbstractHandler {
    
    // Parameters
    private final int kInit;
    private final int k;
    private final int w;
    // 2 symbols if we merge 0 and 1, 3 symbols otherwise
    private final boolean merge01ForContext;
    private final int nbSymbolsForContext;
    
    // Initial index.
    private int nbIndexesInitBefore;
    // indexInit is a bijection between kInit-uplets of trits and { 0, ..., nbSymbolsForContext^kInit - 1 }.
    private int indexInit;
    private final int moduloForInitial;
    
    // Generic index.
    // Number of "2" in the w trits before the k last ones. This is a value in { 0, ..., w }.
    private int numberOfTwos;
    // subIndex is a bijection between k-uplets of trits and { 0, ..., nbSymbolsForContext^k - 1 }.
    private int subIndex;
    // index is a bijection between the k-uplet of preceding trits,
    // the number of "2" in the w trits before the k preceding trits,
    // and { 0, ..., nbSymbolsForContext^k * (w+1) - 1 }.
    private int indexGeneric;
    private int indexNextTritToLeaveTheWTrits;
    private final int moduloForGeneric;
    
    // The trit list to handle.
    protected int nbTritsHandled;
    protected TritSequence tritSequence;
    
    public TritSequenceSingleHandler(int k, int w, int kInit, boolean merge01ForContext) {
        this.k = k;
        this.w = w;
        this.kInit = kInit;
        this.merge01ForContext = merge01ForContext;
        nbSymbolsForContext = merge01ForContext ? nbTrits - 1 : nbTrits;
        moduloForGeneric = Tools.pow(nbSymbolsForContext, k);
        moduloForInitial = Tools.pow(nbSymbolsForContext, kInit);
    }
    
    @Override
    public void init() {
        this.nbIndexesInitBefore = 0;
        this.indexInit = 0;
        this.nbTritsHandled = 0;
    }
    
    @Override
    public int getCurrentTrit() {
        return tritSequence.get(nbTritsHandled);
    }
    
    public static int trit2symbol(int trit) {
        switch(trit) {
            case 0:
            case 1:
                return 0;
            case 2:
                return 1;
            default:
                throw new RuntimeException("Use trit2symbol only on integer values in {0, 1, 2}.");
        }
    }
    
    private int getSymbol(int position) {
        return merge01ForContext
                ? trit2symbol(tritSequence.get(position))
                : tritSequence.get(position);
    }
    
    protected void initializeGenericIndex() {
        // k last trits
        subIndex = 0;
        for (int i = 0; i < k; i++) {
            subIndex *= nbSymbolsForContext;
            subIndex += getSymbol(nbTritsHandled - k + i);
        }
        // w trits before
        indexNextTritToLeaveTheWTrits = nbTritsHandled - k - w;
        numberOfTwos = tritSequence.numberOfTwos(
                indexNextTritToLeaveTheWTrits,
                indexNextTritToLeaveTheWTrits + w);
        // merge the two
        indexGeneric = (w + 1) * subIndex + numberOfTwos;
    }
    
    @Override
    public boolean isInInitialState() {
        if (nbTritsHandled >= k + w) {
            initializeGenericIndex();
            return false;
        }
        return isStillActive();
    }
    
    @Override
    public void updateInitialIndex() {
        int currentSymbol = getSymbol(nbTritsHandled);
        indexInit = (nbSymbolsForContext * indexInit + currentSymbol) % moduloForInitial;
        nbTritsHandled++;
        if (nbTritsHandled <= kInit) {
            nbIndexesInitBefore = nbTrits * (1 - Tools.pow(nbSymbolsForContext, nbTritsHandled)) / (1 - nbSymbolsForContext);
        }
    }
    
    @Override
    public int getInitialIndex() {
        return nbIndexesInitBefore + indexInit;
    }
    
    @Override
    public boolean isInGenericState() {
        return isStillActive() && nbTritsHandled >= k + w;
    }
    
    @Override
    public void updateGenericIndex() {
        int currentSymbol = getSymbol(nbTritsHandled);
        if (tritSequence.get(indexNextTritToLeaveTheWTrits) == 2) {
            numberOfTwos--;
        }
        if (tritSequence.get(indexNextTritToLeaveTheWTrits + w) == 2) {
            numberOfTwos++;
        }
        indexNextTritToLeaveTheWTrits++;
        subIndex = (nbSymbolsForContext * subIndex + currentSymbol) % moduloForGeneric;
        indexGeneric = (w + 1) * subIndex + numberOfTwos;
        nbTritsHandled++;
    }
    
    @Override
    public int getGenericIndex() {
        return indexGeneric;
    }
    
}
