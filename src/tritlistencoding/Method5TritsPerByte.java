/*
 * We convert the trit sequence to a bit sequence to store it in a file. To do
 * the conversion, we use the fact that 3**5 = 243 and 2**8 = 256, so we store
 * 5 trits on 8 bits (1 byte).
 */
package tritlistencoding;

import integerencoding.BinaryEncoding;
import io.BitInputStream;
import io.BitOutputStream;
import static compresslists.Tools.bitArray2byte;
import static compresslists.Tools.byte2int;
import static compresslists.Tools.nbBitsPerByte;
import static compresslists.Tools.nbTrits;
import static compresslists.Tools.nbTritsPerByte;

/**
 *
 * @author yann
 */
public class Method5TritsPerByte extends Method {
    
    // For writing
    private int currentByte      = 0;
    protected int nbTritsWritten = 0;
    
    // For reading
    protected int[] currentTrits = new int[nbTritsPerByte];
    protected int nbCurrentTritsRead = nbTritsPerByte;
    
    /**
     * Creates a new instance of Method5TritsPerByte.
     */
    public Method5TritsPerByte() {}
    
    @Override
    public String getName() {
        return "5TritsPerByte";
    }
    
    @Override
    public final double computeSize(TritSequence tritSequence) {
        return ((double) tritSequence.size()) * 8.0 / 5.0;
    }
    
    @Override
    public void outputRemainingBits(BitOutputStream bitStream) {
        if (nbTritsWritten != 0) {
            for (int i = 0; i < nbTritsPerByte - nbTritsWritten; i++) {
                currentByte *= nbTrits;
            }
            BinaryEncoding.writeCodeBinary(currentByte, bitStream, nbBitsPerByte);
        }
        currentByte    = 0;
        nbTritsWritten = 0;
    }
    
    @Override
    public void flushRemainingBits() {
        currentTrits = new int[nbTritsPerByte];
        nbCurrentTritsRead = nbTritsPerByte;
    }
    
    @Override
    public final void writeTritSequence(BitOutputStream bitStream, TritSequence tritSequence) {
        // Converts the trits to bits (5 trits per byte).
        for (int i = 0; i < tritSequence.size(); i++) {
            currentByte *= nbTrits;
            currentByte += tritSequence.get(i);
            nbTritsWritten = (nbTritsWritten + 1) % nbTritsPerByte;
            if (nbTritsWritten == 0) {
                BinaryEncoding.writeCodeBinary(currentByte, bitStream, nbBitsPerByte);
                currentByte = 0;
            }
        }
    }
    
    @Override
    public final void readTritSequence(BitInputStream bitStream, int nbTwosToRead, TritSequence tritSequence) {
        tritSequence.reset();
        int nbTwosRead = 0;
        while (nbTwosRead < nbTwosToRead) {
            // If there are no more trits to read, read a new byte from the stream.
            if (nbCurrentTritsRead == nbTritsPerByte) {
                int[] currentBits = new int[nbBitsPerByte];
                for (int i = 0; i < nbBitsPerByte; i++) {
                    currentBits[i] = bitStream.getNextBit();
                }
                nbCurrentTritsRead = 0;
                int byteRead = byte2int(bitArray2byte(currentBits));
                for (int i = 0; i < nbTritsPerByte; i++) {
                    currentTrits[nbTritsPerByte - 1 - i] = byteRead % nbTrits;
                    byteRead /= nbTrits;
                }
            }
            // Extract a trit.
            int tritRead = currentTrits[nbCurrentTritsRead++];
            tritSequence.add(tritRead);
            if (tritRead == 2) {
                // If the trit is 2, it means we finished reading a number.
                nbTwosRead++;
            }
        }
    }
    
}
