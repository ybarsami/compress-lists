/**
 * Sequence of trits (integers in {0, 1, 2}).
 * 
 * This class internally uses java.util.BitSet. We only need to append new bits
 * to the bitset, so the interface is different. Furthermore, we here provide
 * a way to know what is the last trit which is relevant. We thus store a
 * private int "lastPosition" indicating that all trits at index less or equal
 * to lastPosition are relevant.
 * 
 * Remark: This class could implement java.util.Iterator<Integer>, if this could
 * somehow help new code.
 */

package tritlistencoding;

import compresslists.Tools;
import static compresslists.Tools.nbBitsPerByte;

import java.util.BitSet;

/**
 *
 * @author yann
 */
public class TritSequence {
    
    private BitSet bitSet1;
    private BitSet bitSet2;
    private int lastPosition; // The last position of a trit which have been added to this trit sequence.
    
    /**
     * Creates a new instance of TritSequence.
     */
    public TritSequence() {
        bitSet1 = new BitSet();
        bitSet2 = new BitSet();
        lastPosition = -1;
    }
    
    public TritSequence(String s) {
        this();
        for (int i = 0; i < s.length(); i++) {
            add(Integer.parseInt(s.substring(i, i + 1)));
        }
    }
    
    /*
     * WARNING: it does not copy anything but the pointers, so any modification
     * of bitSet1 / bitSet2 in the trit sequence given as argument or the newly
     * created one will affect the other one.
     */
    protected TritSequence(TritSequence tritSequence) {
        bitSet1 = tritSequence.bitSet1;
        bitSet2 = tritSequence.bitSet2;
        lastPosition = tritSequence.lastPosition;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Accessor methods
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * The number of trits in the TritSequence. Because indexes start at 0, it
     * is thus just lastPosition + 1.
     */
    public int size() {
        return lastPosition + 1;
    }
    
    private static int getBit(BitSet bitSet, int i) {
        return bitSet.get(i) ? 1 : 0;
    }
    
    /*
     * Returns the value of the trit with the specified index.
     */
    public int get(int i) throws IndexOutOfBoundsException {
        if (i <= lastPosition && i >= 0) {
            BitSet bitSet = bitSet1;
            int index = i;
            if (i > Integer.MAX_VALUE / 2) {
                bitSet = bitSet2;
                index = i - Integer.MAX_VALUE / 2;
            }
            return 2 * getBit(bitSet, 2 * index) + getBit(bitSet, 2 * index + 1);
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
    
    /*
     * Returns a new byte array containing all the bits in this trit sequence.
     * It writes the bits as they are set in the underlying BitSet(s).
     * The bits are written in big endian by default, but this can be modified
     * by setting Tools.isBigEndian to false.
     * 
     * Remark: The BitSet class (internally used by BitSequence) also has a
     * toByteArray() method, that uses the little endian representation.
     * e.g. 242_{10} = 11110010_2 would be written as 01001111_2 = 79_{10}.
     */
    public byte[] toByteArray() {
        final int nbBytes = Tools.ceilingDivision(size(), nbBitsPerByte / 2);
        byte[] bytes = new byte[nbBytes];
        final int[] bits = new int[nbBitsPerByte];
        for (int i = 0; i < nbBytes; i++) {
            for (int j = 0; j < nbBitsPerByte / 2; j++) {
                int index = i * nbBitsPerByte / 2 + j;
                int trit = index >= size() ? 0 : get(index);
                bits[2 * j    ] = trit / 2;
                bits[2 * j + 1] = trit % 2;
            }
            bytes[i] = Tools.bitArray2byte(bits);
        }
        return bytes;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= lastPosition; i++) {
            sb.append(String.valueOf(get(i)));
        }
        return sb.toString();
    }
    
    /*
     * Output the index of the next "2" in the trit sequence.
     */
    public int indexOfNextTwo(int indexBegin) {
        int index = indexBegin;
        while (get(index) != 2) {
            index++;
        }
        return index;
    }
    
    /*
     * Output the number of "2" in the trit sequence.
     */
    public int numberOfTwos() {
        return numberOfTwos(0, size());
    }
    
    /*
     * Output the number of "2" in the trit sequence, contained in positions k
     * such that beginIndex <= k < endIndex.
     */
    public int numberOfTwos(int beginIndex, int endIndex) {
	int nbTwos = 0;
        for (int i = beginIndex; i < endIndex; i++) {
            if (get(i) == 2) {
                nbTwos++;
            }
        }
	return nbTwos;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Modifying methods
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Allow this TritSequence to be used once more. This is a memory
     * optimization to avoid frequent allocation / deallocation of the
     * long[] arrays that are used by the two BitSets.
     */
    public void reset() {
        lastPosition = -1;
    }
    
    private static void addBit(BitSet bitSet, int bit, int position) {
        if (bit == 1) {
            bitSet.set(position);
        } else {
            bitSet.clear(position);
        }
    }
    
    /*
     * Append the specified value to the trit sequence.
     *
     * @param value a {0, 1, 2} value to append.
     */
    protected void addChecked(int value) throws IndexOutOfBoundsException {
        if (size() == Integer.MAX_VALUE) {
            throw new IndexOutOfBoundsException();
        }
        lastPosition++;
        BitSet bitSet = bitSet1;
        int index = lastPosition;
        if (lastPosition > Integer.MAX_VALUE / 2) {
            bitSet = bitSet2;
            index = lastPosition - Integer.MAX_VALUE / 2;
        }
        addBit(bitSet, value / 2, 2 * index);
        addBit(bitSet, value % 2, 2 * index + 1);
    }
    
    /*
     * Append the specified value to the trit sequence.
     *
     * @param value an integer value to append.
     */
    public void add(int value) throws IndexOutOfBoundsException {
        switch(value) {
            case 0:
            case 1:
            case 2:
                addChecked(value);
                return;
            default:
                throw new RuntimeException("Use TritSequence only on integer values in {0, 1, 2}.");
        }
    }
    
    /*
     * Concatenate the specified trit sequence to the current trit sequence.
     * If it is not possible, no trit is added to this trit sequence and an
     * exception occurs.
     *
     * @param value a trit sequence to be concatenated.
     */
    public void concatenate(TritSequence tritSequence) throws IndexOutOfBoundsException {
        try {
            Math.addExact(size(), tritSequence.size());
            for (int i = 0; i < tritSequence.size(); i++) {
                addChecked(tritSequence.get(i));
            }
        } catch (ArithmeticException e) {
            throw new IndexOutOfBoundsException();
        }
    }
}
