package compresslists;

import bitlistencoding.BitSequenceMarkovHandler;
import gaparrayencoding.*;
import io.Dataset;
import io.DatasetInfo;
import io.DatasetReader;
import static gaparrayencoding.DaMethods.*;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnspecifiedParameterException;
import me.lemire.integercompression.synth.ClusteredDataGenerator;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author yann
 */
public class CompressLists {
    
    public static enum CompressionMethod {
        // Parameter-less integer codes
        METHOD_UNARY("unary"),
        METHOD_BINARY("binary"),
        METHOD_DELTA("delta"),
        METHOD_GAMMA("gamma"),
        METHOD_VARIABLE_BYTE("variable_byte"),
        METHOD_VARIABLE_NIBBLE("variable_nibble"),
        // One-parameter integer codes
        METHOD_ZETA("zeta"),
        METHOD_BERNOULLI("bernoulli"),
        METHOD_GOLOMB("golomb"),
        METHOD_RICE("rice"),
        // Arithmetic and/or trit codes
        METHOD_BOOKSTEIN("bookstein"),
        METHOD_PACKED_ARIT("packed_arit"),
        METHOD_IT("information_theory"),
        METHOD_TRITS("trits"),
        METHOD_CONTEXT("context"),
        METHOD_CONTEXT_ADAPTATIVE("context_adaptative"),
        METHOD_CONTEXT_BIT("context_bit"),
        // Other methods
        METHOD_INTERPOLATIVE("interpolative"),
        METHOD_SIMPLE9("simple9"),
        METHOD_SIMPLE16("simple16"),
        METHOD_SIMPLE8B("simple8b"),
        METHOD_QMX("qmx"),
        METHOD_BP32("bp32"),
        METHOD_FAST_PFOR("fast_pfor"),
        METHOD_NEW_PFD("new_pfd"),
        METHOD_OPT_PFD("opt_pfd"),
        METHOD_AFOR3("afor3"),
        METHOD_ELIAS_FANO("elias_fano"),
        // Meta codes that use other codes for their encoding
        // Blocks uses one code; meta_batch and meta_cluster take multiple other
        // codes and test to keep the best one but are not "true methods", for now
        // it is just to test sizes
        METHOD_BLOCKS("blocks"),
        METHOD_META_BATCH("meta_batch"),
        METHOD_META_CLUSTER("meta_cluster"),
        // Not true method, just to test size
        METHOD_GAP_BOUND("gap_bound");
//        METHOD_REPAIR("repair");
        
        private final String name;
        
        CompressionMethod(String name) {
            this.name = name;
        }
        
        public String getName() { return name; }
    };
    public static final String[] getMethodNames(CompressionMethod methods[]) {
        String[] names = new String[methods.length];
        for (int i = 0; i < methods.length; i++) {
            names[i] = methods[i].getName();
        }
        return names;
    }
    public static final String[] getMethodNames() {
        CompressionMethod methods[] = CompressionMethod.values();
        return getMethodNames(methods);
    }
    
    public static final String getIndexesFolderName(Dataset dataset) {
        return "indexes";
    }
    public static int toAddToBitMethod = 2;
    private static final boolean hasToLog = true;
    
    public static void exportNeededInformation(Method method, String filename) {
        try (FileOutputStream fout = new FileOutputStream(filename);
                DataOutputStream out = new DataOutputStream(fout)) {
            method.exportNeededInformation(out);
        } catch (IOException e) {
            throw new RuntimeException("IOException in '" + filename + "'.");
        }
    }
    
    public static class ContextParameters {
        public int k;
        public int[] wList;
        public int kInit;
        // When merge01ForContext = true and merge01ForProbas = false, we have
        // the following quasi-equivalence:
        //   ContextParameters(k, w, kInit, true, false) ~==
        //   ContextParameters(0, { 1, 1, ..., 1, 1, w }, kInit).
        //                          ---  k "1"  ---
        // There are just two differences: when wList = { 1, 1, ..., 1, 1, w },
        // the cost of storing the wList in the index is slightly higher.
        // When merge01ForContext = true, we merge the context also in the
        // initial trits.
        public boolean merge01ForContext;
        public boolean merge01ForProbas;
        // -1 means we store the exact occurrences, and compute on the fly the
        // number of bits needed for it.
        // Another value means that we implicitely set the sum of probas of all
        // symbols, for each context, to 2^{nbBitsForOccurrences} - 1 (- 1 so
        // that all the probas can always be written on nbBitsForOccurrences
        // bits), hence, we can save 1 proba to save by context.
        public int nbBitsPerOccurrence;
        
        public ContextParameters(int k, int[] wList, int kInit, boolean merge01ForContext, boolean merge01ForProbas, int nbBitsPerOccurrence) {
            this.k = k;
            this.wList = wList;
            this.kInit = kInit;
            this.merge01ForContext = merge01ForContext;
            this.merge01ForProbas = merge01ForProbas;
            this.nbBitsPerOccurrence = nbBitsPerOccurrence;
            int sum = k + Arrays.stream(wList).reduce(0, Math::addExact);
            if (kInit >= sum && kInit > 0) {
                System.out.println("kInit is greater or equal to k + sum(wList), we set it to k + sum(wList) - 1 instead.");
                this.kInit = Math.max(sum - 1, 0);
            }
        }
        
        public ContextParameters(int k, int[] wList, int kInit, boolean merge01ForContext, boolean merge01ForProbas) {
            this(k, wList, kInit, merge01ForContext, merge01ForProbas, -1);
        }
        
        public ContextParameters(int k, int[] wList, int kInit) {
            this(k, wList, kInit, true, false);
        }
        
        public ContextParameters(int k, int w, int kInit, boolean merge01ForContext, boolean merge01ForProbas) {
            this(k, new int[] { w }, kInit, merge01ForContext, merge01ForProbas);
        }
        
        public ContextParameters(int k, int w, int kInit) {
            this(k, new int[] { w }, kInit);
        }
        
        /*
         * It outputs parameters:
         *     k,
         *     wList = { k + 1 },
         *     kInit = ceiling(k / 3)
         * such that the size needed for occurrences is less than a given
         * threshold. A value of 20 for perThousand is set, which means that
         * this threshold is set to 1% of 2 bits per pointer.
         *
         * Remark: As a future work, it would be interesting to see if a good
         * parameterization of nbBitsPerOccurrence w.r.t. nbPointers could be
         * made.
         */
        public ContextParameters(int nbDocuments, long nbPointers) {
            this(nbDocuments, nbPointers, 8, 20);
        }
        public ContextParameters(int nbDocuments, long nbPointers,
                int nbBitsPerOccurrence, int perThousand) {
            this(nbDocuments, nbPointers, nbBitsPerOccurrence, perThousand, false);
        }
        /*
         * @param[in] writeAllProbas: false means that nbBitsPerOccurrence is
         * set (as a value we choose), then, for each context, we write occ(0)
         * and occ(1) on exactly nbBitsPerOccurrence bits, and we deduce occ(2)
         * by 2^{nbBitsPerOccurrence} - 1 - occ(0) - occ(1).
         * True means that, nbBitsPerOccurrence is computed w.r.t. the
         * maximum occurrence, and, for each context, the exact occurrences are
         * written for occ(0), occ(1) and occ(2).
         * Thus, if we set writeAllProbas to true and choose nbBitsPerOccurrence
         * w.r.t. the maximum occurrence, we lose no information, and we gain
         * 33% in the space occupied by occurrences.
         *
         * As example, if you want to use probabilities stored on 16 bits, the
         * model to take maximum 3% * nbPointers bits, and the probabilities to
         * be stored in exact form, then you would call:
         * ContextParameters(nbDocuments, nbPointers, 16, 30, true);
         */
        public ContextParameters(int nbDocuments, long nbPointers,
                int nbBitsPerOccurrence, int perThousand, boolean writeAllProbas) {
            this.merge01ForContext = true;
            this.merge01ForProbas = false;
            this.nbBitsPerOccurrence = writeAllProbas ? -1 : nbBitsPerOccurrence;
            k = 19;
            long maximumSize = perThousand * nbPointers / 1000;
            long sizeOccurrences = maximumSize + 1;
            tritlistencoding.MethodContext methodContext = new tritlistencoding.MethodContext();
            do {
                k--;
                // First stop at Tools.ceilingLog2(nbDocuments) - 1 to have
                // free "2"s at the end of the biggest gaps.
                int remaining = 2 * k + 1 - (Tools.ceilingLog2(nbDocuments) - 1);
                wList = remaining > 0 && k + 1 - remaining > 0
                        ? new int[] { k + 1 - remaining }//, remaining }
                        : new int[] { k + 1 };
                kInit = Tools.ceilingDivision(k, 3);
                try {
                    methodContext.setParameters(this);
                    sizeOccurrences = (long)methodContext.computeSizeNeededInformation();
                } catch (ArithmeticException e) {
                    // An arithmetic exception is thrown when the computations
                    // lead to overflow values. In this case, the loop continues
                    // and k is decreased. There will thus always be a point
                    // where it is guaranteed that the computation will not
                    // overflow.
                }
            } while (k > 0 && sizeOccurrences > maximumSize);
        }
    }
    
    public static ContextParameters AdaptiveContextParameters(long nbPointers) {
        // a and b were fitted thanks to (i) the following data that comes from
        // a hand-optimization:
        // # NbPointers | Optimal params
        //     3606327  7
        //     6169953  7
        //    42253227  8
        //   119802501  9
        //  5979715441 11
        // 15641166521 12
        // and (ii) the following gnuplot code for the fitting:
        // f(x) = log(x) / a + b
        // fit f(x) 'params.txt' using 1:2 via a,b
        double a = 1.67264;
        double b = -2.24758;
        int k = Math.max(7, (int)Math.round(Math.log(nbPointers) / a + b));
        return new ContextParameters(k, new int[] { k }, Math.min(Math.max(2 * k - 1, 0), 16),
                false, false, Math.min(Math.max(k, 8), 16));
    }
    
    public static class BitContextParameters {
        public int k;
        public int w;
        public int kInit;
        public int nbBitsPerOccurrence;
        
        public BitContextParameters(int k, int w, int kInit, int nbBitsPerOccurrence) {
            this.k = k;
            this.w = w;
            this.kInit = kInit;
            int sum = k + w;
            if (kInit >= sum && kInit > 0) {
                System.out.println("kInit is greater or equal to k + w, we set it to k + w - 1 instead.");
                this.kInit = Math.max(sum - 1, 0);
            }
            this.nbBitsPerOccurrence = nbBitsPerOccurrence;
        }
        
        public BitContextParameters(int k, int w, int kInit) {
            this(k, w, kInit, 16);
        }
        
        /*
         * It outputs parameters:
         *     k,
         *     w = k + 1,
         *     kInit = ceiling(k / 3)
         * such that the size needed for occurrences is less than a given
         * threshold.
         *
         * Remark: For BitContext, we set this threshold to 1% of 3 bits per
         * pointer. This is different from Context, where we set this threshold
         * as 1% of 2 bits per pointer.
         *
         * Remark: As a future work, it would be interesting to see if a good
         * parameterization of nbBitsPerOccurrence w.r.t. nbPointers could be
         * made.
         */
        public BitContextParameters(int nbDocuments, long nbPointers) {
            nbBitsPerOccurrence = 16;
            k = 19;
            long maximumSize = 3 * nbPointers / 100; // 1% of 3 bits per pointer
            long sizeOccurrences = maximumSize + 1;
            bitlistencoding.MethodContext methodContext = new bitlistencoding.MethodContext();
            do {
                k--;
                w = k + 1;
                kInit = Tools.ceilingDivision(k, 3);
                try {
                    methodContext.setParameters(this);
                    sizeOccurrences = (long)methodContext.computeSizeNeededInformation();
                } catch (ArithmeticException e) {
                    // An arithmetic exception is thrown when the computations
                    // lead to overflow values. In this case, the loop continues
                    // and k is decreased. There will thus always be a point
                    // where it is guaranteed that the computation will not
                    // overflow.
                }
            } while (k > 0 && sizeOccurrences > maximumSize);
        }
    }
    
    public static BitContextParameters AdaptiveBitContextParameters(int nbDocuments) {
        int k = Tools.ceilingLog2(nbDocuments);
        return new BitContextParameters(k, k, k, k);
    }
    
    public static String[] validEFOptimizations = new String[] {
            "base",
            "strictly-increasing",
            "s-i-opt-block",
            "quasi-succinct",
            "q-s-approx-block",
            "q-s-opt-block"
    };
    
    public static void addMethods(String[] subMethods, JSAPResult config, DatasetReader datasetReader, List<Method> methods) {
        /************************************
         * Get info from the DatasetReader. *
         ************************************/
        int nbWords = datasetReader.getNbWords();
        int nbDocuments = datasetReader.getNbDocuments();
        long nbPointers = datasetReader.getNbPointers();
        
        /*****************************
         * Get info from the config. *
         *****************************/
        // The Elias-Fano optimization chosen, if applicable.
        String efType = config.getString("efType");
        if (!Arrays.asList(validEFOptimizations).contains(efType)) {
            System.err.println(efType + " is not a valid Elias-Fano optimization." +
                    " Valid optimizations are: " +
                    String.join(", ", validEFOptimizations) + ".");
            System.exit(1);
        }
        
        // The bookstein model chosen, if applicable.
        String booksteinModel = config.getString("bookstein_model");
        if (!Arrays.asList(BitSequenceMarkovHandler.getModelNames()).contains(booksteinModel)) {
            System.err.println(booksteinModel + " is not a valid model." +
                    " Valid models are: " +
                    String.join(", ", BitSequenceMarkovHandler.getModelNames()) + ".");
            System.exit(1);
        }
        boolean computeLowerBound = config.getBoolean("bookstein_lower_bound");
        
        /******************************
         * Create context parameters. *
         ******************************/
        // We use automatic parameters unless they have been provided in config.
        List<String> compressionMethodsList = Arrays.asList(subMethods);
        ContextParameters contextParameters;
        BitContextParameters bitContextParameters;
        ContextParameters adaptiveContextParameters;
        BitContextParameters adaptiveBitContextParameters;
        try {
            int k = config.getInt("k");
            int[] wList = config.getIntArray("wList");
            if (wList.length == 0) {
                throw new UnspecifiedParameterException("wList");
            }
            boolean merge01ForContext = !config.getBoolean("dontMerge01ForContext");
            boolean merge01ForProbas = config.getBoolean("merge01ForProbas");
            int nbBitsPerOccurrence = config.getInt("nbBitsPerOccurrence");
            int kInitContext;
            int kInitContextAdaptative;
            try {
                kInitContext = config.getInt("kInit");
                kInitContextAdaptative = config.getInt("kInit");
            } catch (UnspecifiedParameterException e) {
                if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT.getName()) ||
                        compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT_BIT.getName())) {
                    System.out.println("kInit was not specified, we will use default value ceiling(k/3) instead for context method.");
                }
                if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT_ADAPTATIVE.getName())) {
                    System.out.println("kInit was not specified, we will use default value min(2 * k - 1, 16) instead for context_adaptative method.");
                }
                kInitContext = Tools.ceilingDivision(k, 3);
                kInitContextAdaptative = Math.min(Math.max(2 * k - 1, 0), 16);
            }
            contextParameters = new ContextParameters(k, wList, kInitContext, merge01ForContext, merge01ForProbas, nbBitsPerOccurrence);
            bitContextParameters = new BitContextParameters(k, wList[0], kInitContext);
            adaptiveContextParameters = new ContextParameters(k, wList, kInitContextAdaptative, false, false, Math.min(Math.max(k, 8), 16));
            adaptiveBitContextParameters = new BitContextParameters(k, wList[0], kInitContext);
        } catch (UnspecifiedParameterException e) {
            contextParameters = new ContextParameters(datasetReader.getNbDocuments(), datasetReader.getNbPointers());
            bitContextParameters = new BitContextParameters(datasetReader.getNbDocuments(), datasetReader.getNbPointers());
            adaptiveContextParameters = AdaptiveContextParameters(nbPointers);
            adaptiveBitContextParameters = AdaptiveBitContextParameters(nbDocuments);
            if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT.getName()) ||
                    compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT_BIT.getName())) {
                System.out.println("At least a value from (k, wList) was not specified, we will use default values instead.");
                System.out.println("Default context parameters for this dataset are: k = " + contextParameters.k + "; wList = " + Arrays.toString(contextParameters.wList) + "; kInit = " + contextParameters.kInit + ".");
            }
            if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT_ADAPTATIVE.getName())) {
                System.out.println("At least a value from (k, wList) was not specified, we will use default values instead.");
                System.out.println("Default context_adaptative parameters for this dataset are: k = " + adaptiveContextParameters.k + "; wList = " + Arrays.toString(adaptiveContextParameters.wList) + "; kInit = " + adaptiveContextParameters.kInit + ".");
            }
        }
        
        /****************************
         * Add methods to the list. *
         ****************************/
        // Interpolative is added first because it is used as baseline for comparisons.
        if (compressionMethodsList.contains(CompressionMethod.METHOD_INTERPOLATIVE.getName())) {
            methods.add(new MethodInterpolative(nbDocuments));
//            methods.add(new MethodInterpolative.MethodDynamicInterpolative(nbDocuments));
//            methods.add(new MethodInterpolative.MethodDynamicInterpolativeOptimistic(nbDocuments));
        }
        
        // Parameter-less integer codes
        if (compressionMethodsList.contains(CompressionMethod.METHOD_UNARY.getName())) {
            methods.add(new MethodUnary());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_BINARY.getName())) {
            methods.add(new MethodBinary(Tools.ceilingLog2(nbDocuments)));
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_DELTA.getName())) {
            methods.add(new MethodDelta());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_GAMMA.getName())) {
            methods.add(new MethodGamma());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_VARIABLE_BYTE.getName())) {
            methods.add(new MethodVariableByte());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_VARIABLE_NIBBLE.getName())) {
            methods.add(new MethodVariableNibble());
        }
        
        // One-parameter integer codes
        if (compressionMethodsList.contains(CompressionMethod.METHOD_ZETA.getName())) {
            int zetaK = config.getInt("zetaK");
            methods.add(new MethodZeta(zetaK));
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_BERNOULLI.getName())) {
            double bernoulliP = config.getDouble("bernoulliP");
            Method methodBernoulliGlobal = (bernoulliP <= 0 || bernoulliP >= 1)
                    ? new MethodBernoulliGlobal(nbWords, nbDocuments, nbPointers)
                    : new MethodBernoulliGlobal(nbDocuments, bernoulliP);
            Method methodBernoulliLocal = new MethodBernoulliLocal (nbDocuments);
            methods.add(methodBernoulliGlobal);
            methods.add(methodBernoulliLocal);
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_GOLOMB.getName())) {
            int golombB = config.getInt("golombB");
            Method methodGolombGlobal = golombB <= 0
                    ? new MethodGolombGlobal(nbWords, nbDocuments, nbPointers)
                    : new MethodGolombGlobal(golombB);
            Method methodGolombLocal = new MethodGolombLocal(nbDocuments);
            methods.add(methodGolombGlobal);
            methods.add(methodGolombLocal);
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_RICE.getName())) {
            int riceK = config.getInt("riceK");
            Method methodRice = riceK <= 0
                    ? new MethodRice(nbWords, nbDocuments, nbPointers)
                    : new MethodRice(riceK);
            methods.add(methodRice);
        }
        
        // Arithmetic and/or trit codes
        if (compressionMethodsList.contains(CompressionMethod.METHOD_BOOKSTEIN.getName())) {
            methods.add(new MethodBookstein(BitSequenceMarkovHandler.getModel(booksteinModel), computeLowerBound));
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_PACKED_ARIT.getName())) {
            methods.add(new MethodPackedArithmetic());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_IT.getName())) {
            methods.add(new MethodInformationTheoretic(nbDocuments));
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_TRITS.getName())) {
            methods.add(new MethodTrits());
            /*
            methods.add(new MethodArithmeticTrits(nbDocuments, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_DENSITY));
            methods.add(new MethodArithmeticTrits(nbDocuments, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_REAL));
            methods.add(new MethodArithmeticTrits(nbDocuments, MethodArithmeticTrits.OccurrenceType.OCCURRENCE_UNIFORM));
            for (int i = 7; i < 11; i++) {
                methods.add(new MethodArithmeticXTrits(i)); // Processes i trits at a time.
            }
            */
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT.getName())) {
            // Do not work with wList
            /*
            methods.add(new MethodFContextTrit        (kContext, wListContext, kInitContext));
            methods.add(new MethodFContextTritBis     (kContext, wListContext, kInitContext, kFinalContext));
            methods.add(new MethodFContextBit         (kContext, wListContext, kInitContext, kFinalContext));
            methods.add(new MethodSplitContextInterp  (kContext, wListContext, kInitContext, nbDocuments));
            methods.add(new MethodBitVectorContext    (kContext, wListContext, kInitContext));
            methods.add(new MethodContextBit          (kContext, wListContext, kInitContext, kFinalContext));
            methods.add(new MethodSplitContextContext (kContext, wListContext, kInitContext, kFinalContext));
            methods.add(new MethodSplitContextContext2(kContext, wListContext, kInitContext, kFinalContext));
            methods.add(new MethodContextTrit         (kContext, wListContext, kInitContext));
            */
            // Not taken into consideration
            /*
            methods.add(new MethodTSplitContextContext (contextParameters, kFinalContext));
            methods.add(new MethodTSplitContextContext2(contextParameters, kFinalContext));
            methods.add(new MethodTTSplit              (contextParameters, kFinalContext));
            methods.add(new MethodFContextTestingOnly1 (kFinalContext, nbDocuments));
            methods.add(new MethodFContextTestingOnly2 (kFinalContext, nbDocuments));
            methods.add(new MethodFContextTestingOnly3 (               nbDocuments));
            methods.add(new MethodSplitGammaContext   (kFinalContext));
            methods.add(new MethodSplitITContext      (nbDocuments,  kFinalContext));
            methods.add(new MethodSplitInterpOptimisticContext(nbDocuments,  kFinalContext));
            methods.add(new MethodSplitInterpContext  (nbDocuments,  kFinalContext));
            methods.add(new MethodTTSplitInterpOpt    (nbDocuments,  kFinalContext));
            methods.add(new MethodTritConcatenateContext(contextParameters, nbDocuments));
            methods.add(new MethodQuatritConcatenateContext(contextParameters, nbDocuments));
            methods.add(new MethodQuatritListContext  (contextParameters, nbDocuments));
            */
            methods.add(new MethodTritListContext     (contextParameters));
            /*
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 16, 40)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 16, 30)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 16, 20)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 16, 10)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 8, 40)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 8, 30)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 8, 20)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 8, 15)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 8, 10)));
            methods.add(new MethodTritListContext     (new ContextParameters(nbDocuments, nbPointers, 8, 5)));
            */
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT_ADAPTATIVE.getName())) {
//            methods.add(new MethodTritListAdaptiveContext(adaptiveContextParameters, nbDocuments, datasetReader.getGapArrayLength(0), false));
            methods.add(new MethodTritListAdaptiveContext(adaptiveContextParameters, nbDocuments, datasetReader.getGapArrayLength(0), true));
//            methods.add(new MethodTritConcatenateAdaptiveContext(new ContextParameters(10, new int[] { 11 }, 4, true, true, 30), nbDocuments, datasetReader.getGapArrayLength(0), true));
            /*
            for (int k : new int[] { 10, 11, 12 }) {
                for (int nbBitsOcc : new int[] { k }) {
                    methods.add(new MethodTritListAdaptiveContext(new ContextParameters(k, new int[] { k }, 16, true, true, nbBitsOcc), nbDocuments, datasetReader.getGapArrayLength(0), true));
                }
            }
            */
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_CONTEXT_BIT.getName())) {
            // Context on the output of another method
            /*
            methods.add(new MethodDeltaContext(bitContextParameters));
            methods.add(new MethodDeltaContext(new BitContextParameters(9, 5, 7)));
            methods.add(new MethodByBitComposition(
                    new MethodInterpolative(nbDocuments),
                    new bitlistencoding.MethodContext(new BitContextParameters(9, 5, 7))));
            // Context on the bitvector
            methods.add(new MethodBitVectorContext(new BitContextParameters(15, 0, 9)));
            methods.add(new MethodBitVectorAdaptiveContext(new BitContextParameters(7, 7, 3, 15), nbDocuments, datasetReader.getGapArrayLength(0), true));
            for (int k : new int[] { 13, 14, 15 }) {
                for (int nbBits : new int[] { Tools.ceilingLog2(nbDocuments), Tools.ceilingLog2(nbDocuments) + 1, Tools.ceilingLog2(nbDocuments) - 1 }) {
                    methods.add(new MethodBitVectorAdaptiveContext(new BitContextParameters(k, k, k, nbBits), nbDocuments, datasetReader.getGapArrayLength(0), true));
                }
            }
            */
            methods.add(new MethodBitVectorContext(bitContextParameters));
            methods.add(new MethodBitVectorAdaptiveContext(adaptiveBitContextParameters, nbDocuments, datasetReader.getGapArrayLength(0), true));
        }
        
        // Other methods
        if (compressionMethodsList.contains(CompressionMethod.METHOD_SIMPLE9.getName())) {
            methods.add(new MethodSimple9());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_SIMPLE16.getName())) {
            methods.add(new MethodSimple16());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_SIMPLE8B.getName())) {
            methods.add(new MethodSimple8b());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_QMX.getName())) {
            methods.add(new MethodQMX());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_BP32.getName())) {
            methods.add(new MethodBinaryPacking32());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_FAST_PFOR.getName())) {
            methods.add(new MethodFastPFOR());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_NEW_PFD.getName())) {
            methods.add(new MethodNewPFD());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_OPT_PFD.getName())) {
            methods.add(new MethodOptPFD());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_AFOR3.getName())) {
            methods.add(new MethodAfor3());
        }
        if (compressionMethodsList.contains(CompressionMethod.METHOD_ELIAS_FANO.getName())) {
            if (efType.equals("base")) {
                methods.add(new MethodEliasFano(nbDocuments, false));
            } else if (efType.equals("strictly-increasing")) {
                methods.add(new MethodEliasFano(nbDocuments));
            } else if (efType.equals("s-i-opt-block")) {
                methods.add(new MethodEliasFanoOptBlock());
            } else if (efType.equals("quasi-succinct")) {
                methods.add(new MethodEliasFano(nbDocuments));
            } else if (efType.equals("q-s-approx-block")) {
                methods.add(new MethodQuasiSuccinctApproxBlock());
            } else if (efType.equals("q-s-opt-block")) {
                methods.add(new MethodQuasiSuccinctOptBlock());
            }
            /*
            // Code for partial computations, which may be needed for OptBlock.
            ArrayList<Method> methodsEF = new ArrayList<>();
            methodsEF.add(new MethodQuasiSuccinctOptBlock());
            for (Method method : methodsEF) {
                method.init(datasetReader);
//                    sizeInBits = method.computeSize(datasetReader, 0, 0.); // For partial computations
                sizeInBits = method.computeSize(datasetReader, 79, 380022589.); // e.g., for TREC stopped at 79%
            }
            */
        }
        // Not taken into consideration
        /*
        methods.add(new MethodBitVectorIT           ());
        methods.add(new MethodSplitInterpContext    (nbDocuments, kInit));
        methods.add(new MethodTritConcatenateContext(contextParameters, nbDocuments));
        methods.add(new MethodQuatritListContext    (contextParameters, nbDocuments));
        methods.add(new MethodByBlockNoPadding      (new MethodEliasFano(nbDocuments)));
        methods.add(new MethodByBlockNoPadding      (new MethodQuasiSuccinct(nbDocuments)));
        */
    }
    
    /**
     * @param args the command line arguments
     * @throws JSAPException
     */
    public static void main(String[] args) throws JSAPException {
        JSAP jsap = new JSAP();
        
        Switch helpOpt1 = new Switch("help")
                        .setShortFlag('h')
                        .setLongFlag("help");
        helpOpt1.setHelp("Displays help message.");
        jsap.registerParameter(helpOpt1);
        
        FlaggedOption opt1 = new FlaggedOption("input")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setRequired(false)
                                .setShortFlag('i')
                                .setLongFlag("input");
        opt1.setHelp("The input file to compress.");
        jsap.registerParameter(opt1);
        FlaggedOption opt2 = new FlaggedOption("type")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setRequired(false)
                                .setShortFlag('t')
                                .setLongFlag("type");
        opt2.setHelp("The type of the input file to compress. Valid types are: " +
                String.join(", ", Dataset.getFormatNames()) + ". If the type is not given, we will " +
                "try to guess it from the file extension.");
        jsap.registerParameter(opt2);
        FlaggedOption opt3 = new FlaggedOption("compressionMethods")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setList(true)
                                .setListSeparator(',')
                                .setDefault("interpolative,context,context_adaptative")
                                .setRequired(false)
                                .setShortFlag('m')
                                .setLongFlag("compressionMethods");
        opt3.setHelp("The compression method(s) you want to use. Valid methods are: " +
                String.join(", ", getMethodNames()) + ".");
        jsap.registerParameter(opt3);
        Switch opt4 = new Switch("output")
                        .setShortFlag('o')
                        .setLongFlag("output");
        opt4.setHelp("Requests the creation of compressed file(s). " +
                "When not activated, instead requests the computation of the compressed file size(s). " +
                "Those size(s) will be computed without actually creating the file(s). " +
                "Additional information is provided for some methods.");
        jsap.registerParameter(opt4);
        FlaggedOption opt5 = new FlaggedOption("convert")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setRequired(false)
                                .setShortFlag('c')
                                .setLongFlag("convert");
        opt5.setHelp("Requests the conversion of the input file to another format." +
                " Possible conversions are: csv, pisa_docs." +
                " If a conversion is chosen, only the conversion will be done, no other computation.");
        jsap.registerParameter(opt5);
        
        FlaggedOption contextOpt1 = new FlaggedOption("k")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setRequired(false)
                                .setShortFlag('k')
                                .setLongFlag("k");
        contextOpt1.setHelp("The value of k (context methods).");
        jsap.registerParameter(contextOpt1);
        FlaggedOption contextOpt2 = new FlaggedOption("wList")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setList(true)
                                .setListSeparator(',')
                                .setRequired(false)
                                .setShortFlag('w')
                                .setLongFlag("wList");
        contextOpt2.setHelp("The value of wList (context methods).");
        jsap.registerParameter(contextOpt2);
        FlaggedOption contextOpt3 = new FlaggedOption("kInit")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("kInit");
        contextOpt3.setHelp("The value of kInit (context methods). Default value: k/3.");
        jsap.registerParameter(contextOpt3);
        FlaggedOption contextOpt4 = new FlaggedOption("nbBitsPerOccurrence")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setRequired(false)
                                .setDefault("8")
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("nbBitsPerOccurrence");
        contextOpt4.setHelp("The value of nbBitsPerOccurrence (context methods). " +
                "Set the value to -1 to store exact probabilities.");
        jsap.registerParameter(contextOpt4);
        Switch contextOpt5 = new Switch("dontMerge01ForContext")
                        .setShortFlag(JSAP.NO_SHORTFLAG)
                        .setLongFlag("dontMerge01ForContext");
        contextOpt5.setHelp("If not activated, the context will see 0 and 1 as the same thing (context methods)."
                + "For instance contexts [120] and [020] are treated as the same context [*2*] "
                + " ('*' being 0 or 1).");
        jsap.registerParameter(contextOpt5);
        Switch contextOpt6 = new Switch("merge01ForProbas")
                        .setShortFlag(JSAP.NO_SHORTFLAG)
                        .setLongFlag("merge01ForProbas");
        contextOpt6.setHelp("If activated, the arithmetic encoder will encode 0 and 1 with the same probability (context methods).");
        jsap.registerParameter(contextOpt6);
        
        FlaggedOption zetaOpt1 = new FlaggedOption("zetaK")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setDefault("1")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("zetaK");
        zetaOpt1.setHelp("The value of k (Zeta method).");
        jsap.registerParameter(zetaOpt1);
        
        FlaggedOption bernoulliOpt1 = new FlaggedOption("bernoulliP")
                                .setStringParser(JSAP.DOUBLE_PARSER)
                                .setDefault("0")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("bernoulliP");
        bernoulliOpt1.setHelp("The value of p (Global Bernoulli method). " +
                "A value <= 0 or >= 1 indicates that p has to be computed automatically.");
        jsap.registerParameter(bernoulliOpt1);
        
        FlaggedOption golombOpt1 = new FlaggedOption("golombB")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setDefault("0")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("golombB");
        golombOpt1.setHelp("The value of b (Global Golomb method). " +
                "A value <= 0 indicates that b has to be computed automatically.");
        jsap.registerParameter(golombOpt1);
        
        FlaggedOption riceOpt1 = new FlaggedOption("riceK")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setDefault("0")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("riceK");
        riceOpt1.setHelp("The value of k (Rice method). " +
                "A value <= 0 indicates that k has to be computed automatically.");
        jsap.registerParameter(riceOpt1);
        
        FlaggedOption eliasFanoOpt1 = new FlaggedOption("efType")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setDefault("strictly-increasing")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("efType");
        eliasFanoOpt1.setHelp("The chosen Elias-Fano optimization (Elias-Fano methods). Valid optimizations are: " +
                String.join(", ", validEFOptimizations) + ".");
        jsap.registerParameter(eliasFanoOpt1);
        
        FlaggedOption booksteinOpt1 = new FlaggedOption("bookstein_model")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setDefault("M-4C1")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("bookstein_model");
        booksteinOpt1.setHelp("The chosen model (Bookstein methods). Valid models are: " +
                String.join(", ", BitSequenceMarkovHandler.getModelNames()) + ".");
        jsap.registerParameter(booksteinOpt1);
        Switch booksteinOpt2 = new Switch("bookstein_lower_bound")
                        .setLongFlag("bookstein_lower_bound");
        booksteinOpt2.setHelp("Requests to get a lower bound on the size of compressed file (Bookstein methods). " +
                "CAUTION! When activated, the created index is not invertible. " +
                "Use it only to get an estimate of the best compressed size that can be achieved.");
        jsap.registerParameter(booksteinOpt2);
        
        // WARNING: a method is valid by block if it is a MethodByBitSequence.
        CompressionMethod[] validBlockMethods = new CompressionMethod[] {
                // Parameter-less integer codes
                CompressionMethod.METHOD_UNARY,
                CompressionMethod.METHOD_BINARY,
                CompressionMethod.METHOD_DELTA,
                CompressionMethod.METHOD_GAMMA,
                CompressionMethod.METHOD_VARIABLE_BYTE,
                CompressionMethod.METHOD_VARIABLE_NIBBLE,
                // One-parameter integer codes
                CompressionMethod.METHOD_ZETA,
                // Arithmetic and/or trit codes
                CompressionMethod.METHOD_BOOKSTEIN,
                CompressionMethod.METHOD_PACKED_ARIT,
                CompressionMethod.METHOD_IT,
                CompressionMethod.METHOD_TRITS,
                CompressionMethod.METHOD_CONTEXT,
                CompressionMethod.METHOD_CONTEXT_ADAPTATIVE,
                CompressionMethod.METHOD_CONTEXT_BIT,
                // Other methods
                CompressionMethod.METHOD_INTERPOLATIVE,
                CompressionMethod.METHOD_ELIAS_FANO
        };
        FlaggedOption blocksOpt1 = new FlaggedOption("blockSubmethod")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setDefault("interpolative")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("blockSubmethod");
        blocksOpt1.setHelp("The compression method you want to use for the blocks. Valid methods are: " +
                String.join(", ", getMethodNames(validBlockMethods)) + ".");
        jsap.registerParameter(blocksOpt1);
        Switch blocksOpt2 = new Switch("usePadding")
                        .setShortFlag(JSAP.NO_SHORTFLAG)
                        .setLongFlag("usePadding");
        blocksOpt2.setHelp("Enables the use of padding between the encoding of two consecutive blocks" +
                " (block method).");
        jsap.registerParameter(blocksOpt2);
        
        CompressionMethod[] validMetaMethods = new CompressionMethod[] {
                // Parameter-less integer codes
                CompressionMethod.METHOD_UNARY,
                CompressionMethod.METHOD_BINARY,
                CompressionMethod.METHOD_DELTA,
                CompressionMethod.METHOD_GAMMA,
                CompressionMethod.METHOD_VARIABLE_BYTE,
                CompressionMethod.METHOD_VARIABLE_NIBBLE,
                // One-parameter integer codes
                CompressionMethod.METHOD_ZETA,
                CompressionMethod.METHOD_BERNOULLI,
                CompressionMethod.METHOD_GOLOMB,
                CompressionMethod.METHOD_RICE,
                // Arithmetic and/or trit codes
                CompressionMethod.METHOD_BOOKSTEIN,
                CompressionMethod.METHOD_PACKED_ARIT,
                CompressionMethod.METHOD_IT,
                CompressionMethod.METHOD_TRITS,
                CompressionMethod.METHOD_CONTEXT,
                CompressionMethod.METHOD_CONTEXT_ADAPTATIVE,
                CompressionMethod.METHOD_CONTEXT_BIT,
                // Other methods
                CompressionMethod.METHOD_INTERPOLATIVE,
                CompressionMethod.METHOD_SIMPLE9,
                CompressionMethod.METHOD_SIMPLE16,
                CompressionMethod.METHOD_SIMPLE8B,
                CompressionMethod.METHOD_QMX,
                CompressionMethod.METHOD_BP32,
                CompressionMethod.METHOD_FAST_PFOR,
                CompressionMethod.METHOD_NEW_PFD,
                CompressionMethod.METHOD_OPT_PFD,
                CompressionMethod.METHOD_AFOR3,
                CompressionMethod.METHOD_ELIAS_FANO
        };
        FlaggedOption metaOpt1 = new FlaggedOption("subMethods")
                                .setStringParser(JSAP.STRING_PARSER)
                                .setList(true)
                                .setListSeparator(',')
                                .setDefault("interpolative,context,context_adaptative")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("subMethods");
        metaOpt1.setHelp("The compression method(s) you want to test for each of the batches / clusters (meta methods). Valid methods are: " +
                String.join(", ", getMethodNames(validMetaMethods)) + ".");
        jsap.registerParameter(metaOpt1);
        
        FlaggedOption metaBatchOpt1 = new FlaggedOption("batchLimits")
                                .setStringParser(JSAP.DOUBLE_PARSER)
                                .setList(true)
                                .setListSeparator(',')
                                .setDefault("0.,0.0002,0.001,0.01,0.03,0.1,0.2,1.")
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("batchLimits");
        metaBatchOpt1.setHelp("The density limits you want for the different batches (meta_batch method). " +
                "Limits have to be sorted in strictly ascending order. You can specify the 0. and 1. at borders or not.");
        jsap.registerParameter(metaBatchOpt1);
        FlaggedOption metaBatchOpt2 = new FlaggedOption("nbBatches")
                                .setStringParser(JSAP.INTEGER_PARSER)
                                .setRequired(false)
                                .setShortFlag(JSAP.NO_SHORTFLAG)
                                .setLongFlag("nbBatches");
        metaBatchOpt2.setHelp("The number of batches you want (meta_batch method), in case you want the batches to be computed automatically. " +
                "If this parameter is set, it overrides the batchLimits parameter.");
        jsap.registerParameter(metaBatchOpt2);
        Switch metaBatchOpt3 = new Switch("useUniformBatches")
                        .setShortFlag(JSAP.NO_SHORTFLAG)
                        .setLongFlag("useUniformBatches");
        metaBatchOpt3.setHelp("By default, the automatic batches (meta_batch method) are computed logarithmically. " +
                "This requests the creation of batches computed uniformly. " +
                "In any case, we will use nbBatches batches, that has to be provided (no default value).");
        jsap.registerParameter(metaBatchOpt3);
        
        Switch metaClusterOpt1 = new Switch("useShrinkedClusters")
                        .setShortFlag(JSAP.NO_SHORTFLAG)
                        .setLongFlag("useShrinkedClusters");
        metaClusterOpt1.setHelp("Shrink is an optimization with respect to the " +
                "original clustering algorithm that will take less space. If this option " +
                "is chosen, we encode an optimized integer list.");
        jsap.registerParameter(metaClusterOpt1);
        
        JSAPResult config = jsap.parse(args);
        
        // check whether the command line was valid, and if it wasn't,
        // display usage information and exit.
        if (!config.success() || config.getBoolean("help")) {
            System.err.println();
            // print out specific error messages describing the problems with
            // the command line, then print usage, then print full help.
            for (java.util.Iterator errs = config.getErrorMessageIterator();
                    errs.hasNext();) {
                System.err.println("Error: " + errs.next());
            }
            System.err.println();
            System.err.println("Usage: java " + CompressLists.class.getName());
            System.err.println("                " + jsap.getUsage());
            System.err.println();
            System.err.println(jsap.getHelp());
            System.exit(1);
        }
        
        // Input dataset chosen.
        Dataset[] datasets;
        String inputFile = config.getString("input");
        if (inputFile == null) {
            System.out.println("The input dataset was not specified, we will use default value instead.");
            inputFile = Dataset.datasetsFolderName + File.separatorChar + "mail1-bisection.csv";
        }
        String typeFile = config.getString("type");
        if (typeFile == null) {
            System.out.println("The type of the dataset was not specified, we will try to find out automatically. This can fail.");
            int indexExtension = inputFile.lastIndexOf(".");
            String extension = indexExtension == -1
                    ? ""
                    : inputFile.substring(indexExtension + 1);
            typeFile = Dataset.getFormatType(extension);
            System.out.println("The type of the dataset found was: " + typeFile + ".");
        } else {
            if (!Arrays.asList(Dataset.getFormatNames()).contains(typeFile)) {
                System.err.println(typeFile + " is not a valid format." +
                        " Valid types are: " +
                        String.join(", ", Dataset.getFormatNames()) + ".");
                System.exit(1);
            }
        }
        // TODO: handle different input files (with corresponding types)
        datasets = new Dataset[] { new Dataset(inputFile, Dataset.getFormat(typeFile)) };
        // For huge datasets, we must take special care.
        boolean isThisGov2      = datasets.length > 0 && datasets[0].getDatasetName().startsWith("gov2");
        boolean isThisClueWeb09 = datasets.length > 0 && datasets[0].getDatasetName().startsWith("clueweb09");
        
        // Conversion of the dataset to another format.
        // If this is chosen, only this will be done.
        String convert = config.getString("convert");
        if (convert != null) {
            // binary is a hidden feature, as it was only developped for testing.
            if (!Arrays.asList(new String[] { "csv", "pisa_docs", "binary" }).contains(convert)) {
                System.err.println(convert + " is not a valid conversion format." +
                        " Possible conversion formats are: csv, pisa_docs.");
                System.exit(1);
            }
        }
        boolean exportToCsv    = "csv".equals(convert);       // Export this dataset --- CSV format.
        boolean exportToPisa   = "pisa_docs".equals(convert); // Export this dataset --- PISA format.
        boolean exportToBinary = "binary".equals(convert);    // Export this dataset --- BINARY format.
        
        // Export the compressed index file.
        boolean exportFile = config.getBoolean("output");
        // Compute the theoretical size and provides additional information.
        boolean computeTheoreticSizes = !exportFile;
        
        // Compression methods chosen.
        String[] compressionMethods = config.getStringArray("compressionMethods");
        List<String> compressionMethodsList = Arrays.asList(compressionMethods);
        for (String compressionMethod : compressionMethods) {
            if (!Arrays.asList(getMethodNames()).contains(compressionMethod)) {
                System.err.println(compressionMethod + " is not a valid compression method." +
                        " Valid methods are: " +
                        String.join(", ", getMethodNames()) + ".");
                System.exit(1);
            }
        }
        
        // Batch parameters.
        double[] batchLimits = config.getDoubleArray("batchLimits");
        if (batchLimits.length == 0) {
            batchLimits = new double[] { 0. , 1. };
        } else {
            if (batchLimits[0] < 0 || batchLimits[batchLimits.length - 1] > 1) {
                System.err.println("batchLimits must contain only frequencies between 0. and 1.");
                System.exit(1);
            }
            for (int i = 1; i < batchLimits.length; i++) {
                if (batchLimits[i] <= batchLimits[i - 1]) {
                    System.err.println("batchLimits must be sorted in strictly ascending order.");
                    System.exit(1);
                }
            }
            int batchLimitsSize = batchLimits.length;
            if (batchLimits[0] > 0) {
                batchLimitsSize++;
            }
            if (batchLimits[batchLimitsSize - 1] < 1) {
                batchLimitsSize++;
            }
            double[] batchLimitsTmp = new double[batchLimitsSize];
            int iBatch = 0;
            if (batchLimits[0] > 0) {
                batchLimitsTmp[iBatch++] = 0.;
            }
            for (int i = 0; i < batchLimits.length; i++) {
                batchLimitsTmp[iBatch++] = batchLimits[i];
            }
            if (batchLimits[batchLimitsSize - 1] < 1) {
                batchLimitsTmp[iBatch++] = 1.;
            }
            batchLimits = batchLimitsTmp;
        }
        boolean useABatchForSingletonGapArrays = false;
        
        // The meta submethod(s) chosen, if applicable.
        String[] subMethods = config.getStringArray("subMethods");
        for (String subMethod : subMethods) {
            if (!Arrays.asList(getMethodNames(validMetaMethods)).contains(subMethod)) {
                System.err.println(subMethod + " is not a valid compression method to be composed with meta." +
                        " Valid methods are: " +
                        String.join(", ", getMethodNames(validMetaMethods)) + ".");
                System.exit(1);
            }
        }
        
        // Shrink is an optimization of the meta_clusters method with respect to
        // the original clustering algorithm that will take less space: we
        // encode an optimized integer list.
        boolean shrinkResiduals = config.getBoolean("useShrinkedClusters");
        
        double sizeInBits; // Stores the theoretical sizes (it is then compared
                           // to interpolative, if possible).
        boolean generateDataset = false; // Generate synthetic datasets
        boolean onlyIterateInClusters = false; // Only iterate on the gap arrays that are in a cluster.
                                               // Set this boolean to true if you want to test filtered datasets.
        boolean onlyIterateOutClusters = false; // Only iterate on the gap arrays that are in no cluster.
        
        // For the following booleans, only one can be activated, and only this will be done.
        boolean exportLengthOccurrences = false; // Export the occurrences of the lengths.
        boolean exportClusterSubDataset = false; // Export the subset of this dataset corresponding to the clusters --- PISA format.
        
        // If you want to test encodings on synthetic datasets.
        if (generateDataset) {
            int[] example = (new ClusteredDataGenerator())
                    .generateClustered(20, 20);
            System.out.println(Arrays.toString(example));
            return;
        }
        
        // We output blank lines between datasets, thanks to this boolean.
        boolean firstDataset = true;
        for (Dataset dataset : datasets) {
            DatasetReader datasetReader = dataset.getDatasetReader();
            if (onlyIterateInClusters) {
                datasetReader.onlyIterateInClusters(dataset.getClusterFileName());
            } else if (onlyIterateOutClusters) {
                datasetReader.onlyIterateOutClusters(dataset.getClusterFileName());
            }
            int nbWords = datasetReader.getNbWords();
            int nbDocuments = datasetReader.getNbDocuments();
            long nbPointers = datasetReader.getNbPointers();
            String indexesFolderName = getIndexesFolderName(dataset);
            
            if (exportLengthOccurrences) {
                Method.exportLengthOccurrences(datasetReader, dataset.getDatasetName() + "-lengthOccurrences.txt");
                continue;
            }
            
            if (exportClusterSubDataset) {
                MetaMethodCluster.exportClusterSubDataset(dataset);
                continue;
            }
            
            if (exportToPisa) {
                datasetReader.writePisaDataset(dataset.getDatasetFileName());
                continue;
            }
            
            if (exportToCsv) {
                datasetReader.writeCsvDataset(dataset.getDatasetFileName());
                continue;
            }
            
            if (exportToBinary) {
                datasetReader.writeBinaryDataset(dataset.getDatasetFileName());
                continue;
            }
            
            if (firstDataset) {
                firstDataset = false;
            } else {
                System.out.println("");
            }
            System.out.println(dataset.getDatasetName());
            datasetReader.printStatistics();
            
            if (computeTheoreticSizes) {
                System.out.println("Gaps (in MB):");
            }
            
            if (compressionMethodsList.contains(CompressionMethod.METHOD_GAP_BOUND.getName()) && computeTheoreticSizes) {
                Method.standardOutputGapBound(datasetReader);
            }
            
            // Methods are always compared w.r.t. interpolative.
            // For huge datasets, it is stored explicitely (to avoid recomputation).
            // For other databases, it is recomputed on the fly.
            double sizeInBitsInterpolative = 0.;
            // Sizes of some datasets (in MB), computed with the Interpolative method.
            HashMap<String, Double> mapSizesInterpolative = new HashMap<>();
            if (onlyIterateInClusters) {
                // Gov2
                mapSizesInterpolative.put("gov2.sorted", 1754.77451975);
                mapSizesInterpolative.put("gov2-url", 1754.77451975);
                mapSizesInterpolative.put("gov2-url-sortedByDensity", 1754.77451975);
                mapSizesInterpolative.put("gov2", 1396.6485765);
                mapSizesInterpolative.put("gov2-bisection", 1396.6485765);
                mapSizesInterpolative.put("gov2-bisection-sortedByDensity", 1396.6485765);
                // ClueWeb09
                mapSizesInterpolative.put("clueweb09.sorted", 7994.23980875);
                mapSizesInterpolative.put("clueweb09-url", 7994.23980875);
                mapSizesInterpolative.put("clueweb09-url-sortedByDensity", 7994.23980875);
                mapSizesInterpolative.put("clueweb09", 6941.577537875);
                mapSizesInterpolative.put("clueweb09-bisection", 6941.577537875);
                mapSizesInterpolative.put("clueweb09-bisection-sortedByDensity", 6941.577537875);
            } else {
                // TREC
                mapSizesInterpolative.put("TREC-bisection", 67.82517975);
                mapSizesInterpolative.put("TREC-bisection-sortedByDensity", 67.82517975);
                // ClueWeb09
                mapSizesInterpolative.put("clueweb09.sorted", 9772.98581425);
                mapSizesInterpolative.put("clueweb09-url", 9772.98581425);
                mapSizesInterpolative.put("clueweb09-url-sortedByDensity", 9772.98581425);
                mapSizesInterpolative.put("clueweb09", 8425.1189565);
                mapSizesInterpolative.put("clueweb09-bisection", 8425.1189565);
                mapSizesInterpolative.put("clueweb09-bisection-sortedByDensity", 8425.1189565);
                // Gov2
                mapSizesInterpolative.put("gov2.sorted", 2604.854259625);
                mapSizesInterpolative.put("gov2-url", 2604.854259625);
                mapSizesInterpolative.put("gov2-url-sortedByDensity", 2604.854259625);
                mapSizesInterpolative.put("gov2", 2011.280535125);
                mapSizesInterpolative.put("gov2-bisection", 2011.280535125);
                mapSizesInterpolative.put("gov2-bisection-sortedByDensity", 2011.280535125);
                // Gov2 batches
                mapSizesInterpolative.put("gov2-bisection-batch0", 464.733744375);
                mapSizesInterpolative.put("gov2-bisection-batch0-sortedByDensity", 464.733744375);
                mapSizesInterpolative.put("gov2-bisection-batch1", 247.837156375);
                mapSizesInterpolative.put("gov2-bisection-batch1-sortedByDensity", 247.837156375);
                mapSizesInterpolative.put("gov2-bisection-batch2", 516.237109625);
                mapSizesInterpolative.put("gov2-bisection-batch2-sortedByDensity", 516.237109625);
                mapSizesInterpolative.put("gov2-bisection-batch3", 296.529291625);
                mapSizesInterpolative.put("gov2-bisection-batch3-sortedByDensity", 296.529291625);
                mapSizesInterpolative.put("gov2-bisection-batch4", 323.097382875);
                mapSizesInterpolative.put("gov2-bisection-batch4-sortedByDensity", 323.097382875);
                mapSizesInterpolative.put("gov2-bisection-batch5", 112.65986275);
                mapSizesInterpolative.put("gov2-bisection-batch5-sortedByDensity", 112.65986275);
                mapSizesInterpolative.put("gov2-bisection-batch6", 50.1860385);
                mapSizesInterpolative.put("gov2-bisection-batch6-sortedByDensity", 50.1860385);
                // ClueWeb09 batches
                mapSizesInterpolative.put("clueweb09-bisection-batch0", 1249.281186875);
                mapSizesInterpolative.put("clueweb09-bisection-batch1", 886.24818775);
                mapSizesInterpolative.put("clueweb09-bisection-batch2", 2189.17358275);
                mapSizesInterpolative.put("clueweb09-bisection-batch3", 1508.944694125);
                mapSizesInterpolative.put("clueweb09-bisection-batch4", 1561.985634375);
                mapSizesInterpolative.put("clueweb09-bisection-batch5", 620.91130475);
                mapSizesInterpolative.put("clueweb09-bisection-batch6", 408.574437875);
            }
            for (String s : mapSizesInterpolative.keySet()) {
                if (dataset.getDatasetName().equals(s)) {
                    sizeInBitsInterpolative = mapSizesInterpolative.get(s) * 8000000.;
                }
            }
            
            ArrayList<Method> methodsToTest = new ArrayList<>();
            addMethods(compressionMethods, config, datasetReader, methodsToTest);
            
            // The block submethod chosen, if applicable.
            String blockSubmethod = config.getString("blockSubmethod");
            if (!Arrays.asList(getMethodNames(validBlockMethods)).contains(blockSubmethod)) {
                System.err.println(blockSubmethod + " is not a valid compression method to be composed with blocks." +
                        " Valid methods are: " +
                        String.join(", ", getMethodNames(validBlockMethods)) + ".");
                System.exit(1);
            }
            // Use padding between consecutive blocks.
            boolean usePadding = config.getBoolean("usePadding");
            if (compressionMethodsList.contains(CompressionMethod.METHOD_BLOCKS.getName())) {
                ArrayList<Method> methodsToBlock = new ArrayList<>();
                addMethods(new String[] { blockSubmethod }, config, datasetReader, methodsToBlock);
                if (methodsToBlock.isEmpty()) {
                    System.err.println("I was not able to load the following method as a block submethod: " + blockSubmethod + ".");
                    System.exit(1);
                }
                Method methodToBlock = methodsToBlock.get(0);
                if (usePadding) {
                    methodsToTest.add(new MethodByBlock(methodToBlock));
                } else {
                    if (methodToBlock instanceof MethodByBitSequence) {
                        methodsToTest.add(new MethodByBlockNoPadding((MethodByBitSequence)methodToBlock));
                    } else {
                        // validBlockMethods should already contain only MethodByBitSequence.
                        // This is just another check to be sure.
                        System.err.println(blockSubmethod + " is not a valid compression method to be composed with blocks without padding." +
                                " Valid methods are instances of MethodByBitSequence.");
                        System.exit(1);
                    }
                }
            }
            
            // Actual testing of the methods.
            for (Method method : methodsToTest) {
                try {
                    long timeStart = System.currentTimeMillis();
                    method.init(datasetReader);
                    if (computeTheoreticSizes) {
                        if (method instanceof MethodInterpolative) {
                            sizeInBitsInterpolative = method.computeSize(datasetReader, nbDocuments, nbPointers, hasToLog);
                        } else {
                            sizeInBits = method.computeSize(datasetReader, nbDocuments, nbPointers, hasToLog);
                            if (sizeInBitsInterpolative != 0) {
                                System.out.println("Gains w.r.t. Interpolative: " +
                                        (100*(sizeInBitsInterpolative - sizeInBits) / sizeInBitsInterpolative) +
                                        "%");
                            }
                        }
                    }
                    if (exportFile) {
                        System.out.println("Creating compressed index using " + method.getName() + ".");
                        method.exportToFile(indexesFolderName + File.separatorChar + dataset.getDatasetName(), datasetReader, nbDocuments);
                    }
                    long timeStop = System.currentTimeMillis();
                    long nbSeconds = (timeStop - timeStart)/1000;
                    long nbMinutes = nbSeconds / 60;
                    long nbSecondsRemaining = nbSeconds - 60 * nbMinutes;
                    System.out.println("Time taken: " + (timeStop - timeStart)/1000. + " seconds (" +
                            nbMinutes + " minutes " + nbSecondsRemaining + " seconds).");
                } catch (OutOfMemoryError e) {
                    System.out.println("Maximum memory allocation exceeded for method " + method.getName() +
                            ". Please try to increase Java heap space, e.g. with -Xmx8g.");
                }
            }
            
            
            ////////////////////////////////////////////////////////////////////
            // Tests without export, just for theoretical size.
            ////////////////////////////////////////////////////////////////////
            
            // Meta method cluster and the test scripts need the datasetReader
            // to be put back to all words.
            datasetReader.iterateOnEveryWord();
            
            // The following code needs to use gapArrayList.
            // A rule of thumb is that we can use a gapArrayList if the dataset
            // takes less than 2 GB of RAM, i.e. 500 M pointers.
            boolean canUseGapArrayList = nbPointers <= 500000000 && !isThisGov2 && !isThisClueWeb09;
            
            if (canUseGapArrayList) {
                // For the following booleans, we remove some of the gap arrays
                // for refactoring.
                boolean extractGapLengths = false; // Export a new dataset with the gapArrayLength
                boolean sortByDensity = false; // Sort the words by density
                boolean exportRandomized = false; // Export the gap array list, randomized.
                boolean onlyDensity0_2 = false; // Only keep words whose density is >= 0.2%.
                
                DatasetInfo datasetInfo = new DatasetInfo(datasetReader);
                
                /*
                // Bookstein, Klein, and Raita, "Modeling Word Occurrences for the Compression of Concordances" (1997)
                // In Table III, p. 285, Bible was tested only for words appearing between
                // 60 and 929 documents. We wanted to do the same experiment.
                // Unfortunately, there are different changes that did not allow
                // to do the same one: probably the dataset is slightly different,
                // due to, e.g., a different stemmer.
                datasetInfo.printStatistics();
                DatasetInfo cutByLengths = datasetInfo.cutByLengths(60, 929);
                cutByLengths.printStatistics();
                cutByLengths.writeCsvDataset(dataset.getDatasetName() + "-60-929.csv");
                */
                if (extractGapLengths) {
                    datasetInfo.toGapArrayLength().
                            writeCsvDataset(dataset.getDatasetName() + "-TL");
                }
                if (onlyDensity0_2) {
                    datasetInfo.cutByMinimumLength((int)Math.ceil(0.002 * nbDocuments)).
                            writeCsvDataset(dataset.getDatasetName() + "-onlyDensityGreater0_2");
                }
                if (sortByDensity) {
                    datasetInfo.sortByDensity();
                    datasetInfo.writeCsvDataset(dataset.getDatasetName() + "-sortedByDensity");
//                    Method method = new MethodDelta();
//                    method.exportToFile(indexesFolderName + File.separatorChar + dataset.getDatasetName() + "-sortedByDensity.Delta", datasetInfo, nbDocuments);
                }
                if (exportRandomized) {
                    DatasetInfo randomizedDataset = datasetInfo.randomize();
//                    DatasetInfo randomizedDataset = datasetInfo.permute("permut.csv");
                    randomizedDataset.writeCsvDataset(dataset.getDatasetName() + "-randomized");
                }
                
                if (compressionMethodsList.contains(CompressionMethod.METHOD_META_BATCH.getName())) {
                    // This is the list of batch limits. See the comments by going in
                    // the file MetaMethodBatch.java.
                    int nbBatches;
                    double[] batchLimitsLocal;
                    try {
                        nbBatches = config.getInt("nbBatches");
                        boolean useUniformBatches = config.getBoolean("useUniformBatches");
                        batchLimitsLocal = new double[nbBatches + 1];
                        batchLimitsLocal[0] = 0.;
                        if (useUniformBatches) {
                            // uniform batches
                            for (int i = 1; i < nbBatches; i++) {
                                batchLimitsLocal[i] = 0.5 * (i + 1) / (double)nbBatches;
                            }
                        } else {
                            // logarithmic batches
                            for (int i = 1; i < nbBatches; i++) {
                                batchLimitsLocal[i] = Math.pow(nbDocuments, (double)i / (double)nbBatches) / nbDocuments;
                            }
                        }
                        batchLimitsLocal[nbBatches] = 1.;
                    } catch (UnspecifiedParameterException e) {
                        // The batches given as parameter (by default, the 7
                        // given in the beginning of this file)
                        batchLimitsLocal = batchLimits;
                    }
                    /* Other solution where we use 6 batches computed logarithmically with custom limits
                    batchLimitsLocal = new double[] {
                        0.,
                        Math.pow(nbDocuments, 0.25) / nbDocuments,
                        Math.pow(nbDocuments, 0.5) / nbDocuments,
                        Math.pow(nbDocuments, 0.75) / nbDocuments,
                        Math.pow(nbDocuments, 0.875) / nbDocuments,
                        0.6,
                        1.
                    };
                    */
                    Method methodMeta = new MetaMethodBatch(datasetInfo, batchLimitsLocal, useABatchForSingletonGapArrays, subMethods, config);
                    if (computeTheoreticSizes) {
                        sizeInBits = methodMeta.computeSize(datasetInfo, nbDocuments, nbPointers, hasToLog);
                        if (sizeInBitsInterpolative != 0) {
                            System.out.println("Gains w.r.t. Interpolative: " +
                                    (100*(sizeInBitsInterpolative - sizeInBits) / sizeInBitsInterpolative) +
                                    "%");
                        }
                    }
                }
                
                if (compressionMethodsList.contains(CompressionMethod.METHOD_META_CLUSTER.getName())) {
                    String clusterFileName = dataset.getClusterFileName();
                    Method methodMeta = new MetaMethodCluster(datasetInfo, clusterFileName, shrinkResiduals, subMethods, config, onlyIterateInClusters);
                    if (computeTheoreticSizes) {
                        sizeInBits = methodMeta.computeSize(datasetInfo, nbDocuments, nbPointers, hasToLog);
                        if (sizeInBitsInterpolative != 0) {
                            System.out.println("Gains w.r.t. Interpolative: " +
                                    (100*(sizeInBitsInterpolative - sizeInBits) / sizeInBitsInterpolative) +
                                    "%");
                        }
                    }
                }
            } else {
                if (compressionMethodsList.contains(CompressionMethod.METHOD_META_CLUSTER.getName())) {
                    Method methodMeta = new MetaMethodCluster(dataset, shrinkResiduals, subMethods, config, onlyIterateInClusters);
                    if (computeTheoreticSizes) {
                        sizeInBits = methodMeta.computeSize(datasetReader, nbDocuments, nbPointers, hasToLog);
                        if (sizeInBitsInterpolative != 0) {
                            System.out.println("Gains w.r.t. Interpolative: " +
                                    (100*(sizeInBitsInterpolative - sizeInBits) / sizeInBitsInterpolative) +
                                    "%");
                        }
                    }
                }
            }
        }
    }
}
