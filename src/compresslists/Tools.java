/**
 * Tools.java
 *
 * Created on 5 juin 2019
 */

package compresslists;

import arrays.IntsRef;
import bitlistencoding.BitSequence;
import gaparrayencoding.MethodTrits;
import integerencoding.UnaryEncoding;
import io.BitOutputStreamArray;
import quatritlistencoding.QuatritSequence;
import tritlistencoding.TritSequence;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author yann
 */
public class Tools {
    
    public static final int nbBits = 2;
    public static final int nbTrits = 3;
    public static final int nbQuatrits = 4;
    public static final int nbBitsPerByte  = 8;
    public static final int nbBitsPerInt   = 32;
    public static final int nbBitsPerLong  = 64;
    public static final int nbTritsPerByte = 5;
    
    // Here, when converting, we read /write the bits in big endian.
    public static final boolean isBigEndian = true;
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Gap utils
    ////////////////////////////////////////////////////////////////////////////
    
    public static void intArray2quatritSequence(IntsRef intArrayRef, QuatritSequence quatritSequence) {
        intArray2tritSequence(intArrayRef, quatritSequence);
        quatritSequence.add(3);
    }
    
    /*
     * Concatenation of the trit codes of each int in the array.
     */
    public static void intArray2tritSequence(IntsRef intArrayRef, TritSequence tritSequence) {
        int[] intArray = intArrayRef.ints;
        int length = intArrayRef.length;
        int offset = intArrayRef.offset;
        tritSequence.reset();
        for (int i = 0; i < length; i++) {
            MethodTrits.writeCodeTrits(intArray[offset + i], tritSequence);
        }
    }
    
    public static void tritSequence2intArray(TritSequence tritSequence, IntsRef intArrayRef) {
        int length = tritSequence.numberOfTwos();
        if (intArrayRef.ints.length < length) {
            intArrayRef.ints = new int[length];
        }
        intArrayRef.offset = 0;
        intArrayRef.length = length;
        int nbIntsTreated = 0;
        int indexTrit = 0;
        while (nbIntsTreated < length) {
            int integer = MethodTrits.readCodeTrits(tritSequence, indexTrit);
            intArrayRef.ints[nbIntsTreated++] = integer;
            indexTrit = tritSequence.indexOfNextTwo(indexTrit) + 1;
        }
        assert(tritSequence.size() == indexTrit ||
                (tritSequence instanceof QuatritSequence && tritSequence.size() == indexTrit + 1));
    }
    
    public static int tritSequenceLength2nb01(TritSequence tl) {
        int nb01 = 0;
        int indexTrit = 0;
        int length = tl.numberOfTwos();
        for (int i = 0; i < length; i++) {
            int integer = MethodTrits.readCodeTrits(tl, indexTrit);
            // integer now stores 1 + the size in bits of the i-th gap, as it
            // is stored in a trit sequence, cf. tritSequence2intSizeArray.
            nb01 += integer - 1;
            indexTrit = tl.indexOfNextTwo(indexTrit) + 1;
        }
        return nb01;
    }
    
    /*
     * A tritList is of the form (b_{0,0}, ... b_{0,k_0-1}, 2, b_{1,0}, ..., b_{1,k_1-1}, 2, [...], b_{l-1,0}, ..., b{l-1,k_{l-1}-1}, 2)
     * Where the b_{i,j} are 0 or 1, and where "1b_{i,0}...b_{i,k_i-1}" is the
     * binary encoding of the i-th int (ints are always >= 1 so there is a "1"
     * at the beginning, which is implicit in the representation). We here
     * output an array of size l (where l is the number of ints), where
     * intSize[i] = k_i + 1, the number of bits used for the binary encoding of
     * the i-th int --- that is, floor(log_2(int_i)) + 1.
     */
    public static void tritSequence2intSizeArray(TritSequence tritSequence, IntsRef intSizeArrayRef) {
        int length = tritSequence.numberOfTwos();
        if (intSizeArrayRef.ints.length < length) {
            intSizeArrayRef.ints = new int[length];
        }
        intSizeArrayRef.offset = 0;
        intSizeArrayRef.length = length;
        int index = 0;
        int nbBitsForGap = 1;
        for (int i = 0; i < tritSequence.size(); i++) {
            if (tritSequence.get(i) == 2) {
                intSizeArrayRef.ints[index++] = nbBitsForGap;
                nbBitsForGap = 1;
            } else {
                nbBitsForGap++;
            }
        }
    }
    
    /*
     * Compute the gap occurrences.
     */
    public static long[] gapOccurrences(IntsRef gapArrayRef, int nbDocuments) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        long[] nbGaps = new long[nbDocuments + 1];
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            nbGaps[gapArray[offset + idGap]]++;
        }
        return nbGaps;
    }
    
    /*
     * Given a bit vector that contains 1 at positions (document IDs - 1),
     * returns an array that contains the document IDs.
     * N.B.: document ids are in [1; nbDocuments].
     */
    public static void bitVector2documentArray(BitSequence bitSequence, IntsRef documentArrayRef) {
        bitVector2documentArray(bitSequence, documentArrayRef, 0);
    }
    public static void bitVector2documentArray(BitSequence bitSequence, IntsRef documentArrayRef, int baseDocId) {
        int nbDocuments = bitSequence.numberOfOnes();
        if (documentArrayRef.ints.length < nbDocuments) {
            documentArrayRef.ints = new int[nbDocuments];
        }
        documentArrayRef.offset = 0;
        documentArrayRef.length = nbDocuments;
        int idBitSequence = 0;
        for (int idDoc = 0; idDoc < nbDocuments; idDoc++) {
            while (bitSequence.get(idBitSequence) != 1) {
                idBitSequence++;
            }
            documentArrayRef.ints[idDoc] = idBitSequence + 1 + baseDocId;
            idBitSequence++;
        }
    }
    
    /*
     * Given a bit vector that contains 1 at positions (document IDs - 1),
     * returns an array that contains the gaps between those document IDs.
     * N.B.: document ids are in [1; nbDocuments].
     */
    public static void bitVector2gapArray(BitSequence bitSequence, IntsRef gapArrayRef) {
        int nbDocuments = bitSequence.numberOfOnes();
        if (gapArrayRef.ints.length < nbDocuments) {
            gapArrayRef.ints = new int[nbDocuments];
        }
        gapArrayRef.offset = 0;
        gapArrayRef.length = nbDocuments;
        int nbDocumentsTreated = 0;
        int bitIndex = 0;
        // Gap starts at 1 and not 0 because what is actually stored is gap - 1.
        int gap = 1;
        while (nbDocumentsTreated < nbDocuments) {
            // Extract a bit.
            int bitRead = bitSequence.get(bitIndex++);
            if (bitRead == 1) {
                // If the bit is 1, add the gap to the array.
                gapArrayRef.ints[nbDocumentsTreated++] = gap;
                gap = 1;
            } else {
                // Else, update the gap.
                gap++;
            }
        }
    }
    
    /*
     * Given an array that contains the document IDs, returns a bit vector
     * that contains 1 at positions (document IDs - 1).
     * N.B.: document ids are in [1; nbDocuments].
     */
    public static void documentArray2bitVector(IntsRef documentArrayRef, BitSequence bitVector) {
        documentArray2bitVector(documentArrayRef, bitVector, 0);
    }
    public static void documentArray2bitVector(IntsRef documentArrayRef, BitSequence bitVector, int baseDocId) {
        int[] documentArray = documentArrayRef.ints;
        int nbDocumentsLocal = documentArrayRef.length;
        int offset = documentArrayRef.offset;
        bitVector.reset();
        BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(bitVector);
        if (nbDocumentsLocal > 0) {
            int firstGap = documentArray[offset] - baseDocId;
            UnaryEncoding.writeCodeUnaryInverted(firstGap, bitOutputStream);
            for (int idDoc = 1; idDoc < nbDocumentsLocal; idDoc++) {
                int gap = documentArray[offset + idDoc] - documentArray[offset + idDoc - 1];
                UnaryEncoding.writeCodeUnaryInverted(gap, bitOutputStream);
            }
        }
    }
    
    /*
     * Given an array that contains the gaps between document IDs, returns a bit
     * vector that contains 1 at positions (document IDs - 1).
     * N.B.: It can be seen as the concatenation of the inverted unary codes of
     * each gap in the array --- where inverted means 0 and 1 are swapped.
     */
    public static void gapArray2bitVector(IntsRef gapArrayRef, BitSequence bitVector) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        bitVector.reset();
        BitOutputStreamArray bitOutputStream = new BitOutputStreamArray(bitVector);
        for (int idGap = 0; idGap < nbGapsLocal; idGap++) {
            UnaryEncoding.writeCodeUnaryInverted(gapArray[offset + idGap], bitOutputStream);
        }
    }
    
    /*
     * As input, we are given an array that contains the document IDs (sorted).
     * We output an array that contains the gaps between those IDs.
     * N.B.: document ids are in [1; nbDocuments].
     * N.B.: this method will work if gapArrayRef.ints == documentArrayRef.ints.
     */
    public static void documentArray2gapArray(IntsRef documentArrayRef, IntsRef gapArrayRef) {
        int[] documentArray = documentArrayRef.ints;
        int nbDocumentsLocal = documentArrayRef.length;
        int offset = documentArrayRef.offset;
        for (int idDoc = 1; idDoc < nbDocumentsLocal; idDoc++) {
            assert(documentArray[offset + idDoc] > documentArray[offset + idDoc - 1]);
        }
        if (gapArrayRef.ints.length < nbDocumentsLocal) {
            gapArrayRef.ints = new int[nbDocumentsLocal];
        }
        gapArrayRef.length = nbDocumentsLocal;
        gapArrayRef.offset = 0;
        if (nbDocumentsLocal > 0) {
            int previousDocId = documentArray[offset];
            gapArrayRef.ints[0] = documentArray[offset];
            for (int idDoc = 1; idDoc < nbDocumentsLocal; idDoc++) {
                int tmp = documentArray[offset + idDoc];
                gapArrayRef.ints[idDoc] = documentArray[offset + idDoc] - previousDocId;
                previousDocId = tmp;
            }
        }
    }
    
    /*
     * As input, we are given an array that contains the gaps between document
     * IDs. We output an array that contains the document IDs.
     * N.B.: document ids are in [1; nbDocuments].
     * N.B.: this method will work if gapArrayRef.ints == documentArrayRef.ints.
     */
    public static void gapArray2documentArray(IntsRef gapArrayRef, IntsRef documentArrayRef) {
        int[] gapArray = gapArrayRef.ints;
        int nbGapsLocal = gapArrayRef.length;
        int offset = gapArrayRef.offset;
        if (documentArrayRef.ints.length < nbGapsLocal) {
            documentArrayRef.ints = new int[nbGapsLocal];
        }
        documentArrayRef.length = nbGapsLocal;
        documentArrayRef.offset = 0;
        if (nbGapsLocal > 0) {
            documentArrayRef.ints[0] = gapArray[offset];
            for (int i = 1; i < nbGapsLocal; i++) {
                documentArrayRef.ints[i] = gapArray[offset + i] + documentArrayRef.ints[i - 1];
            }
        }
    }
    
    /*
     * documentArray and referenceList must be sorted. Numbers that are
     * duplicates in both arrays will be put as duplicates in the intersection.
     */
    public static int[] sortedIntersectIds(int[] documentArray, int nbDocuments, int[] referenceList) {
        ArrayList<Integer> identifiers = new ArrayList<>();
        int iDocArray = 0;
        int iRefList = 0;
        while (iDocArray < nbDocuments && iRefList < referenceList.length) {
            if (documentArray[iDocArray] == referenceList[iRefList]) {
                identifiers.add(iRefList);
                iDocArray++;
                iRefList++;
            } else if (documentArray[iDocArray] < referenceList[iRefList]) {
                iDocArray++;
            } else {
                iRefList++;
            }
        }
        return identifiers
                .stream()
                .mapToInt(Integer::intValue)
                .toArray();
    }
    public static int[] sortedIntersectIds(int[] documentArray, int[] referenceList) {
        return sortedIntersectIds(documentArray, documentArray.length, referenceList);
    }
    
    /*
     * documentArray1 and documentArray2 must be sorted. Numbers that are
     * duplicates in both arrays will be put as duplicates in the intersection.
     */
    public static int[] sortedIntersect(int[] documentArray1, int[] documentArray2) {
        ArrayList<Integer> elements = new ArrayList<>();
        int iDocArray1 = 0;
        int iDocArray2 = 0;
        while (iDocArray1 < documentArray1.length && iDocArray2 < documentArray2.length) {
            if (documentArray1[iDocArray1] == documentArray2[iDocArray2]) {
                elements.add(documentArray1[iDocArray1]);
                iDocArray1++;
                iDocArray2++;
            } else if (documentArray1[iDocArray1] < documentArray2[iDocArray2]) {
                iDocArray1++;
            } else {
                iDocArray2++;
            }
        }
        return elements
                .stream()
                .mapToInt(Integer::intValue)
                .toArray();
    }
    
    /*
     * documentArray1 and documentArray2 must be sorted and not contain any
     * duplicate.
     * Return elements in documentArray1 but not in documentArray2.
     */
    public static int[] sortedIntersectNot(int[] documentArray1, int nbDocuments1, int[] documentArray2) {
        ArrayList<Integer> elements = new ArrayList<>();
        int iDocArray1 = 0;
        int iDocArray2 = 0;
        while (iDocArray1 < nbDocuments1) {
            if (iDocArray2 == documentArray2.length) {
                elements.add(documentArray1[iDocArray1]);
                iDocArray1++;
            } else if (documentArray1[iDocArray1] == documentArray2[iDocArray2]) {
                iDocArray1++;
                iDocArray2++;
            } else if (documentArray1[iDocArray1] < documentArray2[iDocArray2]) {
                elements.add(documentArray1[iDocArray1]);
                iDocArray1++;
            } else {
                iDocArray2++;
            }
        }
        return elements
                .stream()
                .mapToInt(Integer::intValue)
                .toArray();
    }
    public static int[] sortedIntersectNot(int[] documentArray1, int[] documentArray2) {
        return sortedIntersectNot(documentArray1, documentArray1.length, documentArray2);
    }
    
    /*
     * WARNING: complexity = O(documentArray1.length * documentArray2.length).
     * Please consider using another method :)
     */
    public static int[] intersect(int[] documentArray1, int[] documentArray2) {
        return Arrays.stream(documentArray1)
                     .filter(x -> Arrays.stream(documentArray2).anyMatch(y -> y == x))
                     .toArray();
    }
    
    /*
     * WARNING: complexity = O(documentArray1.length * documentArray2.length).
     * Please consider using another method :)
     */
    public static int[] intersectNot(int[] documentArray1, int[] documentArray2) {
        return Arrays.stream(documentArray1)
                     .filter(x -> Arrays.stream(documentArray2).allMatch(y -> y != x))
                     .toArray();
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Double utils
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Returns log_2(x) with the convention that log_2(0) = 0.
     * Correct since we'll always compute x * log2(x).
     */
    public static double log2(double x) {
        return x == 0
                ? 0
                : Math.log(x) / Math.log(2);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Integer utils
    ////////////////////////////////////////////////////////////////////////////
    
    public static int[] int2bitArray(int number) {
        long myInt = int2long(number);
        int[] bitRepresentation = new int[nbBitsPerInt];
        for (int i = 0; i < nbBitsPerInt; i++) {
            bitRepresentation[isBigEndian ? nbBitsPerInt - 1 - i : i] = (int)(myInt % 2);
            myInt /= 2;
        }
        return bitRepresentation;
    }
    
    public static int[] long2bitArray(long number) {
        BigInteger myLong = long2bigInteger(number);
        int[] bitRepresentation = new int[nbBitsPerLong];
        for (int i = 0; i < nbBitsPerLong; i++) {
            bitRepresentation[isBigEndian ? nbBitsPerLong - 1 - i : i] = myLong.remainder(BigInteger.valueOf(2)).intValue();
            myLong = myLong.divide(BigInteger.valueOf(2));
        }
        return bitRepresentation;
    }
    
    public static long bitArray2long(int[] bitArray) {
        assert(bitArray.length == nbBitsPerLong);
        BigInteger myLong = BigInteger.ZERO;
        for (int i = 0; i < nbBitsPerLong; i++) {
            if (bitArray[i] == 1) {
                myLong.add(BigInteger.valueOf(1 << (isBigEndian ? nbBitsPerLong - 1 - i : i)));
            }
        }
        return myLong.longValue();
    }
    
    /*
     * Transform a byte to an array of 8 bits.
     * This function uses a big endian or little endian representation of the
     * byte, depending on the value of the boolean isBigEndian.
     */
    public static int[] byte2bitArray(byte b) {
        int[] bitArray = new int[nbBitsPerByte];
        int myByte = byte2int(b);
        for (int i = 0; i < nbBitsPerByte; i++) {
            bitArray[isBigEndian ? nbBitsPerByte - 1 - i : i] = myByte % 2;
            myByte /= 2;
        }
        return bitArray;
    }
    
    /*
     * Transform an array of 8 bits to a byte.
     * This function uses a big endian or little endian representation of the
     * byte, depending on the value of the boolean isBigEndian.
     */
    public static byte bitArray2byte(int[] bitArray) {
        assert(bitArray.length == nbBitsPerByte);
        byte b = 0;
        for (int i = 0; i < nbBitsPerByte; i++) {
            b |= bitArray[i] << (isBigEndian ? nbBitsPerByte - 1 - i : i);
        }
        return b;
    }
    
    /*
     * Converts a signed long l (-9 223 372 036 854 775 808 <= l <= 9 223 372 036 854 775 807)
     * to an unsigned big integer (if the long is negative, adds 18 446 744 073 709 551 615).
     */
    public static BigInteger long2bigInteger(long l) {
        BigInteger myLong = BigInteger.valueOf(l);
        if (myLong.signum() < 0) {
            myLong = myLong.add(BigInteger.ONE.shiftLeft(64));
        }
        return myLong;
    }
    
    /*
     * Converts a signed int i (-2 147 483 648 <= i <= 2 147 483 647) to an
     * unsigned long (if the int is negative, adds 4 294 967 296).
     */
    public static long int2long(int i) {
        return i < 0 ? (long)i + (1L << 32) : (long)i;
        // return (long)i & 0xffffffffL; // Also works
    }
    
    /*
     * Converts a signed byte b (-128 <= b <= 127) to an unsigned int
     * (if the byte is negative, adds 256).
     */
    public static int byte2int(byte b) {
        return b < 0 ? (int)b + (1 << 8) : (int)b;
        // return (int)b & 0xff; // Also works.
    }
    
    /*
     * Returns the ceiling of log2(x).
     * x must be >= 1.
     */
    public static int ceilingLog2(int x) {
        assert(x >= 1);
        return ilog2(x - 1) + 1;
    }
    
    /*
     * Returns the ceiling of x / y.
     * x must be >= 0.
     * y must be >= 1.
     */
    public static int ceilingDivision(int x, int y) {
        assert(x >= 0);
        assert(y >= 1);
        return (x > 0)
                ? 1 + (x - 1) / y
                : x / y;
    }
    
    /*
     * Returns the ceiling of x / y.
     * x must be >= 0.
     * y must be >= 1.
     */
    public static long ceilingDivision(long x, long y) {
        assert(x >= 0);
        assert(y >= 1);
        return (x > 0)
                ? 1 + (x - 1) / y
                : x / y;
    }
    
    /*
     * Returns the smallest multiple of y that is greater than, or equal to, x.
     * x must be >= 0.
     * y must be >= 1.
     */
    public static int nextMultiple(int x, int y) {
        return ceilingDivision(x, y) * y;
    }
    
    /*
     * Returns the smallest multiple of y that is greater than, or equal to, x.
     * x must be >= 0.
     * y must be >= 1.
     */
    public static long nextMultiple(long x, long y) {
        return ceilingDivision(x, y) * y;
    }
    
    /*
     * Returns the floor of log2(x) if x >= 1 ; return -1 if x == 0 (this allows
     * the formula for ceilingLog2(x) = ilog2(x - 1) + 1).
     * x must be >= 0.
     *
     * NOTE: efficiently replaced by a call to the assembly function bsr
     * (bit scan reverse) on x86 platforms.
     * http://hg.openjdk.java.net/jdk8/jdk8/hotspot/file/87ee5ee27509/src/share/vm/classfile/vmSymbols.hpp#l680
     * http://hg.openjdk.java.net/jdk8/jdk8/hotspot/file/87ee5ee27509/src/cpu/x86/vm/x86_64.ad#l6191
     */
    public static int ilog2(int x) {
        assert(x >= 0);
        return 31 - Integer.numberOfLeadingZeros(x);
    }
    
    /*
     * Returns a^b. Only used for small values of a and b >= 0.
     * Will throw ArithmeticException if overflow happens.
     * In this project, only used to compute powers of 3. When a = 3, it will
     * overflow iff b > 19 (3^19 = 1 162 261 467 < 2^31 - 1 = 2 147 483 647).
     */
    public static int pow(int a, int b) throws ArithmeticException {
        assert(b >= 0);
        int power = 1;
        for (int i = 0; i < b; i++) {
            power = Math.multiplyExact(power, a);
        }
        return power;
    }
    public static long pow(long a, long b) throws ArithmeticException {
        assert(b >= 0);
        long power = 1;
        for (int i = 0; i < b; i++) {
            power = Math.multiplyExact(power, a);
        }
        return power;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    // BigInteger utils
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * Returns true iff x represents a power of two.
     */
    public static boolean isPowerOfTwo(BigInteger x) {
        return x.signum() > 0 && x.getLowestSetBit() == x.bitLength() - 1;
    }
    
    /*
     * Returns the ceiling of log2(x).
     */
    public static int ceilingLog2(BigInteger x) {
        int logFloor = x.bitLength() - 1;
        return isPowerOfTwo(x) ? logFloor : logFloor + 1;
    }
    
    /*
     * Returns n choose k, also known as the binomial coefficient of n and
     * k, that is, n! / (k! (n - k)!).
     *
     * Warning: the result can take as much as O(k log n) space.
     */
    public static BigInteger binomial(int n, int k) {
        // When k > n/2, it is quicker to compute binomial(n, n - k).
        if (k > (n >> 1)) {
            k = n - k;
        }
        
        BigInteger accum = BigInteger.ONE;
        long numeratorAccum = n;
        long denominatorAccum = 1;
        int bits = ceilingLog2(BigInteger.valueOf(n));
        int numeratorBits = bits;
        
        // We compute the binomial coefficient by the formula
        // n choose k = n * (n-1) * (n-2) * ... * (n-k+1) / (1 * 2 * ... * k)
        for (int i = 1; i < k; i++) {
            int p = n - i;
            int q = i + 1;
            // k <= n/2, thus i < n/2 thus p = n - i >= n/2,
            // thus log2(p) >= bits - 1.
            
            if (numeratorBits + bits >= Long.SIZE - 1) {
                // The long multiplications might overflow.
                // Perform BigInteger multiplications and divisions.
                accum = accum
                    .multiply(BigInteger.valueOf(numeratorAccum))
                    .divide(BigInteger.valueOf(denominatorAccum));
                numeratorAccum = p;
                denominatorAccum = q;
                numeratorBits = bits;
            } else {
                // The long multiplications cannot overflow.
                numeratorAccum *= p;
                denominatorAccum *= q;
                numeratorBits += bits;
            }
        }
        return accum
            .multiply(BigInteger.valueOf(numeratorAccum))
            .divide(BigInteger.valueOf(denominatorAccum));
    }

}
