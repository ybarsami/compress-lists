# CompressLists

A library to test different compression methods targeting increasing lists
of positive integers, written in Java.
This library is distributed with the GNU general public license version 3,
see the details in the file named "LICENSE".


## Run ##

The java files have been compiled to a .jar executable file which is located
in:

    dist/CompressLists.jar

If this java executable does not work, please recompile it with your own java
distribution.

To run this executable file, just go in the root folder of this library (the
one in which this README.md file is located) and execute:

    launch.sh

It is mandatory to test the library from this folder, as the datasets are
accessed through the relative path directory datasets/ --- it would be also
possible to launch the test examples by copying the datasets folder in the
folder in which we want to perform the tests.


## Build ##

The library has been written with the Netbeans I.D.E., and all the files
associated to the Netbeans project are contained in this repository. The
easiest method is thus to open this Netbeans project and build it, if
necessary.


## Content of this repository ##

**datasets:** different sets of documents that can be used to test the library:
dasovich, jones, kaminski and shackleton are four e-mail boxes taken from the
Enron corpus of e-mails [[7]](#note7), which contains professional e-mail boxes
of 150 persons. We cleaned the original folders by removing the e-mails which
were there multiple times, as suggested in the article.
The dasovich-j dataset contains 15748 e-mails.

**lib:** the libraries on which this one depends.

**nbproject:** Netbeans project files.

**src:** the source files of this library.

**test:** the unit tests of this library.


## The inverted index: a simple example ##

Let us start with a simple example, taken from "Managing Gigabytes"
[[1]](#note1). For now, we will omit the technical details which are part of
the representation of an e-mail, and will just consider a set of documents to
index. [Table 1](#table1) shows a set of 6 documents that we want to index.
[Table 2](#table2) shows the inverted index of this set of documents (sorted
alphabetically, in ascending order). The word "cold" is part of 2 documents:
document number 1, and document number 4 ; the word "days" is part of 2
documents: document number 3, and document number 6 ; etc.

<a name="table1">Table 1</a>: Example text; each line is one document.

| Document |                    Text                  |
|---------:|------------------------------------------|
|     1    | Pease porridge hot, pease porridge cold, |
|     2    | Pease porridge in the pot,               |
|     3    | Nine days old.                           |
|     4    | Some like it hot, some like it cold,     |
|     5    | Some like it in the pot,                 |
|     6    | Nine days old.                           |


<a name="table2">Table 2</a>: Inverted file for text of [Table 1](#table1).

| Number |   Term   | Documents |
|-------:|----------|:---------:|
|    1   | cold     | {1, 4}    |
|    2   | days     | {3, 6}    |
|    3   | hot      | {1, 4}    |
|    4   | in       | {2, 5}    |
|    5   | it       | {4, 5}    |
|    6   | like     | {4, 5}    |
|    7   | nine     | {3, 6}    |
|    8   | old      | {3, 6}    |
|    9   | pease    | {1, 2}    |
|   10   | porridge | {1, 2}    |
|   11   | pot      | {2, 5}    |
|   12   | some     | {4, 5}    |
|   13   | the      | {2, 5}    |

If the user wants to retrieve the documents which contain the word "old", it
is thus straightforward: the index directly tells that those are documents
number 3 and 6.

Now, what happens if the user wants to retrieve documents which contains
multiple words? The library will get the sets of documents which contain each
of those words, and intersect them. For example, if the user wants to retrieve
the documents which contain both "some" and "the", the index tells that
those documents are in the intersection between {4, 5} and {2, 5} which
is the set {5}. Only the document number 5 contains those 2 words.
If the user wants to retrieve the documents which contain both "cold"
and "days", the index tells us that those documents are the intersection
between {1, 4} and {3, 6} which is the empty set. No document
contains those 2 words.


## The dependencies ##

* the JavaFastPFOR library [[13]](#note13) for efficient implementations of
  some compression methods.

* the Java Diff Utils library [[14]](#note14) for unit tests.


### How can we compress increasing lists of positive integers? ###

The main idea is the following: instead of storing all the numbers in the list,
we store the *differences* (or *gaps*) between two consecutive integers in that
list.

For example, the sorted sequence of integers

    7, 18, 19, 22, 23, 25, 63, ...

can be represented by gaps

    7, 11, 1, 3, 1, 2, 38, ...

Multiple ways to compress this list of gaps have been proposed.
[Table 3](#table3) [[1]](#note1) shows the compression which can be
obtained via most of those methods. In this table, four datasets have been
used: the King James' Bible, a bibliographic dataset (GNUbib), a collection of
law documents (Comact: the Commonwealth Acts of Austria), and a collection of
documents frequently used in the Information Retrieval community (TREC: Text
REtrieval Conference). In this table, the word *pointer* means an e-mail ID in
an inverted list. The less bits a pointer takes in memory, the more efficient
is the compression method.

<a name="table3">Table 3</a>: Compression of inverted files in bits per pointer.

| Method                                         | Bible  | GNUbib | Comact | TREC    |
|------------------------------------------------|-------:|-------:|-------:|--------:|
| *Global methods*                               |        |        |        |         |
| Unary                                          | 262.00 | 909.00 | 487.00 | 1918.00 |
| Binary                                         |  15.00 |  16.00 |  18.00 |   20.00 |
| Bernoulli [[6]](#note6) [[5]](#note5)          |   9.86 |  11.06 |  10.90 |   12.30 |
| gamma [[4]](#note4) [[2]](#note2)              |   6.51 |   5.68 |   4.48 |    6.63 |
| delta [[4]](#note4) [[2]](#note2)              |   6.23 |   5.08 |   4.35 |    6.38 |
| Observed frequency                             |   5.90 |   4.82 |   4.20 |    5.97 |
| *Local methods*                                |        |        |        |         |
| Bernoulli [[12]](#note12) [[3]](#note3)        |   6.09 |   6.16 |   5.40 |    5.84 |
| Hyperbolic [[10]](#note10)                     |   5.75 |   5.16 |   4.65 |    5.89 |
| Skewed Bernoulli [[11]](#note11) [[9]](#note9) |   5.65 |   4.70 |   4.20 |    5.44 |
| Batched frequency [[9]](#note9)                |   5.58 |   4.64 |   4.02 |    5.41 |
| Interpolative [[8]](#note8)                    |   5.24 |   3.98 |   3.87 |    5.18 |



# References #

## Books ##

<a name="note1">\[1\]</a> I. H. Witten, A. Moffat, and T. C. Bell. "Managing Gigabytes". Morgan Kaufmann Publishing, San Francisco, 1999. https://people.eng.unimelb.edu.au/ammoffat/mg/

## Articles ##

<a name="note2">\[2\]</a> J. L. Bentley and A. C.-C. Yao. “An almost optimal algorithm for unbounded searching”. Information Processing Letters 5.3 (1976), pp. 82–87. http://dx.doi.org/10.1016/0020-0190(76)90071-5

<a name="note3">\[3\]</a> A. Bookstein, S. T. Klein, and T. Raita. “Model based concordance compression”. Proceedings of the 2nd Data Compression Conference (DCC). 1992. http://dx.doi.org/10.1109/DCC.1992.227473

<a name="note4">\[4\]</a> P. Elias. “Universal Codeword Sets and Representations of the Integers”. IEEE Transactions on Information Theory 21.2 (1975), pp. 194–203. http://dx.doi.org/10.1109/TIT.1975.1055349

<a name="note5">\[5\]</a> R. Gallager and D. van Voorhis. “Optimal source codes for geometrically distributed integer alphabets (Correspondence)”. IEEE Transactions on Information Theory 21.2 (1975), pp. 228–230. http://dx.doi.org/10.1109/TIT.1975.1055357

<a name="note6">\[6\]</a> S. Golomb. “Run-length Encodings (Correspondence)”. IEEE Transactions on Information Theory 12.3 (1966), pp. 399–401. http://dx.doi.org/10.1109/TIT.1966.1053907

<a name="note7">\[7\]</a> B. Klimt and Y. Yang. "The Enron Corpus: A New Dataset for Email Classification Research". Proceedings of the 15th European Conference on Machine Learning (ECML). Springer Berlin Heidelberg, 2004. http://dx.doi.org/10.1007/978-3-540-30115-8_22

<a name="note8">\[8\]</a> A. Moffat and L. Stuiver. “Exploiting clustering in inverted file compression”. Proceedings of the 6th Data Compression Conference (DCC). 1996, pp. 82–91. http://dx.doi.org/10.1109/DCC.1996.488313

<a name="note9">\[9\]</a> A. Moffat and J. Zobel. “Parameterised Compression for Sparse Bitmaps”. Proceedings of the 15th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval. ACM, 1992, pp. 274–285. http://dx.doi.org/10.1145/133160.133210

<a name="note10">\[10\]</a> E. J. Schuegraf. “Compression of large inverted files with hyperbolic term distribution”. Information Processing & Management 12.6 (1976), pp. 377–384. http://dx.doi.org/10.1016/0306-4573(76)90035-2

<a name="note11">\[11\]</a> J. Teuhola. “A compression method for clustered bit-vectors”. Information Processing Letters 7.6 (1978), pp. 308–311. http://dx.doi.org/10.1016/0020-0190(78)90024-8

<a name="note12">\[12\]</a> I. H. Witten, T. C. Bell, and C. G. Nevill. “Indexing and compressing full-text databases for CD-ROM”. Journal of Information Science 17.5 (1991), pp. 265–271. http://dx.doi.org/10.1177/016555159101700502

## Tools ##

<a name="note13">\[13\]</a> D. Lemire. JavaFastPFOR. 2012. https://github.com/lemire/JavaFastPFOR

<a name="note14">\[14\]</a> wumpz. Java Diff Utils. 2017. https://github.com/java-diff-utils/java-diff-utils

