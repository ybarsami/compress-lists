#!/bin/bash

#java -jar dist/CompressLists.jar -i datasets/mail1-bisection.csv --help

#java -jar dist/CompressLists.jar -i datasets/mail1-bisection.csv -m meta_batch --submethods interpolative,context,elias_fano
java -jar dist/CompressLists.jar -i datasets/mail1-bisection.csv -m interpolative,context
# -o 

#Very long Elias-Fano with opt blocks
#java -jar dist/CompressLists.jar -i datasets/TREC-bisection.csv -m elias_fano
#java -jar dist/CompressLists.jar -i datasets/TREC-bisection.csv -m meta_cluster --shrink_clusters

